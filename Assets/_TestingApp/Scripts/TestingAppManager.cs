﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestingApp
{
	public class TestingAppManager : MonoBehaviour {
		
		public static TestingAppManager Instance;

		#region USER DETAILS
		public string user_email;
		public string user_name;
		public string user_password;
		public int user_id;
		public string user_accesstoken;
		#if UNITY_EDITOR
		public int opponent_id = 31;
		#else
		public int opponent_id = 36;
		#endif
		#endregion

		public Button checkQuestionButton;
		public Button checkbadgesButton;

		public LoadingIndicator p_LoadingIndicator;
		public CategoryPanel p_CategoryPanel;
		public BadgesPanel p_BadgesPanel;
		public QuestionsPanel p_QuestionsPanel;
		public List<SeralizedClassServer.CategoryDictionary> categoryDictionaryList = new List<SeralizedClassServer.CategoryDictionary> ();
		public int PARENTCATEGORY { get { return m_currentCategory;} }
		private int m_currentCategory = 0;
		public int question_mode;
		public string current_title;
		public int current_title_id;
		public QuestionsPanel m_CurrentQuestion;

		private InternetConnection m_connection;
		private List<CategoryPanel> m_CategoryPanelList = new List<CategoryPanel> ();
		private BadgesPanel m_CurrentBadgesPanel;

		void Awake ()
		{
			if (Instance == null)
				Instance = this;
			else
				DestroyImmediate (this);

			if (m_connection == null) {

				m_connection = InternetConnection.Init(this.gameObject);
			}
		}
		
		// Use this for initialization
		void Start () {

			checkQuestionButton.onClick.AddListener (OnClickCheckQuestionButton);
			checkbadgesButton.onClick.AddListener (OnClickCheckBadgesButton);

			ShowLoading ();
			Login ();
		}
		
		void Login ()
		{
			#if UNITY_EDITOR
			//36
			user_email = "app.tester@TestingApp.com";
			user_password = "app.tester";
			#else
			//31
			user_email = "tester.app@TestingApp.com";
			user_password = "tester@app";
			#endif
			Server.Instance.LogInToServer (LoginCallback);
		}
		
		void LoginCallback (bool success)
		{
			if (success) {
				//GET CATEGORY
				ShowLoading (false);
			}
		}

		void OnClickCheckQuestionButton () 
		{
			GetCategory (0);
		}

		void OnClickCheckBadgesButton ()
		{
//			ShowLoading ();
//			Server.Instance.GetBadges (GetBadgesCallback);
		}

		void GetBadgesCallback (List<TestingAppManager.AllBadges> badgesList)
		{
			BadgesPanel _panel = (BadgesPanel)Instantiate (p_BadgesPanel);
			RectTransformMakeChild (_panel.gameObject, p_BadgesPanel.transform.parent);
			_panel.InitBadgesPanel (badgesList);
			m_CurrentBadgesPanel = _panel;
			ShowLoading (false);
		}

		public void GetCategory (int currentCategory)
		{
			if(InternetConnection.Check) {
				
				m_currentCategory = currentCategory;
				GetCategory (GetCategoryCallback);
			} else {

				Debug.LogError ("NO INTERNET CONNECTION");
			}
		}

		void GetCategory (System.Action<bool> callback)
		{
			ShowLoading ();
			Server.Instance.GetCategory (GetCategoryCallback);
		}

		void GetCategoryCallback (bool success)
		{
			ShowLoading (false);
			CategoryPanel _categoryPanel = (CategoryPanel) Instantiate (p_CategoryPanel);
			RectTransformMakeChild (_categoryPanel.gameObject, p_CategoryPanel.transform.parent);
			_categoryPanel.UpdateCategory (categoryDictionaryList [categoryDictionaryList.Count - 1]);
			m_CategoryPanelList.Add (_categoryPanel);
//			if (categoryDictionaryList.Count > 1) {
//				_categoryPanel.backButton.gameObject.SetActive (true);
//			} else {
//				_categoryPanel.backButton.gameObject.SetActive (false);
//			}
		}

		public void GetQuestions (int topicID)
		{
			if (Instance.question_mode == 0) {
				
				Server.Instance.GetQuestions (topicID, GetQuestionsCallback);

			} else  {

				Server.Instance.GetDualQuestions (topicID, GetDualQuestionsCallback);
			}

            Instance.ShowLoading(true);
		}

		void GetQuestionsCallback (List<SeralizedClassServer.QuizQuestion> callback)
		{
            Instance.ShowLoading(false);
			Debug.Log (JsonConvert.SerializeObject (callback));
			QuestionsPanel _panel = (QuestionsPanel)Instantiate (p_QuestionsPanel);
			_panel.InitQuestionsPanel (callback);
			RectTransformMakeChild (_panel.gameObject, p_QuestionsPanel.transform.parent);
			m_CurrentQuestion = _panel;
		}

		void GetDualQuestionsCallback (SeralizedClassServer.DuelQuizQuestionsList callback)
		{
			Debug.Log (JsonConvert.SerializeObject (callback));
//			QuestionsPanel _panel = (QuestionsPanel)Instantiate (p_QuestionsPanel);
//			_panel.InitQuestionsPanel (callback);
//			RectTransformMakeChild (_panel.gameObject, p_QuestionsPanel.transform.parent);
//			m_CurrentQuestion = _panel;
		}

		public void ShowLoading (bool show = true)
		{
			p_LoadingIndicator.gameObject.SetActive (show);
		}

		public void CategoryPanel ()
		{
			if (m_CurrentQuestion != null) {
				Destroy (m_CurrentQuestion.gameObject);
			}

			categoryDictionaryList.RemoveRange (1, categoryDictionaryList.Count - 1);
			for (int i = m_CategoryPanelList.Count - 1; i > 0; i-- ) {

				Destroy (m_CategoryPanelList [i].gameObject);
			}
			m_CategoryPanelList.RemoveRange (1, m_CategoryPanelList.Count - 1);
		}

		public void TopicPanel ()
		{
			categoryDictionaryList.RemoveAt (categoryDictionaryList.Count - 1);
			Destroy (m_CategoryPanelList [m_CategoryPanelList.Count - 1].gameObject);
			m_CategoryPanelList.RemoveAt (m_CategoryPanelList.Count - 1);
		}

		public void BackToTopic ()
		{
			if (m_CurrentQuestion != null) {
				Destroy (m_CurrentQuestion.gameObject);
			}
		}

		public void CloseBadesPanel ()
		{
			Destroy (m_CurrentBadgesPanel.gameObject);
		}

		public static void RectTransformMakeChild (UnityEngine.GameObject rectTransform, Transform parent)
		{
			RectTransform _recttransform = rectTransform.GetComponent<RectTransform> ();
			rectTransform.transform.SetParent (parent, false);
			rectTransform.transform.localScale = new Vector3 (1f, 1f, 1f);
			rectTransform.SetActive (true);
		}

		[System.Serializable]
		public class AllBadges
		{
			public int badge_id;
			public string condition;
			public string description;
			public string title;
			public string image;
			public string created_at;
			public int status;
		}
	}
}

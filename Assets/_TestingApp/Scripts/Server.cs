﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestingApp
{
	public class Server : MonoBehaviour {
		
		private static Server m_instance;
		public static Server Instance {
			get {
				return Init ();
			} 
			private set { }
		}

		public static Server Init()
		{
			if (m_instance == null) {
				GameObject instGO = new GameObject ("~Server");
				m_instance = instGO.AddComponent<Server> ();
				DontDestroyOnLoad (instGO);
			}
			return m_instance;
		}

		void Awake ()
		{
			if (m_instance == null) {
				m_instance = this;
			} else {
				DestroyImmediate (this);
			}
		}

		public bool CheckInternetConnection ()
		{
			if (InternetConnection.Check) {
				return true;
			} else {
				Debug.LogError (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
				return false;
			}
		}

		public void LogInToServer (System.Action<bool> callback)
		{
			string url = ServerConstant.BASEURL + ServerConstant.USER_LOGIN;

			WWWForm wwwForm = new WWWForm ();

			wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			wwwForm.AddField (ServerConstant.LOGIN_TYPE, 3);

			wwwForm.AddField (ServerConstant.USER_EMAIL_LOGIN, TestingAppManager.Instance.user_email);
			wwwForm.AddField (ServerConstant.USER_PASSWORD, TestingAppManager.Instance.user_password);

			#if UNITY_EDITOR
			wwwForm.AddField (ServerConstant.DEVICE_ID, ServerConstant.EDITOR_DEVICEID);
			#elif UNITY_IOS || UNITY_IPHONE
			wwwForm.AddField (ServerConstant.DEVICE_ID, SystemInfo.deviceUniqueIdentifier);
			#elif UNITY_ANDROID
			wwwForm.AddField (ServerConstant.DEVICE_ID, SystemInfo.deviceUniqueIdentifier);
			#endif

			Debug.Log ("URL Send---> " + url + wwwForm.ToString ());
			WWW www = new WWW (url, wwwForm);

			StartCoroutine (LoginToServerCallback (www, callback));
		}

			IEnumerator LoginToServerCallback (WWW www, System.Action<bool> callback)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);
				
				IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize (www.text);
				int responseCodeInt = int.Parse (string.Format ("{0}", response [ServerConstant.RESPONSE_CODE]));

				if (responseCodeInt == ServerConstant.WORKING_FINE) {
					string _response_msg = JsonConvert.SerializeObject (response [ServerConstant.RESPONSE_MSG]);
					IDictionary responseMsg = (IDictionary)Facebook.MiniJSON.Json.Deserialize (_response_msg);
					
					TestingAppManager.Instance.user_id = int.Parse (responseMsg [ServerConstant.USER_ID].ToString ());
					TestingAppManager.Instance.user_name = responseMsg [ServerConstant.LOGIN_NAME].ToString ();
					TestingAppManager.Instance.user_accesstoken = responseMsg [ServerConstant.ACCESS_TOKEN].ToString ();

				callback (true);
					
				} else {
					if (responseCodeInt == ServerConstant.EMAIL_DOESNT_EXIT) {
						
						SignUpToServer (callback);
					} else {
			callback (false);
						Debug.LogError (response [ServerConstant.RESPONSE_MSG].ToString ());
					}
				}
			} else {
			callback (false);
				Debug.LogError (www.error);
			}
		}

		public void SignUpToServer (System.Action<bool> callback)
		{
			string url = ServerConstant.BASEURL + ServerConstant.USER_REGISTER;
			
			WWWForm wwwForm = new WWWForm ();
			wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			wwwForm.AddField (ServerConstant.LOGIN_TYPE, (int) 3);
			wwwForm.AddField (ServerConstant.USER_EMAIL_SIGNUP, TestingAppManager.Instance.user_email);
			wwwForm.AddField (ServerConstant.USER_PASSWORD, TestingAppManager.Instance.user_password);
			
			Debug.Log ("URL Send---> "+url + wwwForm.ToString());
			WWW www = new WWW (url, wwwForm);
			
			StartCoroutine (SignUpToServerCallback (www, callback));
		}

			IEnumerator SignUpToServerCallback (WWW www, System.Action<bool> callback)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);

				IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize (www.text);
				int responseCodeInt = int.Parse (string.Format ("{0}", response [ServerConstant.RESPONSE_CODE]));

			if (responseCodeInt == 1) {

					LogInToServer (callback);
				}
			} else {
				Debug.LogError (www.error);
			callback (false);
			}
		}

		public void GetCategory (System.Action<bool> callback)
		{
			string url = ServerConstant.BASEURL + ServerConstant.GET_CATEGORY;
			WWWForm wwwForm = new WWWForm ();
			wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, TestingAppManager.Instance.user_id);
			wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, TestingAppManager.Instance.user_accesstoken);
			wwwForm.AddField (ServerConstant.PARENT_CATEGORY_ID, TestingAppManager.Instance.PARENTCATEGORY);

			Debug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
			WWW www = new WWW (url, wwwForm);

			StartCoroutine (GetCategoryFromServerCallback (www, callback));
		}

		IEnumerator GetCategoryFromServerCallback (WWW www, System.Action<bool> callback)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);

				if (IsResponseCodeCorrect (www.text)) {

					SeralizedClassServer.CategoryDictionary newCategory = new SeralizedClassServer.CategoryDictionary ();
					newCategory = JsonConvert.DeserializeObject<SeralizedClassServer.CategoryDictionary> (GetResponseMessage (www.text));

					Debug.Log (JsonConvert.SerializeObject (newCategory));

					if (newCategory != null) {

						TestingAppManager.Instance.categoryDictionaryList.Add (newCategory);
						callback (true);

					} else {
						callback (false);
					}
				}
			} else {
				TestingAppManager.Instance.ShowLoading (false);
				Debug.LogError (www.error);
				callback (false);
			}
		}

		public void GetQuestions (int category, System.Action<List<SeralizedClassServer.QuizQuestion>> callback = null)
		{
			string url = ServerConstant.BASEURL + "category.getQuestions";
			WWWForm wwwForm = new WWWForm();
			wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			
//			wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, TestingAppManager.Instance.user_id);
//			wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, TestingAppManager.Instance.user_accesstoken);
			wwwForm.AddField(ServerConstant.CATEGORY_ID, category);
			
			Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
			WWW www = new WWW(url, wwwForm);
			
			StartCoroutine(GetQuestionsCallback(www, callback));
		}
		
		IEnumerator GetQuestionsCallback (WWW www, System.Action<List<SeralizedClassServer.QuizQuestion>> callback = null)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);
				
				if (IsResponseCodeCorrect (www.text)) {
					callback (JsonConvert.DeserializeObject<List<SeralizedClassServer.QuizQuestion>> (GetResponseMessage (www.text)));
				} else {
					if (callback == null)
						callback (null);
				}
			} else {
				Debug.LogError (www.error);
			}
		}

		public void GetDualQuestions (int category, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
		{
			string url = ServerConstant.BASEURL + ServerConstant.GET_DUEL;
			WWWForm wwwForm = new WWWForm();
			wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			
			wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, TestingAppManager.Instance.user_id);
			wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, TestingAppManager.Instance.user_accesstoken);
			wwwForm.AddField (ServerConstant.OTHER_USER_TYPE, 1);
			wwwForm.AddField (ServerConstant.OTHER_USER_ID, TestingAppManager.Instance.opponent_id);
			wwwForm.AddField (ServerConstant.CATEGORY_ID, category);

			Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
			WWW www = new WWW(url, wwwForm);
			
			StartCoroutine(GetDualQuestionsCallback(www, callback));
		}
		
		IEnumerator GetDualQuestionsCallback (WWW www, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);
				
				if (IsResponseCodeCorrect (www.text)) {
					callback (JsonConvert.DeserializeObject<SeralizedClassServer.DuelQuizQuestionsList>
						(GetResponseMessage (www.text)));
				} else {
					if (callback == null)
						callback (null);
				}
			} else {
				Debug.LogError (www.error);
			}
		}

		public void GetBadges (System.Action<List<TestingAppManager.AllBadges>> callback)
		{
			string url = ServerConstant.BASEURL + "master.badge";
			WWWForm wwwForm = new WWWForm();
			wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
			
			wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, TestingAppManager.Instance.user_id);
			wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, TestingAppManager.Instance.user_accesstoken);

			Debug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
			WWW www = new WWW(url, wwwForm);
			
			StartCoroutine(GetBadgesCallback(www, callback));
		}
		
		IEnumerator GetBadgesCallback (WWW www, System.Action<List<TestingAppManager.AllBadges>> callback)
		{
			yield return www;
			if (www.error == null) {
				Debug.Log (www.text);
				
				if (IsResponseCodeCorrect (www.text)) {
					callback (JsonConvert.DeserializeObject<List<TestingAppManager.AllBadges>>
								(GetResponseMessage (www.text)));
				} else {
					if (callback != null)
						callback (null);
				}
			} else {
				Debug.LogError (www.error);
			}
		}


		bool IsResponseCodeCorrect (string serverResponse)
		{
			IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(serverResponse);
			int responseCodeInt = int.Parse(string.Format("{0}", response[ServerConstant.RESPONSE_CODE]));
			if (responseCodeInt == ServerConstant.WORKING_FINE)
				return true;
			else
			{
				
				TestingAppManager.Instance.ShowLoading (false);
				Debug.LogError (response [ServerConstant.RESPONSE_INFO].ToString ());
				Debug.LogError(response[ServerConstant.RESPONSE_INFO].ToString());
			}
			return false;
		}

		string GetResponseMessage (string serverResponse)
		{
			IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(serverResponse);
			string _response_msg = JsonConvert.SerializeObject(response[ServerConstant.RESPONSE_MSG]);

			return _response_msg;
		}

	}
}

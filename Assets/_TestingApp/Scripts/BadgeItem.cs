﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

namespace TestingApp
{
	public class BadgeItem : MonoBehaviour {

		[SerializeField] private Image m_badge_image;
		[SerializeField] private Text m_badge_id, m_badge_title, m_badge_condition, m_badge_descritption, m_urlText;
		[SerializeField] private Button m_viewImageButton, m_closeImageButton;
		[SerializeField] private GameObject m_imagePanel;

		private string m_url;
		private int m_status;

		// Use this for initialization
		void Start () {
			m_viewImageButton.onClick.AddListener (OnClickViewImage);
			m_closeImageButton.onClick.AddListener (OnClickCloseImage);
		}
		
		// Update is called once per frame
		public void UpdateBadgeItem (TestingAppManager.AllBadges badge) {

			m_badge_id.text = "ID : " + badge.badge_id.ToString ();
			m_badge_title.text = badge.title;
			m_badge_condition.text = badge.condition;
			m_badge_descritption.text = badge.description;
			m_urlText.text = badge.image;
			m_url = badge.image;
			m_status = badge.status;
		}

		void OnClickViewImage ()
		{
			TestingAppManager.Instance.ShowLoading ();
			StartCoroutine ("LoadImage");
		}

		IEnumerator LoadImage ()
		{
			WWW image = new WWW (m_url);
			yield return image;

			if (image.error == null) {
				m_badge_image.CreateImageSprite (image.texture);
			} else {
				Debug.LogError (image.error);
				m_urlText.text += "\n"+"<color=red>" +image.error +"</color>";
			}
			m_imagePanel.SetActive (true);

			TestingAppManager.Instance.ShowLoading (false);
		} 

		void OnClickCloseImage ()
		{
			m_imagePanel.SetActive (false);
		}
	}
}

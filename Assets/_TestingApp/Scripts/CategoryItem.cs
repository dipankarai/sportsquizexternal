﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TestingApp
{
	public class CategoryItem : MonoBehaviour {

		[SerializeField] private Item[] m_categoryItem;
		
		// Use this for initialization
		void Start () {
			
		}
		
		public bool InitCategoryItem (SeralizedClassServer.Category categoryitem)
		{
			//categoryitem.category_id
			for (int i = 0; i < m_categoryItem.Length; i++) {
				if ((m_categoryItem[i].gameObject.activeSelf == false)) {

					m_categoryItem [i].InitItem (
						categoryitem.category_id,
						categoryitem.category_title,
						categoryitem.type);

					m_categoryItem [i].gameObject.SetActive (true);

					return true;
				}
			}

			return false;
		}
	}
}

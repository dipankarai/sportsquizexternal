﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace TestingApp
{
	public class BadgesPanel : MonoBehaviour {

		[SerializeField] private Button m_backButton;
		[SerializeField] private BadgeItem badgeItem;
		
		// Use this for initialization
		void Start () {
			m_backButton.onClick.AddListener (OnClickBackButton);
		}
		
		public void InitBadgesPanel (List<TestingAppManager.AllBadges> badgesList)
		{
			Q.Utils.QDebug.Log (badgesList.Count);
			for (int i = 0; i < badgesList.Count; i++) {
				Q.Utils.QDebug.Log (badgesList[i].description);
				BadgeItem _item = Instantiate (badgeItem);
				_item.UpdateBadgeItem (badgesList [i]);
				TestingAppManager.RectTransformMakeChild (_item.gameObject, badgeItem.transform.parent);
			}
		}

		void OnClickBackButton ()
		{
			Destroy (this.gameObject);
		}
	}
}

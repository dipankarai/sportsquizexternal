﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace TestingApp
{
	public class CategoryPanel : MonoBehaviour {
	
		[SerializeField] private ScrollRect m_scrollRect;
		[SerializeField] private CategoryItem m_categoryItem;
		[SerializeField] private Dropdown m_Dropdown;
		public Button backButton;

		private List<CategoryItem> m_categoryItemList = new List<CategoryItem> ();

		void Awake () {
			
		}

		// Use this for initialization
		void Start () {
			m_Dropdown.onValueChanged.AddListener (OnDropdownValueChanged);
			backButton.onClick.AddListener (OnClickBackButton);
		}

		void OnDropdownValueChanged (int val)
		{
			Q.Utils.QDebug.Log ("OnDropdownValueChanged "+val);
			TestingAppManager.Instance.question_mode = val;
		}

		void OnClickBackButton ()
		{
			TestingAppManager.Instance.TopicPanel ();
		}
		
		public void UpdateCategory (SeralizedClassServer.CategoryDictionary categoryList)
		{
			/*if (categoryList.categoryList[0].type == 2) {
				m_Dropdown.gameObject.SetActive (true);
			} else {
				m_Dropdown.gameObject.SetActive (false);
			}*/

			for (int i = 0; i < categoryList.categoryList.Count; i++) {


				if (m_categoryItemList.Count > 0) {

					if (m_categoryItemList [m_categoryItemList.Count - 1].
						InitCategoryItem (categoryList.categoryList [i]) == false)
					{
						InstantiateCategoryItem (categoryList.categoryList [i]);
					}
				} else {

					InstantiateCategoryItem (categoryList.categoryList [i]);
				}
			}
		}

		void InstantiateCategoryItem (SeralizedClassServer.Category categoryitem)
		{
			CategoryItem item = (CategoryItem)Instantiate (m_categoryItem);
			TestingAppManager.RectTransformMakeChild (item.gameObject, m_categoryItem.transform.parent);
			item.InitCategoryItem (categoryitem);

			m_categoryItemList.Add (item);
		}
	}
	
}

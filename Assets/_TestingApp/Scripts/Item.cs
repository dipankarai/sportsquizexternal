﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TestingApp
{
	public class Item : MonoBehaviour {

		[SerializeField] private Button m_itemButton;
		[SerializeField] private Text m_itemTitle;

		private int m_category_ID;
		private int m_category_type;

		// Use this for initialization
		void Start () {
			m_itemButton.onClick.AddListener (OnClickItemButton);
		}

		public void InitItem (int id, string title, int type)
		{
			m_category_ID = id;
			m_itemTitle.text = title;
			m_category_type = type;
		}

		void OnClickItemButton ()
		{
			if (m_category_type == 1) {

				TestingAppManager.Instance.GetCategory (m_category_ID);
			} else {

				TestingAppManager.Instance.current_title = m_itemTitle.text;
				TestingAppManager.Instance.current_title_id = m_category_ID;
				TestingAppManager.Instance.GetQuestions (m_category_ID);
			}
		}
	}
}

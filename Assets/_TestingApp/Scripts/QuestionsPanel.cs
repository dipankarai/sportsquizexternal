﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

namespace TestingApp
{
	public class QuestionsPanel : MonoBehaviour {

		[SerializeField] private Text questionText, titleText, quizIDText, afterTestText, hintText, mediaText, detailsText, topicID;
		[SerializeField] private Button viewMediaButton, nextButton, categoryButton, backButton;
		[SerializeField] private Text[] answerText;
		[SerializeField] private Image questionImage;
		[SerializeField] private VideoPanel videoPanel;

		private MediaPlayerCtrl m_mediaPlayerCtrl;
		private YoutubeVideo m_youtubeVideoStreamer;
		private SeralizedClassServer.SingleQuizQuestionsList m_SingleQuizQuestionsList;
		private SeralizedClassServer.DuelQuizQuestionsList m_DuelQuizQuestionsList;
		private string m_url, filename;
		private int m_currentQuestion = 0;
		private int m_totalQuestion;
		private int m_questionType;
		private string m_questionDetails;

		private List<SeralizedClassServer.QuizQuestion> m_quizQuestionList;

		// Use this for initialization
		void Start () {
			viewMediaButton.onClick.AddListener (OnClickViewMedia);
			nextButton.onClick.AddListener (OnClickViewNext);
			categoryButton.onClick.AddListener (OnClickCategoryButton);
			backButton.onClick.AddListener (OnClickBackButton);
		}

		public void InitQuestionsPanel (List<SeralizedClassServer.QuizQuestion> questionDetails)
		{
			titleText.text = TestingAppManager.Instance.current_title;
			topicID.text = TestingAppManager.Instance.current_title_id.ToString ();

			m_quizQuestionList = questionDetails;

//			if (questionDetails.GetType () == typeof(SeralizedClassServer.SingleQuizQuestionsList)) {
//				m_SingleQuizQuestionsList = (SeralizedClassServer.SingleQuizQuestionsList)questionDetails;
//			} else if (questionDetails.GetType () == typeof(SeralizedClassServer.DuelQuizQuestionsList)) {
//				m_DuelQuizQuestionsList = (SeralizedClassServer.DuelQuizQuestionsList)questionDetails;
//			}



			ShowQuestion ();
		}

		void ShowQuestion ()
		{
			questionImage.rectTransform.sizeDelta = new Vector2 (500f, 500f);

			List<SeralizedClassServer.QuizQuestion> questionList = new List<SeralizedClassServer.QuizQuestion> ();
			questionList = m_quizQuestionList;
//			if (m_SingleQuizQuestionsList != null) {
//
//				quizIDText.text = m_SingleQuizQuestionsList.userQuizId.ToString ();
//				questionList = m_SingleQuizQuestionsList.questionList;
//			} else if (m_DuelQuizQuestionsList != null) {
//
//				quizIDText.text = m_DuelQuizQuestionsList.userQuizId.ToString ();
//				questionList = m_DuelQuizQuestionsList.questionList;
//				afterTestText.text = m_DuelQuizQuestionsList.after_taste;
//			}

			m_totalQuestion = questionList.Count;

			for (int i = 0; i < questionList.Count; i++) {

				questionText.text = questionList [m_currentQuestion].question.title;
				afterTestText.text = questionList [m_currentQuestion].question.aftertaste;
				hintText.text = questionList [m_currentQuestion].question.hint;
				m_url = questionList [m_currentQuestion].question.url;

				m_questionDetails = "Total Question: " + m_totalQuestion.ToString () +
				"\n Current Question ID: " + questionList [m_currentQuestion].question.questionId.ToString ();
				detailsText.text = m_questionDetails;

				m_questionType = questionList [m_currentQuestion].question.type;
				questionImage.sprite = null;
				if (questionList [m_currentQuestion].question.type == 1) {
					mediaText.text = "";
					viewMediaButton.interactable = false;
				}
				else if (questionList [m_currentQuestion].question.type == 2) {
					mediaText.text = "VIEW IMAGE";
					viewMediaButton.interactable = true;
				} else if (questionList [m_currentQuestion].question.type == 3) {
					mediaText.text = "VIEW VIDEO";
					viewMediaButton.interactable = true;
				}

				for (int j = 0; j < questionList [m_currentQuestion].answer.Count; j++) {

					answerText [j].text = questionList [m_currentQuestion].answer [j].answer_title +
						"\n <size=8> ANSWER ID: " + questionList [m_currentQuestion].answer [j].answer_id.ToString () + "</size>";
					if (questionList [m_currentQuestion].answer [j].is_correct == 1) {
						GetImage (answerText [j].transform.parent).color = Color.green;
					} else {
						GetImage (answerText [j].transform.parent).color = Color.red;
					}
				}
			}

			if (m_currentQuestion <= 0) {
				backButton.interactable = false;
			} else {
				backButton.interactable = true;
			}
		}

		void OnClickViewMedia ()
		{
			if (m_questionType == 1) {
				return;
			} else if (m_questionType == 2) {

				StartCoroutine ("LoadImage");
			} else if (m_questionType == 3) {

				if (m_url == "index.jpg") {
					mediaText.text = "<color=red>" + m_url + "</color>";
				} else if (m_url.Contains ("index.jpg")) {
					mediaText.text = "<color=red>" + m_url + "</color>";
				}

				StartCoroutine ("LoadYouTubeVideo", m_url);
			}
			TestingAppManager.Instance.ShowLoading ();
		}

		IEnumerator LoadImage ()
		{
			WWW image = new WWW (m_url);
			yield return image;

			if (image.error == null) {
				Q.Utils.QDebug.Log (image.size);
//				1024 *1000
				float imagesize = (float) Profiler.GetRuntimeMemorySize (image.texture);
				imagesize = imagesize / (1024f * 1000f);
				imagesize = Mathf.Round (imagesize);
				string size = detailsText.text + "\n Image Size: " + imagesize.ToString () + " Megabyte";
				detailsText.text = size;
				questionImage.CreateImageSprite (image.texture);
				mediaText.text = "";
			} else {
				Q.Utils.QDebug.LogError (image.error);
				mediaText.text = "<color=red>" + image.error + "</color>";
			}

			TestingAppManager.Instance.ShowLoading (false);
		}

		void OnClickBackButton ()
		{
			m_currentQuestion--;

			if (m_currentQuestion < 0) {
				backButton.interactable = false;
			} else {
				ShowQuestion ();
			}
		}

		void OnClickCategoryButton ()
		{
			TestingAppManager.Instance.BackToTopic ();
//			TestingAppManager.Instance.CategoryPanel ();
		}

		void OnClickViewNext ()
		{
			m_currentQuestion++;

			if (m_currentQuestion >= m_totalQuestion) {
				nextButton.interactable = false;
			} else {
				ShowQuestion ();
			}
		}

		Image GetImage (Transform transform)
		{
			return transform.GetComponent<Image> ();
		}

		IEnumerator LoadYouTubeVideo (string strURL)
		{
			InitVideo ();

			QuizManager.CurrentQuizAssets quizAssets = new QuizManager.CurrentQuizAssets ();
			Q.Utils.QDebug.Log ("TestLoadYouTubeVideoCallback");
			string parsedStrUrl = YoutubeVideo.Instance.RequestVideo (strURL, YoutubeVideo.eVideoQuality.medium480, quizAssets);
			Q.Utils.QDebug.Log (parsedStrUrl);

			if (string.IsNullOrEmpty (parsedStrUrl) == false) {

				parsedStrUrl = parsedStrUrl.Trim ();

				#if UNITY_EDITOR2
				string write_directory = Application.dataPath + "/StreamingMedia";
				#else
				string write_directory = Application.persistentDataPath + "/StreamingMedia";
				#endif

				string file_name = ConvertLongString (parsedStrUrl.Substring (parsedStrUrl.LastIndexOf ("/")));
				string write_path = write_directory + "/" + file_name;

				if (System.IO.Directory.Exists (write_directory) == false)
					System.IO.Directory.CreateDirectory (write_directory);

				if (System.IO.File.Exists (write_path) == false) {

					Q.Utils.QDebug.Log (write_directory);

					WWW www = new WWW (parsedStrUrl);
					yield return www;
					if (string.IsNullOrEmpty (www.error)) {
						System.IO.File.WriteAllBytes (write_path, www.bytes);

						filename = "file://" + write_path;
					}

					www.Dispose ();
					www = null;

					videoPanel.gameObject.SetActive (true);
					videoPanel.InitQuizVideoPlayer (filename);

				} else {
					filename = "file://" + write_path;
				}
				Q.Utils.QDebug.Log (filename);
				Q.Utils.QDebug.Log ("Done Loading Data.....");
			}

			TestingAppManager.Instance.ShowLoading (false);
		}

		void InitVideo ()
		{
			if (m_mediaPlayerCtrl == null) {
				if (GetComponent<MediaPlayerCtrl> () == null) {
					m_mediaPlayerCtrl = gameObject.AddComponent<MediaPlayerCtrl> ();
				} else {
					m_mediaPlayerCtrl = GetComponent<MediaPlayerCtrl> ();
				}
			}
			m_mediaPlayerCtrl.m_bAutoPlay = false;
			m_mediaPlayerCtrl.m_bLoop = false;

			if (m_youtubeVideoStreamer == null) {
				if (GetComponent<YoutubeVideo> () == null) {
					m_youtubeVideoStreamer = gameObject.AddComponent<YoutubeVideo> ();
				} else {
					m_youtubeVideoStreamer = GetComponent<YoutubeVideo> ();
				}
			}
		}

		public MediaPlayerCtrl AttachScreenToMediaPlayer (GameObject screen)
		{
			m_mediaPlayerCtrl.m_TargetMaterial = new GameObject[1];
			m_mediaPlayerCtrl.m_objResize = null;

			m_mediaPlayerCtrl.m_TargetMaterial [0] = screen;
			//		m_mediaPlayerCtrl.m_objResize [0] = screen;

			return m_mediaPlayerCtrl;
		}

		string ConvertLongString (string _longString) {
			//scrMedia.Pause ();

			string stringId = "";
			System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex ("[^a-zA-Z0-9]");

			string str = rgx.Replace (_longString, "");

			List<char> stringIdList = new List<char> ();

			for (int i = 0; i < str.Length; i++) {
				if (stringIdList.Contains (str [i]) == false) {

					stringIdList.Add (str [i]);
				}
			}

			for (int i = 0; i < stringIdList.Count; i++) {
				stringId += stringIdList [i];
			}

			return stringId;
		}
	}
}

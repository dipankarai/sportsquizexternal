﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TestingApp
{
	public class VideoPanel : MonoBehaviour {
		
		[SerializeField] private RawImage m_videoScreen;
		[SerializeField] private Button m_closeButton;
		[SerializeField] private QuestionsPanel p_questionPanel;

		private MediaPlayerCtrl m_mediaPlayerCtrl;
		private string m_filepath;

		void OnEnable () {

		}

		// Use this for initialization
		void Start () {
			m_closeButton.onClick.AddListener (OnClickCloseButton);
		}

		public void InitQuizVideoPlayer (string filepath)
		{
			m_mediaPlayerCtrl = p_questionPanel.AttachScreenToMediaPlayer (m_videoScreen.gameObject);

			m_mediaPlayerCtrl.OnEnd = OnVideoEnd;
			m_filepath = filepath;
			LoadVideo (m_filepath);
		}

		void Update ()
		{
			if (m_mediaPlayerCtrl.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.READY &&
				m_mediaPlayerCtrl.GetCurrentState () != MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
			{
				ResizeVideoScreen ((float) m_mediaPlayerCtrl.GetVideoHeight (), (float) m_mediaPlayerCtrl.GetVideoWidth ());
				PlayVideo ();
			}
		}

		public void LoadVideo (string filepath) { m_mediaPlayerCtrl.Load (filepath); }
		public void PlayVideo () { 
			m_videoScreen.color = Color.white;
			m_mediaPlayerCtrl.Play (); 
		}
		public void PauseVideo () { m_mediaPlayerCtrl.Pause (); }
		public void StopVideo () { m_mediaPlayerCtrl.Stop (); }

		void OnClickCloseButton ()
		{
			StopVideo ();
			QuizManager.Instance.VideoPlayerClosed ();
			gameObject.SetActive (false);
		}

		void OnVideoEnd () {
			OnClickCloseButton ();
		}

		void ResizeVideoScreen (float videoHeight, float videWidth)
		{
			float fRatioScreen = videoHeight / videWidth;
			Vector2 screen = m_videoScreen.rectTransform.sizeDelta;
			screen.x = screen.y / fRatioScreen;

			m_videoScreen.rectTransform.sizeDelta = screen;
		}
	}
}

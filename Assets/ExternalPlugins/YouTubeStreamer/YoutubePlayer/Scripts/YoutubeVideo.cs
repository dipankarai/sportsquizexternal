﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YoutubeExtractor;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public class YoutubeVideo : MonoBehaviour {

    public static YoutubeVideo Instance;

    void Awake()
    {
        Instance = this;
    }

	public enum eVideoQuality
	{
		low144 = 144,
		low240 = 240,
		low360 = 360,
		medium480 = 480,
		high720 = 720,
		high1080 = 1080
	}

    public bool drawBackground = false;
    public Texture2D backgroundImage;

	public eVideoQuality quality;

	public string RequestVideo(string urlOrId, eVideoQuality quality, QuizManager.CurrentQuizAssets quizAssets)
    {
        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

        Uri uriResult;
        bool result = Uri.TryCreate(urlOrId, UriKind.Absolute, out uriResult)
            && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

        if (!result)
            urlOrId = "https://youtube.com/watch?v=" + urlOrId;

		int qualityVersion = (int)quality;

		IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(urlOrId, false);
        VideoInfo video = null;

		List<VideoInfo> list = videoInfos.ToList<VideoInfo> ();

		bool foundDesiredQual = false;
		bool found360 = false;
		bool found240 = false;

		for (int idx = 0; idx < list.Count; idx++)
		{
			Q.Utils.QDebug.Log (list [idx].Resolution);
			if (list[idx].Resolution == qualityVersion && list[idx].VideoType == VideoType.Mp4)
				foundDesiredQual = true;
			if (list[idx].Resolution == 360 && list[idx].VideoType == VideoType.Mp4)
				found360 = true;
			if (list[idx].Resolution == 240 && list[idx].VideoType == VideoType.Mp4)
				found240 = true;

			if (foundDesiredQual)
			{
				video = videoInfos
					.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == qualityVersion);
				Q.Utils.QDebug.Log("Found Desired Quality "+qualityVersion);
				quizAssets.quality = YoutubeVideo.GetQualityEnum (qualityVersion);
				break;
			}
			else
			{
				if (found360)
				{
					video = videoInfos
						.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360);
					quizAssets.quality = YoutubeVideo.GetQualityEnum (360);
					Q.Utils.QDebug.Log("The video is not in selected quallity changing to 360");
					break;
				}

				if (!found360 && found240)
				{
					video = videoInfos
						.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 240);
					quizAssets.quality = YoutubeVideo.GetQualityEnum (240);
					Q.Utils.QDebug.Log("The video is not in selected quallity changing to 240");
					break;
				}
			}
		}

        //Search for video with desired format and desired resolution, you can filter if your own methods if you need.
		/*var enumerator = videoInfos.GetEnumerator();
		while (enumerator.MoveNext())
        {

			if (enumerator.Current.Resolution != qualityVersion) {
				qualityVersion = enumerator.Current.Resolution;
			}

			quizAssets.quality = YoutubeVideo.GetQualityEnum (qualityVersion);

			if (enumerator.Current.VideoType == VideoType.Mp4 && enumerator.Current.Resolution == qualityVersion)
            {
                video = enumerator.Current;
                break;
            }
        }*/

        if (video.RequiresDecryption)
        {
            DownloadUrlResolver.DecryptDownloadUrl(video);
        }

		Q.Utils.QDebug.Log("Quality: " + quizAssets.quality);
        Q.Utils.QDebug.Log("The mp4 is: " + video.DownloadUrl);
        return video.DownloadUrl;
    }

	public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }

	public static eVideoQuality GetQualityEnum (int quality)
	{
		if (quality <= 144) {
			return eVideoQuality.low144;
		} else if (quality <= 240) {
			return eVideoQuality.low240;
		} else if (quality <= 360) {
			return eVideoQuality.low360;
		} else if (quality <= 480) {
			return eVideoQuality.medium480;
		} else if (quality <= 720) {
			return eVideoQuality.high720;
		} else if (quality <= 1080) {
			return eVideoQuality.high1080;
		}

		return eVideoQuality.low360;
	}

	void OnGUI()
	{
		GUI.depth = 1;
		if(drawBackground)
		{
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), backgroundImage);
		}
	}

}

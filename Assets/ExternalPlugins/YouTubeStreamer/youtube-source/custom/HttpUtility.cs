﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

public static class HttpUtility
{
    public static string HtmlEncode(string s)
    {
        if (s == null)
        {
            return null;
        }

        var result = new StringBuilder(s.Length);

        foreach (char ch in s)
        {
            if (ch <= '>')
            {
                switch (ch)
                {
                    case '<':
                        result.Append("&lt;");
                        break;

                    case '>':
                        result.Append("&gt;");
                        break;

                    case '"':
                        result.Append("&quot;");
                        break;

                    case '\'':
                        result.Append("&#39;");
                        break;

                    case '&':
                        result.Append("&amp;");
                        break;

                    default:
                        result.Append(ch);
                        break;
                }
            }
            else if (ch >= 160 && ch < 256)
            {
                result.Append("&#").Append(((int)ch).ToString(CultureInfo.InvariantCulture)).Append(';');
            }
            else
            {
                result.Append(ch);
            }
        }

        return result.ToString();
    }

    public static string HtmlDecode(string s)
    {
        if (s == null)
            return null;

        using (var sw = new StringWriter())
        {
            HttpEncoder.Current.HtmlDecode(s, sw);
            return sw.ToString();
        }
    }

    public static string UrlEncode(string text)
    {
        // Sytem.Uri provides reliable parsing
        return System.Uri.EscapeDataString(text);
    }

    public static string UrlDecode(string text)
    {
        // pre-process for + sign space formatting since System.Uri doesn't handle it
        // plus literals are encoded as %2b normally so this should be safe
        text = text.Replace("+", " ");
        return System.Uri.UnescapeDataString(text);
    }

    public static string GetUrlEncodedKey(string urlEncoded, string key)
    {
        urlEncoded = "&" + urlEncoded + "&";

        int Index = urlEncoded.IndexOf("&" + key + "=", StringComparison.OrdinalIgnoreCase);
        if (Index < 0)
            return "";

        int lnStart = Index + 2 + key.Length;

        int Index2 = urlEncoded.IndexOf("&", lnStart);
        if (Index2 < 0)
            return "";

        return UrlDecode(urlEncoded.Substring(lnStart, Index2 - lnStart));
    }
}

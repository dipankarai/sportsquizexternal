using System;

namespace YoutubeExtractor
{
	public abstract class Downloader
	{
		public event EventHandler DownloadFinished;

		public event EventHandler DownloadStarted;

		public int? BytesToDownload
		{
			get;
			private set;
		}

		public string SavePath
		{
			get;
			private set;
		}

		public VideoInfo Video
		{
			get;
			private set;
		}

		protected Downloader(VideoInfo video, string savePath, int? bytesToDownload = null)
		{
			if (video == null)
			{
				throw new ArgumentNullException("video");
			}
			if (savePath == null)
			{
				throw new ArgumentNullException("savePath");
			}
			this.Video = video;
			this.SavePath = savePath;
			this.BytesToDownload = bytesToDownload;
		}

		public abstract void Execute();

		protected void OnDownloadFinished(EventArgs e)
		{
			if (this.DownloadFinished != null)
			{
				this.DownloadFinished(this, e);
			}
		}

		protected void OnDownloadStarted(EventArgs e)
		{
			if (this.DownloadStarted != null)
			{
				this.DownloadStarted(this, e);
			}
		}
	}
}

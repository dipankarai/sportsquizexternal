using System;

namespace YoutubeExtractor
{
	public class ProgressEventArgs : EventArgs
	{
		public bool Cancel
		{
			get;
			set;
		}

		public double ProgressPercentage
		{
			get;
			private set;
		}

		public ProgressEventArgs(double progressPercentage)
		{
			this.ProgressPercentage = progressPercentage;
		}
	}
}

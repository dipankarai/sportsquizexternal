using System;
using System.IO;

namespace YoutubeExtractor
{
	internal class AacAudioExtractor : IAudioExtractor, IDisposable
	{
		private readonly FileStream fileStream;

		private int aacProfile;

		private int channelConfig;

		private int sampleRateIndex;

		public string VideoPath
		{
			get;
			private set;
		}

		public AacAudioExtractor(string path)
		{
			this.VideoPath = path;
			this.fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
		}

		public void Dispose()
		{
			this.fileStream.Dispose();
		}

		public void WriteChunk(byte[] chunk, uint timeStamp)
		{
			if (chunk.Length >= 1)
			{
				if (chunk[0] == 0)
				{
					if (chunk.Length >= 3)
					{
						ulong value = (ulong)BigEndianBitConverter.ToUInt16(chunk, 1) << 48;
						this.aacProfile = BitHelper.Read(ref value, 5) - 1;
						this.sampleRateIndex = BitHelper.Read(ref value, 4);
						this.channelConfig = BitHelper.Read(ref value, 4);
						if (this.aacProfile < 0 || this.aacProfile > 3)
						{
							throw new AudioExtractionException("Unsupported AAC profile.");
						}
						if (this.sampleRateIndex > 12)
						{
							throw new AudioExtractionException("Invalid AAC sample rate index.");
						}
						if (this.channelConfig > 6)
						{
							throw new AudioExtractionException("Invalid AAC channel configuration.");
						}
					}
				}
				else
				{
					int num = chunk.Length - 1;
					ulong value = 0uL;
					BitHelper.Write(ref value, 12, 4095);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 2, 0);
					BitHelper.Write(ref value, 1, 1);
					BitHelper.Write(ref value, 2, this.aacProfile);
					BitHelper.Write(ref value, 4, this.sampleRateIndex);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 3, this.channelConfig);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 1, 0);
					BitHelper.Write(ref value, 13, 7 + num);
					BitHelper.Write(ref value, 11, 2047);
					BitHelper.Write(ref value, 2, 0);
					this.fileStream.Write(BigEndianBitConverter.GetBytes(value), 1, 7);
					this.fileStream.Write(chunk, 1, num);
				}
			}
		}
	}
}

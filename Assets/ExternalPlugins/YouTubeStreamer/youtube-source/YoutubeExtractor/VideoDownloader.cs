using System;
using System.IO;
using System.Net;

namespace YoutubeExtractor
{
	public class VideoDownloader : Downloader
	{
		public event EventHandler<ProgressEventArgs> DownloadProgressChanged;

		public VideoDownloader(VideoInfo video, string savePath, int? bytesToDownload = null) : base(video, savePath, bytesToDownload)
		{
		}

		public override void Execute()
		{
			base.OnDownloadStarted(EventArgs.Empty);
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(base.Video.DownloadUrl);
			if (base.BytesToDownload.HasValue)
			{
				httpWebRequest.AddRange(0, base.BytesToDownload.Value - 1);
			}
			using (WebResponse response = httpWebRequest.GetResponse())
			{
				using (Stream responseStream = response.GetResponseStream())
				{
					using (FileStream fileStream = File.Open(base.SavePath, FileMode.Create, FileAccess.Write))
					{
						byte[] array = new byte[1024];
						bool flag = false;
						int num = 0;
						int num2;
						while (!flag && (num2 = responseStream.Read(array, 0, array.Length)) > 0)
						{
							fileStream.Write(array, 0, num2);
							num += num2;
							ProgressEventArgs progressEventArgs = new ProgressEventArgs((double)num * 1.0 / (double)response.ContentLength * 100.0);
							if (this.DownloadProgressChanged != null)
							{
								this.DownloadProgressChanged(this, progressEventArgs);
								if (progressEventArgs.Cancel)
								{
									flag = true;
								}
							}
						}
					}
				}
			}
			base.OnDownloadFinished(EventArgs.Empty);
		}
	}
}

using System;
using System.IO;

namespace YoutubeExtractor
{
	internal class FlvFile : IDisposable
	{
		private readonly long fileLength;

		private readonly string inputPath;

		private readonly string outputPath;

		private IAudioExtractor audioExtractor;

		private long fileOffset;

		private FileStream fileStream;

		public event EventHandler<ProgressEventArgs> ConversionProgressChanged;

		public bool ExtractedAudio
		{
			get;
			private set;
		}

		public FlvFile(string inputPath, string outputPath)
		{
			this.inputPath = inputPath;
			this.outputPath = outputPath;
			this.fileStream = new FileStream(this.inputPath, FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
			this.fileOffset = 0L;
			this.fileLength = this.fileStream.Length;
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		public void ExtractStreams()
		{
			this.Seek(0L);
			if (this.ReadUInt32() != 1179407873u)
			{
				throw new AudioExtractionException("Invalid input file. Impossible to extract audio track.");
			}
			this.ReadUInt8();
			uint num = this.ReadUInt32();
			this.Seek((long)((ulong)num));
			this.ReadUInt32();
			while (this.fileOffset < this.fileLength)
			{
				if (!this.ReadTag())
				{
					break;
				}
				if (this.fileLength - this.fileOffset < 4L)
				{
					break;
				}
				this.ReadUInt32();
				double progressPercentage = (double)this.fileOffset * 1.0 / (double)this.fileLength * 100.0;
				if (this.ConversionProgressChanged != null)
				{
					this.ConversionProgressChanged(this, new ProgressEventArgs(progressPercentage));
				}
			}
			this.CloseOutput(false);
		}

		private void CloseOutput(bool disposing)
		{
			if (this.audioExtractor != null)
			{
				if (disposing && this.audioExtractor.VideoPath != null)
				{
					try
					{
						File.Delete(this.audioExtractor.VideoPath);
					}
					catch
					{
					}
				}
				this.audioExtractor.Dispose();
				this.audioExtractor = null;
			}
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.fileStream != null)
				{
					this.fileStream.Close();
					this.fileStream = null;
				}
				this.CloseOutput(true);
			}
		}

		private IAudioExtractor GetAudioWriter(uint mediaInfo)
		{
			uint num = mediaInfo >> 4;
			uint num2 = num;
			IAudioExtractor result;
			if (num2 != 2u)
			{
				if (num2 == 10u)
				{
					result = new AacAudioExtractor(this.outputPath);
					return result;
				}
				if (num2 != 14u)
				{
					string str;
					switch (num)
					{
					case 1u:
						str = "ADPCM";
						goto IL_7A;
					case 4u:
					case 5u:
					case 6u:
						str = "Nellymoser";
						goto IL_7A;
					}
					str = "format=" + num;
					IL_7A:
					throw new AudioExtractionException("Unable to extract audio (" + str + " is unsupported).");
				}
			}
			result = new Mp3AudioExtractor(this.outputPath);
			return result;
		}

		private byte[] ReadBytes(int length)
		{
			byte[] array = new byte[length];
			this.fileStream.Read(array, 0, length);
			this.fileOffset += (long)length;
			return array;
		}

		private bool ReadTag()
		{
			bool result;
			if (this.fileLength - this.fileOffset < 11L)
			{
				result = false;
			}
			else
			{
				uint num = this.ReadUInt8();
				uint num2 = this.ReadUInt24();
				uint num3 = this.ReadUInt24();
				num3 |= this.ReadUInt8() << 24;
				this.ReadUInt24();
				if (num2 == 0u)
				{
					result = true;
				}
				else if (this.fileLength - this.fileOffset < (long)((ulong)num2))
				{
					result = false;
				}
				else
				{
					uint mediaInfo = this.ReadUInt8();
					num2 -= 1u;
					byte[] chunk = this.ReadBytes((int)num2);
					if (num == 8u)
					{
						if (this.audioExtractor == null)
						{
							this.audioExtractor = this.GetAudioWriter(mediaInfo);
							this.ExtractedAudio = (this.audioExtractor != null);
						}
						if (this.audioExtractor == null)
						{
							throw new InvalidOperationException("No supported audio writer found.");
						}
						this.audioExtractor.WriteChunk(chunk, num3);
					}
					result = true;
				}
			}
			return result;
		}

		private uint ReadUInt24()
		{
			byte[] array = new byte[4];
			this.fileStream.Read(array, 1, 3);
			this.fileOffset += 3L;
			return BigEndianBitConverter.ToUInt32(array, 0);
		}

		private uint ReadUInt32()
		{
			byte[] array = new byte[4];
			this.fileStream.Read(array, 0, 4);
			this.fileOffset += 4L;
			return BigEndianBitConverter.ToUInt32(array, 0);
		}

		private uint ReadUInt8()
		{
			this.fileOffset += 1L;
			return (uint)this.fileStream.ReadByte();
		}

		private void Seek(long offset)
		{
			this.fileStream.Seek(offset, SeekOrigin.Begin);
			this.fileOffset = offset;
		}
	}
}

using System;
using System.Collections.Generic;
using System.IO;

namespace YoutubeExtractor
{
	internal class Mp3AudioExtractor : IAudioExtractor, IDisposable
	{
		private readonly List<byte[]> chunkBuffer;

		private readonly FileStream fileStream;

		private readonly List<uint> frameOffsets;

		private readonly List<string> warnings;

		private int channelMode;

		private bool delayWrite;

		private int firstBitRate;

		private uint firstFrameHeader;

		private bool hasVbrHeader;

		private bool isVbr;

		private int mpegVersion;

		private int sampleRate;

		private uint totalFrameLength;

		private bool writeVbrHeader;

		public string VideoPath
		{
			get;
			private set;
		}

		public IEnumerable<string> Warnings
		{
			get
			{
				return this.warnings;
			}
		}

		public Mp3AudioExtractor(string path)
		{
			this.VideoPath = path;
			this.fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
			this.warnings = new List<string>();
			this.chunkBuffer = new List<byte[]>();
			this.frameOffsets = new List<uint>();
			this.delayWrite = true;
		}

		public void Dispose()
		{
			this.Flush();
			if (this.writeVbrHeader)
			{
				this.fileStream.Seek(0L, SeekOrigin.Begin);
				this.WriteVbrHeader(false);
			}
			this.fileStream.Dispose();
		}

		public void WriteChunk(byte[] chunk, uint timeStamp)
		{
			this.chunkBuffer.Add(chunk);
			this.ParseMp3Frames(chunk);
			if (this.delayWrite && this.totalFrameLength >= 65536u)
			{
				this.delayWrite = false;
			}
			if (!this.delayWrite)
			{
				this.Flush();
			}
		}

		private static int GetFrameDataOffset(int mpegVersion, int channelMode)
		{
			return 4 + ((mpegVersion == 3) ? ((channelMode == 3) ? 17 : 32) : ((channelMode == 3) ? 9 : 17));
		}

		private static int GetFrameLength(int mpegVersion, int bitRate, int sampleRate, int padding)
		{
			return ((mpegVersion == 3) ? 144 : 72) * bitRate / sampleRate + padding;
		}

		private void Flush()
		{
			foreach (byte[] current in this.chunkBuffer)
			{
				this.fileStream.Write(current, 0, current.Length);
			}
			this.chunkBuffer.Clear();
		}

		private void ParseMp3Frames(byte[] buffer)
		{
			int[] array = new int[]
			{
				0,
				32,
				40,
				48,
				56,
				64,
				80,
				96,
				112,
				128,
				160,
				192,
				224,
				256,
				320,
				0
			};
			int[] array2 = new int[]
			{
				0,
				8,
				16,
				24,
				32,
				40,
				48,
				56,
				64,
				80,
				96,
				112,
				128,
				144,
				160,
				0
			};
			int[] array3 = new int[]
			{
				44100,
				48000,
				32000,
				0
			};
			int[] array4 = new int[]
			{
				22050,
				24000,
				16000,
				0
			};
			int[] array5 = new int[]
			{
				11025,
				12000,
				8000,
				0
			};
			int num = 0;
			int frameLength;
			for (int i = buffer.Length; i >= 4; i -= frameLength)
			{
				ulong num2 = (ulong)BigEndianBitConverter.ToUInt32(buffer, num) << 32;
				if (BitHelper.Read(ref num2, 11) != 2047)
				{
					break;
				}
				int num3 = BitHelper.Read(ref num2, 2);
				int num4 = BitHelper.Read(ref num2, 2);
				BitHelper.Read(ref num2, 1);
				int num5 = BitHelper.Read(ref num2, 4);
				int num6 = BitHelper.Read(ref num2, 2);
				int padding = BitHelper.Read(ref num2, 1);
				BitHelper.Read(ref num2, 1);
				int num7 = BitHelper.Read(ref num2, 2);
				if (num3 == 1 || num4 != 1 || num5 == 0 || num5 == 15 || num6 == 3)
				{
					break;
				}
				num5 = ((num3 == 3) ? array[num5] : array2[num5]) * 1000;
				switch (num3)
				{
				case 2:
					num6 = array4[num6];
					break;
				case 3:
					num6 = array3[num6];
					break;
				default:
					num6 = array5[num6];
					break;
				}
				frameLength = Mp3AudioExtractor.GetFrameLength(num3, num5, num6, padding);
				if (frameLength > i)
				{
					break;
				}
				bool flag = false;
				if (this.frameOffsets.Count == 0)
				{
					int startIndex = num + Mp3AudioExtractor.GetFrameDataOffset(num3, num7);
					if (BigEndianBitConverter.ToUInt32(buffer, startIndex) == 1483304551u)
					{
						flag = true;
						this.delayWrite = false;
						this.hasVbrHeader = true;
					}
				}
				if (!flag)
				{
					if (this.firstBitRate == 0)
					{
						this.firstBitRate = num5;
						this.mpegVersion = num3;
						this.sampleRate = num6;
						this.channelMode = num7;
						this.firstFrameHeader = BigEndianBitConverter.ToUInt32(buffer, num);
					}
					else if (!this.isVbr && num5 != this.firstBitRate)
					{
						this.isVbr = true;
						if (!this.hasVbrHeader)
						{
							if (this.delayWrite)
							{
								this.WriteVbrHeader(true);
								this.writeVbrHeader = true;
								this.delayWrite = false;
							}
							else
							{
								this.warnings.Add("Detected VBR too late, cannot add VBR header.");
							}
						}
					}
				}
				this.frameOffsets.Add(this.totalFrameLength + (uint)num);
				num += frameLength;
			}
			this.totalFrameLength += (uint)buffer.Length;
		}

		private void WriteVbrHeader(bool isPlaceholder)
		{
			byte[] array = new byte[Mp3AudioExtractor.GetFrameLength(this.mpegVersion, 64000, this.sampleRate, 0)];
			if (!isPlaceholder)
			{
				uint num = this.firstFrameHeader;
				int frameDataOffset = Mp3AudioExtractor.GetFrameDataOffset(this.mpegVersion, this.channelMode);
				num &= 4294839807u;
				num |= ((this.mpegVersion == 3) ? 5u : 8u) << 12;
				BitHelper.CopyBytes(array, 0, BigEndianBitConverter.GetBytes(num));
				BitHelper.CopyBytes(array, frameDataOffset, BigEndianBitConverter.GetBytes(1483304551u));
				BitHelper.CopyBytes(array, frameDataOffset + 4, BigEndianBitConverter.GetBytes(7u));
				BitHelper.CopyBytes(array, frameDataOffset + 8, BigEndianBitConverter.GetBytes((uint)this.frameOffsets.Count));
				BitHelper.CopyBytes(array, frameDataOffset + 12, BigEndianBitConverter.GetBytes(this.totalFrameLength));
				for (int i = 0; i < 100; i++)
				{
					int index = (int)((double)i / 100.0 * (double)this.frameOffsets.Count);
					array[frameDataOffset + 16 + i] = (byte)(this.frameOffsets[index] / this.totalFrameLength * 256.0);
				}
			}
			this.fileStream.Write(array, 0, array.Length);
		}
	}
}

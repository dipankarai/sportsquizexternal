using System;
using System.IO;

namespace YoutubeExtractor
{
	public class AudioDownloader : Downloader
	{
		private bool isCanceled;

		public event EventHandler<ProgressEventArgs> AudioExtractionProgressChanged;

		public event EventHandler<ProgressEventArgs> DownloadProgressChanged;

		public AudioDownloader(VideoInfo video, string savePath, int? bytesToDownload = null) : base(video, savePath, bytesToDownload)
		{
		}

		public override void Execute()
		{
			string tempFileName = Path.GetTempFileName();
			this.DownloadVideo(tempFileName);
			if (!this.isCanceled)
			{
				this.ExtractAudio(tempFileName);
			}
			base.OnDownloadFinished(EventArgs.Empty);
		}

		private void DownloadVideo(string path)
		{
			VideoDownloader videoDownloader = new VideoDownloader(base.Video, path, base.BytesToDownload);
			videoDownloader.DownloadProgressChanged += delegate(object sender, ProgressEventArgs args)
			{
				if (this.DownloadProgressChanged != null)
				{
					this.DownloadProgressChanged(this, args);
					this.isCanceled = args.Cancel;
				}
			};
			videoDownloader.Execute();
		}

		private void ExtractAudio(string path)
		{
			using (FlvFile flvFile = new FlvFile(path, base.SavePath))
			{
				flvFile.ConversionProgressChanged += delegate(object sender, ProgressEventArgs args)
				{
					if (this.AudioExtractionProgressChanged != null)
					{
						this.AudioExtractionProgressChanged(this, new ProgressEventArgs(args.ProgressPercentage));
					}
				};
				flvFile.ExtractStreams();
			}
		}
	}
}

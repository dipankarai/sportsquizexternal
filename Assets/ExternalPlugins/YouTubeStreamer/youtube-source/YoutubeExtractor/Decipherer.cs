using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace YoutubeExtractor
{
	internal static class Decipherer
	{
		public static string DecipherWithVersion(string cipher, string cipherVersion)
		{
			string url = string.Format("http://s.ytimg.com/yts/jsbin/player-{0}.js", cipherVersion);
			string input = HttpHelper.DownloadString(url);
			string pattern = "\\.sig\\s*\\|\\|([a-zA-Z0-9\\$]+)\\(";
			string text = Regex.Match(input, pattern).Groups[1].Value;
			if (text.Contains("$"))
			{
				text = "\\" + text;
			}
			string pattern2 = "var " + text + "=function\\(\\w+\\)\\{.*?\\};";
			string value = Regex.Match(input, pattern2).Value;
			string[] array = value.Split(new char[]
			{
				';'
			});
			string text2 = "";
			string text3 = "";
			string text4 = "";
			string text5 = "";
			foreach (string current in array.Skip(1).Take(array.Length - 2))
			{
				if (!string.IsNullOrEmpty(text2) && !string.IsNullOrEmpty(text3) && !string.IsNullOrEmpty(text4))
				{
					break;
				}
				string functionFromLine = Decipherer.GetFunctionFromLine(current);
				string pattern3 = string.Format("{0}:\\bfunction\\b\\(\\w+\\)", functionFromLine);
				string pattern4 = string.Format("{0}:\\bfunction\\b\\([a],b\\).(\\breturn\\b)?.?\\w+\\.", functionFromLine);
				string pattern5 = string.Format("{0}:\\bfunction\\b\\(\\w+\\,\\w\\).\\bvar\\b.\\bc=a\\b", functionFromLine);
				if (Regex.Match(input, pattern3).Success)
				{
					text2 = functionFromLine;
				}
				if (Regex.Match(input, pattern4).Success)
				{
					text3 = functionFromLine;
				}
				if (Regex.Match(input, pattern5).Success)
				{
					text4 = functionFromLine;
				}
			}
			foreach (string current in array.Skip(1).Take(array.Length - 2))
			{
				string functionFromLine = Decipherer.GetFunctionFromLine(current);
				Match match;
				if ((match = Regex.Match(current, "\\(\\w+,(?<index>\\d+)\\)")).Success && functionFromLine == text4)
				{
					text5 = text5 + "w" + match.Groups["index"].Value + " ";
				}
				if ((match = Regex.Match(current, "\\(\\w+,(?<index>\\d+)\\)")).Success && functionFromLine == text3)
				{
					text5 = text5 + "s" + match.Groups["index"].Value + " ";
				}
				if (functionFromLine == text2)
				{
					text5 += "r ";
				}
			}
			text5 = text5.Trim();
			return Decipherer.DecipherWithOperations(cipher, text5);
		}

		private static string ApplyOperation(string cipher, string op)
		{
			char c = op[0];
			string result;
			switch (c)
			{
			case 'r':
				result = new string(cipher.ToCharArray().Reverse<char>().ToArray<char>());
				break;
			case 's':
			{
				int opIndex = Decipherer.GetOpIndex(op);
				result = cipher.Substring(opIndex);
				break;
			}
			default:
			{
				if (c != 'w')
				{
					throw new NotImplementedException("Couldn't find cipher operation.");
				}
				int opIndex = Decipherer.GetOpIndex(op);
				result = Decipherer.SwapFirstChar(cipher, opIndex);
				break;
			}
			}
			return result;
		}

		private static string DecipherWithOperations(string cipher, string operations)
		{
			return operations.Split(new string[]
			{
				" "
			}, StringSplitOptions.RemoveEmptyEntries).Aggregate(cipher, new Func<string, string, string>(Decipherer.ApplyOperation));
		}

		private static string GetFunctionFromLine(string currentLine)
		{
			Regex regex = new Regex("\\w+\\.(?<functionID>\\w+)\\(");
			Match match = regex.Match(currentLine);
			return match.Groups["functionID"].Value;
		}

		private static int GetOpIndex(string op)
		{
			string s = new Regex(".(\\d+)").Match(op).Result("$1");
			return int.Parse(s);
		}

		private static string SwapFirstChar(string cipher, int index)
		{
			StringBuilder stringBuilder = new StringBuilder(cipher);
			stringBuilder[0] = cipher[index];
			stringBuilder[index] = cipher[0];
			return stringBuilder.ToString();
		}
	}
}

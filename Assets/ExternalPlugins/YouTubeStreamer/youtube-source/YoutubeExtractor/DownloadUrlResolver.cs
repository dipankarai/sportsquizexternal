using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace YoutubeExtractor
{
	public static class DownloadUrlResolver
	{
		private class ExtractionInfo
		{
			public bool RequiresDecryption
			{
				get;
				set;
			}

			public Uri Uri
			{
				get;
				set;
			}
		}

		private const string RateBypassFlag = "ratebypass";

		private const int CorrectSignatureLength = 81;

		private const string SignatureQuery = "signature";

		public static void DecryptDownloadUrl(VideoInfo videoInfo)
		{
			IDictionary<string, string> dictionary = HttpHelper.ParseQueryString(videoInfo.DownloadUrl);
			if (dictionary.ContainsKey("signature"))
			{
				string signature = dictionary["signature"];
				string decipheredSignature;
				try
				{
					decipheredSignature = DownloadUrlResolver.GetDecipheredSignature(videoInfo.HtmlPlayerVersion, signature);
				}
				catch (Exception innerException)
				{
					throw new YoutubeParseException("Could not decipher signature", innerException);
				}
				videoInfo.DownloadUrl = HttpHelper.ReplaceQueryStringParameter(videoInfo.DownloadUrl, "signature", decipheredSignature);
				videoInfo.RequiresDecryption = false;
			}
		}

		public static IEnumerable<VideoInfo> GetDownloadUrls(string videoUrl, bool decryptSignature = true)
		{
			if (videoUrl == null)
			{
				throw new ArgumentNullException("videoUrl");
			}
			if (!DownloadUrlResolver.TryNormalizeYoutubeUrl(videoUrl, out videoUrl))
			{
				throw new ArgumentException("URL is not a valid youtube URL!");
			}
			IEnumerable<VideoInfo> result;
			try
			{
				JObject json = DownloadUrlResolver.LoadJson(videoUrl);

				string videoTitle = DownloadUrlResolver.GetVideoTitle(json);
				IEnumerable<DownloadUrlResolver.ExtractionInfo> extractionInfos = DownloadUrlResolver.ExtractDownloadUrls(json);
				IEnumerable<VideoInfo> enumerable = DownloadUrlResolver.GetVideoInfos(extractionInfos, videoTitle).ToList<VideoInfo>();
				string html5PlayerVersion = DownloadUrlResolver.GetHtml5PlayerVersion(json);
				foreach (VideoInfo current in enumerable)
				{
					current.HtmlPlayerVersion = html5PlayerVersion;
					if (decryptSignature && current.RequiresDecryption)
					{
						DownloadUrlResolver.DecryptDownloadUrl(current);
					}
				}
				result = enumerable;
				return result;
			}
			catch (Exception ex)
			{
				if (ex is WebException || ex is VideoNotAvailableException)
				{
					throw;
				}
				DownloadUrlResolver.ThrowYoutubeParseException(ex, videoUrl);
			}
			result = null;
			return result;
		}

        public static IEnumerable<VideoInfo> GetVideoInfoListFromResponse(string videoRespData, bool decryptSignature = true)
        {            
            IEnumerable<VideoInfo> result;
            try
            {
                if (DownloadUrlResolver.IsVideoUnavailable(videoRespData))
                {
                    throw new VideoNotAvailableException();
                }
                Regex regex = new Regex("ytplayer\\.config\\s*=\\s*(\\{.+?\\});", RegexOptions.Multiline);
                string text2 = regex.Match(videoRespData).Result("$1");
                JObject json = JObject.Parse(text2);

                string videoTitle = DownloadUrlResolver.GetVideoTitle(json);
                IEnumerable<DownloadUrlResolver.ExtractionInfo> extractionInfos = DownloadUrlResolver.ExtractDownloadUrls(json);
                IEnumerable<VideoInfo> enumerable = DownloadUrlResolver.GetVideoInfos(extractionInfos, videoTitle).ToList<VideoInfo>();
                string html5PlayerVersion = DownloadUrlResolver.GetHtml5PlayerVersion(json);
                foreach (VideoInfo current in enumerable)
                {
                    current.HtmlPlayerVersion = html5PlayerVersion;
                    if (decryptSignature && current.RequiresDecryption)
                    {
                        DownloadUrlResolver.DecryptDownloadUrl(current);
                    }
                }
                result = enumerable;
                return result;
            }
            catch (Exception ex)
            {
                if (ex is WebException || ex is VideoNotAvailableException)
                {
                    throw;
                }
                DownloadUrlResolver.ThrowYoutubeParseException(ex, "Video Response.");
            }
            result = null;
            return result;
        }

		public static bool TryNormalizeYoutubeUrl(string url, out string normalizedUrl)
		{
			url = url.Trim();
			url = url.Replace("youtu.be/", "youtube.com/watch?v=");
			url = url.Replace("www.youtube", "youtube");
			url = url.Replace("youtube.com/embed/", "youtube.com/watch?v=");
			if (url.Contains("/v/"))
			{
				url = "https://youtube.com" + new Uri(url).AbsolutePath.Replace("/v/", "/watch?v=");
			}
			url = url.Replace("/watch#", "/watch?");
			IDictionary<string, string> dictionary = HttpHelper.ParseQueryString(url);
			string str;
			bool result;
			if (!dictionary.TryGetValue("v", out str))
			{
				normalizedUrl = null;
				result = false;
			}
			else
			{
				normalizedUrl = "https://youtube.com/watch?v=" + str;
				result = true;
			}
			return result;
		}

		private static IEnumerable<DownloadUrlResolver.ExtractionInfo> ExtractDownloadUrls(JObject json)
		{
			string[] array = DownloadUrlResolver.GetStreamMap(json).Split(new char[]
			{
				','
			});
			string[] second = DownloadUrlResolver.GetAdaptiveStreamMap(json).Split(new char[]
			{
				','
			});
			array = array.Concat(second).ToArray<string>();
			try
			{
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string s = array2[i];
					IDictionary<string, string> dictionary = HttpHelper.ParseQueryString(s);
					bool requiresDecryption = false;
					string text;
					if (dictionary.ContainsKey("s") || dictionary.ContainsKey("sig"))
					{
						requiresDecryption = dictionary.ContainsKey("s");
						string arg = dictionary.ContainsKey("s") ? dictionary["s"] : dictionary["sig"];
						text = string.Format("{0}&{1}={2}", dictionary["url"], "signature", arg);
						string str = dictionary.ContainsKey("fallback_host") ? ("&fallback_host=" + dictionary["fallback_host"]) : string.Empty;
						text += str;
					}
					else
					{
						text = dictionary["url"];
					}
					text = HttpHelper.UrlDecode(text);
					text = HttpHelper.UrlDecode(text);
					IDictionary<string, string> dictionary2 = HttpHelper.ParseQueryString(text);
					if (!dictionary2.ContainsKey("ratebypass"))
					{
						text += string.Format("&{0}={1}", "ratebypass", "yes");
					}
					yield return new DownloadUrlResolver.ExtractionInfo
					{
						RequiresDecryption = requiresDecryption,
						Uri = new Uri(text)
					};
				}
			}
			finally
			{
			}
			yield break;
		}

		private static string GetAdaptiveStreamMap(JObject json)
		{
			JToken jToken = json["args"]["adaptive_fmts"];
			return jToken.ToString();
		}

		private static string GetDecipheredSignature(string htmlPlayerVersion, string signature)
		{
			string result;
			if (signature.Length == 81)
			{
				result = signature;
			}
			else
			{
				result = Decipherer.DecipherWithVersion(signature, htmlPlayerVersion);
			}
			return result;
		}

		private static string GetHtml5PlayerVersion(JObject json)
		{
			Regex regex = new Regex("player-(.+?).js");
			string input = json["assets"]["js"].ToString();
			return regex.Match(input).Result("$1");
		}

		private static string GetStreamMap(JObject json)
		{
			JToken jToken = json["args"]["url_encoded_fmt_stream_map"];
			string text = (jToken == null) ? null : jToken.ToString();
			if (text == null || text.Contains("been+removed"))
			{
				throw new VideoNotAvailableException("Video is removed or has an age restriction.");
			}
			return text;
		}

		private static IEnumerable<VideoInfo> GetVideoInfos(IEnumerable<DownloadUrlResolver.ExtractionInfo> extractionInfos, string videoTitle)
		{
			List<VideoInfo> list = new List<VideoInfo>();
			foreach (DownloadUrlResolver.ExtractionInfo current in extractionInfos)
			{
				string s = HttpHelper.ParseQueryString(current.Uri.Query)["itag"];
				int formatCode = int.Parse(s);
				VideoInfo videoInfo2 = VideoInfo.Defaults.SingleOrDefault((VideoInfo videoInfo) => videoInfo.FormatCode == formatCode);
				if (videoInfo2 != null)
				{
					videoInfo2 = new VideoInfo(videoInfo2)
					{
						DownloadUrl = current.Uri.ToString(),
						Title = videoTitle,
						RequiresDecryption = current.RequiresDecryption
					};
				}
				else
				{
					videoInfo2 = new VideoInfo(formatCode)
					{
						DownloadUrl = current.Uri.ToString()
					};
				}
				list.Add(videoInfo2);
			}
			return list;
		}

		private static string GetVideoTitle(JObject json)
		{
			JToken jToken = json["args"]["title"];
			return (jToken == null) ? string.Empty : jToken.ToString();
		}

		private static bool IsVideoUnavailable(string pageSource)
		{
			return pageSource.Contains("<div id=\"watch-player-unavailable\">");
		}

		private static JObject LoadJson(string url)
		{
			string text = HttpHelper.DownloadString(url);
			if (DownloadUrlResolver.IsVideoUnavailable(text))
			{
				throw new VideoNotAvailableException();
			}
			Regex regex = new Regex("ytplayer\\.config\\s*=\\s*(\\{.+?\\});", RegexOptions.Multiline);
			string text2 = regex.Match(text).Result("$1");
			return JObject.Parse(text2);
		}

		private static void ThrowYoutubeParseException(Exception innerException, string videoUrl)
		{
			throw new YoutubeParseException("Could not parse the Youtube page for URL " + videoUrl + "\nThis may be due to a change of the Youtube page structure.\nPlease report this bug at www.github.com/flagbug/YoutubeExtractor/issues", innerException);
		}
	}
}

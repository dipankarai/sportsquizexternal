using System;

namespace YoutubeExtractor
{
	public class AudioExtractionException : Exception
	{
		public AudioExtractionException(string message) : base(message)
		{
		}
	}
}

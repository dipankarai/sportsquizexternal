using System;

namespace YoutubeExtractor
{
	internal static class BigEndianBitConverter
	{
		public static byte[] GetBytes(ulong value)
		{
			return new byte[]
			{
				(byte)(value >> 56),
				(byte)(value >> 48),
				(byte)(value >> 40),
				(byte)(value >> 32),
				(byte)(value >> 24),
				(byte)(value >> 16),
				(byte)(value >> 8),
				(byte)value
			};
		}

		public static byte[] GetBytes(uint value)
		{
			return new byte[]
			{
				(byte)(value >> 24),
				(byte)(value >> 16),
				(byte)(value >> 8),
				(byte)value
			};
		}

		public static byte[] GetBytes(ushort value)
		{
			return new byte[]
			{
				(byte)(value >> 8),
				(byte)value
			};
		}

		public static ushort ToUInt16(byte[] value, int startIndex)
		{
			return (ushort)((int)value[startIndex] << 8 | (int)value[startIndex + 1]);
		}

		public static uint ToUInt32(byte[] value, int startIndex)
		{
			return (uint)((int)value[startIndex] << 24 | (int)value[startIndex + 1] << 16 | (int)value[startIndex + 2] << 8 | (int)value[startIndex + 3]);
		}

		public static ulong ToUInt64(byte[] value, int startIndex)
		{
			return (ulong)value[startIndex] << 56 | (ulong)value[startIndex + 1] << 48 | (ulong)value[startIndex + 2] << 40 | (ulong)value[startIndex + 3] << 32 | (ulong)value[startIndex + 4] << 24 | (ulong)value[startIndex + 5] << 16 | (ulong)value[startIndex + 6] << 8 | (ulong)value[startIndex + 7];
		}
	}
}

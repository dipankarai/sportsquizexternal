using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace YoutubeExtractor
{
	internal static class HttpHelper
	{
		public static string DownloadString(string url)
		{
			string result;
			using (WebClient webClient = new WebClient())
			{
				webClient.Encoding = Encoding.UTF8;
				result = webClient.DownloadString(url);
			}
			return result;
		}

		public static string HtmlDecode(string value)
		{
			return HttpUtility.HtmlDecode(value);
		}

		public static IDictionary<string, string> ParseQueryString(string s)
		{
			if (s.Contains("?"))
			{
				s = s.Substring(s.IndexOf('?') + 1);
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			string[] array = Regex.Split(s, "&");
			for (int i = 0; i < array.Length; i++)
			{
				string input = array[i];
				string[] array2 = Regex.Split(input, "=");
				dictionary.Add(array2[0], (array2.Length == 2) ? HttpHelper.UrlDecode(array2[1]) : string.Empty);
			}
			return dictionary;
		}

		public static string ReplaceQueryStringParameter(string currentPageUrl, string paramToReplace, string newValue)
		{
			IDictionary<string, string> dictionary = HttpHelper.ParseQueryString(currentPageUrl);
			dictionary[paramToReplace] = newValue;
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = true;
			foreach (KeyValuePair<string, string> current in dictionary)
			{
				if (!flag)
				{
					stringBuilder.Append("&");
				}
				stringBuilder.Append(current.Key);
				stringBuilder.Append("=");
				stringBuilder.Append(current.Value);
				flag = false;
			}
			UriBuilder uriBuilder = new UriBuilder(currentPageUrl)
			{
				Query = stringBuilder.ToString()
			};
			return uriBuilder.ToString();
		}

		public static string UrlDecode(string url)
		{
			return HttpUtility.UrlDecode(url);
		}

		private static string ReadStreamFromResponse(WebResponse response)
		{
			string result;
			using (Stream responseStream = response.GetResponseStream())
			{
				using (StreamReader streamReader = new StreamReader(responseStream))
				{
					result = streamReader.ReadToEnd();
				}
			}
			return result;
		}
	}
}

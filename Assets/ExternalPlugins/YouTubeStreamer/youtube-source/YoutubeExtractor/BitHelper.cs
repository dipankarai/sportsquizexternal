using System;

namespace YoutubeExtractor
{
	internal static class BitHelper
	{
		public static byte[] CopyBlock(byte[] bytes, int offset, int length)
		{
			int num = offset / 8;
			int num2 = (offset + length - 1) / 8;
			int num3 = offset % 8;
			int num4 = 8 - num3;
			byte[] array = new byte[(length + 7) / 8];
			if (num3 == 0)
			{
				Buffer.BlockCopy(bytes, num, array, 0, array.Length);
			}
			else
			{
				int i;
				for (i = 0; i < num2 - num; i++)
				{
					array[i] = (byte)((int)bytes[num + i] << num3 | bytes[num + i + 1] >> num4);
				}
				if (i < array.Length)
				{
					array[i] = (byte)(bytes[num + i] << num3);
				}
			}
			byte[] expr_AD_cp_0 = array;
			int expr_AD_cp_1 = array.Length - 1;
			expr_AD_cp_0[expr_AD_cp_1] &= (byte)(255 << array.Length * 8 - length);
			return array;
		}

		public static void CopyBytes(byte[] dst, int dstOffset, byte[] src)
		{
			Buffer.BlockCopy(src, 0, dst, dstOffset, src.Length);
		}

		public static int Read(ref ulong x, int length)
		{
			int result = (int)(x >> 64 - length);
			x <<= length;
			return result;
		}

		public static int Read(byte[] bytes, ref int offset, int length)
		{
			int num = offset / 8;
			int num2 = (offset + length - 1) / 8;
			int num3 = offset % 8;
			ulong num4 = 0uL;
			for (int i = 0; i <= Math.Min(num2 - num, 7); i++)
			{
				num4 |= (ulong)bytes[num + i] << 56 - i * 8;
			}
			if (num3 != 0)
			{
				BitHelper.Read(ref num4, num3);
			}
			offset += length;
			return BitHelper.Read(ref num4, length);
		}

		public static void Write(ref ulong x, int length, int value)
		{
			ulong num = 18446744073709551615uL >> 64 - length;
			x = (x << length | (ulong)((long)value & (long)num));
		}
	}
}

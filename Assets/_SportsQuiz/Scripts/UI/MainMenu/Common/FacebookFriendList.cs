﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class FacebookFriendList : FriendList {

	[SerializeField] private GameObject socialLoginPanel;
	[SerializeField] private Button socialLoginButton;
	[SerializeField] private Text friendsNotPresentText;

	void OnEnable () {
		friendsNotPresentText.text = "";
		socialLoginPanel.gameObject.SetActive (false);
		GetFriendList ();
	}

	// Use this for initialization
	void Start () {
		socialLoginButton.onClick.AddListener (OnClickSocialButton);
	}

	void OnClickSocialButton ()
	{
		SocialHandler.Instance.GetFacebookFriendList (FacebookFriendListCallback);
		socialLoginPanel.gameObject.SetActive (false);
	}

	private List<FollowerItemPanel> m_followerItemList = new List<FollowerItemPanel> ();

	public override void GetFriendList ()
	{
		if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.FacebookUser)
			SocialHandler.Instance.GetFacebookFriendList (FacebookFriendListCallback);
		else {

			if ((Facebook.Unity.FB.IsLoggedIn == false) || SocialHandler.Instance.facebookFriendList == null) {
				
				for (int i = 0; i < m_followerItemList.Count; i++) {
					Destroy (m_followerItemList [i].gameObject);
				}
				m_followerItemList.Clear ();
				socialLoginPanel.gameObject.SetActive (true);
			}
		}
	}

	void FacebookFriendListCallback (bool success)
	{
		Q.Utils.QDebug.Log ("FacebookFriendListCallback "+success);
		if (success) {

			GenerateObjectInScrollContent ();
		} else {
			if (SportsQuizManager.Instance.loginType != SportsQuizManager.eUserType.FacebookUser) {
				
				socialLoginPanel.gameObject.SetActive (true);
			} else {

				NoFriendsInList ();
			}
		}
	}

	void GenerateObjectInScrollContent ()
	{
		if (SocialHandler.Instance.facebookFriendList.friends.data.Count > 0) {
			
			for (int i = 0; i < SocialHandler.Instance.facebookFriendList.friends.data.Count; i++) {
				
				SocialHandler.FacebookData _facebookData = SocialHandler.Instance.facebookFriendList.friends.data [i];
				
				if (CheckContainInItem (_facebookData.id) == true) {
					
					FollowerItemPanel _follower = Instantiate (itemPanel);
					_follower.gameObject.RectTransformMakeChild (scrollRect.content.transform);
					_follower.InitFacebookFriendItemPanel (_facebookData.id, _facebookData.name, _facebookData.picture.data.url, this);
					
					m_followerItemList.Add (_follower);
				}
				
			}		
		} else {

			NoFriendsInList ();
		}
	}

	void NoFriendsInList ()
	{
		friendsNotPresentText.text = TagConstant.PopUpMessages.NO_FRIEND_LIST_FB;
	}

	bool CheckContainInItem (string facebookid)
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			if (m_followerItemList [i].facebook_id == facebookid)
				return false;
		}

		return true;
	}

	SportsQuizManager.InviteFriendDetail GetUserDetail (string id)
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			if (m_followerItemList [i].facebook_id == id)
			{
				SportsQuizManager.InviteFriendDetail m_follower = new SportsQuizManager.InviteFriendDetail ();
				m_follower.name = m_followerItemList [i].usernameText.text;
				m_follower.user_id = (object) m_followerItemList [i].facebook_id;
				m_follower.userType = SportsQuizManager.eUserType.FacebookUser;

				Q.Utils.QDebug.Log (m_follower.name);
				return m_follower;
			}
		}

		return null;
	}

	public override void OnClickFollowerSelected (object userID)
	{
		Q.Utils.QDebug.Log (userID);
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.FacebookUser;
		selectFriendPanel.SelectedUser = GetUserDetail ((string)userID);
	}

	public override void RemoveSelectedFolower ()
	{
		selectFriendPanel.SelectedUser = null;
	}

	public override void OnGameobjectDisable ()
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			Destroy (m_followerItemList [i].gameObject);
		}

		m_followerItemList.Clear ();
		Resources.UnloadUnusedAssets ();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using Q.Utils;

public class GooglePlusFriendList : FriendList {

	[SerializeField] private GameObject socialLoginPanel;
	[SerializeField] private Button socialLoginButton;
	[SerializeField] private Text friendsNotPresentText;

	void OnEnable () {
		friendsNotPresentText.text = "";
		socialLoginPanel.gameObject.SetActive (false);
		GetFriendList ();
	}

	// Use this for initialization
	void Start () {
		socialLoginButton.onClick.AddListener (OnClickSocialButton);
	}

	void OnClickSocialButton ()
	{
		SocialHandler.Instance.GoogleFriends (GoogleFriendsCallback);
		socialLoginPanel.gameObject.SetActive (false);
	}

	private List<FollowerItemPanel> m_followerItemList = new List<FollowerItemPanel> ();

	public void GetFriendList ()
	{
		if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GPlus)
			SocialHandler.Instance.GoogleFriends (GoogleFriendsCallback);
		else {

			if ((Social.localUser.authenticated == false) || SocialHandler.Instance.googleFriendList == null) {

				for (int i = 0; i < m_followerItemList.Count; i++) {
					Destroy (m_followerItemList [i].gameObject);
				}
				m_followerItemList.Clear ();
				socialLoginPanel.gameObject.SetActive (true);
			}
		}
	}

	void GoogleFriendsCallback (bool success)
	{
		Q.Utils.QDebug.Log ("GoogleFriendsCallback "+success);
		if (success) {
			
			GenerateObjectInScrollContent ();
		} else {
			if (SportsQuizManager.Instance.loginType != SportsQuizManager.eUserType.GPlus) {
				
				socialLoginPanel.gameObject.SetActive (true);
			} else {

				NoFriendsInList ();
			}
		}
	}


	void GenerateObjectInScrollContent ()
	{
		if (SocialHandler.Instance.googleFriendList.Count > 0) {
			
			for (int i = 0; i < SocialHandler.Instance.googleFriendList.Count; i++) {
				
				SocialHandler.GoogleFriendDetail _gPlusFriendData = SocialHandler.Instance.googleFriendList [i];
				
				if (CheckContainInItem (_gPlusFriendData.id) == true) {
					
					FollowerItemPanel _follower = Instantiate (itemPanel);
					_follower.gameObject.RectTransformMakeChild (scrollRect.content.transform);
					_follower.InitGoogleFriendItemPanel (_gPlusFriendData.id, _gPlusFriendData.name, _gPlusFriendData.image, this);
					
					m_followerItemList.Add (_follower);
				}
			}		
		} else {

			NoFriendsInList ();
		}
	}

	void NoFriendsInList ()
	{
		friendsNotPresentText.text = TagConstant.PopUpMessages.NO_FRIEND_LIST_GP;
	}

	bool CheckContainInItem (string facebookid)
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			if (m_followerItemList [i].facebook_id == facebookid)
				return false;
		}

		return true;
	}

	SportsQuizManager.InviteFriendDetail GetUserDetail (string id)
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			if (m_followerItemList [i].facebook_id == id)
			{
				SportsQuizManager.InviteFriendDetail m_follower = new SportsQuizManager.InviteFriendDetail ();
				m_follower.name = m_followerItemList [i].name;
				m_follower.user_id = (object) m_followerItemList [i].facebook_id;
				m_follower.userType = SportsQuizManager.eUserType.FacebookUser;

				Q.Utils.QDebug.Log (m_follower.name);
				return m_follower;
			}
		}

		return null;
	}

	public override void OnClickFollowerSelected (object userID)
	{
		Q.Utils.QDebug.Log (userID);
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.GPlus;
		selectFriendPanel.SelectedUser = GetUserDetail ((string)userID);
	}

	public override void RemoveSelectedFolower ()
	{
		selectFriendPanel.SelectedUser = null;
	}
}

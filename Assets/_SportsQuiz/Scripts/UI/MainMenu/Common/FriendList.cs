﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class FriendList : MonoBehaviour {

	public ScrollRect scrollRect;
	public FollowerItemPanel itemPanel;
	[HideInInspector]public SelectFriendPanel selectFriendPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void GetFriendList ()
	{
		
	}

	public virtual void OnClickFollowerSelected (object userID)
	{
		
	}

	public virtual void RemoveSelectedFolower ()
	{
		
	}

	public virtual void OnGameobjectDisable ()
	{
		
	}
}

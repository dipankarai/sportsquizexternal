﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class BadgeAchievedPopUp : MonoBehaviour {

	[SerializeField] private Image m_badgeImage;
    [SerializeField] private Text m_badgeDescription, m_title;
    [SerializeField] private Button m_closeButton;
	[SerializeField] private GameObject m_loadingImage;
	private SportsQuizManager.Badges m_badge;
	System.Action<SeralizedClassServer.UserBadges> callback;
	SeralizedClassServer.UserBadges badge;

    // Use this for initialization
    void Start () {
        m_closeButton.onClick.AddListener(OnClickCloseButton);
	}

	public void InitBadgeAchieved (SeralizedClassServer.UserBadges badge, System.Action<SeralizedClassServer.UserBadges> callback)
    {
        this.callback = callback;
        this.badge = badge;

		m_badge = SportsQuizManager.Instance.GetBadge(badge.badge_id);

		if (m_badge != null)
        {
			if (References.GetImageFromLocal (m_badge.id, m_badge.url, References.eImageType.Badges) == null) {
				StartCoroutine (LoadBadge (m_badge.url));
			} else {
				ViewBadge (References.GetImageFromLocal (m_badge.id, m_badge.url, References.eImageType.Badges));
			}

			m_title.text = m_badge.title;
			m_badgeDescription.text = m_badge.description;
        }
    }

	IEnumerator LoadBadge (string url)
	{
		WWW image = new WWW (url);
		yield return image;
		if (image.error == null) {
			References.SaveImageToLocal (m_badge.id, url, image.texture, References.eImageType.Badges);
			ViewBadge (image.texture);
		} else {
			Q.Utils.QDebug.Log (image.error);
		}
	}

	void ViewBadge (Texture2D texture)
	{
		m_loadingImage.SetActive (false);
		m_badgeImage.enabled = true;
		m_badgeImage.CreateImageSprite (texture);
	}
	
	void OnClickCloseButton ()
    {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ClosePanel ();
    }

	public void ClosePanel ()
	{
		this.callback(this.badge);
		this.gameObject.SetActive(false);
		m_loadingImage.SetActive (true);
		m_badgeImage.enabled = false;
	}
}

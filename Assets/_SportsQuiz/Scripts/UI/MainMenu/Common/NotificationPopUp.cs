﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class NotificationPopUp : MonoBehaviour {

	[SerializeField] private Text header, description;
	[SerializeField] private Button acceptButton, rejectButton, closeButton;
	[SerializeField] private Image profile;

	private int m_notification_ID;
	private NotificationItem.eNotificatioType m_notificatioType;
	private SeralizedClassServer.NotificationData m_notificationData;
	private string m_message;
	private SeralizedClassServer.OpponentDetails m_opponentDetails;
	private SeralizedClassServer.DuelQuizQuestionsList m_dualCallback;

	void OnEnable () {
		StartCoroutine (LoadImage ());
	}

	// Use this for initialization
	void Start () {
		acceptButton.onClick.AddListener (OnClickAcceptButton);
		closeButton.onClick.AddListener (OnClickCloseButton);
		rejectButton.onClick.AddListener (OnClickRejectButton);
	}

	void OnClickCloseButton () {
		this.gameObject.SetActive (false);
	}

	void OnClickAcceptButton () {
		if (InternetConnection.Check) {
			ServerManager.Instance.GetChallenge (m_notificationData.user_quiz_id, m_notification_ID, GetChallengeCallback);
			this.gameObject.SetActive (false);
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void OnClickRejectButton () {
		if (InternetConnection.Check) {
			ServerManager.Instance.RejectChallenge (m_notificationData.user_quiz_id, m_notification_ID, ChallengeRejectCallback);
			this.gameObject.SetActive (false);
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	public void InitNotificationPopUp (int id, int type, SeralizedClassServer.NotificationData data, string message)
	{
		Q.Utils.QDebug.Log ("InitNotificationPopUp "+message);
		m_notification_ID = id;
		m_notificatioType = (NotificationItem.eNotificatioType)type;
		m_notificationData = data;
		m_message = message;
		SportsQuizManager.Instance.GetOpponentDetail(m_notificationData.user_id, UserInfoCallback);
	}
	
	public void UserInfoCallback (SeralizedClassServer.OpponentDetails callback)
	{
		m_opponentDetails = callback;

		if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadImage ());
		} else {
			ActiveObject ();
		}
	}

	IEnumerator LoadImage ()
	{
		if (SportsQuizManager.IsNullOrEmptyOrZero (m_opponentDetails.user_info.facebook_id)) {
			DisplayImage (SportsQuizManager.Instance.references.GetAvatarImage (m_opponentDetails.user_info.avatar_type));
		} else {
			string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (m_opponentDetails.user_info.facebook_id);
			if (string.IsNullOrEmpty (_facebookImageURL)) {
				DisplayImage (SportsQuizManager.Instance.references.GetAvatarImage (m_opponentDetails.user_info.avatar_type));
			} else {

				WWW image = new WWW (_facebookImageURL);
				yield return image;
				if (image.error == null) {
					DisplayImage (image.texture);
				} else {
					DisplayImage (SportsQuizManager.Instance.references.GetAvatarImage (m_opponentDetails.user_info.avatar_type));
				}
			}
		}
	}

	void DisplayImage (Texture2D texture) {
		profile.CreateImageSprite (texture);
		ActiveObject ();
	}

	void ActiveObject ()
	{
		header.text = m_notificationData.category_title;
		description.text = m_message;

		if (MainMenuPanelManager.Instance.categoryPanel.inviteFriendPanel.RequestingChallenge == true) {
			return;
		}

		if (MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.gameObject.activeInHierarchy) {
			return;
		}

		if (MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel == MainMenuPanel.eSubMenuPanel.Notfications) {
			return;
		}

		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.Gameplay) {

			if (QuizPanelManager.Instance.currentPanelState == QuizPanelManager.eQuizPanelState.ResultPanel) {
				this.gameObject.SetActive (true);
			} else {
				return;
			}
		}

		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.CategoryPanel) {
			this.gameObject.SetActive (true);
		}

		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.MainMenuPanel) {
			this.gameObject.SetActive (true);
		}
	}

	void GetChallengeCallback (SeralizedClassServer.DuelQuizQuestionsList callback)
	{
		if (callback != null) {

			m_dualCallback = callback;

			SportsQuizManager.Instance.selectedMode = (QuizManager.eQuizMode)m_notificationData.quiz_type;
			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();
			SportsQuizManager.Instance.currentSelectedTopics.category_id = m_notificationData.category_id;
			SportsQuizManager.Instance.currentSelectedTopics.category_title = m_notificationData.category_title;

			SportsQuizManager.Instance.GetOpponentDetail (m_notificationData.user_id, OpponentDetailCall);
		} else {
			SportsQuizManager.Instance.GetOpponentDetail (m_notificationData.user_id, OponentCancelCallback);
		}
	}

	void OpponentDetailCall (SeralizedClassServer.OpponentDetails callback)
	{
		SportsQuizManager.Instance.opponentInformation.serverID = callback.user_info.user_id;
		SportsQuizManager.Instance.opponentInformation.name = callback.user_info.name;
		SportsQuizManager.Instance.opponentInformation.age = callback.user_info.age;
		SportsQuizManager.Instance.opponentInformation.avatarType = callback.user_info.avatar_type;
		SportsQuizManager.Instance.opponentInformation.country = callback.user_info.country;
		SportsQuizManager.Instance.opponentInformation.email = callback.user_info.email_id;
		SportsQuizManager.Instance.opponentInformation.facebook_id = callback.user_info.facebook_id;
		SportsQuizManager.Instance.opponentInformation.gender = callback.user_info.gender;
		SportsQuizManager.Instance.opponentInformation.isOnline = true;

		SportsQuizManager.Instance.selectedDuelFriendDetail = new SportsQuizManager.InviteFriendDetail ();
		SportsQuizManager.Instance.selectedDuelFriendDetail.name = callback.user_info.name;
		SportsQuizManager.Instance.selectedDuelFriendDetail.userType = SportsQuizManager.eUserType.EmailUser;
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.EmailUser;
		SportsQuizManager.Instance.selectedDuelFriendDetail.user_id = callback.user_info.user_id;

		QuizManager.Instance.LoadQuizData(m_dualCallback);

		RemoveNotification ();
	}

	void OponentCancelCallback (SeralizedClassServer.OpponentDetails callback)
	{
		string message = "";
		if (string.IsNullOrEmpty (callback.user_info.name) == false) {

			message = m_notificationData.category_title
				+ "\n" + callback.user_info.name + "\n has cancelled the challenge.";
		} else {
			message = m_notificationData.category_title
				+ "\n" + "This challenge has been cancelled.";
		}

		MainMenuPanelManager.Instance.ShowMessage (message);
		RemoveNotification ();
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	void ChallengeRejectCallback (bool callback)
	{
		if (callback == true) {
			RemoveNotification ();
		}
	}

	void RemoveNotification ()
	{
		for (int i = 0; i < SportsQuizManager.Instance.notification.content.Count; i++) {
			if (SportsQuizManager.Instance.notification.content[i].notification_type == (int)m_notificatioType &&
				SportsQuizManager.Instance.notification.content[i].notification_id == m_notification_ID) {
				SportsQuizManager.Instance.notification.content.RemoveAt (i);
				break;
			}
		}
		MainMenuPanelManager.Instance.mainMenuPanel.DecreaseNotification ();
	}
}

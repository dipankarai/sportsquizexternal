﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPanel : MonoBehaviour {

	[Header("INFO")]
	public GameObject infoPanel;
	public Text infoDescription;
	public Button infoOkButton;
	public Button cancelButton;

	private Image m_this_image;

	public enum eInfoState
	{
		FeelingLuckyMode,
		LeisureMode,
		DualMode,
		TournamentMode
	}


	private eInfoState m_currentState;

	void OnEnable () {
		if (m_this_image == null)
			m_this_image = GetComponent<Image> ();
		infoDescription.text = "";
		infoPanel.SetActive (false);
		m_this_image.enabled = false;
	}

	// Use this for initialization
	void Start () {
		if (m_this_image == null)
			m_this_image = GetComponent<Image> ();

		infoOkButton.onClick.AddListener (OnClickOkButton);
		cancelButton.onClick.AddListener (OnClickCancelButton);

		m_this_image.enabled = false;
	}

	public void PopUpInfo (eInfoState state)
	{
		m_currentState = state;

		switch (m_currentState)
		{
		case eInfoState.FeelingLuckyMode:
			infoDescription.text = TagConstant.PopUpMessages.Feeling_Lucy_Mode_Info;
			break;
		case eInfoState.LeisureMode:
			infoDescription.text = TagConstant.PopUpMessages.Leisure_Mode_Info;
			break;
		case eInfoState.DualMode:
			infoDescription.text = TagConstant.PopUpMessages.Duel_Mode_Info;
			break;
		case eInfoState.TournamentMode:
			infoDescription.text = TagConstant.PopUpMessages.Tournament_Mode_Info;
			break;
		}

		m_this_image.enabled = true;
		infoPanel.SetActive (true);
	}

	void OnClickOkButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		infoDescription.text = "";
		m_this_image.enabled = false;
		infoPanel.SetActive (false);

		if (m_currentState == eInfoState.LeisureMode) {
			SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Leisure;
			ConnectionHandler.Instance.GetCategory ();
		}
		else if (m_currentState == eInfoState.DualMode) {
			SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Dual;
			ConnectionHandler.Instance.GetCategory ();
		}
		else if (m_currentState == eInfoState.FeelingLuckyMode) {
			SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.IMFLucky;
			ConnectionHandler.Instance.GetCategory ();
		}
		else if (m_currentState == eInfoState.TournamentMode) {
			//SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Tournament;
		}

		MainMenuPanelManager.Instance.WaitingPanel (true);
	}

	void OnClickCancelButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		infoDescription.text = "";
		m_this_image.enabled = false;
		infoPanel.SetActive (false);
	}
}

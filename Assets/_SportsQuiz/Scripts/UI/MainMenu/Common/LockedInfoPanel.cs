﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LockedInfoPanel : MonoBehaviour {

	public GameObject infoPanel;
	public Button okButton;
	public Text infoText;
	private Image m_this_image;

	// Use this for initialization
	void Start () {
		if (m_this_image == null)
			m_this_image = GetComponent<Image> ();

		okButton.onClick.AddListener (OnClickOkButton);
		infoPanel.SetActive (false);
		m_this_image.enabled = false;

	}
	
	// Update is called once per frame
	void OnClickOkButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		infoPanel.SetActive (false);
		m_this_image.enabled = false;
	}

	public void ShowLockedInfo ()
	{
		infoText.text = TagConstant.PopUpMessages.Locked_Info;
		
		infoPanel.SetActive (true);
		m_this_image.enabled = true;
	}
}

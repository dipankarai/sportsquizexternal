﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class LockedButton : MonoBehaviour {

	private Button lockButton;
	[SerializeField] private Toggle toggle;
	[SerializeField] private Button button;
	[SerializeField] private Image lockImage;

	void Awake () {
		
		if (lockButton == null)
			lockButton = GetComponent<Button> ();
	}

	void OnEnable () {

		if (button != null)
			button.interactable = SportsQuizManager.Instance.loginType != SportsQuizManager.eUserType.GuestUser;
		if (toggle != null)
			toggle.interactable = SportsQuizManager.Instance.loginType != SportsQuizManager.eUserType.GuestUser;
		
		lockImage.gameObject.SetActive (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser);
		lockButton.interactable = SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser;
		
	}

	// Use this for initialization
	void Start () {
		lockButton.onClick.AddListener (OnClickLockButton);
	}
	
	void OnClickLockButton ()
	{
		MainMenuPanelManager.Instance.lockedInfoPanel.ShowLockedInfo ();
	}
}

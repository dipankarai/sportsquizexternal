﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class FollowersFriendList : FriendList {

	[SerializeField] private Text friendsNotPresentText;

	void OnEnable () {
		friendsNotPresentText.text = "";
		GetFriendList ();
	}

	void OnDisable () {
//		CancelInvoke ("GetFriendList");
	}

	// Use this for initialization
	void Start () {
	
	}

	private List<SeralizedClassServer.FollowerDetails> m_followers = new List<SeralizedClassServer.FollowerDetails> ();
	private List<FollowerItemPanel> m_followerItemList = new List<FollowerItemPanel> ();

	public override void GetFriendList ()
	{
		if (SportsQuizManager.Instance.playerInformation.followerList.Count == 0) {
			friendsNotPresentText.text = TagConstant.PopUpMessages.NOBODY_FOLLOWING;
			return;
		} 
		friendsNotPresentText.text = "";
		
		if (m_followers.Count != SportsQuizManager.Instance.playerInformation.followerList.Count) {

			for (int i = 0; i < SportsQuizManager.Instance.playerInformation.followerList.Count; i++) {
				if (CheckConatinInFollower (SportsQuizManager.Instance.playerInformation.followerList [i])) {
					m_followers.Add (SportsQuizManager.Instance.playerInformation.followerList [i]);
				}
			}
		}

		GenerateObjectInScrollContent ();
	}

	void GenerateObjectInScrollContent ()
	{
		for (int i = 0; i < m_followers.Count; i++) {
			if (CheckConatinInItem(m_followers[i])) {
				FollowerItemPanel _follower = Instantiate (itemPanel);
				_follower.InitFollowerItemPanel (m_followers [i], this);
				_follower.gameObject.RectTransformMakeChild (scrollRect.content.transform);

				m_followerItemList.Add (_follower);
			}
		}		
	}

	bool CheckConatinInFollower (SeralizedClassServer.FollowerDetails follower)
	{
		for (int i = 0; i < m_followers.Count; i++) {
			if (m_followers [i].user_id == follower.user_id)
				return false;
		}

		return true;
	}

	bool CheckConatinInItem (SeralizedClassServer.FollowerDetails follower)
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			if (m_followerItemList [i].profileId == (int)follower.user_id)
				return false;
		}

		return true;
	}

	SportsQuizManager.InviteFriendDetail GetUserDetail (int id)
	{
		for (int i = 0; i < m_followers.Count; i++) {
			if (m_followers [i].user_id == id)
			{
				SportsQuizManager.InviteFriendDetail m_follower = new SportsQuizManager.InviteFriendDetail ();
				m_follower.name = m_followers [i].name;
				m_follower.user_id = (object) m_followers[i].user_id;
				m_follower.userType = SportsQuizManager.eUserType.EmailUser;

				return m_follower;
			}
		}

		return null;
	}

	public override void OnClickFollowerSelected (object userID)
	{
		Debug.Log (userID);
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.EmailUser;
		selectFriendPanel.SelectedUser = GetUserDetail ((int)userID);
	}

	public override void RemoveSelectedFolower ()
	{
		selectFriendPanel.SelectedUser = null;
	}

	public override void OnGameobjectDisable ()
	{
		for (int i = 0; i < m_followerItemList.Count; i++) {
			Destroy (m_followerItemList [i].gameObject);
		}

		m_followerItemList.Clear ();
		m_followers.Clear ();
		Resources.UnloadUnusedAssets ();
	}
}

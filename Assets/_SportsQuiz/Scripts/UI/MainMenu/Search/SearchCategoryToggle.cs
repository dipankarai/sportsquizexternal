﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class SearchCategoryToggle : MonoBehaviour {

	public Toggle ToggleButon { get { return m_ToggleButton; } }
	[SerializeField] private Toggle m_ToggleButton;
	[SerializeField] private Text m_TitleText;
	[SerializeField] private Image m_badgeImage;
	[SerializeField] private GameObject m_loading;
	private int m_Toggle_id;
	private string m_Toggle_title;
	private int m_Toggle_type;
	private SearchCategory m_SearchCategory;

	void OnEnable () {
		if (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (m_Toggle_id) == null) {
			if (this.gameObject.activeInHierarchy) {
				StartCoroutine (LoadImage (m_Toggle_id));
			}
		} else {
			ViewCategoryImage (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (m_Toggle_id));
		}
	}

	void Start () {
		m_ToggleButton.onValueChanged.AddListener (OnValueChangedToogle);
	}

	public void InitSearchCategoryToggle (SearchCategory category, int id, string title, int type)
	{
		m_SearchCategory = category;

		m_Toggle_id = id;
		m_Toggle_title = title;
		m_Toggle_type = type;

		m_TitleText.text = m_Toggle_title;

		if (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (id) == null) {
			if (this.gameObject.activeInHierarchy) {
				StartCoroutine (LoadImage (id));
			}
		} else {
			ViewCategoryImage (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (id));
		}

		/*if (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (id) != null)
		{
			m_badgeImage.CreateImageSprite (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (id));
		}*/
	}

	IEnumerator LoadImage (int id)
	{
		if (SportsQuizManager.Instance.GetCategoryClass (id) != null) {
			
			WWW image = new WWW (SportsQuizManager.Instance.GetCategoryClass (id).image);
			yield return image;

			if (image.error == null) {
				ViewCategoryImage (image.texture);

				References.SaveImageToLocal (
					SportsQuizManager.Instance.GetCategoryClass (id).category_id,
					SportsQuizManager.Instance.GetCategoryClass (id).image,
					image.texture,
					References.eImageType.Category);
			} else {
				Q.Utils.QDebug.Log (image.error);
			}
		}

		yield return null;
	}

	void ViewCategoryImage (Texture2D texture)
	{
		m_loading.SetActive (false);
		m_badgeImage.color = Color.white;
		m_badgeImage.CreateImageSprite (texture);
	}
	
	void OnValueChangedToogle (bool isOn)
	{
		m_SearchCategory.searchPanel.SearchCategory (isOn, m_Toggle_id);
	}
}

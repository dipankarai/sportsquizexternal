﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class SearchPanel : MonoBehaviour {

	public ScrollRect searchScrollRect;
	public SearchCategory searchItem;
	public Animation animation;
    [SerializeField] private Button searchButton;
    [SerializeField] private LoadingImage m_LoadingImage;

	[HideInInspector]
	public List<int> m_categoryIDList = new List<int> ();
	public SearchedUserProfilePanel searchedUserProfilePanel;

	[HideInInspector]
	public string categoryID_String = "";

	private List<SearchCategory> m_searchItemList = new List<SearchCategory> ();
	private int m_intervelCount;

	void OnEnable ()
	{
		m_LoadingImage.gameObject.SetActive(true);
		searchedUserProfilePanel.gameObject.SetActive (false);
		Invoke ("Init", 0.5f);
	}

	void OnDisable ()
	{
		for (int i=0; i< m_searchItemList.Count;i ++) {
			Destroy(m_searchItemList[i].gameObject);
		}
		m_searchItemList.Clear();
		Resources.UnloadUnusedAssets ();
	}

	public void Init()
	{
		if (SportsQuizManager.Instance.currentCategory != null &&
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count > 0) {

			if (m_intervelCount > 5) {
				m_intervelCount = 0;
				SportsQuizManager.Instance.ResetCategoryData ();
				ServerManager.Instance.GetMainCategoryOnly(CategoryCallback);
			} else {
				m_intervelCount++;
				CategoryCallback (SportsQuizManager.Instance.currentCategory.categoryDictionaryList[0]);
			}
		} else {
			SportsQuizManager.Instance.ResetCategoryData ();
			ServerManager.Instance.GetMainCategoryOnly(CategoryCallback);
		}
	}

    // Use this for initialization
	void Start () {
        searchButton.onClick.AddListener(OnClickSearchButton);
    }

    void CategoryCallback(SeralizedClassServer.CategoryDictionary callback)
    {
		for (int i=0; i< m_searchItemList.Count;i ++) {
			Destroy(m_searchItemList[i].gameObject);
		}
		m_searchItemList.Clear();

        for (int i = 0; i < callback.categoryList.Count; i++)
        {
            SearchCategory item = GetLastCategory();
            if (item != null)
            {
                SearchCategoryToggle toggle = item.GetToggle();
                if (toggle != null)
                    InitCategoryData(callback.categoryList[i], item, toggle);
                else
                    InstantiateCategoryItem(callback.categoryList[i]);
            }
            else
            {
                InstantiateCategoryItem(callback.categoryList[i]);
            }
        }
		//StartCoroutine ("WaitForSeconds");
		m_LoadingImage.gameObject.SetActive(false);
    }

	IEnumerator WaitForSeconds ()
	{
		yield return new WaitForSeconds (1f);
	}

    void InstantiateCategoryItem (SeralizedClassServer.Category category)
    {
        SearchCategory categoryItem = (SearchCategory)Instantiate(searchItem);
		categoryItem.searchPanel = this;
        categoryItem.gameObject.RectTransformMakeChild(searchScrollRect.content.transform);
        InitCategoryData(category, categoryItem, categoryItem.GetToggle());

        m_searchItemList.Add(categoryItem);
    }

    void InitCategoryData (SeralizedClassServer.Category category, SearchCategory searchCategory, SearchCategoryToggle toggle)
    {
        searchCategory.InitCategory( toggle, category.category_id, category.category_title, category.type);
    }

    SearchCategory GetLastCategory ()
	{
        if (m_searchItemList.Count > 0)
        {
           return  m_searchItemList[m_searchItemList.Count - 1];
        }

        return null;
    }

	public void SearchCategory (bool canAdd, int cat_id)
	{
		if (canAdd == true) {
			if (m_categoryIDList.Contains (cat_id) == false) {
				m_categoryIDList.Add (cat_id);
			}
				
		} else {
			if (m_categoryIDList.Contains (cat_id) == true) {
				m_categoryIDList.Remove (cat_id);
			}
		}

		if (m_categoryIDList.Count > 0)
		{
			if (animation.isPlaying == false)
				animation.Play ();
		} else {
			animation.Stop ();
			searchButton.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);
		}
	}

	void OnClickSearchButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		animation.Stop ();
		searchButton.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);
		if (m_categoryIDList.Count == 0)
			return;

		categoryID_String = "";

		for (int i = 0; i < m_categoryIDList.Count; i++)
		{
			categoryID_String += m_categoryIDList [i].ToString ();

			if (i < m_categoryIDList.Count - 1)
				categoryID_String += ",";
		}

		ServerManager.Instance.SearchFollow (categoryID_String, SerachFollowCallback);
		m_LoadingImage.gameObject.SetActive (true);
	}

	void SerachFollowCallback(SeralizedClassServer.FollowSearchUserInfo followSearchUserInfo)
	{
		MainMenuPanelManager.Instance.WaitingPanel (true);
		if (followSearchUserInfo != null) {
			searchedUserProfilePanel.gameObject.SetActive (true);
			m_categoryIDList.Clear ();
			
			if (followSearchUserInfo.user_id != 0) {
				MainMenuPanelManager.Instance.mainMenuPanel.searchedUserProfilePanel.IsAlreadyFollowing (followSearchUserInfo.user_id);
				searchedUserProfilePanel.gameObject.SetActive (true);
				searchedUserProfilePanel.AssignDetails (followSearchUserInfo);
			} else {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_QUIZZERS);//, MainMenuPanelManager.eMainMenuState.MainMenuPanel);
			}
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_QUIZZERS);//("Unable to search Quizzer please try again later.");
			m_LoadingImage.gameObject.SetActive (false);

			for (int i = 0; i < m_searchItemList.Count; i++) {
				for (int j = 0; j < m_searchItemList [i].categoryToggle.Length; j++) {
					m_searchItemList [i].categoryToggle [j].ToggleButon.isOn = false;
				}
			}
		}
	}
}
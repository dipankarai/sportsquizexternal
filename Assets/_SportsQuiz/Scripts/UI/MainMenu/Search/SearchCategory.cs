﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SearchCategory : MonoBehaviour {

	public SearchCategoryToggle[] categoryToggle;
	[HideInInspector] public SearchPanel searchPanel;

	// Use this for initialization
    void Start () {
        
    }

    public void InitCategory(SearchCategoryToggle toggle, int id, string title, int type)
    {
        for (int i = 0; i < categoryToggle.Length; i++)
        {
            if (categoryToggle[i] == toggle)
            {
				categoryToggle [i].InitSearchCategoryToggle (this, id, title, type);
				categoryToggle[i].gameObject.SetActive(true);
            }
        }
    }
	
	public SearchCategoryToggle GetToggle ()
    {
        for (int i = 0; i < categoryToggle.Length; i++)
        {
            if (categoryToggle[i].gameObject.activeSelf == false)
                return categoryToggle[i];
        }

        return null;
    }
}

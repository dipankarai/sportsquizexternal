﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class MainMenuPanel : MonoBehaviour {

	public enum eSubMenuPanel
	{
		Mode,
		Profile,
		Search,
		Notfications,
		Settings
	}

	public eSubMenuPanel currentSubPanel;

	[Header("TOGGLE BUTTON")]
	public Toggle modeSelectionToggle;
	public Toggle profileToggle;
	public Toggle networkToggle;
	public Toggle notificationsToggle;
	public Toggle settingsToggle;
	[Header("TOGGLE PANEL")]
	public ModeSelectionPanel modeSelectionPanel;
	public UserProfilePanel profilePanel;
	public SearchPanel networkPanel;
	public NotificationPanel notificationsPanel;
	public SettingsPanel settingsPanel;
	public SearchedUserProfilePanel searchedUserProfilePanel;

    [Header("OTHERS")]
	public GameObject notificatioCountOBj;
    public Text notificationCount;
	public SportPreferencesPanel preferencePanel;
	public FollowersPanel followersPanel;
	public EditProfilePanel editProfilePanel;
	public int m_lastNotification = 0;
	private bool m_isAndroidback = false;
	private bool firstTimeLoad = true;

	void Awake () {
		preferencePanel.gameObject.SetActive (false);
		followersPanel.gameObject.SetActive (false);
		editProfilePanel.gameObject.SetActive (false);
	}

	void OnEnable () {

		//AUDIO
		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Main) == false)
			GenericAudioManager.PlayBG (GenericAudioManager.BGSounds.BG_Music_Main, GenericAudioManager.PlayMode.loop);

		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Gameplay) == true)
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Gameplay);

		SwitchTogglePanel (eSubMenuPanel.Mode);

		/*InitTryMode ();*/

        //InvokeRepeating("UpdateNotifiaction", 1f, 30f);
		if (firstTimeLoad == false) {
			Invoke ("CallAfterSeconds", 1.5f);
		}
		firstTimeLoad = false;
        //GoogleAdHandler.Instance.ShowBanner ();
    }

	void CallAfterSeconds ()
	{
		ConnectionHandler.Instance.GetUserInformation ();
	}

	void OnDisable()
	{
		DisableAllToggles ();
		MainMenuPanelManager.Instance.mainMenuPanel.modeSelectionToggle.isOn = true;
		NotificationHandler.Instance.callTime = 2f;
		notificationsPanel.ClearForMemory ();
		MainMenuPanelManager.Instance.mainMenuPanel.NewNotification (0);
	}

	void DisableAllToggles()
	{
		MainMenuPanelManager.Instance.mainMenuPanel.modeSelectionToggle.isOn = false;
		MainMenuPanelManager.Instance.mainMenuPanel.profileToggle.isOn = false;
		MainMenuPanelManager.Instance.mainMenuPanel.networkToggle.isOn = false;
		MainMenuPanelManager.Instance.mainMenuPanel.notificationsToggle.isOn = false;
		MainMenuPanelManager.Instance.mainMenuPanel.settingsToggle.isOn = false;
	}

	// Use this for initialization
	void Start () {
		modeSelectionToggle.onValueChanged.AddListener (OnClickModeSelectionToggle);
		profileToggle.onValueChanged.AddListener (OnClickProfileToggle);
		networkToggle.onValueChanged.AddListener (OnClickNetworkToggle);
		notificationsToggle.onValueChanged.AddListener (OnClickNotificationsToggle);
		settingsToggle.onValueChanged.AddListener (OnClickSettingsToggle);
    }

	public void MainToogle ()
	{
		m_isAndroidback = true;
		Q.Utils.QDebug.LogError ("MainToogle "+m_isAndroidback);
		modeSelectionToggle.isOn = true;
	}

	void OnClickModeSelectionToggle (bool isOn)
	{
		if (isOn) {
			PlayToogleSound ();
		}
		SwitchTogglePanel (eSubMenuPanel.Mode);
	}

	void OnClickProfileToggle (bool isOn)
	{
		if (isOn) {
			PlayToogleSound ();
		}
		SwitchTogglePanel (eSubMenuPanel.Profile);
	}

	void OnClickNotificationsToggle (bool isOn)
	{
		if (isOn) {
			PlayToogleSound ();
		}
		SwitchTogglePanel (eSubMenuPanel.Notfications);
	}

	void OnClickNetworkToggle (bool isOn)
	{
		if (isOn) {
			PlayToogleSound ();
		}
		SwitchTogglePanel (eSubMenuPanel.Search);
	}

	void OnClickSettingsToggle (bool isOn)
	{
		if (isOn) {
			PlayToogleSound ();
		}
		SwitchTogglePanel (eSubMenuPanel.Settings);
	}

	void PlayToogleSound ()
	{
		if (GenericAudioManager.instance != null && m_isAndroidback == false) {
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		}
		m_isAndroidback = false;
	}

	public void SwitchTogglePanel (eSubMenuPanel state)
	{
		currentSubPanel = state;

		modeSelectionPanel.gameObject.SetActive (state == eSubMenuPanel.Mode);
		profilePanel.gameObject.SetActive (state == eSubMenuPanel.Profile);
		networkPanel.gameObject.SetActive (state == eSubMenuPanel.Search);
		notificationsPanel.gameObject.SetActive (state == eSubMenuPanel.Notfications);
		settingsPanel.gameObject.SetActive (state == eSubMenuPanel.Settings);
	}

    /*void UpdateNotifiaction ()
    {
        Q.Utils.QDebug.Log("UpdateNotifiaction");
        if (SportsQuizManager.Instance.notification == null)
            return;

        if (m_lastNotification != SportsQuizManager.Instance.notification.last_notification_id)
        {
            //NEW NOTIFICATION....
            notificationCount.text = SportsQuizManager.Instance.notification.content.Count.ToString();
            notificationCount.gameObject.SetActive (true);
        } else
        {
            notificationCount.gameObject.SetActive(false);
        }

        ServerManager.Instance.GetNotification();
    }*/
		
	private int m_notificationCount = 0;
	public void NewNotification (int newCount)
	{
		m_notificationCount = newCount;
		if (m_notificationCount == 0) {
			notificatioCountOBj.SetActive (false);
//			notificationCount.gameObject.SetActive(false);
		} else {

			if (m_lastNotification != m_notificationCount && this.gameObject.activeSelf) {

				m_lastNotification = m_notificationCount;
				GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Match_Pending);
			}

			notificationCount.text = m_notificationCount.ToString ();
			notificatioCountOBj.SetActive (true);
		}
	}

	public void DecreaseNotification ()
	{
		m_notificationCount--;
		if (m_notificationCount < 0)
		{
			m_notificationCount = 0;
		}
		NewNotification (m_notificationCount);
	}
}

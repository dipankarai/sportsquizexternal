﻿using UnityEngine;
using System.Collections;

public class MainMenuPanelManager : MonoBehaviour
{
	public static MainMenuPanelManager Instance;

	public enum eMainMenuState
	{
		Unknown = -1,
		SplashScreen = 0,
		SignInUpPanel,
		MainMenuPanel,
		CategoryPanel,
		Gameplay,
		Ads,
	}

	public Canvas maincanvas;
    public eMainMenuState currentMainMenuState = eMainMenuState.SplashScreen;

	public AppUpdatesPanel appUpdatesPanel;
	public SplashScreen splashScreen;
	public MainMenuPanel mainMenuPanel;
	public PopUpMessage popUpMessage;
	public LoadingIndicator loadingIndicator;
	public CategoryPanel categoryPanel;
	public LockedInfoPanel lockedInfoPanel;
	public FacebookCommonHandler facebookCommonHandler;
	public QuitAppPanel quitAppPanel;
    public BadgeAchievedPopUp badgeAchievedPopUp;
	public QuizPanelManager quizPanelManager;
	public NotificationPopUp notificationPopUp;
	public FeedbackHandler feedbackPanel;
    public GameObject rateAppPanel;
	[HideInInspector]public ConnectionHandler p_ConnectionManager;
    private bool isFirstCheck = true;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else
			DestroyImmediate (this);

		if (p_ConnectionManager == null) {
			p_ConnectionManager = ConnectionHandler.Init(this.gameObject);
		}
	}
	
	// Use this for initialization
    void Start () {
		InitPanel ();
	}

	void InitPanel ()
	{
		if (quizPanelManager.gameObject.activeInHierarchy == false) {
			
			quizPanelManager.gameObject.SetActive (true);
		}
		splashScreen.gameObject.SetActive (true);
		mainMenuPanel.gameObject.SetActive (false);
		popUpMessage.gameObject.SetActive (false);
		categoryPanel.gameObject.SetActive (false);

		CheckMusicAndSounds ();

		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Main) == false)
			GenericAudioManager.PlayBG (GenericAudioManager.BGSounds.BG_Music_Main, GenericAudioManager.PlayMode.loop);

		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Gameplay) == true)
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Gameplay);
	}

	void CheckMusicAndSounds()
	{
		if (GamePrefs.HasKey (GamePrefs.bgKey)) {
			if (GamePrefs.isBgOn == 1) {
				GenericAudioManager.isBG_On = true;
				mainMenuPanel.settingsPanel.musicToggle.isOn = true;
			} else {
				GenericAudioManager.isBG_On = false;
				mainMenuPanel.settingsPanel.musicToggle.isOn = false;
			}
		} else {
			GenericAudioManager.isBG_On = true;
			GamePrefs.isBgOn = 1;
		}

		if (GamePrefs.HasKey (GamePrefs.FxKey)) {
			if (GamePrefs.isFxOn == 1) {
				GenericAudioManager.isFX_On = true;
				mainMenuPanel.settingsPanel.soundToggle.isOn = true;
			} else {
				GenericAudioManager.isFX_On = false;
				mainMenuPanel.settingsPanel.soundToggle.isOn = false;
			}
		} else {
			GenericAudioManager.isFX_On = true;
			GamePrefs.isFxOn = 1;
		}
	}

	public void SwitchPanel (eMainMenuState nextState)
	{
        if (currentMainMenuState == nextState)
            return;

		Q.Utils.QDebug.Log (currentMainMenuState+" :: "+nextState);
		if (nextState == eMainMenuState.Gameplay) {
			GetPanelObject (currentMainMenuState).SetActive (false);
		} else if (currentMainMenuState == eMainMenuState.Gameplay) {
			GetPanelObject (nextState).SetActive (true);
		} else {
			GetPanelObject (currentMainMenuState).SetActive (false);
			GetPanelObject (nextState).SetActive (true);
		}

		currentMainMenuState = nextState;
	}

	public void ShowMessage (string msg, eMainMenuState nextstate = eMainMenuState.Unknown, 
		PopUpMessage.eMessageType msgType = PopUpMessage.eMessageType.Normal)
	{		
		if (nextstate == MainMenuPanelManager.eMainMenuState.Ads) {
			GoogleAdHandler.Instance.FullScreenAds ();
			nextstate = eMainMenuState.Unknown;
		}

		popUpMessage.PopUpPanel (msg, nextstate, msgType);
		popUpMessage.gameObject.SetActive (true);
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	public void ShowMessage (string msg, System.Action callback, PopUpMessage.eMessageType msgType = PopUpMessage.eMessageType.Normal)
	{
		popUpMessage.PopUpPanel (msg, eMainMenuState.Unknown, msgType);
		popUpMessage.PopUpPanel (msg, callback, msgType);
		popUpMessage.gameObject.SetActive (true);
	}

	public void WaitingPanel (bool show)
	{
		loadingIndicator.gameObject.SetActive (show);
	}

	public void BadgeAchieved (SeralizedClassServer.UserBadges badge, System.Action<SeralizedClassServer.UserBadges> callback)
    {
        badgeAchievedPopUp.InitBadgeAchieved(badge, callback);
        badgeAchievedPopUp.gameObject.SetActive(true);
    }

	public GameObject GetPanelObject (eMainMenuState state)
	{
		switch (state)
		{
		case eMainMenuState.MainMenuPanel:
			return mainMenuPanel.gameObject;
			break;

		case eMainMenuState.SplashScreen:
		case eMainMenuState.SignInUpPanel:
			return splashScreen.gameObject;
			break;

		case eMainMenuState.CategoryPanel:
			return categoryPanel.gameObject;
			break;

		default: return null;
			break;
		}
		
		return null;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class ModeSelectionPanel : MonoBehaviour {

	[Header("MODE BUTTON")]
	public Button fellingLuckyModeButton;
	public Button leisureModeButton;
	public Button dualModeButton;
	public Button tournamentModeButton;

	private bool isFirstcall = false;
	/*public InfoPanel infoPanel;*/

	void OnEnable () {
		if (isFirstcall == false) {
			isFirstcall = true;
			ShowAds ();
		} else {
			Invoke ("ShowAds", 2f);
		}
	}

	void ShowAds () {
		GoogleAdHandler.Instance.ShowBanner ();
	}

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
	}

	// Use this for initialization
	void Start () {
		fellingLuckyModeButton.onClick.AddListener (OnClickFellingMode);
		leisureModeButton.onClick.AddListener (OnClickLeisureMode);
		dualModeButton.onClick.AddListener (OnClickDuelMode);
		tournamentModeButton.onClick.AddListener (OnClickTournamentMode);

	}

	/*void InitTryMode ()
	{
		TransformUtils.GetLockImage (fellingLuckyModeButton.gameObject).enabled = (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser);
		TransformUtils.GetLockImage (dualModeButton.gameObject).enabled = (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser);
		TransformUtils.GetLockImage (tournamentModeButton.gameObject).enabled = (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser);
	}*/

	void OnClickFellingMode()
	{ 
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.IMFLucky;
		CommonInit ();
	}

	void OnClickLeisureMode () 
	{ 
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Leisure;
		CommonInit ();
	}


	void OnClickTournamentMode ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		//SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Tournament;
		MainMenuPanelManager.Instance.ShowMessage("Tournament mode is coming soon...");
//		CommonInit ();
	}

	void OnClickDuelMode ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		SportsQuizManager.Instance.selectedMode = QuizManager.eQuizMode.Dual;
		CommonInit ();
	}

	void CommonInit ()
	{

		MainMenuPanelManager.Instance.WaitingPanel (true);
		SportsQuizManager.Instance.ResetCategoryData ();
		ConnectionHandler.Instance.GetCategory ();
		//SPONSERED CATEGORY
		SponseredQuiz.Instance.sponseredCategoryCategory = null;
		ServerManager.Instance.GetSponsoredCategory (SponseredQuiz.Instance.UpdateSponsoredQuiz);
	}

	void OnClickBackButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
	}
}

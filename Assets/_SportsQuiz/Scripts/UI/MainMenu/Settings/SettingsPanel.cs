﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Facebook.Unity;
using System;

public class SettingsPanel : MonoBehaviour {

	//[Header("Buttons")]
	public Toggle soundToggle;
	public Toggle musicToggle;
	public Button editProfileButton;
	public Button rateGameButton;
	public Button creditsButton;
	public GameObject creditsPanel;
	public Button creditsCloseButton;
	public Button signOutButton;
	public Button inviteFriendsButton;
	public Button feedbackButton;
	public Button googlePlusShareButton;

	void OnEnable()
	{
		GoogleAdHandler.Instance.ShowBanner ();
	}

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
		Resources.UnloadUnusedAssets ();
	}


	// Use this for initialization
	void Start () {
		soundToggle.onValueChanged.AddListener (OnValueChangedSoundToggle);
		musicToggle.onValueChanged.AddListener (OnValueChangedMusicToggle);

		editProfileButton.onClick.AddListener (OnClickEditProfileButton);
		rateGameButton.onClick.AddListener (OnClickRateGameButton);
		creditsButton.onClick.AddListener (OnClickCreditsButton);
		creditsCloseButton.onClick.AddListener (OnClickCreditsCloseButton);

		signOutButton.onClick.AddListener (OnClickSignOut);
		inviteFriendsButton.onClick.AddListener (OnClickInviteFriendsButton);
		feedbackButton.onClick.AddListener (OnClickFeedbackButton);
		googlePlusShareButton.onClick.AddListener (OnClickGooglePlusShareButton);
	}

	void OnClickGooglePlusShareButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (InternetConnection.Check) {
			SocialHandler.Instance.GoogleShare ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void OnClickFeedbackButton()
	{
		//Application.OpenURL ("https://docs.google.com/forms/d/e/1FAIpQLSddutUHkNadnCVoXdS7hqn5r246Ig2ovN9r6PYE_tYo-OTz6w/viewform?c=0&w=1&usp=mail_form_link");
		Application.OpenURL("https://docs.google.com/forms/d/e/1FAIpQLSddutUHkNadnCVoXdS7hqn5r246Ig2ovN9r6PYE_tYo-OTz6w/viewform?c=0&w=1");
//		MainMenuPanelManager.Instance.feedbackPanel.SetActive (true);
	}

	void OnClickSignIn ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.SplashScreen);
	}

	void OnClickSignOut ()
	{
		//GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.ClearFields ();
		MainMenuPanelManager.Instance.mainMenuPanel.editProfilePanel.ClearUserData ();
		SportsQuizManager.Instance.ClearUserInfo ();
		SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.GuestUser;
		GamePrefs.DeleteLoginDetails ();
        SocialHandler.Instance.FacebookLogout();
        SocialHandler.Instance.GoogleSignOut();
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.SplashScreen);
		ConnectionHandler.Instance.ClearPreviousData ();
	}
	
	void OnValueChangedSoundToggle (bool isOn)
	{
		if (isOn) {
			GenericAudioManager.isFX_On = true;
			GamePrefs.isFxOn = 1;
		} else {
			GenericAudioManager.isFX_On = false;
			GamePrefs.isFxOn = 0;
		}
	}

	void OnValueChangedMusicToggle (bool isOn)
	{
		if (isOn) {
			GenericAudioManager.isBG_On = true;
			GenericAudioManager.PlayBG (GenericAudioManager.BGSounds.BG_Music_Main, GenericAudioManager.PlayMode.loop);
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Gameplay);
			GamePrefs.isBgOn = 1;
		}else {
			GenericAudioManager.isBG_On = false;
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Main);
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Gameplay);
			GamePrefs.isBgOn = 0;
		}
	}

	void OnClickRateGameButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.rateAppPanel.SetActive (true);
	}

	void OnClickCreditsButton ()
	{
		creditsPanel.SetActive (true);
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
	}

	void OnClickCreditsCloseButton()
	{
		creditsPanel.SetActive (false);
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
	}

	void OnClickEditProfileButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.mainMenuPanel.editProfilePanel.gameObject.SetActive (true);
	}

	void OnClickInviteFriendsButton()
	{
		StartCoroutine(InviteFriend ());
	}

	IEnumerator InviteFriend()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		yield return new WaitForSeconds (0.1f);
		#if UNITY_ANDROID
		FB.Mobile.AppInvite(new Uri("https://fb.me/892708710750483"), callback: MainMenuPanelManager.Instance.facebookCommonHandler.HandleResult);

		#elif UNITY_IOS || UNITY_IPHONE
		FB.Mobile.AppInvite(new Uri("https://fb.me/810530068992919"), callback: MainMenuPanelManager.Instance.facebookCommonHandler.HandleResult);
		#endif
	}
}
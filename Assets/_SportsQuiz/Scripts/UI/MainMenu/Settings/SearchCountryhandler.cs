﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SearchCountryhandler : MonoBehaviour {

	public GameObject searchPanel;
	public Button dropdownArrowButton;
	public InputField searchInput;
	public GameObject cloneObj;
	public GameObject contentPanel;
	public Text dropDownLabel;

	[HideInInspector]
	public List<GameObject> listOfObjects = new List<GameObject>();
	[HideInInspector]
	public int selectedIndex = 99;

	public enum SearchPanelState
	{
		Closed = 0,
		Opened,
	}

	private SearchPanelState searchPanelState;

	void Start () {
		searchPanel.SetActive (false);
		dropdownArrowButton.onClick.AddListener (SearchPanelView);
		searchInput.onValueChanged.AddListener (MainMenuPanelManager.Instance.mainMenuPanel.editProfilePanel.SearchCountryByName);
	}

	void SearchPanelView()
	{
		if (searchPanelState == SearchPanelState.Closed) {
			searchPanel.SetActive (true);
			searchPanelState = SearchPanelState.Opened;
		} else {
			searchPanel.SetActive (false);
			searchPanelState = SearchPanelState.Closed;
		}
		searchInput.Select ();
	}

	public void GenerateCountryList(List<string> countryList)
	{
		for (int i = 0; i < listOfObjects.Count; i++) {
			Destroy (listOfObjects [i]);
		}
		listOfObjects.Clear ();

		for (int i = 0; i < countryList.Count; i++) {
			GameObject countryItem = Instantiate (cloneObj);
			countryItem.SetActive (true);
			countryItem.transform.SetParent (contentPanel.transform, false);
			listOfObjects.Add (countryItem);
			CountryItemHandler countryItemHandler = countryItem.GetComponent<CountryItemHandler> ();
			countryItemHandler.countryText.text = countryList [i];
			countryItemHandler.indexVal = i;
		}
	}

	public void AssignCountryValue(int index)
	{
		selectedIndex = index;
		dropDownLabel.text = listOfObjects [index].GetComponent<CountryItemHandler> ().countryText.text;
	}

	public void ResetFields()
	{
		searchPanel.SetActive (false);
		searchInput.text = "";
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class CountryItemHandler : MonoBehaviour {
	public SearchCountryhandler searchCountryhandler;
	public Button countryButton;
	public Text countryText;
	[HideInInspector]
	public int indexVal = 0;

	void Start()
	{
		countryButton.onClick.AddListener (AssignValueInDropDown);
	}

	void AssignValueInDropDown()
	{
		searchCountryhandler.dropDownLabel.text = countryText.text;
		for (int i = 0; i < Country.copyOfCountryCodeList.Count; i++) {
			if (Country.copyOfCountryCodeList [i].country_name.Equals (countryText.text)) {
				searchCountryhandler.selectedIndex = i;
			}
		}

		searchCountryhandler.searchPanel.SetActive (false);
	}
}
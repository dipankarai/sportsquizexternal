﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class EditProfilePanel : MonoBehaviour {

	public Image profileImage;
	public Button profileButton;
	public Button doneButton;
	public InputField usernameInputField;
	public InputField ageInputField;
	public Dropdown genderDropdown;
//	public Dropdown countryDropdown;
	public AvatarPanel avatarPanel;
	public SearchCountryhandler searchCountryhandler;

//	[HideInInspector]
//	public InputField searchCountry;

	private string m_Username;
	private SportsQuizManager.eGender m_Gender;
	private int m_Age;
	private int m_Country;
	private int avatarId = 0;

	private List<Country.CountryCode> countryCodesList = new List<Country.CountryCode>();
	private int countryId;

    void OnEnable ()
    {
		if (countryCodesList.Count == 0) {
			AssignValueToCountryDropDown ();
		}
        AssignUserData();
    }

	// Use this for initialization
	void Start () {

		avatarPanel.gameObject.SetActive (false);
		avatarPanel.editProfilePanel = this;

		doneButton.onClick.AddListener (OnClickDoneButton);
		usernameInputField.onEndEdit.AddListener (OnEndEditUsernameField);
		ageInputField.onEndEdit.AddListener (OnEndEditAgeField);
		genderDropdown.onValueChanged.AddListener (OnValueChangedGenderDropDown);
		profileButton.onClick.AddListener (OnClickProfileButton);
//		searchCountry.onValueChanged.AddListener (SearchCountryByName);
	}

	public void SearchCountryByName(string countryStr)
	{
		if (countryStr.Length > 0) {
			AssignValueToCountryDropDown (countryStr);
		}
		else if (countryStr.Length == 0) {
			AssignValueToCountryDropDown (null);
		}
	}

	void OnClickProfileButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		avatarPanel.gameObject.SetActive (true);
	}

	void OnEndEditUsernameField (string text)
	{
//		if (string.IsNullOrEmpty (usernameInputField.text)) {
//			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.USERNAME_FIELD_EMPTY, UsernameValidationCallback);
//		}  else 
//		{
			m_Username = text;
//		}
	}

	void OnEndEditAgeField (string text)
	{
		if (!string.IsNullOrEmpty (text)) {

//			if (int.Parse (ageInputField.text) < 12) {
//				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.AGE_INVALID, AgeValidateCallback);
//			} else {
				m_Age = int.Parse (text);
//			}
		} else {
			#if UNITY_ANDROID
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.AGE_FIELD_EMPTY, AgeValidateCallback);
			#endif
		}
	}

	void AgeValidateCallback ()
	{
		ageInputField.ActivateInputField ();
	}

	void OnValueChangedGenderDropDown (int value) {
		
		m_Gender = (SportsQuizManager.eGender)value;
//		if ((SportsQuizManager.eGender)genderDropdown.value == SportsQuizManager.eGender.Gender &&
//		    this.gameObject.activeInHierarchy == true) {
//			Debug.LogError ((SportsQuizManager.eGender)genderDropdown.value);
//			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.INVALID_GENDER);
//		} else {
//		}
	}

	void OnClickDoneButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		searchCountryhandler.ResetFields();

		if (string.IsNullOrEmpty (usernameInputField.text)) {
			MainMenuPanelManager.Instance.ShowMessage ("Please Enter User Name");
		} 
//		else if (int.Parse (ageInputField.text) < 12) {
//			MainMenuPanelManager.Instance.ShowMessage ("Please Enter age");
//		} else if (genderDropdown.value == 0) {
//			MainMenuPanelManager.Instance.ShowMessage ("Please Select Gender");
//		} 
		else {
			UpdatePlayerInformation ();
		}
	}

	public void ChangedAvatar (Texture2D texture, int avatarID)
	{
		profileImage.CreateImageSprite (texture);
		avatarId = avatarID;
	}

	void userInfoUpdateSuccess(SeralizedClassServer.UserDetails userDetails)
	{
		Debug.Log ("User Info updated Successfully "+userDetails);		
		MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.UpdateUserdata ();
	}
	
	void OnDisable ()
	{
		avatarPanel.gameObject.SetActive (false);
		ClearUserData ();
	}

	public void AssignUserData()
	{
			if (SportsQuizManager.Instance.playerInformation.name != null) {
				usernameInputField.text = SportsQuizManager.Instance.playerInformation.name;
			} else {
				usernameInputField.text = "";
			}

			m_Gender = SportsQuizManager.Instance.playerInformation.gender;
			genderDropdown.value = (int)SportsQuizManager.Instance.playerInformation.gender;

			avatarId = SportsQuizManager.Instance.playerInformation.avatarType;

        SportsQuizManager.Instance.AssignUserProfileImage(profileImage);

        if (SportsQuizManager.Instance.playerInformation.country != 0)
			{
				m_Country = SportsQuizManager.Instance.playerInformation.country;				
			}
			else
			{
				m_Country = 356;
			}
			searchCountryhandler.AssignCountryValue(AssignCountryInDropDown(m_Country));
			ageInputField.text = SportsQuizManager.Instance.playerInformation.age.ToString();
	}

	public void UpdatePlayerInformation()
	{
		bool isUpdateRequire = false;

		if (!SportsQuizManager.Instance.playerInformation.name.Equals (usernameInputField.text)) {
			isUpdateRequire = true;
		}
		if (!SportsQuizManager.Instance.playerInformation.country.Equals (GetSelectedCountryID ())) {
			isUpdateRequire = true;
		}
		if (!SportsQuizManager.Instance.playerInformation.gender.Equals ((SportsQuizManager.eGender)genderDropdown.value)) {
			isUpdateRequire = true;
		}
		if (!SportsQuizManager.Instance.playerInformation.avatarType.Equals (avatarId)) {
			isUpdateRequire = true;
		}
		if (!string.IsNullOrEmpty (ageInputField.text)) {
			if (!SportsQuizManager.Instance.playerInformation.age.Equals (System.Int32.Parse (ageInputField.text))) {
				isUpdateRequire = true;
			}
		} else {
			isUpdateRequire = true;
			ageInputField.text = "0";
			#if UNITY_ANDROID && !UNITY_IPHONE
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.AGE_INVALID, AgeValidateCallback);
			#elif UNITY_IPHONE
			this.gameObject.SetActive (false);
			#endif
		}

		if (isUpdateRequire) {
			SportsQuizManager.Instance.playerInformation.country = GetSelectedCountryID ();
			SportsQuizManager.Instance.playerInformation.avatarType = avatarId;
			if (!string.IsNullOrEmpty (usernameInputField.text)) {
				if ((SportsQuizManager.eGender)genderDropdown.value != SportsQuizManager.eGender.Gender) {
					if (int.Parse (ageInputField.text) >= 12 && int.Parse (ageInputField.text) <= 99) {
						SportsQuizManager.Instance.playerInformation.gender = (SportsQuizManager.eGender)genderDropdown.value;
						SportsQuizManager.Instance.playerInformation.name = usernameInputField.text;
						SportsQuizManager.Instance.playerInformation.age = int.Parse (ageInputField.text);
						ServerManager.Instance.UpdateUserInfoToServer (userInfoUpdateSuccess);
						MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.UpdateUserInfo ();
						this.gameObject.SetActive (false);
					} else {
						#if UNITY_ANDROID && !UNITY_IPHONE
						MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.AGE_INVALID, AgeValidateCallback);
						#elif UNITY_IPHONE
						this.gameObject.SetActive (false);
						#endif
					}
				} else {
					#if UNITY_ANDROID && !UNITY_IPHONE
					MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.INVALID_GENDER);
					#elif UNITY_IPHONE
					this.gameObject.SetActive (false);
					#endif
				}
			} else {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.USERNAME_FIELD_EMPTY, UsernameValidationCallback);
			}
		} else {
			this.gameObject.SetActive (false);
		}

	}

	void UsernameValidationCallback ()
	{
		usernameInputField.ActivateInputField ();
	}

	void AssignValueToCountryDropDown(string str = null)
	{
		List<string> countryList = new List<string>();
		if (str == null) {
			countryCodesList.Clear ();
			countryCodesList = Country.GetAllCountry ();
		} else {
			countryCodesList.Clear ();
			countryCodesList = Country.GetSpecificCountryList (str);
		}

		for (int i = 0; i < countryCodesList.Count; i++) {
			countryList.Add (countryCodesList [i].country_name);
		}
		searchCountryhandler.GenerateCountryList(countryList);
	}

	public int GetSelectedCountryID()
	{		 
		int selectedDropDownIndex = searchCountryhandler.selectedIndex;
		countryId = countryCodesList [selectedDropDownIndex].iso_numeric_code;
		return countryId;
	}

	public int AssignCountryInDropDown(int countryIsoCode)
	{
		for (int index = 0; index < countryCodesList.Count; index++) {
			if (countryCodesList [index].iso_numeric_code == countryIsoCode) {
				return index;
			}
		}
		return 0;
	}

	public void ClearUserData()
	{
		usernameInputField.text = "";
		ageInputField.text = "";
		genderDropdown.value = 0;
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DoYouLikeGame : MonoBehaviour 
{
	public Button yesButton;
	public Button noButton;

	void Start()
	{
		yesButton.onClick.AddListener (OnClickYesButton);
		noButton.onClick.AddListener (OnClickNoThanksButton);
	}

	void OnClickYesButton()
	{
		MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.PLEASE_RATE_APP, RateApp);
		this.gameObject.SetActive (false);
	}

	void OnClickNoThanksButton()
	{
		this.gameObject.SetActive (false);
		MainMenuPanelManager.Instance.feedbackPanel.gameObject.SetActive (true);
	}

	void RateApp ()
	{
		#if UNITY_ANDROID
		Application.OpenURL(TagConstant.AppStoreLink.APP_STORE_LINK_ANDROID);
		#elif UNITY_IPHONE
		Application.OpenURL(TagConstant.AppStoreLink.APP_STORE_LINK_iOS);
		#endif
		GamePrefs.RateTheApp = 1;
	}
}

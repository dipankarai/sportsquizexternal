﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FeedbackHandler : MonoBehaviour
{
	public Text header;
	public Button submitButton;
	public InputField contentText;
	public Button cancelButton;

	private string m_reportMistakeMessage;
	private bool m_isReportMistake;

	void Start()
	{
		submitButton.onClick.AddListener (OnClickSubmitButton);
		cancelButton.onClick.AddListener (OnClickCancelButton);
	}

	void OnDisable () {
		contentText.text = "";
		m_reportMistakeMessage = "";
		m_isReportMistake = false;
		header.text = TagConstant.PopUpMessages.FEEDBACK_HEADER;
	}

	void OnClickSubmitButton()
	{
		if (string.IsNullOrEmpty (contentText.text) == true) {
			if (m_isReportMistake) {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.REPORT_EMPTY);
			} else {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.FEEDBACK_EMPTY);
			}
		} else {
			string _message = contentText.text;
			if (m_isReportMistake) {
				_message = m_reportMistakeMessage +
				"<p>Message: " +
				contentText.text
				+ "</p>";

				Q.Utils.QDebug.Log (_message);
			}

			ServerManager.Instance.FeedbackSave (_message, feedbackSaveCallback);
			this.gameObject.SetActive (false);
		}
	}

	void feedbackSaveCallback(bool isSaved)
	{
		string _messageCallback = "";
		if (m_isReportMistake) {
			if (isSaved) {
				_messageCallback = TagConstant.PopUpMessages.REPORT_SEND;
			} else {
				_messageCallback = TagConstant.PopUpMessages.REPORT_NOT_SEND;
			}
		} else {
			if (isSaved) {
				_messageCallback = TagConstant.PopUpMessages.FEEDBACK_SEND;
			} else {
				_messageCallback = TagConstant.PopUpMessages.FEEDBACK_NOT_SEND;
			}
		}

		MainMenuPanelManager.Instance.ShowMessage (_messageCallback, MainMenuPanelManager.eMainMenuState.Ads);
	}

	public void ReportMistake ()
	{
		m_isReportMistake = true;

		header.text = TagConstant.PopUpMessages.REPORT_HEADER;

		string _subject = "Subject: Report a Mistake";

		string _categorySelected = "";

		for (int i = 0; i < SportsQuizManager.Instance.currentSelectedCategory.Count; i++) {
			string _temp = "<br>ID: " + SportsQuizManager.Instance.currentSelectedCategory[i].category_id.ToString ();
			if (i == 0) {
				_categorySelected  = _temp + " Sports: " +SportsQuizManager.Instance.currentSelectedCategory[i].category_title
					+ "</br>";
			} else {
				_categorySelected += _temp + " Category: " +SportsQuizManager.Instance.currentSelectedCategory[i].category_title
					+ "</br>";
			}
		}

		string _topicSelected = "<br>ID: " +
		                        SportsQuizManager.Instance.currentSelectedTopics.category_id.ToString () +
		                        " Topic: " +
		                        SportsQuizManager.Instance.currentSelectedTopics.category_title
		                        + "</br>";

		m_reportMistakeMessage = _subject +
		_categorySelected +
		_topicSelected;

		this.gameObject.SetActive (true);
	}

	void OnClickCancelButton()
	{
		this.gameObject.SetActive (false);
		contentText.text = "";
		GoogleAdHandler.Instance.FullScreenAds ();
	}
}
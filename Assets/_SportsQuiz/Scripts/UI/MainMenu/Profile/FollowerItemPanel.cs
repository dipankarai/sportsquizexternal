﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class FollowerItemPanel : MonoBehaviour {

	public int profileId;
	public string facebook_id;
	public int avatar_type = 0;
	public Image profileImage;
	public Text usernameText;
	public Button unfollowButton;
	public Toggle inviteToggle;


	[HideInInspector]
	public FollowersPanel m_followersPanel;
	private FriendList m_friendList;
	private bool isCoroutineFinished;

	void OnEnable () {
		if (isCoroutineFinished == false && profileId > 0) {
			StartCoroutine ("LoadImage");
		}
	}

	// Use this for initialization
	void Start () {

		if (unfollowButton != null)
			unfollowButton.onClick.AddListener (OnClickUnFollowButton);

		if (inviteToggle != null)
			inviteToggle.onValueChanged.AddListener (OnValueChangedToggle);
	}

	public void InitFollowerItemPanel (SeralizedClassServer.FollowerDetails followersDetails, object _followersPanel) {

		profileId = followersDetails.user_id;
		facebook_id = followersDetails.facebook_id;
		avatar_type = followersDetails.avatar_type;
		usernameText.text = followersDetails.name;
		if (_followersPanel != null) {

			if (_followersPanel is FollowersPanel)
				m_followersPanel = (FollowersPanel)_followersPanel;

			if (_followersPanel is FriendList)
				m_friendList = (FriendList)_followersPanel;
		}

		if (avatar_type == 0) {
			if (this.gameObject.activeInHierarchy) {
				StartCoroutine ("LoadImage");
			}
		} else {
			profileImage.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (avatar_type));
		}
	}

	IEnumerator LoadImage ()
	{
		if (SportsQuizManager.IsNullOrEmptyOrZero (facebook_id)) {
			profileImage.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (avatar_type));
		} else {
			string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (facebook_id);
			if (string.IsNullOrEmpty (_facebookImageURL)) {
				profileImage.CreateImageSprite (
					SportsQuizManager.Instance.references.GetAvatarImage (avatar_type));
			} else {

				WWW image = new WWW (_facebookImageURL);
				yield return image;

				if (image.error == null) {
					profileImage.CreateImageSprite (image.texture);
				} else {
					profileImage.CreateImageSprite (
						SportsQuizManager.Instance.references.GetAvatarImage (avatar_type));
				}
			}
		}

		yield return 0;
		isCoroutineFinished = true;
	}

	void OpponentDetailCallback (SeralizedClassServer.OpponentDetails callback)
	{
		if (callback != null)
		{
			SportsQuizManager.Instance.AssignOpponentProfileImage (profileImage, callback.user_info.user_id);
		}
	}

	public void InitFacebookFriendItemPanel (string id, string username, string url, object _followersPanel) {

		facebook_id = id;
		usernameText.text = username;
		if (_followersPanel != null) {

			if (_followersPanel is FollowersPanel)
				m_followersPanel = (FollowersPanel)_followersPanel;

			if (_followersPanel is FriendList)
				m_friendList = (FriendList)_followersPanel;
		}

		//LoadImageFromURL (url, profileImage, username, facebookID);
		if (this.gameObject.activeInHierarchy) {
			StartCoroutine ("LoadFacebookProfileImage", url);
		}
	}

	public void InitGoogleFriendItemPanel (string id, string username, Texture2D image, object _followersPanel) {

		facebook_id = id;
		usernameText.text = username;
		if (_followersPanel != null) {

			if (_followersPanel is FollowersPanel)
				m_followersPanel = (FollowersPanel)_followersPanel;

			if (_followersPanel is FriendList)
				m_friendList = (FriendList)_followersPanel;
		}

		profileImage.CreateImageSprite (image);
	}

	/*public void LoadImageFromURL (string url, Image image, string name, string fbID)
	{
		for (int i = 0; i < SportsQuizManager.Instance.opponentList.Count; i++) {
			if (SportsQuizManager.Instance.opponentList [i].facebook_id == fbID) {
				
				SportsQuizManager.Instance.opponentList [i].profileUrl = url;
				SportsQuizManager.Instance.opponentList [i].name = name;
				
				if (SportsQuizManager.Instance.opponentList [i].userProfile != null) {
					image.CreateImageSprite (SportsQuizManager.Instance.opponentList [i].userProfile);
					
					return;
				} else {
					SportsQuizManager.Instance.opponentList [i].profileImage = image;
					
					StartCoroutine ("LoadFacebookProfileImage", SportsQuizManager.Instance.opponentList[i]);
				}
			}
		}
		
		SportsQuizManager.OpponentInfo _userDetail = new SportsQuizManager.OpponentInfo ();
		_userDetail.facebook_id = fbID;
		_userDetail.profileUrl = url;
		_userDetail.name = name;
		_userDetail.profileImage = image;
		
		SportsQuizManager.Instance.opponentList.Add (_userDetail);
		
		StartCoroutine ("LoadFacebookProfileImage", _userDetail);
	}*/

	/*IEnumerator LoadFacebookProfileImage (SportsQuizManager.OpponentInfo userDetails)
	{
		WWW image = new WWW (userDetails.profileUrl);
		yield return image;

		Q.Utils.QDebug.Log ("LoadFacebookProfileImage "+image.error);

		if (image.error == null) {

//			userDetails.userProfile = image.texture;
//			userDetails.profileImage.CreateImageSprite (userDetails.userProfile);
			userDetails.profileImage.CreateImageSprite (profileImage);

		} else {
			Q.Utils.QDebug.Log (image.error);
		}
		Q.Utils.QDebug.Log ("LoadFacebookProfileImage Done");
	}*/

	IEnumerator LoadFacebookProfileImage (string url)
	{
		WWW image = new WWW (url);
		yield return image;

		Q.Utils.QDebug.Log ("LoadFacebookProfileImage "+image.error);

		if (image.error == null) {
			profileImage.CreateImageSprite (image.texture);
		} else {
			Q.Utils.QDebug.Log (image.error);
		}
		Q.Utils.QDebug.Log ("LoadFacebookProfileImage Done");
	}

	void OnValueChangedToggle (bool isOn)
	{
		if (m_friendList != null) {
			if (isOn) {
				if (profileId != 0) {
					m_friendList.OnClickFollowerSelected ((object)profileId);
				} else if (string.IsNullOrEmpty (facebook_id) == false) {
					m_friendList.OnClickFollowerSelected ((object)facebook_id);
				}
			} else {
				m_friendList.RemoveSelectedFolower ();
			}
		}
	}

	// Update is called once per frame
	public void OnClickUnFollowButton () {			
		m_followersPanel.confirmUnfollowPanel.SetActive (true);
		m_followersPanel.SetFollowerItemPanel (this);
	}

	public void UnfollowUser()
	{
		if (m_followersPanel != null) {
			m_followersPanel.OnClickUnFollowButton (this);
			ServerManager.Instance.UnFollowUser (this.profileId, UnfollowCallback);
		}
	}

	void UnfollowCallback(bool isSuccess)
	{
		if (isSuccess) {
			MainMenuPanelManager.Instance.ShowMessage ("Successfully Unfollowed User", MainMenuPanelManager.eMainMenuState.MainMenuPanel);
			m_followersPanel.confirmUnfollowPanel.SetActive (false);
		}
	}
}

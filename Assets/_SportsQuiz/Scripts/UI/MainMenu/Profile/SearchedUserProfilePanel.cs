﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;
using Facebook.Unity;

public class SearchedUserProfilePanel : MonoBehaviour {

	[Header("PROFILE PANEL")]
	public Image profileImage;

	public Button followButton;
	public Button searchAnotherButton;
	public Button refineSearchButton;
	public Text userDetailsText;
	public Text nameText;

	[Header("BADGES PANEL")]
	public ScrollRect badgesScrollRect;
	public Button badgeButton;
	public Transform badgesParent;
	public Text noBadgesText;
	public BadgesHandler badgeHandler;
	private List<BadgesHandler> badgeHandlerList = new List<BadgesHandler> ();

	public GameObject badgeDetailsPanel;
	public Image badgeDetailsIcon;
	public Text badgeHeader;
	public Text badgeDescription;
	public Button closeBadgeDetailsBtn;

	private Image badgeItemImage;
		
	[Header("USER STATS PANEL")]
	public Text totalWinsText;
	public Text totalLossesText;
	public Text totalTiesText;

	[Header("USER STATS PIE PANEL")]
	public ScrollRect pieDetailsScrollRect;
	public RectTransform pieDetailItem;
	public Text scoreText;
	public PieChartUIManager pieChart;
	public Button statisticsButton;
	public List<float> percentage = new List<float> ();
	private List<SeralizedClassServer.Score_Statistics> scoreStats = new List<SeralizedClassServer.Score_Statistics>();

	public GameObject PieChartObj;
	public GameObject totalScoreHeader;
	public Text noScoreStatsText;

	[Header("USER STATS DETAIL VIEW")]
	public GameObject scoreStatisticsPanel;
	public GameObject statsCloneObj;
	private List<GameObject> statsCloneObjList = new List<GameObject>();
	public Transform statsparentObj;
	public Text addFeelingLucky;
	public Text addLeisure;
	public Text addDuel;
	public Button statsCloseBtn;

	[Header("Reference Class")]
	public SearchPanel searchPanel;
	public PieChartCategoryHandler pieChartCategoryHandler;
	public BadgesHandler badgesHandler;

	private int searchedUserId;

	void Start () {
		profileImage.preserveAspect = true;

		followButton.onClick.AddListener (OnClickFollowButton);
		searchAnotherButton.onClick.AddListener (OnClickSearchAnotherPlayers);
		refineSearchButton.onClick.AddListener (OnClickRefineSearchButton);
		closeBadgeDetailsBtn.onClick.AddListener (CloseBadgeDetails);
		statsCloseBtn.onClick.AddListener (OnClickStatsCloseButton);
		statisticsButton.onClick.AddListener (OnClickStats);
	}

	public void IsAlreadyFollowing(int id)
	{
		if (id == 0) {
			followButton.gameObject.SetActive (false);
		} else {
			followButton.gameObject.SetActive (true);
		}
	}

	void OnEnable () {
		ClearPreviousData ();
	}

	void OnDisable () {
	}

	void ClearPreviousData () {
		for (int i = 0; i < badgeHandlerList.Count; i++)
		{
			Destroy (badgeHandlerList [i].gameObject);
		}
		badgeHandlerList.Clear ();
		profileImage.CreateImageSprite (SportsQuizManager.Instance.references.GetAvatarImage (0));
		nameText.text = "";
		userDetailsText.text = "";
		Resources.UnloadUnusedAssets ();

	}

	void OnClickFollowButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ServerManager.Instance.FollowUser (searchedUserId);
		followButton.gameObject.SetActive (false);
//		MainMenuPanelManager.Instance.mainMenuPanel.SwitchTogglePanel (MainMenuPanel.eSubMenuPanel.Mode);
	}

	void OnClickSearchAnotherPlayers()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		//searchPanel.gameObject.SetActive (true);
		MainMenuPanelManager.Instance.WaitingPanel (true);
		ServerManager.Instance.SearchFollow (searchPanel.categoryID_String, SerachFollowCallback);
	}

	void SerachFollowCallback(SeralizedClassServer.FollowSearchUserInfo followSearchUserInfo)
	{
		if (followSearchUserInfo != null) {
			followButton.gameObject.SetActive (true);
			ClearPreviousData ();
			AssignDetails(followSearchUserInfo);
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_QUIZZERS, RefineSearch);
			MainMenuPanelManager.Instance.WaitingPanel (false);
		}
	}

	void OnClickRefineSearchButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		RefineSearch ();
	}

	void RefineSearch ()
	{
		followButton.gameObject.SetActive (true);
		//		searchPanel.gameObject.SetActive (true);
		searchPanel.Init ();
		this.gameObject.SetActive (false);
	}

	public void AssignDetails(SeralizedClassServer.FollowSearchUserInfo followSearchUserInfo)
	{
		searchedUserId = followSearchUserInfo.user_id;
//		SportsQuizManager.Instance.AssignOpponentProfileImage (profileImage, searchedUserId);
		ServerManager.Instance.GetOpponentInfoFromServer (GetUserInfoCallback, searchedUserId);
	}

	void GetUserInfoCallback()
	{
		Q.Utils.QDebug.Log ("User Info is fetched");
		UpdateUserInfo ();
	}

	public void UpdateUserInfo()
	{
		nameText.text = SportsQuizManager.Instance.searchedUserInfo.name;

		string _player_age = "";
		string _player_gender = "";

		if (SportsQuizManager.Instance.searchedUserInfo.age != 0) {
			_player_age = SportsQuizManager.Instance.searchedUserInfo.age.ToString ()+", ";
		}
		if (SportsQuizManager.Instance.searchedUserInfo.gender != SportsQuizManager.eGender.Gender) {
			_player_gender = SportsQuizManager.Instance.searchedUserInfo.gender + ", ";
		}

		userDetailsText.text = _player_age + _player_gender + Country.GetCountryName (SportsQuizManager.Instance.searchedUserInfo.country);

		totalWinsText.text = SportsQuizManager.Instance.searchedUserInfo.total_win.ToString();
		totalLossesText.text = SportsQuizManager.Instance.searchedUserInfo.total_lost.ToString();
		totalTiesText.text = SportsQuizManager.Instance.searchedUserInfo.total_draw.ToString();

		if (SportsQuizManager.IsNullOrEmptyOrZero (SportsQuizManager.Instance.searchedUserInfo.facebook_id)) {
			profileImage.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.searchedUserInfo.avatarType));
		} else {
			string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (SportsQuizManager.Instance.searchedUserInfo.facebook_id);
			if (string.IsNullOrEmpty (_facebookImageURL)) {
				profileImage.CreateImageSprite (
					SportsQuizManager.Instance.references.GetAvatarImage (0));
			} else {
				StartCoroutine (LoadImage (_facebookImageURL));
			}
		}

		BadgesSecured(SportsQuizManager.Instance.searchedUserInfo.userBadges);
		UpdateUserStatistics ();
	}

	IEnumerator LoadImage (string url)
	{
		WWW image = new WWW (url);
		yield return image;
		if (image.error == null) {
			profileImage.CreateImageSprite (image.texture);
		} else {
			profileImage.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (0));
			Q.Utils.QDebug.Log (image.error);
		}
	}

	void UpdateUserStatistics()
	{
		int isUpdateReq = 0;

		if (SportsQuizManager.Instance.searchedUserInfo.score_statistics.Count == 0) {
			PieChartObj.SetActive (false);
			totalScoreHeader.SetActive (false);
			noScoreStatsText.enabled = true;
		} else {
			PieChartObj.SetActive (true);
			totalScoreHeader.SetActive (true);
			noScoreStatsText.enabled = false;
		}

		List<SeralizedClassServer.Score_Statistics> copyOfScoreStats = new List<SeralizedClassServer.Score_Statistics> (scoreStats);

		if (copyOfScoreStats.Count == SportsQuizManager.Instance.searchedUserInfo.score_statistics.Count) {
			for (int i = 0; i < copyOfScoreStats.Count; i++) {
				if (copyOfScoreStats [i].category_id != SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].category_id) {
					isUpdateReq = 1;
				} else {
					if (copyOfScoreStats [i].score != SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score) {
						isUpdateReq = 1;
					}
				}
			}
		} else {
			isUpdateReq = 1;
		}

		if (isUpdateReq == 1) {
			List<float> percentage = new List<float> ();
			List<string> sportsNames = new List<string> ();
			int scoreInAllSports = 0;
			scoreStats.Clear ();

			for (int i = 0; i < SportsQuizManager.Instance.searchedUserInfo.score_statistics.Count; i++) {
				int scoreValue = 0;
				for (int j = 0; j < SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score.Count; j++) {
					scoreValue += SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score;
					scoreStats.Add (SportsQuizManager.Instance.searchedUserInfo.score_statistics [i]);
				}
				scoreInAllSports += scoreValue;
				if (scoreValue != 0) {
					percentage.Add (scoreValue);
					sportsNames.Add (SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].category_title);
				}
			}
			pieChartCategoryHandler.UpdateTotalScore (sportsNames, pieChart.colorArray);

			scoreText.text = scoreInAllSports.ToString();
			pieChart.InitPieValues (percentage);
			UpdateStatistics ();
		}
	}

	void BadgesSecured(List<SeralizedClassServer.UserBadges> userBadgeList)
	{
		if (userBadgeList.Count == 0) {
			noBadgesText.enabled = true;
		} else {
			noBadgesText.enabled = false;
		}

		/*if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadBadges (userBadgeList));
		}*/

		for (int i = 0; i < userBadgeList.Count; i++) {

			BadgesHandler _badge = CheckForPreviousBadge (userBadgeList [i].badge_id);

			if (_badge == null) {
				BadgesHandler badge = (BadgesHandler)Instantiate (badgeHandler);
				badge.gameObject.RectTransformMakeChild (badgeHandler.transform.parent);
				badge.InitBadgeHandler (userBadgeList [i].badge_id, ShowBadgeDetail);

				badgeHandlerList.Add (badge);
			} else {
				_badge.InitBadgeHandler ();
			}
		}

		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	IEnumerator LoadBadges (List<SeralizedClassServer.UserBadges> userBadgeList)
	{
		for (int i = 0; i < userBadgeList.Count; i++) {

			BadgesHandler _badge = CheckForPreviousBadge (userBadgeList [i].badge_id);

			if (_badge == null) {

				BadgesHandler badge = (BadgesHandler)Instantiate (badgeHandler);
				badge.gameObject.RectTransformMakeChild (badgeHandler.transform.parent);
				badge.gameObject.SetActive (false);
				badge.InitBadgeHandler (userBadgeList [i].badge_id, ShowBadgeDetail);
				badgeHandlerList.Add (badge);

				SportsQuizManager.Badges _getBadge = SportsQuizManager.Instance.GetBadge (userBadgeList [i].badge_id);
				if (_getBadge != null) {

					if (References.GetImageFromLocal(_getBadge.id, _getBadge.url, References.eImageType.Badges) == null) {
						if (this.gameObject.activeInHierarchy) {
							WWW image = new WWW (_getBadge.url);
							yield return image;
							if (image.error == null) {

								References.SaveImageToLocal (_getBadge.id, _getBadge.url, image.texture, References.eImageType.Badges);
								if (badge.badgeImage != null) {
									badge.badgeImage.CreateImageSprite (image.texture);
								}
							} else {
								Q.Utils.QDebug.Log (image.error);
							}
						}
					} else {
						badge.badgeImage.CreateImageSprite (References.GetImageFromLocal(_getBadge.id, _getBadge.url, References.eImageType.Badges));
					}
				}
				badge.gameObject.SetActive (true);

				//badge.badgeDescription.text = userBadgeList [i].description;
			} else {
				_badge.InitBadgeHandler ();
			}
		}

		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	void ShowBadgeDetail (SportsQuizManager.Badges badge) {

		#if !MEMORY_CHECK
		if (badge.badge != null) {
			badgeDetailsIcon.CreateImageSprite (badge.badge);
		}
		#else
		badgeDetailsIcon.CreateImageSprite (References.GetImageFromLocal (badge.id, badge.url, References.eImageType.Badges));
		#endif

		badgeDescription.text = badge.description;
		badgeHeader.text = badge.title;
		badgeDetailsPanel.SetActive (true);
	}

	BadgesHandler CheckForPreviousBadge (int id)
	{
		for (int i = 0; i < badgeHandlerList.Count; i++) {

			if (badgeHandlerList [i].badgeId == id) {

				return badgeHandlerList [i];
			}
		}

		return null;
	}

	void CloseBadgeDetails()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		badgeDetailsPanel.SetActive (false);
	}


	void BadgeDetails(Button clonedBadge)
	{
		badgesHandler = clonedBadge.GetComponent<BadgesHandler> ();
		//		badgeDetailsIcon.sprite = badgesHandler.badgeImage.sprite;
		badgeDescription.text = badgesHandler.badgeDescription.text;
		badgeHeader.text = badgesHandler.badgeHeader.text;
		badgeDetailsPanel.SetActive (true);
	}

	void OnClickStats () {
		if (statsCloneObjList.Count > 0) {
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
			scoreStatisticsPanel.SetActive (true);
		}
	}

	void OnClickStatsCloseButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		scoreStatisticsPanel.SetActive (false);
	}

	void UpdateStatistics()
	{
		int scoreOfDualMode = 0;
		int scoreOfLeisureMode = 0;
		int scoreOfIMFluckyMode = 0;

		for (int i = 0; i < statsCloneObjList.Count; i++) {
			Destroy (statsCloneObjList [i]);
		}
		statsCloneObjList.Clear ();

		for (int i = 0; i < SportsQuizManager.Instance.searchedUserInfo.score_statistics.Count; i++) {

			GameObject statsObj = Instantiate (statsCloneObj);
			statsCloneObjList.Add (statsObj);
			statsObj.SetActive (true);
			statsObj.transform.SetParent (statsparentObj, false);

			StatsCellView cellView = statsObj.GetComponent<StatsCellView> ();


			cellView.sportsType.text = SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].category_title;

			int scoreOfEachCellTotal = 0;

			for (int j = 0; j < SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score.Count; j++) 
			{
				QuizManager.eQuizMode mode = (QuizManager.eQuizMode)SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].type;

				scoreOfEachCellTotal  += (QuizManager.eQuizMode)SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score;

				switch (mode) 
				{
				case QuizManager.eQuizMode.Dual:
					cellView.dual.text = SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score.ToString ();
					scoreOfDualMode += SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score;
					break;

				case QuizManager.eQuizMode.IMFLucky:
					cellView.feelingLucky.text = SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score.ToString ();
					scoreOfIMFluckyMode += SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score;
					break;

				case QuizManager.eQuizMode.Leisure:
					cellView.leisure.text = SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score.ToString ();
					scoreOfLeisureMode += SportsQuizManager.Instance.searchedUserInfo.score_statistics [i].score [j].score;
					break;

				default:
					break;
				}
			}

			cellView.total.text = scoreOfEachCellTotal.ToString();
		}
		addFeelingLucky.text = scoreOfIMFluckyMode.ToString();
		addLeisure.text = scoreOfLeisureMode.ToString();
		addDuel.text = scoreOfDualMode.ToString();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class FrequentlyPlayedHandler : MonoBehaviour 
{
	public GameObject toClone;
	public Transform parentObj;
	public Button closeBtn;
	public Text noFrequentPlayed;
	public GameObject loadingObject;

	[HideInInspector]
	public List<GameObject> cloneList;
//	[HideInInspector]
//	public List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> frequentlyPlayedList;

	private FrequentlyPlayedCell frequentlyPlayedCell;

	void OnEnable ()
	{
		loadingObject.SetActive (true);
	}

	void Start()
	{
		closeBtn.onClick.AddListener (OnClickCloseButton);
	}

	public void UpdateFrequentlyPlayedList(List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> frequentlyPlayedInfo)
	{
//		frequentlyPlayedList = new List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> (frequentlyPlayedInfo);

		cloneList = new List<GameObject> ();

		if (frequentlyPlayedInfo.Count == 0) {
			noFrequentPlayed.enabled = true;
		} else {
			noFrequentPlayed.enabled = false;
		}

		/*for (int i = 0; i < frequentlyPlayedInfo.Count; i++) {
			MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.follow = UserProfilePanel.Follow.frequentlyPlayed;
			GameObject clonedObj = Instantiate (toClone);
			clonedObj.SetActive (true);
			clonedObj.transform.SetParent (parentObj, false);
			cloneList.Add (clonedObj);
			
			frequentlyPlayedCell = clonedObj.GetComponent<FrequentlyPlayedCell> ();
			frequentlyPlayedCell.name.text = frequentlyPlayedInfo[i].name;
			SportsQuizManager.Instance.AssignUserProfileImage (frequentlyPlayedCell.profileImage, i);
		} */

		StartCoroutine (LoadAllPlayer (frequentlyPlayedInfo));
	}

	IEnumerator LoadAllPlayer (List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> frequentlyPlayedInfo)
	{
		for (int i = 0; i < frequentlyPlayedInfo.Count; i++) {
			MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.follow = UserProfilePanel.Follow.frequentlyPlayed;
			GameObject clonedObj = Instantiate (toClone);
			clonedObj.SetActive (true);
			clonedObj.transform.SetParent (parentObj, false);
			cloneList.Add (clonedObj);

			frequentlyPlayedCell = clonedObj.GetComponent<FrequentlyPlayedCell> ();
			frequentlyPlayedCell.name.text = frequentlyPlayedInfo[i].name;

			if (SportsQuizManager.IsNullOrEmptyOrZero (frequentlyPlayedInfo[i].facebook_id)) {
				frequentlyPlayedCell.profileImage.CreateImageSprite (
					SportsQuizManager.Instance.references.GetAvatarImage (frequentlyPlayedInfo [i].avatar_type));
			} else {
				string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (frequentlyPlayedInfo [i].facebook_id);
				if (string.IsNullOrEmpty (_facebookImageURL)) {
					frequentlyPlayedCell.profileImage.CreateImageSprite (
						SportsQuizManager.Instance.references.GetAvatarImage (frequentlyPlayedInfo [i].avatar_type));
				} else {
					
					WWW image = new WWW (_facebookImageURL);
					yield return image;
					
					if (image.error == null) {
						frequentlyPlayedCell.profileImage.CreateImageSprite (image.texture);
					} else {
						frequentlyPlayedCell.profileImage.CreateImageSprite (
							SportsQuizManager.Instance.references.GetAvatarImage (frequentlyPlayedInfo [i].avatar_type));
					}
				}
			}
		}

		loadingObject.SetActive (false);
	}


	void OnClickCloseButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ClearCells ();
		this.gameObject.SetActive (false);
	}

	void ClearCells()
	{
		for (int i = 0; i < cloneList.Count; i++) {
			Destroy (cloneList [i]);
		}
		cloneList.Clear ();
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class FacebookFriendsItem : MonoBehaviour {

	[SerializeField] private Image profileImage;
	[SerializeField] private Text nameText;
	private string m_facebookID;
	private string m_profileURL;

	// Use this for initialization
	void Start () {
	
	}

	public void InitFacebookFriendsItem (string id, string name, string picUrl, Texture2D image)
	{
		nameText.text = name;
		m_facebookID = id;
		m_profileURL = picUrl;

		if (image != null) {
			profileImage.CreateImageSprite (image);
		} else if (string.IsNullOrEmpty (m_profileURL) == false) {
			StartCoroutine ("LoadImage");
		}
	}

	IEnumerator LoadImage ()
	{
		WWW image = new WWW (m_profileURL);
		yield return image;

		if (image.error == null) {

			profileImage.CreateImageSprite (image.texture);

		} else {
			Q.Utils.QDebug.Log (image.error);
		}
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}

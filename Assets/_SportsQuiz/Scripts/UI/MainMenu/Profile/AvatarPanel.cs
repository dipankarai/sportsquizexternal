﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class AvatarPanel : MonoBehaviour {

	public EditProfilePanel editProfilePanel;
	public Button closeButton;
	public ScrollRect avatarScrollRect;
	public Image contentItem;

	private Texture2D[] m_avatarTextureArray;

	void OnEnable () {
	}

	// Use this for initialization
	void Start () {
	
		closeButton.onClick.AddListener (OnClickCloseButton);
		InitScrollItem ();
	}
	

	public void OnClickAvatarButton (int avatarID)
	{
		int _avatarID = avatarID - 1;
		editProfilePanel.ChangedAvatar (m_avatarTextureArray[avatarID], avatarID);
	}

	public void OnClickCloseButton ()
	{
		this.gameObject.SetActive (false);
	}

	void InitScrollItem ()
	{
		if (m_avatarTextureArray == null || m_avatarTextureArray.Length == 0) {
			m_avatarTextureArray = new Texture2D[SportsQuizManager.Instance.references.GetAllAvatar ().Length];
			System.Array.Copy (SportsQuizManager.Instance.references.GetAllAvatar (), m_avatarTextureArray, m_avatarTextureArray.Length);
		}

		for (int i = 0; i < m_avatarTextureArray.Length; i++) {
			Image _image = (Image)Instantiate (contentItem);
			_image.gameObject.RectTransformMakeChild (avatarScrollRect.content.transform);
			_image.name = i.ToString () + "_" + "Avatar";
			_image.GetComponent<AvatarButton> ().InitAvatarButton (i, m_avatarTextureArray [i]);

//			_image.name = (i + 1).ToString () + "_" + contentItem.name;
//			_image.GetComponent<AvatarButton> ().InitAvatarButton ((i + 1), m_avatarTextureArray [i]);
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsCellView : MonoBehaviour 
{
	public Text sportsType;
	public Text feelingLucky;
	public Text leisure;
	public Text dual;
	public Text total;
}
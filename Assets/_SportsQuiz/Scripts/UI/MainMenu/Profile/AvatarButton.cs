﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class AvatarButton : MonoBehaviour {

	public AvatarPanel avatarPanel;
	private int m_avatarID;
	private Image m_AvatarImage;
	private Button m_AvatarButtom;

	// Use this for initialization
	void Start () {

		InitComponent ();

		m_AvatarButtom.onClick.AddListener (OnClickAvatarButton);
	}

	void InitComponent ()
	{
		if (m_AvatarButtom == null)
			m_AvatarButtom = GetComponent<Button> ();
		if (m_AvatarImage == null)
			m_AvatarImage = GetComponent<Image> ();
	}

	public void InitAvatarButton (int avatarid, Texture2D texture)
	{
		InitComponent ();
		m_avatarID = avatarid;
		m_AvatarImage.CreateImageSprite (texture);
	}
	
	void OnClickAvatarButton ()
	{
		avatarPanel.OnClickAvatarButton (m_avatarID);
	}
}

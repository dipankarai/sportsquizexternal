﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SportPreferencesPanel : MonoBehaviour 
{
	public GameObject cloneObjPanel;
	public Transform parentObj;
	private List<Toggle> toggleList = new List<Toggle> ();
	private List<GameObject> clonedObjsList = new List<GameObject>();

	public Button doneButton;
	private List<int> prefsArray;
	private string prefsStr = "";
	private int isUpdatedList = 0;
	private int m_intervelCount;
	public GameObject loadingObject;

	// Use this for initialization
	void Start () {	
		prefsArray = new List<int> ();
		doneButton.onClick.AddListener (OnClickDoneButton);
	}

	public void GetCategoryList()
	{
		if (SportsQuizManager.Instance.currentCategory != null &&
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count > 0) {

			if (m_intervelCount > 5) {
				m_intervelCount = 0;
				SportsQuizManager.Instance.ResetCategoryData ();
				ServerManager.Instance.GetMainCategoryOnly(CategoryCallback);
			} else {
				m_intervelCount++;
				CategoryCallback (SportsQuizManager.Instance.currentCategory.categoryDictionaryList[0]);
			}
		} else {
			SportsQuizManager.Instance.ResetCategoryData ();
			ServerManager.Instance.GetMainCategoryOnly(CategoryCallback);
		}
	}

	void CategoryCallback(SeralizedClassServer.CategoryDictionary callback)
	{
		int isNeedUpdate = 0;
		int toggleCount = 0;

		for (int i = 0; i < toggleList.Count; i++) {
			if (toggleList [i].isActiveAndEnabled) {
				toggleCount++;
			}
		}

		if (callback != null && callback.categoryList != null) {
			if(callback.categoryList.Count == toggleCount)
			{
				for (int i = 0; i < toggleCount; i++) {
					if (int.Parse (toggleList [i].name) != callback.categoryList [i].category_id) {
						isNeedUpdate = 1;
					}
				}
			}
			else
			{
				isNeedUpdate = 1;
			}
		}

		if (isNeedUpdate == 1) {

			for (int i = 0; i < clonedObjsList.Count; i++) {
				Destroy (clonedObjsList [i]);
			}

			clonedObjsList.Clear ();
			GameObject clone;
			float numOfCells = Mathf.Abs(callback.categoryList.Count / 3f);
			
			for (int i = 0; i <= numOfCells; i++) {
				clone = Instantiate (cloneObjPanel);
				clone.transform.SetParent (parentObj, false);
				clone.SetActive (true);
				clonedObjsList.Add (clone);
				SportPrefsCellsHandler sportPrefsCellsHandler = clone.GetComponent<SportPrefsCellsHandler> ();
				for(int j = 0; j < sportPrefsCellsHandler.togglesList.Count; j++)
				{
					toggleList.Add (sportPrefsCellsHandler.togglesList[j]);								
				}
			}
			
			for (int i = 0; i < callback.categoryList.Count; i++) {
				if (callback.categoryList [i].category_id != 0) {
					toggleList [i].GetComponentInChildren<Text>().text = callback.categoryList [i].category_title;
					toggleList [i].name = callback.categoryList [i].category_id.ToString();
					toggleList [i].gameObject.SetActive(true);
				}
			}
			
			AssignPreferences(SportsQuizManager.Instance.playerInformation.categoryPrefsList);
		}

		loadingObject.gameObject.SetActive (false);
	}

	public void AssignPreferences(List<int> intPreferences)
	{				
		for (int i = 0; i < intPreferences.Count; i++) {
			for (int j = 0; j < toggleList.Count; j++) {
				if (toggleList [j].isActiveAndEnabled) {
					if (intPreferences[i] == int.Parse (toggleList [j].name)) {
						toggleList [j].isOn = true;
					}
				}
			}
		}
	}

	void CheckPreferences()
	{	
		prefsArray.Clear ();
		prefsStr = "";
		isUpdatedList = 0;

		for (int i = 0; i < toggleList.Count; i++) {
			if (toggleList [i].isActiveAndEnabled && toggleList[i].isOn) {
				prefsArray.Add (int.Parse(toggleList[i].name));							
			}
		}

		for (int i = 0; i < prefsArray.Count; i++) 
		{
			prefsStr = prefsStr + prefsArray [i];

			if (i < prefsArray.Count - 1) 
			{
				prefsStr += ",";
			}
		}
	}

	void OnClickDoneButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		CheckPreferences ();
		Q.Utils.QDebug.Log (" Prefs : " + prefsStr);
		CheckAnyChangesInPrefs ();

		if (string.IsNullOrEmpty (prefsStr)) {
			prefsStr = "0";	
		}

		if (isUpdatedList == 1) {	
			ServerManager.Instance.UpdateUserCategoryPrefsToServer (prefsStr,CategoryPrefsCallback);
		}

		this.gameObject.SetActive (false);
		loadingObject.gameObject.SetActive (true);
	}

	void CheckAnyChangesInPrefs()
	{
		if(SportsQuizManager.Instance.playerInformation.categoryPrefsList.Count == prefsArray.Count)
		{
			for (int i = 0; i < SportsQuizManager.Instance.playerInformation.categoryPrefsList.Count; i++) {
				if (prefsArray.Contains (SportsQuizManager.Instance.playerInformation.categoryPrefsList [i])) {
					isUpdatedList = 0;
				} else {
					isUpdatedList = 1;
				}
			}
		}
		else 
		{
			isUpdatedList = 1;
		}
	}

	void CategoryPrefsCallback(bool isUpdated)
	{
		if(isUpdated)
		{			
			SportsQuizManager.Instance.playerInformation.categoryPrefsList = new List<int>(prefsArray);		
			Q.Utils.QDebug.Log ("Succcessfuly Updated");
		}
	}
}

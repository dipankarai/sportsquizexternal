﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RanksTitlesHandler : MonoBehaviour 
{
//	public Text headerText;
	public GameObject toClone;
	public Transform parentObj;
	public GameObject loading;

	public Toggle ranksToggle;
	public Toggle titlesToggle;
	public Button OKButton;

	public Text noRanksTitlesText;

	private List<GameObject> cloneList;
	private List<RankTitlesCell> rankTitlesCellList;

	private SeralizedClassServer.UserRankDetails rankTitlesDetails;

	public enum RankTitle
	{
		None = 0,
		Rank,
		Title,
	}
	[HideInInspector]
	public RankTitle rankTitle;


	private string noRank = "No Ranks yet!";
	private string noTitles = "No Titles yet!";

	void OnEnable () {
		loading.SetActive (true);
	}

	void Start()
	{
		rankTitle = RankTitle.Rank;
		ranksToggle.isOn = true;
		titlesToggle.isOn = false;

		ranksToggle.onValueChanged.AddListener (OnClickRanksButton);
		titlesToggle.onValueChanged.AddListener (OnClickTitlesButton);
		OKButton.onClick.AddListener (OnClickOkButton);
	}

	void OnClickRanksButton(bool isOn)
	{
		if (isOn) {
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
			rankTitle = RankTitle.Rank;
			noRanksTitlesText.text = noRank;
			UpdateRank ();
		}
	}

	void OnClickTitlesButton(bool isOn)
	{
		if (isOn) {
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
			rankTitle = RankTitle.Title;
			noRanksTitlesText.text = noTitles;
			UpdateTitle ();
		}
	}

	void OnClickOkButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ClearCells ();
		this.gameObject.SetActive (false);
	}

	public void UpdateRankTitle(SeralizedClassServer.UserRankDetails userRankDetails)
	{
		loading.SetActive (false);
		rankTitlesDetails = userRankDetails;
		cloneList = new List<GameObject>();
		rankTitlesCellList = new List<RankTitlesCell> ();

//		if(userRankDetails.rank_title.Count == 0)
//		{		
//			noRanksTitlesText.enabled = true;
//		}
//		else
//		{
//			noRanksTitlesText.enabled = false;
//		}

		for (int index = 0; index < userRankDetails.rank_title.Count; index++) {
			SeralizedClassServer.UserRanksInfo userRanksInfo = userRankDetails.rank_title [index];
			GameObject cloneObj = Instantiate (toClone);
			cloneObj.transform.SetParent (parentObj, false);
			cloneObj.SetActive (false);
			cloneList.Add (cloneObj);
			
			RankTitlesCell rankTitlesCell = cloneObj.GetComponent<RankTitlesCell> ();
//			rankTitlesCell.sportName.text = userRanksInfo.category_title;
			rankTitlesCellList.Add (rankTitlesCell);
			
		}	

		if (rankTitle == RankTitle.Rank) {
			UpdateRank ();
		} else if (rankTitle == RankTitle.Title) {
			UpdateTitle ();
		}
	}

	public void UpdateRank()
	{
		int isRanksFound = 0;

		for (int i = 0; i < rankTitlesDetails.rank_title.Count; i++) {
			rankTitlesCellList [i].rankTitle.text = "";
			if (rankTitlesDetails.rank_title [i].rank != 0 && rankTitlesDetails.rank_title [i].rank != null) 
			{
				isRanksFound = 1;
				cloneList [i].SetActive (true);
				rankTitlesCellList [i].rankTitle.text = "\"" + rankTitlesDetails.rank_title [i].rank.ToString () + "\"" + " in " + rankTitlesDetails.rank_title [i].category_title;
			} else {
				cloneList [i].SetActive (false);
			}
		}

		if (isRanksFound == 0) {
			noRanksTitlesText.enabled = true;
		}
		else {
			noRanksTitlesText.enabled = false;
		}
	}

	public void UpdateTitle()
	{
		int isTitlesFound = 0;

		for (int i = 0; i < rankTitlesDetails.rank_title.Count; i++) {
			rankTitlesCellList [i].rankTitle.text = "";
			if(!string.IsNullOrEmpty(rankTitlesDetails.rank_title [i].score_title))
			{
				isTitlesFound = 1;
				cloneList [i].SetActive (true);
				rankTitlesCellList[i].rankTitle.text = "\""+rankTitlesDetails.rank_title [i].score_title+"\""+" in "+rankTitlesDetails.rank_title [i].category_title;
			} else {
				cloneList [i].SetActive (false);
			}
		}
		if (isTitlesFound == 0) {
			noRanksTitlesText.enabled = true;
		} else {
			noRanksTitlesText.enabled = false;
		}
	}

	public void ClearCells()
	{
		for (int i = 0; i < cloneList.Count; i++) {
			Destroy(cloneList [i]);
		}
		cloneList.Clear ();
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;
using Facebook.Unity;

public class UserProfilePanel : MonoBehaviour {

	[Header("PROFILE PANEL")]
	public Image profileImage;

	public Button ranksButton;
	public Button frequentlyPlayedPlayers;
	public Button followingButton;
	public Button followersButton;
	public Button preferencesButton;
	public Button facebookFriendsButton;
	public Button statisticsButton;
	public Text userName;
	public Text userDetailsText;
	public GameObject friendListObj;
	public GameObject parentObj;
	public FollowersPanel followersPanel;
	public Text noFollowerFollowing;
	public Text followFollowingHeader;
	private string followerStr = "Followers";
	private string followingStr = "Following";

	[Header("BADGES PANEL")]
	public ScrollRect badgesScrollRect;
	public Button badgeButton;
	public Transform badgesParent;
	public Text noBadgesText;
	public BadgesHandler badgeHandler;
	private List<BadgesHandler> badgeHandlerList = new List<BadgesHandler> ();

	public GameObject badgeDetailsPanel;
	public Image badgeDetailsIcon;
	public Text badgeHeader;
	public Text badgeDescription;
	public Button closeBadgeDetailsBtn;

	private Image badgeItemImage;
	private List<SeralizedClassServer.UserBadges> clonedbadgesList = new List<SeralizedClassServer.UserBadges>();
	private List<SeralizedClassServer.UserBadges> copyOfClonedBadges;
	private List<GameObject> gameObjectsList = new List<GameObject>();

	[Header("USER STATS PANEL")]
	public Text totalWinsText;
	public Text totalLossesText;
	public Text totalTiesText;

	[Header("USER STATS PIE PANEL")]
	public ScrollRect pieDetailsScrollRect;
	public RectTransform pieDetailItem;
	public Text scoreText;
	public PieChartUIManager pieChart;
	private List<SeralizedClassServer.Score_Statistics> scoreStats = new List<SeralizedClassServer.Score_Statistics>();

	public GameObject PieChartObj;
	public GameObject totalScoreHeader;
	public Text noScoreStatsText;
	 
	[Header("USER STATS DETAIL VIEW")]
	public GameObject scoreStatisticsPanel;
	public GameObject statsCloneObj;
	private List<GameObject> statsCloneObjList = new List<GameObject>();
	public Transform statsparentObj;
	public Text addFeelingLucky;
	public Text addLeisure;
	public Text addDuel;
	public Button statsCloseBtn;

	[Header("References")]
	public RanksTitlesHandler ranksTitlesHandler;
	public FrequentlyPlayedHandler frequentlyPlayedHandler;
	public PieChartCategoryHandler pieChartCategoryHandler;
	public BadgesHandler badgesHandler;
	public FacebookFriendsPanel facebookFriendsPanel;

	public enum Follow
	{
		none = 0,
		follwers = 1,
		following = 2,
		frequentlyPlayed = 3,
	}

	[HideInInspector]
	public Follow follow;

	// Use this for initialization
	void Start () {
		profileImage.preserveAspect = true;
		followersButton.onClick.AddListener (OnClickFollowersButton);
		preferencesButton.onClick.AddListener (OnClickPreferenceButton);
		facebookFriendsButton.onClick.AddListener (OnClickFacebookFriendsButton);
		ranksButton.onClick.AddListener (OnClickRanksButton);
		frequentlyPlayedPlayers.onClick.AddListener (OnClickFrequentlyPlayedPlayers);
		followingButton.onClick.AddListener (OnClickFollowingButton);
		statisticsButton.onClick.AddListener (OnClickStats);
		closeBadgeDetailsBtn.onClick.AddListener (CloseBadgeDetails);
		statsCloseBtn.onClick.AddListener (OnClickStatsCloseButton);
	}

	void OnEnable () {
		Invoke ("UpdateUserInfo", 0.5f);
	}

	void OnDisable ()
	{
		for (int i = 0; i < badgeHandlerList.Count; i++)
		{
			Destroy (badgeHandlerList [i].gameObject);
		}
		badgeHandlerList.Clear ();
		Resources.UnloadUnusedAssets ();
	}

	public void UpdateUserInfo()
	{
		UpdateUserdata ();
		SportsQuizManager.Instance.AssignUserProfileImage (profileImage);

		totalWinsText.text = SportsQuizManager.Instance.playerInformation.total_win.ToString();
		totalLossesText.text = SportsQuizManager.Instance.playerInformation.total_lost.ToString();
		totalTiesText.text = SportsQuizManager.Instance.playerInformation.total_draw.ToString();

		BadgesSecured(SportsQuizManager.Instance.playerInformation.userBadges);
		UpdateUserStatistics ();
	}

	public void UpdateUserdata()
	{
		string _player_age = "";
		string _player_gender = "";
		string _player_name = SportsQuizManager.Instance.playerInformation.name;

		if (SportsQuizManager.Instance.playerInformation.age != 0 && SportsQuizManager.Instance.playerInformation.age != null) {
			_player_age = SportsQuizManager.Instance.playerInformation.age.ToString ()+", ";
		}
		if (SportsQuizManager.Instance.playerInformation.gender != SportsQuizManager.eGender.Gender) {
			_player_gender = SportsQuizManager.Instance.playerInformation.gender.ToString()+", ";
		}
		string _player_country = Q.Utils.Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country);

		userName.text = _player_name;
		userDetailsText.text = _player_age + _player_gender + _player_country;
	}

	void UpdateUserStatistics()
	{
		int isUpdateReq = 0;

		if (SportsQuizManager.Instance.playerInformation.score_statistics.Count == 0) {
			PieChartObj.SetActive (false);
			totalScoreHeader.SetActive (false);
			noScoreStatsText.enabled = true;
		} else {
			PieChartObj.SetActive (true);
			totalScoreHeader.SetActive (true);
			noScoreStatsText.enabled = false;
		}

		List<SeralizedClassServer.Score_Statistics> copyOfScoreStats = new List<SeralizedClassServer.Score_Statistics> (scoreStats);

		if (copyOfScoreStats.Count == SportsQuizManager.Instance.playerInformation.score_statistics.Count) {
			for (int i = 0; i < copyOfScoreStats.Count; i++) {
				if (copyOfScoreStats [i].category_id != SportsQuizManager.Instance.playerInformation.score_statistics [i].category_id) {
					isUpdateReq = 1;
				} else {
					if (copyOfScoreStats [i].score != SportsQuizManager.Instance.playerInformation.score_statistics [i].score) {
						isUpdateReq = 1;
					}
				}
			}
		} else {
			isUpdateReq = 1;
		}

		if (isUpdateReq == 1) {
			List<float> percentage = new List<float> ();
			List<string> sportsNames = new List<string> ();
			int scoreInAllSports = 0;
			scoreStats.Clear ();

			for (int i = 0; i < SportsQuizManager.Instance.playerInformation.score_statistics.Count; i++) {
				int scoreValue = 0;
				for (int j = 0; j < SportsQuizManager.Instance.playerInformation.score_statistics [i].score.Count; j++) {
					scoreValue += SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score;
					scoreStats.Add (SportsQuizManager.Instance.playerInformation.score_statistics [i]);
				}
				scoreInAllSports += scoreValue;
				if (scoreValue != 0) {
					percentage.Add (scoreValue);
					sportsNames.Add (SportsQuizManager.Instance.playerInformation.score_statistics [i].category_title);
				}
			}
			pieChartCategoryHandler.UpdateTotalScore (sportsNames, pieChart.colorArray);

			scoreText.text = scoreInAllSports.ToString();
			pieChart.InitPieValues (percentage);
			UpdateStatistics ();
		}
	}

	void OnClickFollowersButton ()
	{
		noFollowerFollowing.text = "";
		followFollowingHeader.text = followingStr;
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		follow = Follow.follwers;
		MainMenuPanelManager.Instance.mainMenuPanel.followersPanel.gameObject.SetActive (true);
		ServerManager.Instance.GetFollowers (UpdateFollowList);
	}

	void OnClickFollowingButton()
	{
		noFollowerFollowing.text = "";
		followFollowingHeader.text = followerStr;
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		follow = Follow.following;
		MainMenuPanelManager.Instance.mainMenuPanel.followersPanel.gameObject.SetActive (true);
		ServerManager.Instance.GetFollowingList (UpdateFollowList);
	}

	void OnClickPreferenceButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (InternetConnection.Check) {
			MainMenuPanelManager.Instance.mainMenuPanel.preferencePanel.gameObject.SetActive (true);
			MainMenuPanelManager.Instance.mainMenuPanel.preferencePanel.GetCategoryList ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void OnClickFacebookFriendsButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		List<object> filter = new List<object>() { "app_users" };
		if (InternetConnection.Check) {
			SocialHandler.Instance.GetFacebookFriendList (FacebookFriendCallback);
			facebookFriendsPanel.gameObject.SetActive (true);
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void FacebookFriendCallback (bool succes)
	{
		Q.Utils.QDebug.Log ("FacebookFriendCallback "+succes);
		if (succes) {
			facebookFriendsPanel.InitFacebookFriendsPanel ();
		} else {
			facebookFriendsPanel.gameObject.SetActive (false);
		}
	}

	void OnClickRanksButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ServerManager.Instance.GetRankInfoFromServer (RankTitlesCallback);
		ranksTitlesHandler.gameObject.SetActive (true);
	}

	void RankTitlesCallback(SeralizedClassServer.UserRankDetails userRankDetails)
	{
		if (userRankDetails != null) {
			ranksTitlesHandler.rankTitle = RanksTitlesHandler.RankTitle.Rank;
			ranksTitlesHandler.UpdateRankTitle (userRankDetails);	
		} else {
			ranksTitlesHandler.gameObject.SetActive (false);
		}
	}

	void OnClickFrequentlyPlayedPlayers()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		frequentlyPlayedHandler.gameObject.SetActive (true);
		ServerManager.Instance.GetFrequentPlayedUserInfo (FrequentPlayedCallback);
	}

	void FrequentPlayedCallback(List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> frequentlyPlayedInfo)
	{
		frequentlyPlayedHandler.UpdateFrequentlyPlayedList (frequentlyPlayedInfo);
	}

	void OnClickStats () {
		if (statsCloneObjList.Count > 0) {
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
			scoreStatisticsPanel.SetActive (true);
		}
	}

	void OnClickStatsCloseButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		scoreStatisticsPanel.SetActive (false);
	}

	void UpdateFollowList(bool isFriendListFetched)
	{
		StartCoroutine (LoadAllImage (isFriendListFetched));
		/*int listCount = 0;

		if (isFriendListFetched) {			
			if (follow == Follow.following) {
				listCount = SportsQuizManager.Instance.playerInformation.followingList.Count;
				if (listCount == 0) {
					noFollowerFollowing.text = noFollowing;
					noFollowerFollowing.enabled = true;
				} else {
					noFollowerFollowing.enabled = false;
				}
				followFollowingHeader.text =  followingStr;
			}
			else if(follow == Follow.follwers){
				listCount = SportsQuizManager.Instance.playerInformation.followerList.Count;
				if (listCount == 0) {
					noFollowerFollowing.text = noFollower;
					noFollowerFollowing.enabled = true;
				}
				 else {
					noFollowerFollowing.enabled = false;
				}
				followFollowingHeader.text =  followerStr;
			}

			for (int i = 0; i < listCount; i++) {
				Q.Utils.QDebug.Log ("Executed");
				GameObject friendListgameObj = Instantiate (friendListObj);
				friendListgameObj.SetActive (true);
				friendListgameObj.transform.SetParent (parentObj.transform,false);
				followersPanel.followerItemPanel = friendListgameObj.GetComponent<FollowerItemPanel> ();
				followersPanel.followerItemPanel.m_followersPanel = followersPanel;
				followersPanel.m_followersItemList.Add (followersPanel.followerItemPanel);

				if (follow == Follow.following) {
					AssignFollowingList (i);
				}
				else if(follow == Follow.follwers){
					AssignFollowersList (i);
				}
			}			
		}*/
	}

	IEnumerator LoadAllImage (bool isFriendListFetched)
	{
		int listCount = 0;

		if (isFriendListFetched) {			
			if (follow == Follow.following) {
				listCount = SportsQuizManager.Instance.playerInformation.followingList.Count;
				if (listCount == 0) {
					noFollowerFollowing.text = TagConstant.PopUpMessages.NO_FOLLOWING;
					noFollowerFollowing.enabled = true;
				} else {
					noFollowerFollowing.enabled = false;
				}
				followFollowingHeader.text =  followingStr;
			}
			else if(follow == Follow.follwers){
				listCount = SportsQuizManager.Instance.playerInformation.followerList.Count;
				if (listCount == 0) {
					noFollowerFollowing.text = TagConstant.PopUpMessages.NO_FOLLOWERS;
					noFollowerFollowing.enabled = true;
				}
				else {
					noFollowerFollowing.enabled = false;
				}
				followFollowingHeader.text =  followerStr;
			}

			for (int i = 0; i < listCount; i++) {
				Q.Utils.QDebug.Log ("Executed");
				GameObject friendListgameObj = Instantiate (friendListObj);
				friendListgameObj.SetActive (true);
				friendListgameObj.transform.SetParent (parentObj.transform,false);
				followersPanel.followerItemPanel = friendListgameObj.GetComponent<FollowerItemPanel> ();
				followersPanel.followerItemPanel.m_followersPanel = followersPanel;
				followersPanel.m_followersItemList.Add (followersPanel.followerItemPanel);
//				followersPanel.followerItemPanel.profileImage

				if (follow == Follow.following) {
					if (SportsQuizManager.IsNullOrEmptyOrZero (SportsQuizManager.Instance.playerInformation.followingList[i].facebook_id)) {
						followersPanel.followerItemPanel.profileImage.CreateImageSprite (
							SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followingList[i].avatar_type));
					} else {
						string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (SportsQuizManager.Instance.playerInformation.followingList[i].facebook_id);
						if (string.IsNullOrEmpty (_facebookImageURL)) {
							followersPanel.followerItemPanel.profileImage.CreateImageSprite (
								SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followingList[i].avatar_type));
						} else {

							WWW image = new WWW (_facebookImageURL);
							yield return image;

							if (image.error == null) {
								followersPanel.followerItemPanel.profileImage.CreateImageSprite (image.texture);
							} else {
								followersPanel.followerItemPanel.profileImage.CreateImageSprite (
									SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followingList[i].avatar_type));
							}
						}
					}
					AssignFollowingList (i);
				}
				else if(follow == Follow.follwers){
					if (SportsQuizManager.IsNullOrEmptyOrZero (SportsQuizManager.Instance.playerInformation.followerList[i].facebook_id)) {
						followersPanel.followerItemPanel.profileImage.CreateImageSprite (
							SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followerList[i].avatar_type));
					} else {
						string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (SportsQuizManager.Instance.playerInformation.followerList[i].facebook_id);
						if (string.IsNullOrEmpty (_facebookImageURL)) {
							followersPanel.followerItemPanel.profileImage.CreateImageSprite (
								SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followerList[i].avatar_type));
						} else {

							WWW image = new WWW (_facebookImageURL);
							yield return image;

							if (image.error == null) {
								followersPanel.followerItemPanel.profileImage.CreateImageSprite (image.texture);
							} else {
								followersPanel.followerItemPanel.profileImage.CreateImageSprite (
									SportsQuizManager.Instance.references.GetAvatarImage (SportsQuizManager.Instance.playerInformation.followerList[i].avatar_type));
							}
						}
					}

					AssignFollowersList (i);
				}
			}			
		} else {
			MainMenuPanelManager.Instance.mainMenuPanel.followersPanel.gameObject.SetActive (false);
		}

		MainMenuPanelManager.Instance.mainMenuPanel.followersPanel.loadingObject.SetActive (false);
	}

	void AssignFollowersList(int index)
	{
		followersPanel.followerItemPanel.profileId = (int)SportsQuizManager.Instance.playerInformation.followerList[index].user_id;
		followersPanel.followerItemPanel.usernameText.text = SportsQuizManager.Instance.playerInformation.followerList[index].name;
//		SportsQuizManager.Instance.AssignUserProfileImage(followersPanel.followerItemPanel.profileImage, index);
		followersPanel.followerItemPanel.unfollowButton.gameObject.SetActive(false);
	}

	void AssignFollowingList(int index)
	{
		followersPanel.followerItemPanel.profileId = SportsQuizManager.Instance.playerInformation.followingList[index].user_id;
		followersPanel.followerItemPanel.usernameText.text = SportsQuizManager.Instance.playerInformation.followingList[index].name;
//		SportsQuizManager.Instance.AssignUserProfileImage(followersPanel.followerItemPanel.profileImage, index);
//		followersPanel.followerItemPanel.unfollowButton.onClick.AddListener(followersPanel.followerItemPanel.OnClickUnFollowButton);
	}

	public void ClearPreviousData ()
	{
		for (int i = 0; i < badgeHandlerList.Count; i++) {
		 	
			GameObject go = badgeHandlerList [i].gameObject;
			Destroy (go);
		}

		badgeHandlerList.Clear ();
		Resources.UnloadUnusedAssets ();
	}

	void BadgesSecured(List<SeralizedClassServer.UserBadges> userBadgeList)
	{
		if (userBadgeList.Count == 0) {
			noBadgesText.enabled = true;
		} else {
			noBadgesText.enabled = false;
		}

		/*if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadBadges (userBadgeList));
		}*/

		for (int i = 0; i < userBadgeList.Count; i++) {

			BadgesHandler _badge = CheckForPreviousBadge (userBadgeList [i].badge_id);

			if (_badge == null) {

				BadgesHandler badge = (BadgesHandler)Instantiate (badgeHandler);
				badge.gameObject.RectTransformMakeChild (badgeHandler.transform.parent);
//				badge.gameObject.SetActive (false);
				badge.InitBadgeHandler (userBadgeList [i].badge_id, ShowBadgeDetail);
				badge.badgeDescription.text = userBadgeList [i].description;
				badgeHandlerList.Add (badge);
			} else {
				_badge.InitBadgeHandler ();
			}
		}

		/*int isUserBadgeUpdated = 0;
		copyOfClonedBadges = new List<SeralizedClassServer.UserBadges> (clonedbadgesList);

		if (userBadgeList.Count == copyOfClonedBadges.Count) {
			for (int i = 0; i < copyOfClonedBadges.Count; i++) {
				if (userBadgeList [i].badge_id != copyOfClonedBadges [i].badge_id) {
					isUserBadgeUpdated = 1;
				}
			}
		} else {
			isUserBadgeUpdated = 1;
		}

		if (isUserBadgeUpdated == 1) {			
			for (int i = 0; i < gameObjectsList.Count; i++) {
				Destroy(gameObjectsList[i]);
				gameObjectsList.Clear ();
			}

			for (int i = 0; i < userBadgeList.Count; i++) {
//				SportsQuizManager.Badges _badge = SportsQuizManager.Instance.GetBadge(userBadgeList[i].badge_id);
				if (userBadgeList[i].badge_id != null) {
					Button clonedBadge = (Button)Instantiate (badgeButton);					
					clonedBadge.gameObject.SetActive (true);
					gameObjectsList.Add (clonedBadge.gameObject);
					clonedBadge.transform.SetParent (badgesParent, false);
					badgeItemImage = clonedBadge.GetComponent<Image>();
					badgesHandler = clonedBadge.GetComponent<BadgesHandler> ();
//					badgeItemImage.sprite = badgesHandler.badgeImage.CreateImageSprite (userBadgeList[i].image);
					badgesHandler.badgeDescription.text = userBadgeList[i].title;
					badgesHandler.badgeHeader.text = userBadgeList[i].title;
					clonedbadgesList.Add (userBadgeList[i]);
					clonedBadge.onClick.AddListener (delegate{BadgeDetails(clonedBadge);});
				}
				
			}
		}*/
	}

	IEnumerator LoadBadges (List<SeralizedClassServer.UserBadges> userBadgeList)
	{
		for (int i = 0; i < userBadgeList.Count; i++) {

			BadgesHandler _badge = CheckForPreviousBadge (userBadgeList [i].badge_id);

			if (_badge == null) {

				BadgesHandler badge = (BadgesHandler)Instantiate (badgeHandler);
				badge.gameObject.RectTransformMakeChild (badgeHandler.transform.parent);
				badge.gameObject.SetActive (false);
				badge.InitBadgeHandler (userBadgeList [i].badge_id, ShowBadgeDetail);
				badgeHandlerList.Add (badge);

				SportsQuizManager.Badges _getBadge = SportsQuizManager.Instance.GetBadge (userBadgeList [i].badge_id);
				if (_getBadge != null) {
					
					if (References.GetImageFromLocal(_getBadge.id, _getBadge.url, References.eImageType.Badges) == null) {
						if (this.gameObject.activeInHierarchy) {
							WWW image = new WWW (_getBadge.url);
							yield return image;
							if (image.error == null) {

								References.SaveImageToLocal (_getBadge.id, _getBadge.url, image.texture, References.eImageType.Badges);
								if (badge.badgeImage != null) {
									badge.badgeImage.CreateImageSprite (image.texture);
								}
							} else {
								Q.Utils.QDebug.Log (image.error);
							}
						}
					} else {
						badge.badgeImage.CreateImageSprite (References.GetImageFromLocal(_getBadge.id, _getBadge.url, References.eImageType.Badges));
					}
				}
				badge.gameObject.SetActive (true);

				//badge.badgeDescription.text = userBadgeList [i].description;
			} else {
				_badge.InitBadgeHandler ();
			}
		}
	}

	BadgesHandler CheckForPreviousBadge (int id)
	{
		for (int i = 0; i < badgeHandlerList.Count; i++) {

			if (badgeHandlerList [i].badgeId == id) {
				
				return badgeHandlerList [i];
			}
		}

		return null;
	}

	void ShowBadgeDetail (SportsQuizManager.Badges badge) {

		#if MEMORY_CHECK
		badgeDetailsIcon.CreateImageSprite (References.GetImageFromLocal (badge.id, badge.url, References.eImageType.Badges));
		#else
		if (badge.badge != null) {
			
			badgeDetailsIcon.CreateImageSprite (badge.badge);
		}
		#endif
		
		badgeDescription.text = badge.description;
		badgeHeader.text = badge.title;
		badgeDetailsPanel.SetActive (true);
	}

	void CloseBadgeDetails()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		badgeDetailsPanel.SetActive (false);
	}

	void UpdateStatistics()
	{
		int scoreOfDualMode = 0;
		int scoreOfLeisureMode = 0;
		int scoreOfIMFluckyMode = 0;

		for (int i = 0; i < statsCloneObjList.Count; i++) {
			Destroy (statsCloneObjList [i]);
		}
		statsCloneObjList.Clear ();

		for (int i = 0; i < SportsQuizManager.Instance.playerInformation.score_statistics.Count; i++) {

			GameObject statsObj = Instantiate (statsCloneObj);
			statsCloneObjList.Add (statsObj);
			statsObj.SetActive (true);
			statsObj.transform.SetParent (statsparentObj, false);
			
			StatsCellView cellView = statsObj.GetComponent<StatsCellView> ();

			
			cellView.sportsType.text = SportsQuizManager.Instance.playerInformation.score_statistics [i].category_title;

			int scoreOfEachCellTotal = 0;

			for (int j = 0; j < SportsQuizManager.Instance.playerInformation.score_statistics [i].score.Count; j++) 
			{
				QuizManager.eQuizMode mode = (QuizManager.eQuizMode)SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].type;

				scoreOfEachCellTotal  += (QuizManager.eQuizMode)SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score;

				switch (mode) 
				{
				case QuizManager.eQuizMode.Dual:
					cellView.dual.text = SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score.ToString ();
					scoreOfDualMode += SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score;
					break;

				case QuizManager.eQuizMode.IMFLucky:
					cellView.feelingLucky.text = SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score.ToString ();
					scoreOfIMFluckyMode += SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score;
					break;

				case QuizManager.eQuizMode.Leisure:
					cellView.leisure.text = SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score.ToString ();
					scoreOfLeisureMode += SportsQuizManager.Instance.playerInformation.score_statistics [i].score [j].score;
					break;

				default:
					break;
				}
			}

			cellView.total.text = scoreOfEachCellTotal.ToString();
		}
		addFeelingLucky.text = scoreOfIMFluckyMode.ToString();
		addLeisure.text = scoreOfLeisureMode.ToString();
		addDuel.text = scoreOfDualMode.ToString();
	}

	public void ClearFields()
	{
		userName.text = "";
		userDetailsText.text = "";
		profileImage.sprite = null;
	}
}
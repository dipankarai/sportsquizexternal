﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class FacebookFriendsPanel : MonoBehaviour {

	[SerializeField] private Button closeButton;
	[SerializeField] private Text nofacebooklistText;
	public FacebookFriendsItem facebookFriendsItem;
	public GameObject loadingPanel;
	private List<FacebookFriendsItem> facebookFriendsItemList = new List<FacebookFriendsItem> ();
	public GameObject androidBackButton;

	void OnEnable () {
		loadingPanel.gameObject.SetActive (true);
		nofacebooklistText.text = TagConstant.PopUpMessages.NO_FRIEND_LIST_FB;
		androidBackButton.SetActive (false);
	}

	void OnDisable () {
		for (int i=0; i<facebookFriendsItemList.Count; i++)
		{
			Destroy (facebookFriendsItemList [i].gameObject);
		}
		facebookFriendsItemList.Clear ();

		loadingPanel.gameObject.SetActive (true);
		nofacebooklistText.gameObject.SetActive (false);
		androidBackButton.SetActive (true);
	}

	// Use this for initialization
	void Start () {
		closeButton.onClick.AddListener (OnClickCloseButton);
	}

	void OnClickCloseButton ()
	{
		this.gameObject.SetActive (false);
	}
	
	public void InitFacebookFriendsPanel ()
	{
		for (int i=0; i<facebookFriendsItemList.Count; i++)
		{
			Destroy (facebookFriendsItemList [i].gameObject);
		}
		facebookFriendsItemList.Clear ();

        /*if (SocialHandler.Instance.facebookFriendList.friends.data.Count > 0) {
			nofacebooklistText.gameObject.SetActive (false);
			for (int i = 0 ; i < SportsQuizManager.Instance.opponentList.Count; i++) {

				if (string.IsNullOrEmpty (SportsQuizManager.Instance.opponentList [i].facebook_id) == false) {

					FacebookFriendsItem fbItem = (FacebookFriendsItem)Instantiate (facebookFriendsItem);
					fbItem.InitFacebookFriendsItem (
						SportsQuizManager.Instance.opponentList [i].facebook_id,
						SportsQuizManager.Instance.opponentList [i].name,
						SportsQuizManager.Instance.opponentList [i].profileUrl,
						null);
					//						SportsQuizManager.Instance.opponentList [i].userProfile);
					fbItem.gameObject.RectTransformMakeChild (facebookFriendsItem.transform.parent);

					facebookFriendsItemList.Add (fbItem);
				}
			}
		} else {
			nofacebooklistText.gameObject.SetActive (true);
		}

		loadingPanel.gameObject.SetActive (false);*/
		StartCoroutine ("LoadAllFriends");

	}

	IEnumerator LoadAllFriends ()
	{
		if (SocialHandler.Instance.facebookFriendList.friends.data.Count > 0) {
			nofacebooklistText.gameObject.SetActive (false);
			List<SocialHandler.FacebookData> _dataList = SocialHandler.Instance.facebookFriendList.friends.data;
			for (int i = 0 ; i < _dataList.Count; i++) {
				Texture2D _imageTexture = null;
				WWW image = new WWW (_dataList [i].picture.data.url);
				yield return image;

				if (image.error == null) {
					_imageTexture = image.texture;
				} else {
					Q.Utils.QDebug.Log (image.error);
				}

				FacebookFriendsItem fbItem = (FacebookFriendsItem)Instantiate (facebookFriendsItem);
				fbItem.InitFacebookFriendsItem (
					_dataList[i].id,
					_dataList [i].name,
					_dataList [i].picture.data.url,
					_imageTexture);
				//						SportsQuizManager.Instance.opponentList [i].userProfile);
				fbItem.gameObject.RectTransformMakeChild (facebookFriendsItem.transform.parent);
				
				facebookFriendsItemList.Add (fbItem);
			}
		} else {
			nofacebooklistText.gameObject.SetActive (true);
		}

		loadingPanel.gameObject.SetActive (false);
	}
}

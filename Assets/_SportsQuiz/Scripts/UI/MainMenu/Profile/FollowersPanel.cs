﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FollowersPanel : MonoBehaviour {

	public Button closeButtom;
	public ScrollRect followersScrollRect;
	public FollowerItemPanel followerItemPanel;

	[Header("Unfollow PANEL")]
	public GameObject confirmUnfollowPanel;
	public Button yesButton;
	public Button noButton;
	public GameObject loadingObject;

	[HideInInspector]
	public List<FollowerItemPanel> m_followersItemList = new List<FollowerItemPanel> ();

	void Awake () {
	}

	void OnEnable ()
	{
		loadingObject.SetActive (true);
	}

	// Use this for initialization
	void Start () {
		closeButtom.onClick.AddListener (OnClickCloseButton);
		yesButton.onClick.AddListener (OnClickUnfollowYes);
		noButton.onClick.AddListener (onClickUnfollowNo);
	}

	public void SetFollowerItemPanel(FollowerItemPanel item)
	{
		followerItemPanel = item;
	}

	public void OnClickUnfollowYes()
	{
		followerItemPanel.UnfollowUser ();
	}

	void onClickUnfollowNo()
	{
		confirmUnfollowPanel.SetActive (false);
	}
	
	void OnClickCloseButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		for (int i = 0; i < m_followersItemList.Count; i++) {
			Destroy (m_followersItemList [i].gameObject);
		}

		m_followersItemList.Clear ();

		this.gameObject.SetActive (false);
	}

	public void OnClickUnFollowButton (FollowerItemPanel item)
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		m_followersItemList.Remove (item);
		Destroy (item.gameObject);
	}
}

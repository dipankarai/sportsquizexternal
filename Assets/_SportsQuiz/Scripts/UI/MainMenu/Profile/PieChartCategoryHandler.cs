﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PieChartCategoryHandler : MonoBehaviour 
{
	public GameObject CloneObject;
	private List<GameObject> cloneObjsList = new List<GameObject> ();
	public GameObject parentObj;

	private TotalScoreItem totalScoreItem;

	public void UpdateTotalScore(List<string> sportsNames, Color[] colorVal)
	{
		for (int i = 0; i < cloneObjsList.Count; i++) {
			Destroy (cloneObjsList[i]);
		}
		cloneObjsList.Clear ();

		for (int i = 0; i < sportsNames.Count; i++) {
			GameObject clone = Instantiate (CloneObject);
			cloneObjsList.Add (clone);
			clone.transform.SetParent (parentObj.transform, false);
			totalScoreItem = clone.GetComponent<TotalScoreItem> ();
			clone.SetActive (true);
			totalScoreItem.sportsType.text = sportsNames [i];
			totalScoreItem.imgColor.color = colorVal[i];
		}
	}
}
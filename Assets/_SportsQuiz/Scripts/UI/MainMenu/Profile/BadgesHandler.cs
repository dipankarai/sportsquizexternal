﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class BadgesHandler : MonoBehaviour 
{
//	public Image badgeImage;
	public Text badgeHeader;
	public Text badgeDescription;
	public Button badgeButton;
	public GameObject loadingImage;
	public int badgeId;

	public Image badgeImage { get { return m_badgeImage; } }
	private Image m_badgeImage;

	private System.Action<SportsQuizManager.Badges> callback;
	private SportsQuizManager.Badges m_badge;

	void Start ()
	{
		badgeButton.onClick.AddListener (OnClickBadgeButton);
	}

	public void InitBadgeHandler ()
	{
		GetBadgeDetail ();
	}

	void OnEnable ()
	{
		if (m_badgeImage == null) {
			m_badgeImage = GetComponent<Image> ();
		}

		if (m_badge != null) {
			if (References.GetImageFromLocal (m_badge.id, m_badge.url, References.eImageType.Badges) != null) {
				ViewBadge (References.GetImageFromLocal (m_badge.id, m_badge.url, References.eImageType.Badges));
			}
		}
	}

	public void InitBadgeHandler (int id, System.Action<SportsQuizManager.Badges> callback)
	{
		badgeId = id;
		this.callback = callback;

		if (m_badgeImage == null) {
			m_badgeImage = GetComponent<Image> ();
		}

		GetBadgeDetail ();
	}

	void GetBadgeDetail ()
	{
		m_badge = SportsQuizManager.Instance.GetBadge (badgeId);
		if (m_badge != null) {
			if (References.GetImageFromLocal(m_badge.id, m_badge.url, References.eImageType.Badges) == null) {
				if (this.gameObject.activeInHierarchy) {
					StartCoroutine (LoadBadge (m_badge.url));
				}
			} else {
//				this.gameObject.SetActive (true);
				ViewBadge (References.GetImageFromLocal(m_badge.id, m_badge.url, References.eImageType.Badges));
			}
			badgeHeader.text = m_badge.title;
			badgeDescription.text = m_badge.description;
		}
	}

	void ViewBadge (Texture2D texture)
	{
		m_badgeImage.color = Color.white;
		loadingImage.SetActive (false);
		badgeButton.interactable = true;
		m_badgeImage.CreateImageSprite (texture);
	}

	IEnumerator LoadBadge (string url)
	{
		WWW image = new WWW (url);
		yield return image;
		if (image.error == null) {
			
			References.SaveImageToLocal (m_badge.id, url, image.texture, References.eImageType.Badges);
			if (badgeImage != null) {
//				this.gameObject.SetActive (true);
				ViewBadge (image.texture);
			}
		} else {
			Q.Utils.QDebug.Log (image.error);
		}
	}

	void OnClickBadgeButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		callback (m_badge);
	}
}
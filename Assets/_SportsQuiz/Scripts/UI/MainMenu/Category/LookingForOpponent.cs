﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LookingForOpponent : MonoBehaviour {

	[SerializeField] private Transform loadingImage;
	[SerializeField] private Text titleText, decsriptionText;
	[SerializeField] private Button m_backButton, m_homeButton;
	private SeralizedClassServer.DuelQuizQuestionsList m_randomQuizData;
	private int m_previousRandomUser = 0;
	private int m_attemptMade = 0;
	private bool isDoneLoading;
	// Use this for initialization

	void OnEnable () {
		m_homeButton.gameObject.SetActive (false);
		GoogleAdHandler.Instance.ShowBanner ();
		decsriptionText.text = TagConstant.PopUpMessages.LOOKING_FOR_OPPONENT;
		isDoneLoading = false;
	}

	void Start () {
		m_backButton.onClick.AddListener (OnClickBackButton);
		m_homeButton.onClick.AddListener (OnClickHomeButton);
	}

	void OnClickBackButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		BackToCategory ();
	}

	void OnClickHomeButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (m_randomQuizData != null) {
			ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, GotoHomeScreenCallback);
			MainMenuPanelManager.Instance.WaitingPanel (true);
		} else {
			GotoHomeScreenCallback (true);
		}
	}

	public void BackToCategory ()
	{
		//m_backButton.gameObject.SetActive (false);
		if (m_randomQuizData != null) {
			ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, CancelCategoryCallback);
			MainMenuPanelManager.Instance.WaitingPanel (true);
		} else {
			CancelCategoryCallback (true);
		}
	}

	void CancelCategoryCallback (bool cancelled) {

		MainMenuPanelManager.Instance.WaitingPanel (false);
		if (cancelled) {
			this.gameObject.SetActive (false);

			if (SportsQuizManager.Instance.currentCategory == null ||
				SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 0) {

				MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.MainMenuPanel);
			} else {

				MainMenuPanelManager.Instance.categoryPanel.OnClickMainCategoryCommonButton ();
			}

			decsriptionText.text = TagConstant.PopUpMessages.LOOKING_FOR_OPPONENT;
			loadingImage.gameObject.SetActive (true);
			QuizManager.Instance.opponentStatus = null;
			StopCoroutine ("WaitForSecondsCallback");
		}
	}

	void GotoHomeScreenCallback (bool cancelled)
	{
		MainMenuPanelManager.Instance.WaitingPanel (false);
		if (cancelled) {
			this.gameObject.SetActive (false);

			MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.MainMenuPanel);

			decsriptionText.text = TagConstant.PopUpMessages.LOOKING_FOR_OPPONENT;
			loadingImage.gameObject.SetActive (true);
			QuizManager.Instance.opponentStatus = null;
			StopCoroutine ("WaitForSecondsCallback");
		}
	}

	public void LoadLuckyModeQuiz ()
	{
		Q.Utils.QDebug.Log ("LoadLuckyModeQuiz");
		titleText.text = SportsQuizManager.Instance.GetQuizModeLabel ();

		if (SportsQuizManager.Instance.currentSelectedTopics == null)
			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();

		SportsQuizManager.Instance.currentSelectedTopics.category_id = SportsQuizManager.Instance.currentCategory.mainCategorySelected;
		SportsQuizManager.Instance.currentSelectedTopics.category_title = SportsQuizManager.Instance.currentCategory.mainCategoryLabel;

		if (m_previousRandomUser != 0 && m_randomQuizData != null) {
			ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, NoOpponentCallback);
		}

		ServerManager.Instance.GetRandom (
			SportsQuizManager.Instance.currentCategory.mainCategorySelected,
			m_previousRandomUser,
			QuizQuestionLuckyMode);
	}

	void QuizQuestionLuckyMode (SeralizedClassServer.DuelQuizQuestionsList callback)
	{
		if (callback == null) {

			ShowingOpponentNotFound ();
		} else {

			Q.Utils.QDebug.LogError (callback.userQuizId);
			m_randomQuizData = callback;
			m_previousRandomUser = callback.opponent.user_id;
			SportsQuizManager.Instance.StoreCurrentOpponentInformation (callback.opponent);
			ServerManager.Instance.GetOpponentDetail (callback.opponent.user_id, GetOppnentDetailCallback);
		}
	}

	void GetOppnentDetailCallback (SeralizedClassServer.OpponentDetails callback)
	{
		if (callback != null) {
			
			SportsQuizManager.Instance.opponentInformation.email = callback.user_info.email_id;
			SportsQuizManager.Instance.opponentInformation.facebook_id = callback.user_info.facebook_id;
			SportsQuizManager.Instance.opponentInformation.google_id = callback.user_info.google_id;
			
			decsriptionText.text = TagConstant.PopUpMessages.REQUEST_SEND_OPPONENT;
			
			GetStatusUpdate ();
			waitingtime = Time.time;
		} else {
			
			ShowingOpponentNotFound ();
		}
	}

	void GetStatusUpdate ()
	{
		QuizManager.Instance.opponentStatus = StatusCallback;
		QuizManager.Instance.StartStatusUpdate(m_randomQuizData.userQuizId, GetStatusUpdateCallback);
	}

	void GetStatusUpdateCallback ()
	{
		Q.Utils.QDebug.Log ("GetStatusUpdateCallback");
		if (this.gameObject.activeSelf) {
			StartCoroutine ("WaitForSecondsCallback");
		} else {
			StopCoroutine ("WaitForSecondsCallback");
		}
	}

	float waitingtime;
	IEnumerator WaitForSecondsCallback ()
	{
		yield return new WaitForSeconds (2f);

		if (Time.time > (waitingtime + 5f)) {
			if (isDoneLoading == false) {
				waitingtime = 0;
				if (m_attemptMade < 1) {
					ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, LookingNewOpponent);
					//LoadLuckyModeQuiz ();
					m_attemptMade++;
				} else {
					ShowingOpponentNotFound ();
				}
			}
		} else {
			
			GetStatusUpdate ();
		}
	}

	void StatusCallback (SeralizedClassServer.Status callback)
	{
		Q.Utils.QDebug.LogError ("StatusCallback "+callback.user_id);

		if (callback.user_quiz_id == m_randomQuizData.userQuizId &&
			callback.user_id == SportsQuizManager.Instance.opponentInformation.serverID)
		{
			StopCoroutine ("WaitForSecondsCallback");
			isDoneLoading = true;

			SeralizedClassServer.StatusDataChallenge data = 
				Newtonsoft.Json.JsonConvert.DeserializeObject<SeralizedClassServer.StatusDataChallenge> (Newtonsoft.Json.JsonConvert.SerializeObject (callback.data));

			if (data.user_quiz_id == m_randomQuizData.userQuizId && 
				data.user_id == SportsQuizManager.Instance.opponentInformation.serverID)
			{
				if (callback.type == (int)ServerManager.eStatusUpdateType.ChallengeAccept)//data.message.Contains (TagConstant.ServerMessage.CHALLENGE_ACCEPT))
				{
					CancelInvoke("InvokeShowWaiting");
					QuizManager.Instance.LoadQuizData(m_randomQuizData);

					SportsQuizManager.Instance.opponentInformation.isOnline = true;
					isDoneLoading = false;
				}
				else
				{
					if (m_attemptMade < 1) {
						decsriptionText.text = TagConstant.PopUpMessages.REQUEST_REJECT_OPPONENT;
						ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, LookingNewOpponent);

						m_attemptMade++;
					} else {
						ShowingOpponentNotFound ();
					}
				}
			}
		}
	}

	void LookingNewOpponent (bool canceled)
	{
		decsriptionText.text = TagConstant.PopUpMessages.LOOKING_NEW_OPPONENT;
		isDoneLoading = false;
		LoadLuckyModeQuiz ();
	}

	void ShowingOpponentNotFound ()
	{
		m_attemptMade = 0;
		m_previousRandomUser = 0;
		loadingImage.gameObject.SetActive (false);
		decsriptionText.text = "";//TagConstant.PopUpMessages.NO_RANDOM_OPPONENT;

		if  (m_randomQuizData != null) {
			ServerManager.Instance.CancelChallenge (m_randomQuizData.userQuizId, NoOpponentCallback);
		}

		m_homeButton.gameObject.SetActive (true);
		MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_RANDOM_OPPONENT, GoBack);
		m_backButton.gameObject.SetActive (true);
		isDoneLoading = false;
	}

	void NoOpponentCallback (bool cancelled)
	{
		//BackToCategory ();
	}

	void GoBack ()
	{
		//CancelCategoryCallback (true);
	}
}

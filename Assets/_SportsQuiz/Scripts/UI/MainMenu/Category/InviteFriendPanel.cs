﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class InviteFriendPanel : MonoBehaviour {

	[SerializeField] private Button m_inviteFriendButton, m_backButton, m_homeButton;
	[SerializeField] private Text m_categoryTitleText, m_topicTitleText, m_descriptionText, m_topicTitle2Text;
	[SerializeField] private Image m_categoryImage;
	[SerializeField] private SelectFriendPanel m_SelectFriendPanel;

	[Header ("REQUESTING CHALLENGE")]
	[SerializeField] private GameObject m_requestingChallenge;
	[SerializeField] private Image m_playerImage, m_opponentImage;
	[SerializeField] private Text m_playerNameText, m_playerDetailsText, m_opponentNameText, m_opponentDetailsText, m_requestingChallengeText;

	[Header ("WAITING FINISH PANEL")]
	[SerializeField] private GameObject m_waitingFinishPanel;
	[SerializeField] private Text m_waitingDescriptionText;
	[SerializeField] private Button m_goFirstButton, m_inviteAnotherButton, m_categorySelectionButton, m_finishHomeButton;

	[SerializeField] private GameObject m_androidbackButon;
	private SeralizedClassServer.DuelQuizQuestionsList m_duelQuizData;
	private int m_previousOpponent;
	public bool RequestingChallenge {
		get {
			return m_requestingChallenge.gameObject.activeInHierarchy;
		}
	}

	void Awake ()
	{
		m_descriptionText.text = TagConstant.PopUpMessages.INVITE_FRIEND_FOR_CHALLENGE;
		m_SelectFriendPanel.inviteFriendPanel = this;
		SwitchSubPanel (0);
	}

	void OnEnable ()
	{
		GoogleAdHandler.Instance.ShowBanner ();
		SwitchSubPanel (0);

		if (SportsQuizManager.Instance.GetCurrentMainCategoryLogo () != null) {
			m_categoryImage.CreateImageSprite (SportsQuizManager.Instance.GetCurrentMainCategoryLogo ());
		}

		m_categoryTitleText.text = SportsQuizManager.Instance.currentCategory.mainCategoryLabel;
		m_topicTitleText.text = SportsQuizManager.Instance.currentSelectedTopics.category_title;
		m_topicTitle2Text.text = SportsQuizManager.Instance.currentSelectedTopics.category_title;

//		GetFriendList ();
		//InvokeRepeating ("GetFriendList", 0.01f, 5f);
	}

	public void SwitchSubPanel (int id)
	{
		switch (id)
		{
		case 0:
			m_SelectFriendPanel.gameObject.SetActive (false);
			m_requestingChallenge.gameObject.SetActive (false);
			m_androidbackButon.gameObject.SetActive (true);
			break;

		case 1:
			GoogleAdHandler.Instance.HideBanner ();
			m_SelectFriendPanel.gameObject.SetActive (true);
			m_androidbackButon.gameObject.SetActive (false);
			break;

		case 2:
			GoogleAdHandler.Instance.ShowBanner ();
			m_SelectFriendPanel.gameObject.SetActive (false);
			m_androidbackButon.gameObject.SetActive (true);
			break;
		}
	}

	void OnDisable ()
	{
		m_categoryImage = null;
//		CancelInvoke ("GetFriendList");
	}

	// Use this for initialization
	void Start () {

		m_inviteFriendButton.onClick.AddListener (OnClickInviteFriendButton);
		m_backButton.onClick.AddListener (OnClickBackButton);
		m_homeButton.onClick.AddListener (OnClickHomeButton);
		m_goFirstButton.onClick.AddListener (OnClickGoFirstButton);
		m_inviteAnotherButton.onClick.AddListener (OnClickInviteAnotherButton);
		m_categorySelectionButton.onClick.AddListener (OnClickCategorySelectionButton);
		m_finishHomeButton.onClick.AddListener (OnClickMainMenuButton);
	}

//	#if UNITY_EDITOR || UNITY_ANDROID
//	void Update ()
//	{
//		if (Input.GetKeyDown (KeyCode.Escape) && (m_SelectFriendPanel.gameObject.activeSelf == false)) {
//
//			MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
//		}
//	}
//	#endif

	public void GetFriendList ()
	{
		ServerManager.Instance.GetFollowers (FriendListCallback);
	}

	void FriendListCallback(bool isFriendListFetched)
	{
	}

	void OnClickInviteFriendButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		SwitchSubPanel (1);
	}

	void OnClickBackButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
	}

	void OnClickHomeButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.MainMenuPanel);
	}

	public void ChallengeAgainSameFriend ()
	{
		GetFriendDuelMode (SportsQuizManager.Instance.selectedDuelFriendDetail);
	}

	public void GetFriendDuelMode (SportsQuizManager.InviteFriendDetail selectedFriendDetail) {

        MainMenuPanelManager.Instance.WaitingPanel(true);

		if (SportsQuizManager.Instance.selectedDuelFriendDetail != null &&
			SportsQuizManager.Instance.selectedDuelFriendDetail.user_id == selectedFriendDetail.user_id) {
			SportsQuizManager.Instance.previousUserID = 0;
		}

		SportsQuizManager.Instance.selectedDuelFriendDetail = selectedFriendDetail;
  
//		CancelInvoke ("GetFriendList");

		SportsQuizManager.Instance.opponentInformation.name = SportsQuizManager.Instance.selectedDuelFriendDetail.name;

        GetDuelModeQuizData();
	}

    void GetDuelModeQuizData ()
    {
        ServerManager.Instance.GetDuelMode(
			SportsQuizManager.Instance.currentSelectedTopics.category_id, 
			SportsQuizManager.Instance.otherUSerType,
			QuizQuestionDuelCallback);
    }

	void QuizQuestionDuelCallback(SeralizedClassServer.DuelQuizQuestionsList callback)
	{
		if (callback == null) {

			string message = "Remind " + SportsQuizManager.Instance.opponentInformation.name +
			              " to participate in " + TagConstant.APP_NAME;

			MainMenuPanelManager.Instance.WaitingPanel (false);
			MainMenuPanelManager.Instance.ShowMessage (
				message,
				SendAppRequest,
				PopUpMessage.eMessageType.Normal);
		} else {
			
			m_duelQuizData = callback;
			SportsQuizManager.Instance.previousUserID = m_duelQuizData.opponent.user_id;
//			SportsQuizManager.Instance.GetOpponentDetail (callback.opponent.user_id, GetOppnentDetailCallback);
			ServerManager.Instance.GetOpponentDetail (callback.opponent.user_id, GetOppnentDetailCallback);
		}
	}

	void SendAppRequest ()
	{
		SocialHandler.Instance.SendAppRequest (SportsQuizManager.Instance.opponentInformation.facebook_id);
	}

	void GetOppnentDetailCallback (SeralizedClassServer.OpponentDetails callback)
	{
		SportsQuizManager.Instance.opponentInformation.email = callback.user_info.email_id;
		SportsQuizManager.Instance.opponentInformation.facebook_id = callback.user_info.facebook_id;
		SportsQuizManager.Instance.opponentInformation.google_id = callback.user_info.google_id;
		QuizQuestionInvite ();
	}

    void QuizQuestionInvite()
    {
        Invoke("GetStatusUpdate", 0.1f);

        waitingTime = Time.time;
        InvokeRepeating("InvokeShowWaiting", 0.1f, 1f);

		SportsQuizManager.Instance.StoreCurrentOpponentInformation (m_duelQuizData.opponent);

		m_playerNameText.text = "<color=" + TagConstant.ColorHexCode.DARK_YELLOW_1+ ">" + SportsQuizManager.Instance.playerInformation.name + "</color> ";
		m_playerDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			SportsQuizManager.Instance.playerInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country));
		
        SportsQuizManager.Instance.AssignUserProfileImage(m_playerImage);

		m_opponentNameText.text = "<color=" + TagConstant.ColorHexCode.DARK_YELLOW_1+ ">" + SportsQuizManager.Instance.opponentInformation.name + "</color> ";
		m_opponentDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			(SportsQuizManager.eGender)SportsQuizManager.Instance.opponentInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.opponentInformation.country));

		SportsQuizManager.Instance.LoadOpponentProfile (m_opponentImage);
//		SportsQuizManager.Instance.AssignOpponentProfileImage (
//			m_opponentImage, SportsQuizManager.Instance.opponentInformation.serverID);
		
		MainMenuPanelManager.Instance.WaitingPanel(false);
        m_requestingChallengeText.gameObject.SetActive(true);
        m_waitingFinishPanel.SetActive(false);
        m_SelectFriendPanel.gameObject.SetActive(false);
        m_requestingChallenge.SetActive(true);
    }

    void GetStatusUpdate ()
    {
        QuizManager.Instance.opponentStatus = StatusCallback;
		QuizManager.Instance.StartStatusUpdate(m_duelQuizData.userQuizId, GetStatusUpdateCallback);
    }

	void GetStatusUpdateCallback ()
	{
		Q.Utils.QDebug.Log ("GetStatusUpdateCallback");
		if (this.gameObject.activeInHierarchy)
			StartCoroutine ("WaitForSecondsCallback");
	}

	IEnumerator WaitForSecondsCallback ()
	{
		yield return new WaitForSeconds (2f);
		GetStatusUpdate ();
	}

    void StatusCallback (SeralizedClassServer.Status callback)
    {
		Q.Utils.QDebug.Log ("StatusCallback "+callback.user_id);
        if (callback.user_quiz_id == m_duelQuizData.userQuizId &&
			callback.user_id == SportsQuizManager.Instance.opponentInformation.serverID)
        {
			StopCoroutine ("WaitForSecondsCallback");

			SeralizedClassServer.StatusDataChallenge data = 
				Newtonsoft.Json.JsonConvert.DeserializeObject<SeralizedClassServer.StatusDataChallenge> (Newtonsoft.Json.JsonConvert.SerializeObject (callback.data));

			if (data.user_quiz_id == m_duelQuizData.userQuizId &&
				data.user_id == SportsQuizManager.Instance.opponentInformation.serverID)
			{
				Q.Utils.QDebug.Log ("StatusCallback "+data.message);
				if (callback.type == (int)ServerManager.eStatusUpdateType.ChallengeAccept)//data.message.Contains (TagConstant.ServerMessage.CHALLENGE_ACCEPT))
				{
					waitingTime = 0;
					CancelInvoke("InvokeShowWaiting");
					GoogleAdHandler.Instance.HideBanner ();
					m_waitingFinishPanel.SetActive(true);
					m_requestingChallengeText.gameObject.SetActive(false);
					QuizManager.Instance.LoadQuizData(m_duelQuizData);

					SportsQuizManager.Instance.opponentInformation.isOnline = true;
					SportsQuizManager.Instance.previousUserID = 0;
				}
				else
				{
					MainMenuPanelManager.Instance.ShowMessage(TagConstant.PopUpMessages.CHALLENGE_REJECTED,
						MainMenuPanelManager.eMainMenuState.Unknown,
						PopUpMessage.eMessageType.Warning);
					
					SwitchSubPanel (1);
					m_requestingChallenge.SetActive(false);
				}
			}
        }
    }

    private string dot = ".";
	private float waitingTime = 0;
	void InvokeShowWaiting ()
	{
		if (waitingTime + 30f < Time.time)
		{
			waitingTime = 0;

			CancelInvoke ("InvokeShowWaiting");
			CancelInvoke("GetStatusUpdate");

			QuizManager.Instance.opponentStatus = null;
			m_waitingDescriptionText.text = "";//TagConstant.PopUpMessages.FRIEND_NOT_AVAILABLE;
			m_waitingFinishPanel.SetActive (true);
			m_requestingChallengeText.gameObject.SetActive (false);

			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.FRIEND_NOT_AVAILABLE);

			return;
		}

		m_requestingChallengeText.text = dot;
		dot += ".";
		if (dot.Length > 10)
			dot = ".";
	}

	void OnClickGoFirstButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		QuizManager.Instance.LoadQuizData (m_duelQuizData);
		SportsQuizManager.Instance.previousUserID = 0;
	}

	void OnClickInviteAnotherButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		QuizManager.Instance.opponentStatus = null;
		StopCoroutine ("WaitForSecondsCallback");
		ServerManager.Instance.CancelChallenge (m_duelQuizData.userQuizId, CancelCategoryCallback);
		SwitchSubPanel (1);
		m_requestingChallenge.SetActive (false);
		m_waitingFinishPanel.SetActive (false);

//		InvokeRepeating ("GetFriendList", 0.01f, 5f);
//		GetFriendList ();
	}

	void CancelCategoryCallback (bool cancelled) {
	}

	void OnClickCategorySelectionButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.categoryPanel.OnClickMainCategoryCommonButton ();

		StopCoroutine ("WaitForSecondsCallback");
		ServerManager.Instance.CancelChallenge (m_duelQuizData.userQuizId, CancelCategoryCallback);
	}

	void OnClickMainMenuButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);

		StopCoroutine ("WaitForSecondsCallback");
		ServerManager.Instance.CancelChallenge (m_duelQuizData.userQuizId, CancelCategoryCallback);
	}
}

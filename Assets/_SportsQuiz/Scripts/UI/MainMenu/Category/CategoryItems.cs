﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CategoryItems : MonoBehaviour {

	public LayoutElement layoutElement;
	public RectTransform canvasRectTransform;
	public List<CategoryItem> categoryItemsList = new List<CategoryItem> ();

	void Awake () {
//		if (categoryItemsList.Count == 0) {
//			GetComponentsInChildren<CategoryItem> (categoryItemsList);
//		}

//		if (layoutElement != null && canvasRectTransform!= null) {
//			layoutElement.minWidth = canvasRectTransform.sizeDelta.x;
//		}
	}

	void OnEnable () {

		for (int i=0; i < categoryItemsList.Count; i++) {
			categoryItemsList [i].gameObject.SetActive (false);
		}
	}

	public CategoryItem GetCategoryItem ()
	{
		for (int i=0; i < categoryItemsList.Count; i++) {
			if (categoryItemsList [i].gameObject.activeInHierarchy == false)
				return categoryItemsList [i];
		}

		return null;
	}

	// Use this for initialization
	void Start () {
	
	}
}

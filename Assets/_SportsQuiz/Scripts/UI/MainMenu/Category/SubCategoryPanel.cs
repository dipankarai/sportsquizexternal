﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class SubCategoryPanel : MonoBehaviour {

	public CategoryScrollPanel categoryScrollPanel;
	public Button chooseCategoryButton;
	public Button backButton;
	public Button homeButton;
	public Text title;
	public Image categoryImage;

	void OnEnable () {
		InitSubCategory ();
//		SportsQuizManager.Instance.GetCurrentMainCategoryLogo (categoryImage);
	}

	void OnDisable ()
	{
//		categoryImage.sprite = null;
	}

	// Use this for initialization
	void Start () {
		chooseCategoryButton.onClick.AddListener (OnClickSubCategoryButton);
		backButton.onClick.AddListener (OnClickBackButton);
		homeButton.onClick.AddListener (OnClickHomeButton);
	}

	void OnClickBackButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.categoryPanel.OnClickBackCommonButton ();
	}

	void OnClickHomeButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.MainMenuPanel);
//		MainMenuPanelManager.Instance.categoryPanel.OnClickMainCategoryCommonButton ();
	}

	void OnClickSubCategoryButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (InternetConnection.Check) {
			MainMenuPanelManager.Instance.WaitingPanel (true);
			ConnectionHandler.Instance.OnClickCategoryButton ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void InitSubCategory ()
	{
		title.text = SportsQuizManager.Instance.currentSelectedCategory[SportsQuizManager.Instance.currentSelectedCategory.Count - 1].category_title;
		categoryImage.CreateImageSprite (SportsQuizManager.Instance.GetCurrentMainCategoryLogo ());
//		SportsQuizManager.Instance.GetCurrentMainCategoryLogo (categoryImage);
	}
}

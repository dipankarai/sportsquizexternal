﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectFriendPanel : MonoBehaviour {

	[HideInInspector] public InviteFriendPanel inviteFriendPanel;

	[SerializeField] private Toggle followerToggle, facebookToggle, googleToggle;
	[SerializeField] private RectTransform followerPanel, facebookPanel, googlePanel;
	[SerializeField] private Button sendInviteButton, closeButton;
	[SerializeField] private Text followerText, facebookText, googleText;
	[SerializeField] private GameObject loadingPanel;

	public FriendList followerFriendList;
	public FriendList facebookFriendList;
	public FriendList googleFriendList;

	public SportsQuizManager.InviteFriendDetail SelectedUser { set { m_currentUserSelected = value; } }
	private SportsQuizManager.InviteFriendDetail m_currentUserSelected;

	void OnEnable () {
		loadingPanel.SetActive (true);
		GetFriendList ();
	}

	public void GetFriendList ()
	{
		ServerManager.Instance.GetFollowers (FriendListCallback);
	}

	void FriendListCallback(bool isFriendListFetched)
	{
		loadingPanel.SetActive (false);
		followerFriendList.GetFriendList ();
	}

	// Use this for initialization
	void Start () {

		followerFriendList.selectFriendPanel = this;
		facebookFriendList.selectFriendPanel = this;
		googleFriendList.selectFriendPanel = this;

		followerToggle.onValueChanged.AddListener (OnValueChangedFollowerToggle);
		facebookToggle.onValueChanged.AddListener (OnValueChangedFacebookToggle);
		googleToggle.onValueChanged.AddListener (OnValueChangedGooglePlusToggle);
		sendInviteButton.onClick.AddListener (OnClickSenInviteButton);
		closeButton.onClick.AddListener (OnClickCloseButton);
	}


	void OnValueChangedFollowerToggle (bool isOn)
	{
		Q.Utils.QDebug.Log ("FollowerToggle " + isOn);
		followerPanel.gameObject.SetActive (isOn);
		followerText.enabled = isOn;
	}

	void OnValueChangedFacebookToggle (bool isOn)
	{
		Q.Utils.QDebug.Log ("FacebookToggle " + isOn);
		facebookPanel.gameObject.SetActive (isOn);
		facebookText.enabled = isOn;
	}

	void OnValueChangedGooglePlusToggle (bool isOn)
	{
		Q.Utils.QDebug.Log ("GooglePlusToggle " + isOn);
		googlePanel.gameObject.SetActive (isOn);
		googleText.enabled = isOn;
	}

	void OnClickSenInviteButton ()
	{
		if (m_currentUserSelected == null)
			return;

		inviteFriendPanel.GetFriendDuelMode (m_currentUserSelected);
	}

	void OnClickCloseButton ()
	{
		gameObject.SetActive (false);
	}

	void OnDisable ()
	{
		followerFriendList.OnGameobjectDisable ();
		facebookFriendList.OnGameobjectDisable ();
		googleFriendList.OnGameobjectDisable ();
	}
}

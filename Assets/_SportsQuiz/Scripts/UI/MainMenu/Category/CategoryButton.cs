﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CategoryButton : MonoBehaviour {

	private int m_buttonId;
	private Button m_button;
	public System.Action<int> callback;

	void Awake () {
		m_button = GetComponent<Button> ();
	}

	// Use this for initialization
	void Start () {
		m_button.onClick.AddListener (OnClickButton);
	}

	public void InitCategoryButton (int id, System.Action<int> _callback)
	{
		m_buttonId = id;
		callback = _callback;
	}

	void OnClickButton ()
	{
		callback (m_buttonId);
	}

}

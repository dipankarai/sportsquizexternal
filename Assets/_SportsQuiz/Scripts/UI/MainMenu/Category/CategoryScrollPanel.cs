﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class CategoryScrollPanel : MonoBehaviour {

	private enum eCategoryType
	{
		UnKnown = 0,
		MainCategory,
		RandomTopics,
		Category
	}

	public Text topicTypeHeaderText;
	public ScrollRect scrollRect;
	public CategoryItems categoryItems;
	public LockedButton lockedButton;
	/*[HideInInspector] public bool isRandomList = true;*/

	private List<CategoryItems> categoryItemsList = new List<CategoryItems> ();
//	private float contentWidth;
	private eCategoryType m_currentCategoryType;

	void Awake () {
//		contentWidth = MainMenuPanelManager.Instance.maincanvas.GetComponent<RectTransform> ().sizeDelta.x;
	}

	private List<SeralizedClassServer.Category> m_currentCategory = new List<SeralizedClassServer.Category>();
	private List<SeralizedClassServer.Topics> m_currentTopics = new List<SeralizedClassServer.Topics> ();

	void OnEnable () {

		scrollRect.horizontalNormalizedPosition = 0f;
//		categoryItems.layoutElement.minWidth = contentWidth;
		if (topicTypeHeaderText != null) {
			topicTypeHeaderText.text = "";
		}

		if (SportsQuizManager.Instance.currentCategory == null)
			return;

		int _listCount = SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count;
		if (_listCount == 1)
			m_currentCategoryType = eCategoryType.MainCategory;
		else if (_listCount == 2) {
			#if EVENT_QUIZ
			m_currentCategoryType = eCategoryType.Category;
			#else
			m_currentCategoryType = eCategoryType.RandomTopics;
			#endif
		} else if (_listCount > 2) {
			m_currentCategoryType = eCategoryType.Category;
		}

		ShowScrollItem ();
	}

	// Use this for initialization
	void Start () {

	}

	void OnDisable ()
	{
		m_currentCategoryType = eCategoryType.UnKnown;
		ResetCategoryItem ();
	}

	void ResetCategoryItem ()
	{
		for (int i = 0; i < categoryItemsList.Count; i++)
		{
			Destroy (categoryItemsList [i].gameObject);
		}

		categoryItemsList.Clear ();
		Resources.UnloadUnusedAssets ();
	}

	public void OnClickBack ()
	{
		Q.Utils.QDebug.Log ("OnClickBack "+m_currentCategoryType);
		ResetCategoryItem ();

		if (m_currentCategoryType == eCategoryType.Category) {
			m_currentCategoryType = eCategoryType.RandomTopics;
		}
		else if (m_currentCategoryType == eCategoryType.RandomTopics) {
			
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.
			RemoveAt (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count - 1);

			m_currentCategoryType = eCategoryType.MainCategory;
		}

		ShowScrollItem ();
	}

	public void OnClickSubCategory ()
	{
		Q.Utils.QDebug.Log ("OnClickSubCategory");
		ResetCategoryItem ();

		m_currentCategoryType = eCategoryType.Category;

		ShowScrollItem ();
	}

	public void ShowScrollItem ()
	{
		if (m_lockedButtonList.Count > 0) {
			for (int i = 0; i < m_lockedButtonList.Count; i++) {

				if (m_lockedButtonList [i] == null)
					continue;

				if (m_lockedButtonList [i].gameObject == null)
					continue;
				else if (m_lockedButtonList [i].gameObject != null)
					Destroy (m_lockedButtonList [i].gameObject);
			}

			m_lockedButtonList.Clear ();
		}

		int _listCount = SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count;

		Q.Utils.QDebug.Log (m_currentCategoryType);

		if (_listCount == 1) {
			MainMenuPanelManager.Instance.categoryPanel.subCategoryPanel.chooseCategoryButton.gameObject.SetActive (false);
		} else if (_listCount == 2) {
			#if EVENT_QUIZ
			MainMenuPanelManager.Instance.categoryPanel.subCategoryPanel.chooseCategoryButton.gameObject.SetActive (false);
			#else
			MainMenuPanelManager.Instance.categoryPanel.subCategoryPanel.chooseCategoryButton.gameObject.SetActive (true);
			#endif
		}
		else {
			MainMenuPanelManager.Instance.categoryPanel.subCategoryPanel.chooseCategoryButton.gameObject.SetActive (false);
		}

		LoadCategory (_listCount);
	}

	void LoadCategory (int count)
	{
		if (SportsQuizManager.Instance.currentCategory == null ||
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 0) {

			return;
		}
		
		m_currentCategory = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [count - 1].categoryList;

		for (int i = 0; i < m_currentCategory.Count; i++) {

			if (categoryItemsList.Count == 0) {
				CategoryItems _tempCat = InstantiateCategory ();

				CategoryItem _temp_CatItem = _tempCat.GetCategoryItem ();
				InitCategoryItem (_temp_CatItem, m_currentCategory [i]);
				categoryItemsList.Add (_tempCat);
			}
			else {

				CategoryItem _temp_CatItem = categoryItemsList [categoryItemsList.Count - 1].GetCategoryItem ();

				if (_temp_CatItem != null) {
					InitCategoryItem (_temp_CatItem, m_currentCategory [i]);
					_temp_CatItem.gameObject.SetActive (true);
				}
				else{
					CategoryItems _tempCat = InstantiateCategory ();
					_temp_CatItem = _tempCat.GetCategoryItem ();
					InitCategoryItem (_temp_CatItem, m_currentCategory [i]);
					categoryItemsList.Add (_tempCat);
				}

				if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GuestUser)
					ApplyLockButton (_temp_CatItem);
			}
		}

		if (topicTypeHeaderText != null){
			if (m_currentCategoryType == eCategoryType.RandomTopics) {
				topicTypeHeaderText.text = TagConstant.PopUpMessages.SELECT_ANY_TOPIC;
			} else if (m_currentCategoryType == eCategoryType.Category) {
				int _catType = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [count - 1].categoryList[0].type;
				if (_catType == 1) {
					topicTypeHeaderText.text = TagConstant.PopUpMessages.SELECT_CATEGORY;
				} else {
					topicTypeHeaderText.text = TagConstant.PopUpMessages.SELECT_TOPIC;
				}
			}
		}
		scrollRect.horizontalNormalizedPosition = 0f;
	}

	[SerializeField] private List<LockedButton> m_lockedButtonList = new List<LockedButton> ();
	void ApplyLockButton (CategoryItem item)
	{
		Button _button = item.GetComponentInChildren<Button> ();
		LockedButton _lockButton = (LockedButton) Instantiate (lockedButton);
		m_lockedButtonList.Add (_lockButton);

		TransformUtils.RectTransformChildSetLocal (_lockButton.transform, _button.transform);

		_lockButton.transform.SetParent (_button.transform);
	}

	CategoryItems InstantiateCategory ()
	{
		CategoryItems _tempCat = (CategoryItems)Instantiate (categoryItems);
		_tempCat.gameObject.RectTransformMakeChild (scrollRect.content.transform);

		return _tempCat;
	}

	void InitCategoryItem (CategoryItem _temp_CatItem, SeralizedClassServer.Category category)
	{
		_temp_CatItem.SetItemParameter (category, null);
		_temp_CatItem.gameObject.SetActive (true);
	}

	void InitTopicItem (CategoryItem _temp_CatItem, SeralizedClassServer.Topics topic)
	{
		_temp_CatItem.SetItemParameter (topic, null);
		_temp_CatItem.gameObject.SetActive (true);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class SponseredQuizPanel : MonoBehaviour {

	[SerializeField] private Image sponseredImage;
	[SerializeField] private Text sponseredTitleText;

	[SerializeField] private GameObject loadingObject;
	[SerializeField] private GameObject sponseredObject;

	[SerializeField] private Button categoryButton;

	private int m_category_ID;
	private int m_type;
	private string m_category_title;
	private string m_image;

	void OnEnable () {
		CheckForSponsoredQuiz ();
		SponseredQuiz.Instance.sponseredLoadedCallback = CheckForSponsoredQuiz;
	}

	void OnDisable () {
		SponseredQuiz.Instance.sponseredLoadedCallback = null;
	}

	// Use this for initialization
	void Start () {
		categoryButton.onClick.AddListener (OnClickSponsoredQuiz);
	}

	void CheckForSponsoredQuiz ()
	{
		if (SponseredQuiz.Instance.sponseredCategoryCategory == null) {
			loadingObject.SetActive (true);
			sponseredObject.SetActive (false);

			Invoke ("CheckForSponsoredQuiz", 10f);

		} else {

			CancelInvoke ("CheckForSponsoredQuiz");

			m_category_ID = SponseredQuiz.Instance.sponseredCategoryCategory.category_id;
			m_type = SponseredQuiz.Instance.sponseredCategoryCategory.type;
			m_category_title = SponseredQuiz.Instance.sponseredCategoryCategory.category_title;
			sponseredTitleText.text = m_category_title;
			m_image = SponseredQuiz.Instance.sponseredCategoryCategory.image;

			if (SponseredQuiz.Instance.sponseredCategoryCategory.texture != null) {
				
				sponseredImage.CreateImageSprite (SponseredQuiz.Instance.sponseredCategoryCategory.texture);
			}

			loadingObject.SetActive (false);
			sponseredObject.SetActive (true);
		}
	}

	void OnClickSponsoredQuiz ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (SponseredQuiz.Instance.sponseredCategoryCategory == null) {
			Application.OpenURL (TagConstant.AppStoreLink.SPONSORED_QUIZ_LINK);
		} else {
			SportsQuizManager.Instance.currentCategory.mainCategorySelected = m_category_ID;
			SportsQuizManager.Instance.currentCategory.mainCategoryLabel = m_category_title;
			
			SportsQuizManager.Instance.currentCategory.topicLabel = m_category_title;
			
			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();
			SportsQuizManager.Instance.currentSelectedTopics.category_id = m_category_ID;
			SportsQuizManager.Instance.currentSelectedTopics.category_title = m_category_title;
			SportsQuizManager.Instance.currentSelectedTopics.image = m_image;
			SportsQuizManager.Instance.currentSelectedTopics.type = m_type;
			
			if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {
				
				MainMenuPanelManager.Instance.categoryPanel.OpenFriendSelectionPanel ();
				
			} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {
				
				MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.gameObject.SetActive (true);
				MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.LoadLuckyModeQuiz ();
			} else {
				QuizManager.Instance.LoadQuiz ();
			}
		}
	}

}

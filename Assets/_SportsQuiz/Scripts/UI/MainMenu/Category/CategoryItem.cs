﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class CategoryItem : MonoBehaviour {

	public Image icon;
	public Text label;
	public Button categoryButton;
	[SerializeField]private Image[] imageShapeArray;
	private bool m_isTopic = false;

	private SeralizedClassServer.Category m_category;
	private SeralizedClassServer.Topics m_topic;

	void Awake () {
		InitVariable ();
	}

	// Use this for initialization
	void Start () {
		categoryButton.onClick.AddListener (OnClickItemButton);
	}

	void InitVariable ()
	{
		if (icon == null)
			icon = GetComponentInChildren<Image> ();
		if (label == null)
			label = GetComponentInChildren<Text> ();
	}

	void LoadImageIcon ()
	{
		if (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (m_category.category_id) != null)
		{
			icon.CreateImageSprite (SportsQuizManager.Instance.GetCurrentMainCategoryLogoByID (m_category.category_id));
		}
	}

	public void SetItemParameter (SeralizedClassServer.Category category, Texture2D cat_texture = null)
	{
		m_category = new SeralizedClassServer.Category ();
		m_category = category;

		if (m_category != null)
		{
			if (m_category.type == 1 && imageShapeArray.Length > 0) {
				ToogleIcon (1);
			} else if (m_category.type == 2 & imageShapeArray.Length > 0) {
				ToogleIcon (2);
			} else if (m_category.status == (int)CategoryPanel.eCategoryStatus.NonActive) {
				//MainMenuPanelManager.Instance.ShowMessage("Coming soon...");
				//return;
			}
		}

		if (category.type == 1 && SportsQuizManager.Instance.selectedMode != QuizManager.eQuizMode.IMFLucky)
			m_isTopic = false;
		else {
			m_topic = new SeralizedClassServer.Topics ();
			m_topic.category_id = category.category_id;
			m_topic.category_title = category.category_title;
			m_topic.image = category.image;
			m_topic.type = category.type;
			m_isTopic = true;
		}

		InitVariable ();
		LoadImageIcon ();

		label.text = category.category_title;
	}

	void ToogleIcon (int type)
	{
		Debug.Log ("ToogleIcon "+type);
		int _listCount = SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count;
		switch (type)
		{
		case 1:

			if (_listCount == 3) {
				imageShapeArray [0].enabled = true;
				imageShapeArray [1].enabled = true;
				imageShapeArray [2].enabled = false;
			} else {
				imageShapeArray [0].enabled = false;
				imageShapeArray [1].enabled = false;
				imageShapeArray [2].enabled = true;
			}

			break;
		case 2:
			imageShapeArray [0].enabled = true;
			imageShapeArray [1].enabled = false;
			imageShapeArray [2].enabled = false;
			break;
		case 3:
			break;
		}
	}

	public void SetItemParameter (SeralizedClassServer.Topics topics, Texture2D cat_texture = null)
	{
		m_topic = new SeralizedClassServer.Topics ();
		m_topic = topics;

		m_isTopic = true;

		InitVariable ();

		label.text = topics.category_title;
	}

	void OnClickItemButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		if (m_category != null && m_category.type == 1 && m_category.status == (int)CategoryPanel.eCategoryStatus.NonActive)
		{
			//MainMenuPanelManager.Instance.ShowMessage("Coming soon...");
			//return;
		}

		if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {
			SportsQuizManager.Instance.currentCategory.mainCategorySelected = m_category.category_id;
			SportsQuizManager.Instance.currentCategory.mainCategoryLabel = m_category.category_title;
		}

		if (m_isTopic == true) {

			SportsQuizManager.Instance.currentCategory.topicLabel = m_topic.category_title;

			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();
			SportsQuizManager.Instance.currentSelectedTopics = m_topic;

			if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {

				MainMenuPanelManager.Instance.categoryPanel.OpenFriendSelectionPanel ();

			} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {

				MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.gameObject.SetActive (true);
				MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.LoadLuckyModeQuiz ();
			} else {
				QuizManager.Instance.LoadQuiz ();
			}
		}
		else {

			MainMenuPanelManager.Instance.WaitingPanel (true);

			//SportsQuizManager.Instance.currentSelectedCategory = new List<SeralizedClassServer.Category> ();
			SportsQuizManager.Instance.currentSelectedCategory.Add (m_category);

			ConnectionHandler.Instance.OnClickCategoryButton ();
		}
	}
}

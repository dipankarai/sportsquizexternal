﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CategoryPanel : MonoBehaviour {

	public enum eCategoryStatus
	{
		Active = 1,
		NonActive = 6
	}

	public CategoryScrollPanel categoryScrollRectPanel;
	public LookingForOpponent lookingForOpponentPanel;
	public GameObject mainCategoryPanel;
	public SubCategoryPanel subCategoryPanel;
	public InviteFriendPanel inviteFriendPanel;
	public Button mainMenuButton;
	private bool m_replayWithSameUser = false;

	void OnEnable () {
		Q.Utils.QDebug.Log ("OnEnable CategoryPanel");

		if (m_replayWithSameUser == false)
			OpenMainCategory ();
		else
			m_replayWithSameUser = false;
		
		GoogleAdHandler.Instance.ShowBanner ();
		SportsQuizManager.Instance.previousQuizList.Clear ();
	}

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
		OpenMainCategory ();
	}

	// Use this for initialization
	void Start () {
		mainMenuButton.onClick.AddListener (OnClickMainButton);
	}

	public void OpenSubCategory () {

		if (subCategoryPanel.gameObject.activeSelf == true) {
			subCategoryPanel.gameObject.SetActive (false);
			ReOpenSubCategory ();
		}
		else
			subCategoryPanel.gameObject.SetActive (true);

		mainCategoryPanel.SetActive (false);
		inviteFriendPanel.gameObject.SetActive (false);
	}

	void ReOpenSubCategory () {
		subCategoryPanel.gameObject.SetActive (true);
	}

	public void OpenMainCategory () {
		mainCategoryPanel.SetActive (true);
		subCategoryPanel.gameObject.SetActive (false);
		inviteFriendPanel.gameObject.SetActive (false);
		lookingForOpponentPanel.gameObject.SetActive (false);
	}

	public void OpenFriendSelectionPanel ()
	{
		mainCategoryPanel.SetActive (false);
		subCategoryPanel.gameObject.SetActive (false);
		inviteFriendPanel.gameObject.SetActive (true);
		lookingForOpponentPanel.gameObject.SetActive (false);
	}

	public void OpenChallengeRadomFriend ()
	{
		m_replayWithSameUser = true;
		mainCategoryPanel.SetActive (true);
		subCategoryPanel.gameObject.SetActive (false);
		inviteFriendPanel.gameObject.SetActive (false);
		lookingForOpponentPanel.gameObject.SetActive (true);
		MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.LoadLuckyModeQuiz ();
	}

	public void OpenChallengeSameFriendPanel ()
	{
		m_replayWithSameUser = true;
		mainCategoryPanel.SetActive (false);
		subCategoryPanel.gameObject.SetActive (false);
		inviteFriendPanel.gameObject.SetActive (true);

		inviteFriendPanel.ChallengeAgainSameFriend ();
	}
	
	void OnClickMainButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.MainMenuPanel);
	}

	public void OnClickBackCommonButton ()
	{
		if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {
			MainMenuPanelManager.Instance.categoryPanel.OpenMainCategory ();
			SportsQuizManager.Instance.currentSelectedCategory = new List<SeralizedClassServer.Category> ();
		}
		else {
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.
			RemoveAt (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count - 1);

			if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count > 2 ) {
				
				SportsQuizManager.Instance.currentSelectedCategory.
				RemoveAt (SportsQuizManager.Instance.currentSelectedCategory.Count - 1);
			}

			if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {
				MainMenuPanelManager.Instance.categoryPanel.OpenMainCategory ();
			} else {
				MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
			}
		}
	}

	public void OnClickGoToRandomTopicScreen ()
	{
		int _lastCount = SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count;
		_lastCount -= 2;
			
		if (_lastCount > 0) {
			
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.
			RemoveRange (2, _lastCount);

			SportsQuizManager.Instance.currentSelectedCategory.
			RemoveRange (2, SportsQuizManager.Instance.currentSelectedCategory.Count);
		}

		MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
	}

	public void OnClickMainCategoryCommonButton ()
	{
		if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count > 1) {
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList.
			RemoveRange (1, SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count - 1);

			SportsQuizManager.Instance.currentSelectedCategory.
			RemoveRange (1, SportsQuizManager.Instance.currentSelectedCategory.Count - 1);
		}

		MainMenuPanelManager.Instance.categoryPanel.OpenMainCategory ();
	}
}

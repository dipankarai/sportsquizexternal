﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class InviteNotification : MonoBehaviour {

    [SerializeField]
    private Button acceptButton, rejectButton;
    [SerializeField]
	private Text description;// username
    [SerializeField]
    private Image profile;

    private NotificationItem m_itemMain;

    // Use this for initialization
    void Start()
    {
        acceptButton.onClick.AddListener(OnClickAcceptQuiz);
        rejectButton.onClick.AddListener(OnClickRejectQuiz);
    }

    public void InitInviteNotification(NotificationItem itemMain)
    {
        m_itemMain = itemMain;
		description.text = m_itemMain.m_message;
    }

    public void UpdateInfo()
    {
//        username.text = m_itemMain.opponentDetails.user_info.name;
        int _avatar = m_itemMain.opponentDetails.user_info.avatar_type;
        if (_avatar == 0)
        {

        }
        else {
            profile.CreateImageSprite(SportsQuizManager.Instance.references.GetAvatarImage(_avatar));
        }
    }

    void OnClickAcceptQuiz()
    {
		m_itemMain.OnClickAcceptButton ();
    }

    void OnClickRejectQuiz()
    {
		m_itemMain.OnClickRejectButton ();
    }
}

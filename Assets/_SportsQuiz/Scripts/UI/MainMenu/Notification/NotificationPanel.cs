﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class NotificationPanel : MonoBehaviour {

	public float speed = 0.3f;
	public float delay = 0.1f;
	public Button clearAllNotificationButton;
	public ScrollRect notificationScrollRect;
    public NotificationItem notificationItem;
	[SerializeField] private Text m_NoNotification;

    private List<NotificationItem> m_notificationItemList = new List<NotificationItem>();
    private List<int> m_removedNotification = new List<int>();
	private bool m_isClearedAll = false;

    void OnEnable()
    {
		InvokeRepeating ("InitNotification", 0.1f, 2f);
		GoogleAdHandler.Instance.ShowBanner ();
    }

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
		CancelInvoke ("InitNotification");
		m_isClearedAll = false;
		ClearForMemory ();
	}

    // Use this for initialization
    void Start() {
		clearAllNotificationButton.onClick.AddListener (OnClickClearAllNotification);
    }

	void OnClickClearAllNotification ()
	{
		if (m_notificationItemList.Count > 0) {
			if (InternetConnection.Check) {
				ServerManager.Instance.ClearNotification (
					NotificationItem.eNotificationClearType.ClearAll,
					DeleteFromServerCallback);
				
				SlideAllNotificationLeft ();
			} else {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
			}
		}
	}

	void SlideAllNotificationLeft ()
	{
		m_isClearedAll = true;
		List<NotificationItem> _tempNotificationList =  new List<NotificationItem> (m_notificationItemList);
		float _delay = 0.0f;
		_tempNotificationList.Reverse ();
		for (int i = 0; i < _tempNotificationList.Count; i++) {
			Vector2 _tempPosition = _tempNotificationList [i].transform.position;
			_tempPosition.x += (float)Screen.width;
			LeanTween.move (
				_tempNotificationList[i].activePanel.gameObject,
				_tempPosition,
				speed)
				.setOnComplete (_tempNotificationList [i]
					.OnCompleteLeenTween).setDelay (_delay);

			_delay += delay;
		}
	}

	void DeleteFromServerCallback (bool success)
	{
		if (success) {
			m_notificationItemList.Clear ();
			m_removedNotification.Clear ();
			SportsQuizManager.Instance.notification = null;
			Resources.UnloadUnusedAssets ();
			m_NoNotification.gameObject.SetActive (true);
		} else {
			m_notificationItemList.Clear ();
			m_isClearedAll = false;
			InitNotification ();
		}
	}

	public void ClearPreviousData ()
	{
		for (int i = 0; i < m_notificationItemList.Count; i++) {

			GameObject go = m_notificationItemList [i].gameObject;
			Destroy (go);
		}

		m_notificationItemList.Clear ();
		m_removedNotification.Clear ();
		SportsQuizManager.Instance.notification = null;
	}

	public void ClearForMemory ()
	{
		for (int i = 0; i < m_notificationItemList.Count; i++) {

			GameObject go = m_notificationItemList [i].gameObject;
			Destroy (go);
		}

		m_notificationItemList.Clear ();
		Resources.UnloadUnusedAssets ();
	}

    void InitNotification()
    {
		if (m_isClearedAll)
			return;

		if (SportsQuizManager.Instance.notification == null)
			return;
		
		MainMenuPanelManager.Instance.mainMenuPanel.NewNotification (0);
//		GamePrefs.LastNotificationID = SportsQuizManager.Instance.notification.last_notification_id;
		NotificationHandler.Instance.SaveLastNotification (SportsQuizManager.Instance.notification.last_notification_id);

		if (notificationScrollRect.content.transform.childCount > 1)
			m_NoNotification.gameObject.SetActive (false);
		else
			m_NoNotification.gameObject.SetActive (true);


		if (this.gameObject.activeSelf == false)
			return;

		bool asFirstSibling = false;
		if (m_notificationItemList.Count != 0)
			asFirstSibling = true;

        List<SeralizedClassServer.NotificationDetails> m_notificationList = new List<SeralizedClassServer.NotificationDetails>();
        m_notificationList = SportsQuizManager.Instance.notification.content;

		UpdatePreviousNotification ();

		/*if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadBadges (m_notificationList, asFirstSibling));
		}*/

		for (int i = 0; i < m_notificationList.Count; i++)
        {
			if (CheckForNotification (m_notificationList[i].notification_id))
			{
				NotificationItem _item = (NotificationItem)Instantiate(notificationItem);
				m_NoNotification.gameObject.SetActive (false);

				_item.gameObject.RectTransformMakeChild(notificationScrollRect.content.transform, asFirstSibling);
//				_item.gameObject.SetActive (false);
				_item.InitNotification(
					m_notificationList[i].notification_id,
					m_notificationList[i].notification_type,
					m_notificationList[i].data,
					m_notificationList[i].message);

				m_notificationItemList.Add(_item);
			}
        }
    }

	void UpdatePreviousNotification ()
	{
		for (int i = 0; i < m_notificationItemList.Count; i++) {
			if (CheckWithNewNotification (m_notificationItemList[i].m_notification_ID)) {
				RemoveNotificationItem (m_notificationItemList [i]);
			}
		}
	}

	bool CheckWithNewNotification (int notificationID)
	{
		List<SeralizedClassServer.NotificationDetails> m_notificationList = new List<SeralizedClassServer.NotificationDetails>();
		m_notificationList = SportsQuizManager.Instance.notification.content;
		for (int i = 0; i < m_notificationList.Count; i++) {
			if (m_notificationList[i].notification_id == notificationID) {
				return false;
			}
		}
		return true;
	}

	IEnumerator LoadBadges (List<SeralizedClassServer.NotificationDetails> m_notificationList, bool asFirstSibling)
	{
		for (int i = 0; i < m_notificationList.Count; i++)
		{
			if (CheckForNotification (m_notificationList[i].notification_id))
			{
				NotificationItem _item = (NotificationItem)Instantiate(notificationItem);
				m_NoNotification.gameObject.SetActive (false);

				_item.gameObject.RectTransformMakeChild(notificationScrollRect.content.transform, asFirstSibling);
				_item.gameObject.SetActive (false);
				_item.InitNotification(
					m_notificationList[i].notification_id,
					m_notificationList[i].notification_type,
					m_notificationList[i].data,
					m_notificationList[i].message);

				m_notificationItemList.Add(_item);
				if (m_notificationList[i].notification_type == (int) NotificationItem.eNotificatioType.Badges) {

					SportsQuizManager.Badges _badge = SportsQuizManager.Instance.GetBadge (_item.m_notificationData.badge_id);
					if (_badge != null) {
						if (References.GetImageFromLocal(_badge.id, _badge.url, References.eImageType.Badges) == null) {
							WWW image = new WWW (_badge.url);
							yield return image;
							if (image.error == null) {
								References.SaveImageToLocal (_badge.id, _badge.url, image.texture, References.eImageType.Badges);
								if (_item.badgeImage != null) {
									_item.badgeImage.CreateImageSprite (image.texture);
								}
							} else {
								Q.Utils.QDebug.Log (image.error);
							}
						} else {
							_item.badgeImage.CreateImageSprite (References.GetImageFromLocal (_badge.id, _badge.url, References.eImageType.Badges));
						}
					}
				}

				_item.gameObject.SetActive (true);
				yield return 0;
			}
		}
	}

	bool CheckForNotification (int notiID)
	{
		for (int i = 0; i < m_notificationItemList.Count; i++) {
			if (m_notificationItemList [i].m_notification_ID == notiID) {

				return false;
            }
		}

        //CHECK IN REMOVED NOTIFICATION LIST
        for (int i = 0; i < m_removedNotification.Count; i++)
        {
            if (m_removedNotification[i] == notiID)
            {
                return false;
            }
        }

		return true;
	}

    public void RemoveNotificationItem (NotificationItem item)
	{
		for (int i = 0; i < m_notificationItemList.Count; i++) {

			if (m_notificationItemList [i].m_notification_ID == item.m_notification_ID) {
                m_removedNotification.Add(item.m_notification_ID);
				GameObject go = m_notificationItemList [i].gameObject;
				m_notificationItemList.Remove (item);
                Destroy (go);
				break;
			}
		}
	}
}

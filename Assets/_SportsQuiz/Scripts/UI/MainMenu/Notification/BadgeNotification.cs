﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class BadgeNotification : DragAndDelete {

//    [SerializeField] private Button acceptButton;
    [SerializeField] private Text description, title;
	public Image p_badgeImage { get { return badgeImage; } }
	[SerializeField] private Image badgeImage;
	[SerializeField] private GameObject m_loadingImage;
	SportsQuizManager.Badges m_badge;

    private NotificationItem m_itemMain;

	void OnEnable () {
		InitBadge ();
	}

    // Use this for initialization
    void Start () {
//		acceptButton.onClick.AddListener(OnClickViewBadge);
		InitDragAndDrop (
			DeleteGameObject,
			AlphaValueCallback,
			MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.notificationScrollRect);
	}

	public void InitBadgeNotification (NotificationItem itemMain)
    {
        m_itemMain = itemMain;

		description.text = "";//m_itemMain.m_notificationData.description;
		title.text = m_itemMain.m_message;

		InitBadge ();
    }

	void InitBadge ()
	{
		m_badge = SportsQuizManager.Instance.GetBadge (m_itemMain.m_notificationData.badge_id);
		if (m_badge != null) {
			if (References.GetImageFromLocal(m_badge.id, m_badge.url, References.eImageType.Badges) == null) {
				if (this.gameObject.activeInHierarchy == true) {
					StartCoroutine (SaveBadgeToLocal (m_badge.url));
				}
			} else {
				ViewBadge (References.GetImageFromLocal (m_badge.id, m_badge.url, References.eImageType.Badges));
				m_itemMain.gameObject.SetActive (true);
			}
		}
	}

	IEnumerator SaveBadgeToLocal (string url)
	{
		WWW image = new WWW (url);
		yield return image;
		if (image.error == null) {
			References.SaveImageToLocal (m_badge.id, url, image.texture, References.eImageType.Badges);
			m_itemMain.gameObject.SetActive (true);
			ViewBadge (image.texture);
		} else {
			Q.Utils.QDebug.Log (image.error);
		}
	}

	void ViewBadge (Texture2D texture)
	{
		m_loadingImage.SetActive (false);
		badgeImage.enabled = true;
		badgeImage.CreateImageSprite (texture);
	}

    void OnClickViewBadge ()
    {
		m_itemMain.OnClickAcceptButton ();
    }

	void AlphaValueCallback (float value)
	{
		m_itemMain.HighlightDeleteText (value);
	}

	void DeleteGameObject ()
	{
		Q.Utils.QDebug.LogError ("DeleteGameObject");
		if (InternetConnection.Check) {
			m_itemMain.ClearNotification ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}
}

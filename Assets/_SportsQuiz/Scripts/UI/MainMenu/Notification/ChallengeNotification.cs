﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class ChallengeNotification : DragAndDelete {

    [SerializeField] private Button acceptButton, rejectButton;
	[SerializeField] private Text description, title;
	[SerializeField] private Image profile;
    private NotificationItem m_itemMain;

    // Use this for initialization
    void Start()
    {
        acceptButton.onClick.AddListener(OnClickAcceptQuiz);
        rejectButton.onClick.AddListener(OnClickRejectQuiz);
		InitDragAndDrop (
			DeleteGameObject,
			AlphaValueCallback,
			MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.notificationScrollRect);
    }

	void OnEnable ()
	{
		StartCoroutine (LoadAllImage ());
	}

    public void InitChallengeNotification(NotificationItem itemMain)
    {
        m_itemMain = itemMain;
		description.text = m_itemMain.m_message;
		title.text = "";//itemMain.m_notificationData.category_title;
    }

    public void UpdateInfo()
    {
//        username.text = m_itemMain.opponentDetails.user_info.name;

//		SportsQuizManager.Instance.AssignOpponentProfileImage (
//			profile, m_itemMain.opponentDetails.user_info.user_id);
		profile.CreateImageSprite (
			SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
		if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadAllImage ());
		}
    }

	IEnumerator LoadAllImage ()
	{
		if (SportsQuizManager.IsNullOrEmptyOrZero (m_itemMain.opponentDetails.user_info.facebook_id)) {
			profile.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
		} else {
			string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (m_itemMain.opponentDetails.user_info.facebook_id);
			if (string.IsNullOrEmpty (_facebookImageURL)) {
				profile.CreateImageSprite (
					SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
			} else {

				WWW image = new WWW (_facebookImageURL);
				yield return image;
				if (image.error == null) {
					profile.CreateImageSprite (image.texture);
				} else {
					profile.CreateImageSprite (
						SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
				}
			}
		}
	}

    void OnClickAcceptQuiz()
    {
		m_itemMain.OnClickAcceptButton ();
    }

    void OnClickRejectQuiz()
    {
		m_itemMain.OnClickRejectButton ();
    }

	void AlphaValueCallback (float value)
	{
		m_itemMain.HighlightDeleteText (value);
	}

	void DeleteGameObject ()
	{
		Q.Utils.QDebug.LogError ("DeleteGameObject");
		if (InternetConnection.Check) {
			m_itemMain.ClearNotification ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}
}

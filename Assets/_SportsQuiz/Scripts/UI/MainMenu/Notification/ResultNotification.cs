﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class ResultNotification : DragAndDelete {

    [SerializeField]
    private Button acceptButton, rejectButton;
	[SerializeField] private Text description, title;
    [SerializeField] private Image profile;
    private NotificationItem m_itemMain;

    // Use this for initialization
    void Start()
    {
        acceptButton.onClick.AddListener(OnClickViewResult);
		InitDragAndDrop (
			DeleteGameObject,
			AlphaValueCallback,
			MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.notificationScrollRect);
    }

	void OnEnable ()
	{
		StartCoroutine (LoadAllImage ());
	}

    public void InitResultNotification(NotificationItem itemMain)
    {
        m_itemMain = itemMain;
		description.text = m_itemMain.m_message;
		title.text = "";//m_itemMain.m_notificationData.category_title;
    }

    public void UpdateInfo()
    {
//        username.text = m_itemMain.opponentDetails.user_info.name;
		profile.CreateImageSprite (
			SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
		if (this.gameObject.activeInHierarchy) {
			StartCoroutine (LoadAllImage ());
		}     
    }

	IEnumerator LoadAllImage ()
	{
		if (SportsQuizManager.IsNullOrEmptyOrZero (m_itemMain.opponentDetails.user_info.facebook_id)) {
			profile.CreateImageSprite (
				SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
		} else {
			string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (m_itemMain.opponentDetails.user_info.facebook_id);
			if (string.IsNullOrEmpty (_facebookImageURL)) {
				profile.CreateImageSprite (
					SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
			} else {

				WWW image = new WWW (_facebookImageURL);
				yield return image;
				Q.Utils.QDebug.Log ("LoadAllImage");
				if (image.error == null) {
					profile.CreateImageSprite (image.texture);
				} else {
					profile.CreateImageSprite (
						SportsQuizManager.Instance.references.GetAvatarImage (m_itemMain.opponentDetails.user_info.avatar_type));
				}
			}
		}
	}

    void OnClickViewResult()
    {
		m_itemMain.OnClickViewResult ();
    }

	void AlphaValueCallback (float value)
	{
		m_itemMain.HighlightDeleteText (value);
	}

	void DeleteGameObject ()
	{
		Q.Utils.QDebug.LogError ("DeleteGameObject");
		if (InternetConnection.Check) {
			m_itemMain.ClearNotification ();
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}
}

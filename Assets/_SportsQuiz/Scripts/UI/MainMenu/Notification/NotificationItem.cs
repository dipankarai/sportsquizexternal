﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotificationItem : MonoBehaviour {

    public enum eNotificatioType
    {
        Unknown = 0,
		Challenge = 1,
		Result = 2,
		Badges = 3
    }

	public enum eNotificationClearType
	{
		ClearAll = 1,
		Selected = 2
	}

	public SeralizedClassServer.OpponentDetails opponentDetails;
    public int m_notification_ID;
    public eNotificatioType m_notificatioType;
	public SeralizedClassServer.NotificationData m_notificationData;
    public string m_message;
	public Image badgeImage { get { return m_BadgeNotification.p_badgeImage; } }

	[SerializeField] private ChallengeNotification m_ChallengeNotification;
	[SerializeField] private ResultNotification m_ResultNotification;
	[SerializeField] private BadgeNotification m_BadgeNotification;
	[SerializeField] private Text deleteText;
	[HideInInspector] public Transform activePanel;

	// Use this for initialization
	void Start () {
	}

	public void InitNotification (int id, int type, SeralizedClassServer.NotificationData data, string message)
	{
        m_notification_ID = id;
        m_notificatioType = (eNotificatioType)type;
        m_notificationData = data;
		m_message = message;

        InitPanel();
    }

	void InitPanel ()
	{
        switch (m_notificatioType)
        {
		case eNotificatioType.Challenge:
			{
				m_ChallengeNotification.InitChallengeNotification(this);
				m_ChallengeNotification.gameObject.SetActive(true);
				activePanel = m_ChallengeNotification.transform;
				SportsQuizManager.Instance.GetOpponentDetail(m_notificationData.user_id, UserInfoCallback);
				
			}
			break;
            
		case eNotificatioType.Result:
			{
				m_ResultNotification.InitResultNotification(this);
				m_ResultNotification.gameObject.SetActive(true);
				activePanel = m_ResultNotification.transform;
				SportsQuizManager.Instance.GetOpponentDetail(m_notificationData.user_id, UserInfoCallback);
			}
			break;

		case eNotificatioType.Badges:
			{
				m_BadgeNotification.InitBadgeNotification(this);
				m_BadgeNotification.gameObject.SetActive(true);
				activePanel = m_BadgeNotification.transform;
			}
			break;

            default:
                break;
        }
    }

    public void UserInfoCallback (SeralizedClassServer.OpponentDetails callback)
    {
        opponentDetails = callback;
		if (m_notificatioType == eNotificatioType.Challenge) {
			if (m_ChallengeNotification != null) {
				m_ChallengeNotification.UpdateInfo();
			}
		}
		else if (m_notificatioType == eNotificatioType.Result) {
			if (m_ResultNotification != null) {
				m_ResultNotification.UpdateInfo();
			}
		}
		
    }

	public void OnClickViewResult ()
	{
		MainMenuPanelManager.Instance.WaitingPanel (true);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.Gameplay);
		QuizPanelManager.Instance.ShowNotificationResult (m_notificationData);
	}

	public void OnClickAcceptButton ()
	{
		if (InternetConnection.Check) {
			ServerManager.Instance.GetChallenge (m_notificationData.user_quiz_id, m_notification_ID, GetChallengeCallback);
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	private SeralizedClassServer.DuelQuizQuestionsList m_dualCallback;
	void GetChallengeCallback (SeralizedClassServer.DuelQuizQuestionsList callback)
	{
		if (callback != null) {
			
			m_dualCallback = callback;
			
			SportsQuizManager.Instance.selectedMode = (QuizManager.eQuizMode)m_notificationData.quiz_type;
			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();
			SportsQuizManager.Instance.currentSelectedTopics.category_id = m_notificationData.category_id;
			SportsQuizManager.Instance.currentSelectedTopics.category_title = m_notificationData.category_title;
			
			SportsQuizManager.Instance.GetOpponentDetail (m_notificationData.user_id, OpponentDetailCall);
		} else {
			SportsQuizManager.Instance.GetOpponentDetail (m_notificationData.user_id, OponentCancelCallback);
		}
	}

	void OponentCancelCallback (SeralizedClassServer.OpponentDetails callback)
	{
		string message = "";
		if (string.IsNullOrEmpty (callback.user_info.name) == false) {

			message = m_notificationData.category_title
				+ "\n<size=12>" + callback.user_info.name + "\n has cancelled the challenge.</size>";
		} else {
			message = m_notificationData.category_title
			+ "\n<size=12>" + "This challenge has been cancelled.</size>";
		}

		MainMenuPanelManager.Instance.ShowMessage (message);
		MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.RemoveNotificationItem (this);
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	void OpponentDetailCall (SeralizedClassServer.OpponentDetails callback)
	{
		SportsQuizManager.Instance.opponentInformation.serverID = callback.user_info.user_id;
		SportsQuizManager.Instance.opponentInformation.name = callback.user_info.name;
		SportsQuizManager.Instance.opponentInformation.age = callback.user_info.age;
		SportsQuizManager.Instance.opponentInformation.avatarType = callback.user_info.avatar_type;
		SportsQuizManager.Instance.opponentInformation.country = callback.user_info.country;
		SportsQuizManager.Instance.opponentInformation.email = callback.user_info.email_id;
		SportsQuizManager.Instance.opponentInformation.facebook_id = callback.user_info.facebook_id;
		SportsQuizManager.Instance.opponentInformation.gender = callback.user_info.gender;
		SportsQuizManager.Instance.opponentInformation.isOnline = true;

		SportsQuizManager.Instance.selectedDuelFriendDetail = new SportsQuizManager.InviteFriendDetail ();
		SportsQuizManager.Instance.selectedDuelFriendDetail.name = callback.user_info.name;
		SportsQuizManager.Instance.selectedDuelFriendDetail.userType = SportsQuizManager.eUserType.EmailUser;
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.EmailUser;
		SportsQuizManager.Instance.selectedDuelFriendDetail.user_id = callback.user_info.user_id;

		QuizManager.Instance.LoadQuizData(m_dualCallback);

		MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.RemoveNotificationItem (this);
	}

	public void OnClickRejectButton ()
	{
		if (InternetConnection.Check) {

			ServerManager.Instance.RejectChallenge (m_notificationData.user_quiz_id, m_notification_ID, ChallengeRejectCallback);

		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void ChallengeRejectCallback (bool callback)
	{
		if (callback == true)
		{
			MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.RemoveNotificationItem (this);
		}
	}

	public void ClearNotification ()
	{
		this.transform.gameObject.SetActive (false);

		ServerManager.Instance.ClearNotification (
			NotificationItem.eNotificationClearType.Selected,
			DeleteFromServerCallback,
			m_notification_ID.ToString());
	}

	public void HighlightDeleteText (float value)
	{
		float _alpha = (1f - value) + ((1f - value) * (50f / 100f));
		if (_alpha > 1f) {
			_alpha = 1f;
		} else if (_alpha < 0) {
			_alpha = 0;
		}
		Q.Utils.QDebug.LogGreen ((1f - value)+":::"+_alpha);
		Color _tempDeleteTextColor = deleteText.color;
		_tempDeleteTextColor.a = _alpha;
		deleteText.color = _tempDeleteTextColor;
	}

	void DeleteFromServerCallback (bool success)
	{
		if (success) {
			MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.RemoveNotificationItem (this);
		} else {
			this.transform.gameObject.SetActive (true);
		}
	}

	public void OnCompleteLeenTween ()
	{
		Destroy (this.gameObject);
	}

	/*
    #region CHALLENGE PANEL

    void InitChallengePanelUIEvent ()
	{
		acceptChallengeButton.onClick.AddListener (OnClickAcceptChallengeButton);
		rejectChallengeButton.onClick.AddListener (OnClickRejectChallengeButton);
	}

	void OnClickAcceptChallengeButton () {}
	void OnClickRejectChallengeButton () {}

	#endregion CHALLENGE PANEL

	#region RESULT PANEL

	void InitResultPanelUIEvent ()
	{
		viewResultButton.onClick.AddListener (OnClickViewResultButton);
	}

	void OnClickViewResultButton () {}

	#endregion RESULT PANEL

	#region INVITATION PANEL

	void InitInvitationPanelUIEvent ()
	{
		acceptInvitationButton.onClick.AddListener (OnClickAcceptInvitationButton);
		rejectInvitationButton.onClick.AddListener (OnClickRejectInvitationButton);
	}

	void OnClickAcceptInvitationButton () {}
	void OnClickRejectInvitationButton () {}

	#endregion INVITATION PANEL
	*/
}

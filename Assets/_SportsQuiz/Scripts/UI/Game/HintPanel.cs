﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class HintPanel : MonoBehaviour {

	public Button okButton;
	public Image panel;
	public Image subPanel;
	public Text hintDescription;

	public System.Action hintCallback;

	// Use this for initialization
	void Start () {
		okButton.onClick.AddListener (OnClickOkButton);
	}

	void OnEnable () {
		/*if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {
			panel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
			subPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
			okButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LUCKY_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {
			panel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LUCKY_MODE);
			subPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LUCKY_MODE);
			okButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DUAL_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {
			panel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DUAL_MODE);
			subPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DUAL_MODE);
			okButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
		}*/ 
	}

	void OnClickOkButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		gameObject.SetActive (false);
		hintCallback ();
	}

	public void IntiHint (string hint)
	{
		hintDescription.text = hint;
	}
}

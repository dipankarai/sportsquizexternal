﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GamePlayInitPanel : MonoBehaviour {

	[HideInInspector]
	public GamePlayPanel gamePlayPanel;

	// Use this for initialization
	void Start () {
	
	}

	public virtual void InitGamePlay ()
	{
		ScoreManager.Instance.playerCurrentQuizScore = new System.Collections.Generic.List<ScoreManager.Score> ();
	}

	public virtual void MarkQuestion (int index, bool player = true)
	{
		
	}

	public virtual Image GetAnswerBar (int index, bool player = true)
	{
		return null;
	}

	public virtual void ShowPlayerScore (int score)
	{
		
	}

	public virtual void ShowOpponentScore (int score)
	{

	}

	public virtual void LoadPlayerProfileImage (Texture2D texture)
	{

	}

	public virtual void LoadOpponentProfileImage (Texture2D texture)
	{

	}

	public virtual void SaveScore (int _questionNum, float _timeTaken)
	{
		ScoreManager.Instance.AddScore (_questionNum, QuizManager.Instance.currentQuizGame.TotalQuestion, _timeTaken);
	}
}

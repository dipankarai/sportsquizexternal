﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnswerDot : MonoBehaviour {

	[SerializeField] private Image m_blackImage;
	[SerializeField] private Image m_orangeImage;
	[SerializeField] private Image m_greenImage;

	void OnEnable ()
	{
		m_orangeImage.enabled = false;
		m_greenImage.enabled = false;
	}

	// Use this for initialization
	void Start () {
	
	}

	public void ToggleImage () {
		if (m_greenImage.enabled == false) {
			m_greenImage.enabled = true;
		} else {
			m_greenImage.enabled = false;
			m_orangeImage.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

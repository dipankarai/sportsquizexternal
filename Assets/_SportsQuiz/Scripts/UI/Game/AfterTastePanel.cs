﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class AfterTastePanel : MonoBehaviour {

    [SerializeField] private Button okButton;
    [SerializeField] private Text descritionText;
	[SerializeField] private Image panel;
	[SerializeField] private Image subpanel;

    public System.Action nextQuestionCallback;

    void OnEnable ()
    {
        int currentQuestion = QuizManager.Instance.currentQuizGame.currentQuestion;
        descritionText.text = QuizManager.Instance.currentQuizGame.currentQuizList[currentQuestion].question.aftertaste;
    }

	// Use this for initialization
	void Start () {
        okButton.onClick.AddListener(OnClickOkButton);
	}
	
	void OnClickOkButton ()
    {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ClosePanel ();
	}

	void ClosePanel ()
	{
		this.gameObject.SetActive(false);
		nextQuestionCallback();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class GamePlayPanel : MonoBehaviour {

	[SerializeField] private bool m_normalLoad = true;

	public Text titleText, questionsText, questionsTopText;
	public Image timerImage, questionImage;
	public Button cancelButton, hintButton;
	public Button[] optionsButton;
	public Sprite featureIcon;

	public Color normalColor, correctAnswerColor, wrongAnswerColor;
	public QuizVideoPanel videoPanel;

	public SingleGamePlayPanel singleGmaePlayPanel;
	public DuelGamePlayPanel duelGamePlayPanel;

	private bool m_canAnswered = true;
	private int m_selectedButton = -1;
	private bool m_playingVideo = false;

	private GamePlayInitPanel m_gamePlayInitPanel;
	[SerializeField] private RectTransform m_answerPanel;
	private Vector2 m_answerPanelPositionUp;
	private Vector2 m_answerPanelPositionDown;

	void Awake () {
		
		singleGmaePlayPanel.gamePlayPanel = this;
		duelGamePlayPanel.gamePlayPanel = this;

		m_answerPanelPositionDown = m_answerPanel.anchoredPosition;
		m_answerPanelPositionUp = m_answerPanelPositionDown;
		m_answerPanelPositionUp.y += 110f;
	}

	void AnchorsToCorner ()
	{
		RectTransform t = m_answerPanel;
		RectTransform pt = m_answerPanel.parent as RectTransform;

		if (t == null || pt == null) return;

		Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width,
			t.anchorMin.y + t.offsetMin.y / pt.rect.height);
		Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width,
			t.anchorMax.y + t.offsetMax.y / pt.rect.height);

		t.anchorMin = newAnchorsMin;
		t.anchorMax = newAnchorsMax;
		t.offsetMin = t.offsetMax = new Vector2(0, 0);
	}

	void OnEnable () {

		GoogleAdHandler.Instance.HideBanner ();

		timerImage.fillAmount = 1;

		for (int i = 0; i < optionsButton.Length; i++) {
			optionsButton [i].gameObject.SetActive (false);
		}

		titleText.text = SportsQuizManager.Instance.currentSelectedTopics.category_title;

		InitScorePanel ();
		questionImage.gameObject.SetActive (false);

		if (QuizManager.Instance.currentQuizGame != null)
			StartCoroutine ( InitQuestionAndAnswers ());

		MainMenuPanelManager.Instance.WaitingPanel (false);
	
		//AUDIO
		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Main) == true)
			GenericAudioManager.StopSoundBG (GenericAudioManager.BGSounds.BG_Music_Main);

		if (GenericAudioManager.isPlayingBG (GenericAudioManager.BGSounds.BG_Music_Gameplay) == false)
			GenericAudioManager.PlayBG (GenericAudioManager.BGSounds.BG_Music_Gameplay, GenericAudioManager.PlayMode.loop);
	}

	void InitScorePanel ()
	{
		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {

			singleGmaePlayPanel.gameObject.SetActive (true);
			duelGamePlayPanel.gameObject.SetActive (false);
			m_gamePlayInitPanel = singleGmaePlayPanel;
		}
		else 
		{
			singleGmaePlayPanel.gameObject.SetActive (false);
			duelGamePlayPanel.gameObject.SetActive (true);
			m_gamePlayInitPanel = duelGamePlayPanel;
		}
	}

	// Use this for initialization
	void Start () {

		cancelButton.onClick.AddListener (OnClickCancelButton);
		hintButton.onClick.AddListener (OnClickHintButton);
	}

	void StartTimer ()
	{
		//START CLOCK SFX
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Clock_Ticking, GenericAudioManager.PlayMode.loop);

		m_canAnswered = true;
		QuizManager.Instance.StartCountDown (TimeOver);
	}

	void Update ()
	{
		if (GoogleAdHandler.Instance.IsBannerShowing == true) {
			GoogleAdHandler.Instance.HideBanner ();
		}
	}

	IEnumerator InitQuestionAndAnswers ()
	{
		yield return new WaitForSeconds (0.5f);

		while (CheckAllButtonIsActive () == false) {
			yield return null;
		}

		int _index = QuizManager.Instance.currentQuizGame.currentQuestion;

		DisplayQuestion ();

		while (m_playingVideo)
		{
			yield return 0;
		}

		yield return new WaitForSeconds (QuestionsDisplayTime (QuizManager.Instance.currentQuizGame.GetCurrentQuiz));

		//m_gamePlayInitPanel.GetAnswerBar (_index).color = Color.cyan;
		m_gamePlayInitPanel.MarkQuestion (_index);

		for (int i = 0; i < QuizManager.Instance.currentQuizGame.GetCurrentQuiz.answers.Count; i++) {

			OptionButton _button = GetButtonByGameObject (optionsButton [i].gameObject);

			_button.InitButtonsValues (
				QuizManager.Instance.currentQuizGame.GetCurrentQuiz.answers [i].answer_title,
				QuizManager.Instance.currentQuizGame.GetCurrentQuiz.answers [i].answer_id,
				OnClickOptionButton);
		}

		StartCoroutine (ShowButtons (StartTimer));
	}

	IEnumerator HideButtons (System.Action callback)
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			if (GetButtonByGameObject (optionsButton[i].gameObject).button_id != m_selectedButton) {
				yield return new WaitForSeconds (0.3f);
				ResetButton (optionsButton [i].gameObject);
				optionsButton [i].gameObject.SetActive (false);
			}
		}
		yield return new WaitForSeconds (0.5f);
		if (GetButtonById (m_selectedButton) != null) {
			
			GetButtonById (m_selectedButton).gameObject.SetActive (false);
			ResetButton (GetButtonById (m_selectedButton).gameObject);
		}

		if (callback != null)
			callback ();
	}

	void ShowButtons ()
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			optionsButton [i].interactable = true;
			optionsButton [i].gameObject.SetActive (true);
		}
	}

	IEnumerator ShowButtons (System.Action callback)
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			optionsButton [i].interactable = true;
			optionsButton [i].gameObject.SetActive (true);
			yield return new WaitForSeconds (0.05f);
		}

		if (callback != null)
			callback ();
	}

	void DisplayQuestion ()
	{
		questionsText.text = "";
		questionsTopText.text = "";
		questionImage.gameObject.SetActive (false);

		if (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizType == QuizManager.eQuizType.Image) {

			if (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizAssets.image != null)
			{
				TransformUtils.AllignRectTransformDeltaSize (questionImage.gameObject,
					QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizAssets.image);
				questionImage.CreateImageSprite (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizAssets.image);
				questionImage.gameObject.SetActive (true);
			}

			questionsText.text = QuizManager.Instance.currentQuizGame.GetCurrentQuiz.question.title;
			m_answerPanel.anchoredPosition = m_answerPanelPositionDown;

		} else if (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizType == QuizManager.eQuizType.Video) {
			m_playingVideo = true;
			videoPanel.gameObject.SetActive (true);
			string filename = QuizManager.Instance.currentQuizGame.GetCurrentQuiz.quizAssets.mediaPath;
			videoPanel.InitQuizVideoPlayer (filename);

			m_answerPanel.anchoredPosition = m_answerPanelPositionUp;
		} else {
			questionsTopText.text = QuizManager.Instance.currentQuizGame.GetCurrentQuiz.question.title;
			m_answerPanel.anchoredPosition = m_answerPanelPositionUp;
		}

		if (string.IsNullOrEmpty (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.question.hint) == true) {
			hintButton.interactable = false;
		} else {
			hintButton.interactable = true;
		}
	}

	void ResetButton (GameObject button)
	{
		OptionButton _button = GetButtonByGameObject (button);
		_button.ResetValue ();
		_button.ChangeColor (normalColor);
	}

	void OnClickOptionButton (int answeredId)
	{
		if (m_canAnswered == false)
			return;

		m_canAnswered = false;

		DisableOptionsButton ();

		//STOP CLOCK SFX
		GenericAudioManager.StopSoundFX (GenericAudioManager.SFXSounds.Clock_Ticking);

		if (QuizManager.Instance.QuestionAnswered (answeredId)) {
			//AUDIO SFX CORRECT ANSWER
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Correct_Answer);

			//Change BUTTON Color
			GetButtonById (answeredId).ChangeColor (correctAnswerColor);

			m_gamePlayInitPanel.SaveScore (
				QuizManager.Instance.currentQuizGame.currentQuestion,
				QuizManager.Instance.currentQuizGame.GetCurrentTimeTaken);

			m_gamePlayInitPanel.ShowPlayerScore (ScoreManager.Instance.GetTotalScore (true));
			
		} else {
			//AUDIO SFX INCORRECT ANSWER
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Wrong_Answer);

			int _getCorrectAnswer = QuizManager.Instance.currentQuizGame.GetCorrectAnswer;

			GetButtonById (answeredId).ChangeColor (wrongAnswerColor);
			GetButtonById (_getCorrectAnswer).ChangeColor (correctAnswerColor);

			m_gamePlayInitPanel.SaveScore (
				QuizManager.Instance.currentQuizGame.currentQuestion,
				QuizManager.Instance.currentQuizGame.GetTotalTime);
			
			m_gamePlayInitPanel.ShowPlayerScore (ScoreManager.Instance.GetTotalScore (true));
		}

		m_gamePlayInitPanel.MarkQuestion (QuizManager.Instance.currentQuizGame.currentQuestion);
		m_selectedButton = answeredId;

		StartCoroutine (HideButtons (ShowAfterTaste));
	}

	void DisableOptionsButton ()
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			optionsButton [i].interactable = false;
		}
	}

	void CheckForNextQuestion ()
	{
		if (QuizManager.Instance.currentQuizGame.currentQuestion 
			< QuizManager.Instance.currentQuizGame.currentQuizList.Count - 1) {

            NextQuestion();
        } else {
			//RESULTS
			Q.Utils.QDebug.Log ("Results......");
			QuizManager.Instance.QuizComplete (QuizManager.eQuizComplete.GameOver);
		}
	}

	void NextQuestion ()
	{
        QuizManager.Instance.currentQuizGame.currentQuestion++;
		if (QuizManager.Instance.currentQuizGame != null)
			StartCoroutine (InitQuestionAndAnswers ());
	}

    void ShowAfterTaste ()
    {

		if (QuizPanelManager.Instance.hintPanel.gameObject.activeSelf == true)
			return;
		if (QuizPanelManager.Instance.forfeitPanel.gameObject.activeSelf == true)
			return;

		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure)
		{
			if (QuizPanelManager.Instance.afterTastePanel.nextQuestionCallback == null)
				QuizPanelManager.Instance.afterTastePanel.nextQuestionCallback = CheckForNextQuestion;

			if (string.IsNullOrEmpty (QuizManager.Instance.currentQuizGame.GetCurrentQuiz.question.aftertaste) == true) {
				CheckForNextQuestion();
			} else {
				QuizPanelManager.Instance.afterTastePanel.gameObject.SetActive(true);
			}
		}
		else
		{
			CheckForNextQuestion();
		}
    }

	void ForfeitCloseCallback ()
	{
		if (optionsButton [optionsButton.Length - 1].gameObject.activeSelf == false) {
			ShowAfterTaste ();
		}
	}

	void HintCallback ()
	{
		if (optionsButton[optionsButton.Length-1].gameObject.activeSelf == false) {
			ShowAfterTaste ();
		}
	}

	void OnClickCancelButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Forfeit_Button_Click);
		if (QuizPanelManager.Instance.forfeitPanel.forfeitCallback == null) {
			QuizPanelManager.Instance.forfeitPanel.forfeitCallback = ForfeitCloseCallback;
		}
		QuizPanelManager.Instance.forfeitPanel.gameObject.SetActive (true);
	}

	void OnClickHintButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (m_canAnswered == false)
			return;
		if (QuizPanelManager.Instance.hintPanel.hintCallback == null) {
			QuizPanelManager.Instance.hintPanel.hintCallback = HintCallback;
		}
		string _hint = QuizManager.Instance.currentQuizGame.currentQuizList [QuizManager.Instance.currentQuizGame.currentQuestion].question.hint;
		QuizPanelManager.Instance.hintPanel.IntiHint (_hint);
		QuizPanelManager.Instance.hintPanel.gameObject.SetActive (true);
        QuizManager.Instance.currentQuizGame.hintUsed++;
	}

	void TimeOver (bool timeComplete, string timerString, float timerValue)
	{
		if (timeComplete == false){
			timerImage.fillAmount = timerValue;
		} else {
			timerImage.fillAmount = 0;

			DisableOptionsButton ();
		}
	}

	public void VideoPlayComplete ()
	{
		questionsTopText.text = QuizManager.Instance.currentQuizGame.GetCurrentQuiz.question.title;
		m_playingVideo = false;
	}

	public void OnCompleteTimeOver ()
	{
		//STOP CLOCK SFX
		GenericAudioManager.StopSoundFX (GenericAudioManager.SFXSounds.Clock_Ticking);

		m_gamePlayInitPanel.SaveScore (
			QuizManager.Instance.currentQuizGame.currentQuestion,
			QuizManager.Instance.currentQuizGame.GetTotalTime);
		m_gamePlayInitPanel.ShowPlayerScore (ScoreManager.Instance.GetTotalScore (true));

		StartCoroutine (HideButtons (ShowAfterTaste));
	}

	float QuestionsDisplayTime (QuizManager.CurrentQuiz questions)
	{
		if ((QuizManager.eQuizType) questions.quizType == QuizManager.eQuizType.Image) {
			return 1f;
		} else if ((QuizManager.eQuizType) questions.quizType == QuizManager.eQuizType.Video) {
			return 0.8f;
		} else if ((QuizManager.eQuizType) questions.quizType == QuizManager.eQuizType.Text) {
			return 0.8f;
		}
			
		return 0.8f;
	}

	bool CheckAllButtonIsActive ()
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			if (optionsButton [i].gameObject.activeInHierarchy == true)
				return false;
		}

		return true;
	}

	OptionButton GetButtonById (int buttonId)
	{
		for (int i = 0; i < optionsButton.Length; i++) {
			OptionButton _button = optionsButton[i].GetComponent<OptionButton> ();
			if (_button.button_id == buttonId) {
				return _button;
			}
		}

		return null;
	}

	OptionButton GetButtonByGameObject (GameObject obj)
	{
		OptionButton _button = obj.GetComponent<OptionButton> ();
		if (_button == null)
			_button = obj.AddComponent<OptionButton> ();
		
		return _button;
	}

	void OnDisable ()
	{
		timerImage.fillAmount = 1;

		for (int i = 0; i < optionsButton.Length; i++) {
			ResetButton (optionsButton [i].gameObject);
		}

		questionsText.text = "";
		questionsTopText.text = "";
		Resources.UnloadUnusedAssets ();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class GameInitPanel : MonoBehaviour {

	[Header ("COMMON")]
	[SerializeField] private Text labelText;
	[SerializeField] private Text titleText;
	[SerializeField] private Button backButton, homeButton;

	[Header ("SINGLE PLAYER")]
	public GameObject singlePlayerPanel;
	[SerializeField] private Image playerProfileImage;
	[SerializeField] private Text playerNameText, playerDetailsText, playerRank, playerRankTitle;
	[SerializeField] private Button startButton;

	[Header ("DUEL PLAYER")]
	public GameObject duelPlayerPanel;
	[SerializeField] private Image dualPlayerProfileImage;
	[SerializeField] private Image dualOpponentProfileImage;
	[SerializeField] private Text dualPlayerNameText, dualPlayerDetailsText, dualPlayerRank, dualPlayerRankTitle;
	[SerializeField] private Text dualOpponentNameText, dualOpponentDetailsText, dualOpponentRank, dualOpponentRankTitle;
	[SerializeField] private Text loadingStartText;

	void OnEnable ()
	{
		if (MainMenuPanelManager.Instance.badgeAchievedPopUp.gameObject.activeInHierarchy) {
			MainMenuPanelManager.Instance.badgeAchievedPopUp.ClosePanel ();
		}

		GoogleAdHandler.Instance.ShowBanner ();
		labelText.text = SportsQuizManager.Instance.GetQuizModeLabel ();
		titleText.text = SportsQuizManager.Instance.currentSelectedTopics.category_title;

		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {
			OnEnableSinglePlayer ();
        } else {
			OnEnableDuelPlayer ();
		}
	}

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
	}

	void OnEnableSinglePlayer ()
	{
		singlePlayerPanel.SetActive (true);
		duelPlayerPanel.SetActive (false);
		playerNameText.text = "<color=" + TagConstant.ColorHexCode.DARK_YELLOW_1+ ">" + SportsQuizManager.Instance.playerInformation.name + "</color> ";
		playerDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			SportsQuizManager.Instance.playerInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country));//  _playerDetails;

		playerRank.text = SportsQuizManager.RankCountry (SportsQuizManager.Instance.playerInformation.rank, SportsQuizManager.Instance.playerInformation.country);
		playerRankTitle.text = SportsQuizManager.Instance.playerInformation.rankTitle;

		SportsQuizManager.Instance.AssignUserProfileImage(playerProfileImage);
	}

	void OnEnableDuelPlayer ()
	{
		//PLAYER SETTINGS
		singlePlayerPanel.SetActive (false);
		dualPlayerNameText.text = SportsQuizManager.Instance.playerInformation.name;
		dualPlayerDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			SportsQuizManager.Instance.playerInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country));//_playerDetails;
		
		SportsQuizManager.Instance.AssignUserProfileImage(dualPlayerProfileImage);

		dualPlayerRank.text = SportsQuizManager.RankCountry (SportsQuizManager.Instance.playerInformation.rank, SportsQuizManager.Instance.playerInformation.country);
		dualPlayerRankTitle.text = SportsQuizManager.Instance.playerInformation.rankTitle;

		//OPPONENT SETTINGS
		duelPlayerPanel.SetActive (true);
		dualOpponentNameText.text = "<color=" + TagConstant.ColorHexCode.DARK_YELLOW_1+ ">" + SportsQuizManager.Instance.opponentInformation.name + "</color> ";
		dualOpponentDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			(SportsQuizManager.eGender)SportsQuizManager.Instance.opponentInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.opponentInformation.country));//_opponentDetails;

		SportsQuizManager.Instance.LoadOpponentProfile (dualOpponentProfileImage);

		dualOpponentRank.text = SportsQuizManager.RankCountry (SportsQuizManager.Instance.opponentInformation.rank, SportsQuizManager.Instance.opponentInformation.country);
		dualOpponentRankTitle.text = SportsQuizManager.Instance.opponentInformation.rankTitle;

		StartCoroutine ("StartGameAfterSeconds");
	}

	// Use this for initialization
	void Start () {
		
		backButton.onClick.AddListener (OnClickBackButton);
		homeButton.onClick.AddListener (OnClickHomeButton);
		startButton.onClick.AddListener (OnClickStartButton);

	}
	
	void OnClickBackButton () {

		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);

		if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {
			MainMenuPanelManager.Instance.categoryPanel.OpenMainCategory ();
		}
		else {
			MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
		}
	}

	void OnClickHomeButton () {
	
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.UnKnown);
	}

	void OnClickStartButton () {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.PlayPanel);
	}

	void LoadDualPlayersImage (Texture2D texture, bool isPlayer = true)
	{
		if (isPlayer)
			dualPlayerProfileImage.CreateImageSprite (texture);
		else
			dualOpponentProfileImage.CreateImageSprite (texture);
	}

	IEnumerator StartGameAfterSeconds ()
	{
		yield return new WaitForSeconds (0.1f);

		float _waiting_Time = 8f;

		_waiting_Time += Time.time;
		float _tempTime = Time.time + 1f;

		string _temp = "!";
		loadingStartText.text = "GET READY "+_temp;
		while (Time.time < _waiting_Time) {

			if (_tempTime < Time.time)
			{
				_tempTime = Time.time + 1f;
				_temp +=_temp;

				if (_temp.Length > 5)
					_temp = "!";

				loadingStartText.text = "GET READY "+_temp;
			}
			
			yield return 0;
		}

		StopCoroutine ("StartGameAfterSeconds");
		OnClickStartButton ();
	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class SingleGamePlayPanel : GamePlayInitPanel {

	[SerializeField] private Text scoreText;
	[SerializeField] private Image playerProfileImage;
	[SerializeField] private Image[] answerBarArray;
	[SerializeField] private AnswerDot m_AnswerDot;

	private float m_difference = 55f;
	private float m_gap = 110f;
	private AnswerDot[] m_AnswerDotArray;

	void OnEnable () {
		InitAnswerDotImage ();
		scoreText.text = "0";
		InitGamePlay ();
		InitAnswerBar ();
        SportsQuizManager.Instance.AssignUserProfileImage(playerProfileImage);
		answerBarArray [0].color = Color.cyan;
    }

	void OnDisable ()
	{
		if (m_AnswerDotArray.Length > 0) {
			for (int i = 0; i < m_AnswerDotArray.Length; i++) {

				Destroy (m_AnswerDotArray [i].gameObject);
			}
		}
	}

	public override void InitGamePlay ()
	{
		base.InitGamePlay ();
	}

	void InitAnswerBar ()
	{
		for (int i = 0; i < answerBarArray.Length; i++) {
			answerBarArray [i].color = gamePlayPanel.normalColor;
		}
	}

	void InitAnswerDotImage ()
	{
		m_AnswerDotArray = new AnswerDot[QuizManager.Instance.currentQuizGame.TotalQuestion];

		float _total = (float)QuizManager.Instance.currentQuizGame.TotalQuestion;
		_total -= 1f;
		float _totalLength = (_total * m_difference) * -1f;

		for (int i = 0; i < QuizManager.Instance.currentQuizGame.TotalQuestion; i++) {

			AnswerDot dot = (AnswerDot) Instantiate (m_AnswerDot);
			RectTransform rect = dot.GetComponent<RectTransform> ();
			dot.gameObject.RectTransformMakeChild (m_AnswerDot.transform.parent);

			rect.anchoredPosition = new Vector2 (_totalLength, rect.anchoredPosition.y);

			_totalLength = _totalLength + m_gap;
			m_AnswerDotArray [i] = dot;
		}
	}

	public override void MarkQuestion (int index, bool player = true)
	{
		m_AnswerDotArray [index].ToggleImage ();
	}

	public override Image GetAnswerBar (int index, bool player = true)
	{
		if (player)
			return answerBarArray [index];

		return null;
	}

	public override void ShowPlayerScore (int score)
	{
		scoreText.text = score.ToString ();
	}

	public override void SaveScore (int _questionID, float _timeTaken)
	{
		base.SaveScore (_questionID, _timeTaken);
	}
}

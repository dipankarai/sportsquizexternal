﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AnswerLevelAdjuster : MonoBehaviour {

	[SerializeField] private RectTransform m_rectTransform;

	// Use this for initialization
	void Start () {

		if (m_rectTransform == null)
			m_rectTransform = GetComponent<RectTransform> ();

		RectTransform[] children = this.GetComponentsInChildren<RectTransform> ();
		
		List<RectTransform> _tempRecTransformtList = new List<RectTransform> ();
		
		for (int i = 0; i < children.Length; i++) {
			if (children [i] != m_rectTransform && children [i].gameObject.activeSelf) {
				
				_tempRecTransformtList.Add (children [i]);
			}
		}
		
		float _mainHeight = m_rectTransform.rect.height;
		float _mainWidth = m_rectTransform.rect.width;

		if (_mainWidth > _mainHeight) { //HORIZONTAL ALLIGNMENT
			float _height = _mainHeight;
			float _width = _mainWidth / (float)_tempRecTransformtList.Count;
			float _tempWidth = _width / 2f;
			
			for (int i = 0; i < _tempRecTransformtList.Count; i++) {
				
				_tempRecTransformtList [i].sizeDelta = new Vector2 (_width - 5f, _height);
				
				_tempRecTransformtList [i].anchoredPosition = new Vector2 (_tempWidth, 0);
				_tempWidth += _width;
			}
			
		} else { //VERTICAL ALLIGNMENT
			float _width = _mainWidth;
			float _height = _mainHeight / (float)_tempRecTransformtList.Count;
			float _tempHeight = _height / 2f;

			for (int i = 0; i < _tempRecTransformtList.Count; i++) {

				_tempRecTransformtList [i].sizeDelta = new Vector2 (_width, _height - 5f);

				_tempRecTransformtList [i].anchoredPosition = new Vector2 (0, _tempHeight);
				_tempHeight += _height;
			}
		}
	}


	void OnDrawGizmosSelectedz () {
		Debug.Log (m_rectTransform.rect);
	}
}

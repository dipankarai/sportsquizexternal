﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionButton : MonoBehaviour {

	public int button_id;
	public System.Action<int> onClickCallback;

	private Button m_button;
	private Text m_label;
	private Image buttonImage;

	void OnEnable () {
		InitOptionButton ();
	}
	
	void OnClickButton()
	{
		onClickCallback (button_id);
	}

	void InitOptionButton () {
		
		if (m_label == null)
			m_label = this.transform.GetComponentInChildren<Text> ();
		if (m_button == null)
			m_button = GetComponent<Button> ();
		
		m_button.onClick.AddListener (OnClickButton);
	}

	public void InitButtonsValues (string labelName, int buttonid, System.Action<int> callback)
	{
		button_id = buttonid;
		onClickCallback = callback;
		InitOptionButton ();
		m_label.text = labelName;
	}

	public void ResetValue ()
	{
		if (m_label != null)
			m_label.text = "";
		button_id = -1;
//		labelName = "";
	}

	public void ChangeColor (Color colr)
	{
		if (buttonImage == null)
			buttonImage = GetComponent<Image> ();

		buttonImage.color = colr;
	}

	string TempName ()
	{
		if (button_id == 0)
			return "Option A";
		else if (button_id == 1)
			return "Option B";
		else if (button_id == 2)
			return "Option C";
		else if (button_id == 3)
			return "Option D";

		return "";
	}
}

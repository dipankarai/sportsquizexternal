﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class GameResultPanel : MonoBehaviour {

    [Header("COMMON")]
    public Text modeLabelText;
    public Text quizTitleText;
    public Button playAgainButton, catagoryButton, mainMenuButton, feedbackButton;

    [Header("PLAYER")]
	public Image playerProfileImage, scorePanel, playerScoreBox;
	public Text playerNameText, playerDetailsText, playerScoreText, playerScoreLabelText;

    void OnEnable () {
		InitResultPanel ();
	}

	public virtual void InitGameColor ()
	{
//		SportsQuizManager.Instance.ChangeBGColor (GetComponent<Image> ());
		/*if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {

			scorePanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.RED_DARK);
			playerScoreBox.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.RED_SCORE);
			playerScoreLabelText.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);

			playAgainButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);
			catagoryButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);

		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {

			scorePanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DARK_YELLOW);
			playerScoreBox.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);
			playerScoreLabelText.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.RED_SCORE);

			playAgainButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
			catagoryButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);

		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {

			scorePanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.RED_DARK);
			playerScoreBox.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.RED_SCORE);
			playerScoreLabelText.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);

			playAgainButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);
			catagoryButton.GetComponent<Image> ().color = CommonUtils.HexToColor (TagConstant.ColorHexCode.YELLOW);

		}*/
	}

	// Use this for initialization
	void Start () {

		InitButtonEvent ();
	}

	void Update () {
		if (GoogleAdHandler.Instance.IsBannerShowing == true) {
			GoogleAdHandler.Instance.HideBanner ();
		}
	}

	public virtual void ShowFullScreenBanner ()
	{
		Invoke ("ShowAds", 3f);
	}

	void ShowAds ()
	{
		if (this.gameObject.activeInHierarchy) {
			QDebug.Log ("ShowAds");
			if (GamePrefs.HasKey ("PlayCount")) {
				if (GamePrefs.HasKey (TagConstant.PrefsName.RATE_THE_APP) == false) {
					if (GamePrefs.PlayCount % 5 == 0) {
						MainMenuPanelManager.Instance.rateAppPanel.SetActive (true);
					}else {
						GoogleAdHandler.Instance.FullScreenAds ();		
					}
					GamePrefs.PlayCount += 1;
				} else {
					GamePrefs.PlayCount = 1;			
					GoogleAdHandler.Instance.FullScreenAds ();			
				}
			} else {
				GamePrefs.PlayCount = 1;			
				GoogleAdHandler.Instance.FullScreenAds ();			
			}
		}
	}

	public virtual void InitButtonEvent ()
	{
		Q.Utils.QDebug.Log ("InitButtonEvent");
		playAgainButton.onClick.AddListener (OnClickPlayAgainButton);
		catagoryButton.onClick.AddListener (OnClickCategoryButton);
		mainMenuButton.onClick.AddListener (OnClickMainMenuButton);
		if (feedbackButton != null) {
			feedbackButton.onClick.AddListener (OnClickFeedbackButton);
		} else {
			//TODO:: ADD FEEDBACK BUTTON
			QDebug.LogError ("feedbackButton null");
		}
	}

	public virtual void InitResultPanel ()
	{
		Q.Utils.QDebug.Log ("InitResultPanel");

		modeLabelText.text = SportsQuizManager.Instance.GetQuizModeLabel ();
		quizTitleText.text = SportsQuizManager.Instance.currentSelectedTopics.category_title;

		LoadProfileImage ();
	}

	void OnClickPlayAgainButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		SportsQuizManager.Instance.previousQuizList.Add (QuizManager.Instance.currentQuizGame.quizId);

		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {
			MainMenuPanelManager.Instance.categoryPanel.OpenChallengeSameFriendPanel ();
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {
			//QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);
			QuizManager.Instance.LoadQuiz ();
		}
	}

    void OnClickCategoryButton()
    {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		if (SportsQuizManager.Instance.currentCategory == null ||
		    SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 0) {
			if (InternetConnection.Check) {

				MainMenuPanelManager.Instance.WaitingPanel (true);
				SportsQuizManager.Instance.ResetCategoryData ();
				ServerManager.Instance.GetCategoryFromServer (GetCategoryCallback);

				//SPONSERED CATEGORY
				SponseredQuiz.Instance.sponseredCategoryCategory = null;
				ServerManager.Instance.GetSponsoredCategory (SponseredQuiz.Instance.UpdateSponsoredQuiz);

			} else {
				MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
			}
		} else {
			
			MainMenuPanelManager.Instance.categoryPanel.OnClickMainCategoryCommonButton ();
			QuizPanelManager.Instance.SwitchPanel(QuizPanelManager.eQuizPanelState.CategoryPanel);
		}

    }

	void GetCategoryCallback (bool action)
	{
		if (action == true)
		{
			QuizPanelManager.Instance.SwitchPanel(QuizPanelManager.eQuizPanelState.CategoryPanel);
		}

		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

    void OnClickMainMenuButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		this.gameObject.SetActive (false);
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
	}

	void OnClickFeedbackButton ()
	{
		QDebug.Log ("ON CLICK FEEDBACK BUTTON");
		MainMenuPanelManager.Instance.feedbackPanel.ReportMistake ();
	}

    public virtual void LoadScore()
    {
		//playerScoreText.text = ScoreManager.Instance.GetTotalScore (true).ToString ();
    }

    public virtual void LoadUserDetails()
    {

    }

    public virtual void LoadProfileImage ()
    {
        SportsQuizManager.Instance.AssignUserProfileImage(playerProfileImage);
    }
}

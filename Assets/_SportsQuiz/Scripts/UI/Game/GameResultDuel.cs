﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Q.Utils;

public class GameResultDuel : GameResultPanel {

    [Header("OPPONENT")]
    public Image opponentProfileImage, opponentScoreBox;
	public Text opponentNameText, opponentDetailsText, oppoinentScoreText, resultText, afterTestText, forfeitedText, opponentScoreLabelText;
    public Button selectOpponentButton;
    public GameObject opponentWaitResult;
	[SerializeField] private Animation scoreAnim;
	[SerializeField] private Text scoreAnimText;
	private bool isScoreAnimPlayed = false;
	private string m_afterTaste;

    void OnEnable ()
    {
		ShowFullScreenBanner ();
		#if NEW_RESULT_LOGIC
		isResultLoaded = false;
		if (QuizPanelManager.Instance.isFromResultNotification == false) {
			GetStatusUpdate ();
		} else {
			GetResultFromServer (OpponentScoreCallback);
		}
		#endif
		InitGameColor ();
		LoadScore ();
		InitResultPanel ();
    }

	void OnDisable ()
	{
		#if NEW_RESULT_LOGIC
		QuizPanelManager.Instance.isFromResultNotification = false;
		isScoreAnimPlayed = false;
		scoreAnimText.text = "";
		#endif
	}

	public override void InitGameColor ()
	{
		base.InitGameColor ();
	}

    // Use this for initialization
    void Start () {
		InitButtonEvent ();
		selectOpponentButton.onClick.AddListener (OnClickNextOpponentButton);
	}

	public override void ShowFullScreenBanner ()
	{
		base.ShowFullScreenBanner ();
	}

	public override void InitButtonEvent ()
	{
		base.InitButtonEvent ();
	}

	public override void InitResultPanel ()
	{
		base.InitResultPanel ();

		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {
			
			playAgainButton.gameObject.SetActive (false);
			selectOpponentButton.gameObject.SetActive (true);
		}
		else {
			
			playAgainButton.gameObject.SetActive (true);
			selectOpponentButton.gameObject.SetActive (false);
		}
	}
	
	void OnClickNextOpponentButton ()
    {
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {

			MainMenuPanelManager.Instance.categoryPanel.OpenChallengeRadomFriend();
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);
		}
		else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {

			MainMenuPanelManager.Instance.categoryPanel.OpenFriendSelectionPanel();
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);
		}
    }

    public override void LoadScore()
    {
        base.LoadScore();
		playerScoreText.text = "0";
		oppoinentScoreText.text = "0";
		forfeitedText.gameObject.SetActive (false);
		opponentWaitResult.SetActive (true);
		#if NEW_RESULT_LOGIC
		if (QuizPanelManager.Instance.isFromResultNotification == false)
		{
			MainMenuPanelManager.Instance.WaitingPanel (false);
		}
		#endif

		resultText.text = TagConstant.PopUpMessages.WAITING_RESULT;
		playerScoreText.text = ScoreManager.Instance.GetTotalScore (true).ToString ();

		if (QuizManager.Instance.gameComplete == QuizManager.eQuizComplete.Cancelled)
		{
			QuizManager.Instance.gameComplete = QuizManager.eQuizComplete.TimeOver;
			ShowAfterTaste (QuizManager.Instance.currentQuizGame.afterTaste);
			oppoinentScoreText.text = "?";
			resultText.text = TagConstant.PopUpMessages.YOU_LOSE;
			GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Match_Lost_Tie);
			//forfeitedText.gameObject.SetActive (true);

			opponentWaitResult.SetActive(false);
			MainMenuPanelManager.Instance.WaitingPanel (false);

			#if !NEW_RESULT_LOGIC
			ServerManager.Instance.GetQuizResult (
				QuizManager.Instance.currentQuizGame.userQuizId, ForfeitOpponentScoreCallback);
			#endif
			this.LoadUserDetails();

        } else {
			#if NEW_RESULT_LOGIC
			if (QuizPanelManager.Instance.isFromResultNotification == true)
			{
				MainMenuPanelManager.Instance.WaitingPanel (true);
			}
			#else
			MainMenuPanelManager.Instance.WaitingPanel (true);

			ServerManager.Instance.GetQuizResult (
				QuizManager.Instance.currentQuizGame.userQuizId, OpponentScoreCallback);
			#endif
		}

    }

	#if NEW_RESULT_LOGIC
	private bool isResultLoaded = false;
	void GetStatusUpdate ()
	{
		if (isResultLoaded == false) {
			QuizManager.Instance.opponentStatus = UpdateOpponentStatus;
			QuizManager.Instance.StartStatusUpdate (QuizManager.Instance.currentQuizGame.userQuizId, GetStatusUpdateCallback);
		}
	}

	public void UpdateOpponentStatus (SeralizedClassServer.Status opponenetStatus)
	{
		Q.Utils.QDebug.Log ("UpdateOpponentStatus");

		ScoreManager.Instance.opponentCurrentQuizScore = new List<ScoreManager.Score> ();

		List <SeralizedClassServer.StatusData> data = 
			JsonConvert.DeserializeObject<List<SeralizedClassServer.StatusData>> (JsonConvert.SerializeObject (opponenetStatus.data));

		if (data.Count == QuizManager.Instance.currentQuizGame.TotalQuestion ||
		    (data [data.Count - 1].quizComplete == 1)) {

			if (QuizManager.Instance.gameComplete == QuizManager.eQuizComplete.Cancelled) {
				GetResultFromServer (ForfeitOpponentScoreCallback);
			} else {
				GetResultFromServer (OpponentScoreCallback);
			}

			isResultLoaded = true;
			QuizManager.Instance.opponentStatus = null;
			StopCoroutine ("WaitForSecondsCallback");
		}
	}

	void GetStatusUpdateCallback ()
	{
		if (this.gameObject.activeInHierarchy) {
			StartCoroutine ("WaitForSecondsCallback");
		}
	}

	IEnumerator WaitForSecondsCallback ()
	{
		yield return new WaitForSeconds (10f);
		GetStatusUpdate ();
	}
	#endif

	void ForfeitOpponentScoreCallback (SeralizedClassServer.QuizResult callback)
	{
		if (callback != null) {
			ShowAfterTaste (callback.after_taste);
			if (callback.opponent.user_id == SportsQuizManager.Instance.opponentInformation.serverID) {
				playerScoreText.text = callback.user.score.ToString ();
				if (callback.opponent.score < 0) {
					GetResultFromServer (ForfeitOpponentScoreCallback);
				} else {
					oppoinentScoreText.text = callback.opponent.score.ToString ();
				}
			} else {
				GetResultFromServer (ForfeitOpponentScoreCallback);
			}
		} else {
			GetResultFromServer (ForfeitOpponentScoreCallback);
		}
	}

	void GetResultFromServer (System.Action<SeralizedClassServer.QuizResult> callback)
	{
		if (this.gameObject.activeInHierarchy == true) {
			ServerManager.Instance.GetQuizResult (QuizManager.Instance.currentQuizGame.userQuizId, callback);
		}
	}

	void OpponentScoreCallback (SeralizedClassServer.QuizResult callback)
	{
		Q.Utils.QDebug.Log ("OpponentScoreCallback "+callback.user.name);
		MainMenuPanelManager.Instance.WaitingPanel (false);

		if (callback != null) {

			ShowAfterTaste (callback.after_taste);

			if (callback.opponent.user_id == SportsQuizManager.Instance.opponentInformation.serverID) {

				if (QuizPanelManager.Instance.isFromResultNotification == false) {
					if (callback.user.score != ScoreManager.Instance.GetTotalScore (true)) {

						if (callback.user.score > ScoreManager.Instance.GetTotalScore (true)) {
							int _diff = callback.user.score - ScoreManager.Instance.GetTotalScore (true);
							scoreAnimText.text = "+ " + _diff.ToString ();
						} else if (callback.user.score < ScoreManager.Instance.GetTotalScore (true)) {
							int _diff = ScoreManager.Instance.GetTotalScore (true) - callback.user.score;
							scoreAnimText.text = "- " + _diff.ToString ();
						}

						if (isScoreAnimPlayed == false) {
							isScoreAnimPlayed = true;
							scoreAnim.Play ();
						}
					}
				}

				playerScoreText.text = callback.user.score.ToString ();
				if (callback.opponent.score < 0) {
					GetResultFromServer (OpponentScoreCallback);
				} else {
					opponentWaitResult.SetActive (false);
					oppoinentScoreText.text = callback.opponent.score.ToString ();
					
					if (callback.user.score == callback.opponent.score) {
						//MATCH TIE
						resultText.text = TagConstant.PopUpMessages.MATCH_TIE;
						
						GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Match_Lost_Tie);
					} else if (callback.user.score > callback.opponent.score) {
						//PLAYER WIN THE MATCH
						resultText.text = TagConstant.PopUpMessages.YOU_WIN;
						
						GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Match_Won);
					} else if (callback.user.score < callback.opponent.score) {
						//PLAYER LOST THE MATCH
						resultText.text = TagConstant.PopUpMessages.YOU_LOSE;
						
						GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Match_Lost_Tie);
					}
					
				}
			} else {
				GetResultFromServer (OpponentScoreCallback);
			}
		} else {

            ShowAfterTaste (QuizManager.Instance.currentQuizGame.afterTaste);
            playerScoreText.text = ScoreManager.Instance.GetTotalScore (true).ToString ();
			GetResultFromServer (OpponentScoreCallback);
		}


		this.LoadUserDetails ();
		//this.InitResultPanel ();
	}

	public override void LoadUserDetails()
    {
        base.LoadUserDetails();

		playerNameText.text = SportsQuizManager.Instance.playerInformation.name;
		playerDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			SportsQuizManager.Instance.playerInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country));

		opponentNameText.text = SportsQuizManager.Instance.opponentInformation.name;
		opponentDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			(SportsQuizManager.eGender)SportsQuizManager.Instance.opponentInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.opponentInformation.country));
    }

    public override void LoadProfileImage()
    {
        base.LoadProfileImage();
		SportsQuizManager.Instance.LoadOpponentProfile (opponentProfileImage);
    }

	void ShowAfterTaste (string _aftertaste)
    {
		afterTestText.text = _aftertaste;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class GameResultSingle : GameResultPanel {

    
	void OnEnable ()
	{
		ShowFullScreenBanner ();
		InitGameColor ();
		InitResultPanel (); 
	}

	public override void InitGameColor ()
	{
		base.InitGameColor ();
	}

	// Use this for initialization
	void Start () {
		InitButtonEvent ();
	}

	public override void ShowFullScreenBanner ()
	{
		base.ShowFullScreenBanner ();
	}

	public override void InitButtonEvent ()
	{
		base.InitButtonEvent ();
	}

	public override void InitResultPanel ()
	{
		base.InitResultPanel ();
		this.LoadScore ();
		this.LoadUserDetails ();
	}

    public override void LoadScore()
    {
        base.LoadScore();
		playerScoreText.text = ScoreManager.Instance.GetTotalScore (true).ToString ();
    }

    public override void LoadProfileImage()
    {
        base.LoadProfileImage();
    }

    public override void LoadUserDetails()
    {
        base.LoadUserDetails();

		playerNameText.text = "<color=" + TagConstant.ColorHexCode.DARK_YELLOW_1+ ">" + SportsQuizManager.Instance.playerInformation.name + "</color> ";
		playerDetailsText.text = SportsQuizManager.UserDetailsColorFormat (
			SportsQuizManager.Instance.playerInformation.gender,
			Country.GetCountryName (SportsQuizManager.Instance.playerInformation.country));
		MainMenuPanelManager.Instance.WaitingPanel (false);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Q.Utils;

public class DuelGamePlayPanel : GamePlayInitPanel {

	[SerializeField] private Text m_playerScoreText, m_opponentScoreText, m_offLineText;
	[SerializeField] private Image m_playerProfileImage, m_opponentProfileImage, scoreBackPanel, scorePanelPlayer, scorePanelOpponent;
	[SerializeField] private Image[] m_answerBarPlayerArray, m_answerBarOpponentArray;
	[SerializeField] private AnswerDot m_AnswerDotPlayer, m_AnswerDotOpponent;

	private float m_opponentSessionTime;
	private float m_dualGap = 70f;
	private AnswerDot[] m_AnswerDotPlayerArray;
	private AnswerDot[] m_AnswerDotOpponentArray;

	void OnEnable () {
		InitAnswerDotImage ();

		m_playerScoreText.text = "0";
		m_opponentScoreText.text = "0";
		InitGamePlay ();
		InitAnswerBar ();
		SportsQuizManager.Instance.AssignUserProfileImage(m_playerProfileImage);
		SportsQuizManager.Instance.LoadOpponentProfile (m_opponentProfileImage);

		MarkQuestion (0, false);
		GetStatusUpdate ();
		OfflineStatus ();
	}

	void OnDisable ()
	{
		if (m_AnswerDotPlayerArray.Length > 0) {
			for (int i = 0; i < m_AnswerDotPlayerArray.Length; i++) {

				Destroy (m_AnswerDotPlayerArray [i].gameObject);
			}
		}

		if (m_AnswerDotOpponentArray.Length > 0) {
			for (int i = 0; i < m_AnswerDotOpponentArray.Length; i++) {

				Destroy (m_AnswerDotOpponentArray [i].gameObject);
			}
		}
	}

	void Start () {
	}

	void InitAnswerDotImage ()
	{
		m_AnswerDotPlayerArray = new AnswerDot[QuizManager.Instance.currentQuizGame.TotalQuestion];
		m_AnswerDotOpponentArray = new AnswerDot[QuizManager.Instance.currentQuizGame.TotalQuestion];

		float _totalLengthPlayer = m_AnswerDotPlayer.GetComponent<RectTransform>().anchoredPosition.x;
		float _totalLengthOpponent = m_AnswerDotOpponent.GetComponent<RectTransform>().anchoredPosition.x;

		for (int i = 0; i < QuizManager.Instance.currentQuizGame.TotalQuestion; i++) {

			//PLAYER QUESTION DOT
			AnswerDot dotPlayer = (AnswerDot) Instantiate (m_AnswerDotPlayer);
			RectTransform rectPlayer = dotPlayer.GetComponent<RectTransform> ();
			dotPlayer.gameObject.RectTransformMakeChild (m_AnswerDotPlayer.transform.parent);

			rectPlayer.anchoredPosition = new Vector2 (_totalLengthPlayer, rectPlayer.anchoredPosition.y);

			_totalLengthPlayer = _totalLengthPlayer + m_dualGap;
			m_AnswerDotPlayerArray [i] = dotPlayer;

			//OPPONENT QUESTION DOT
			AnswerDot dotOpponent = (AnswerDot) Instantiate (m_AnswerDotOpponent);
			RectTransform rectOpponent = dotOpponent.GetComponent<RectTransform> ();
			dotOpponent.gameObject.RectTransformMakeChild (m_AnswerDotOpponent.transform.parent);

			rectOpponent.anchoredPosition = new Vector2 (_totalLengthOpponent, rectOpponent.anchoredPosition.y);

			_totalLengthOpponent = _totalLengthOpponent - m_dualGap;
			m_AnswerDotOpponentArray [i] = dotOpponent;
		}
	}

	public override void InitGamePlay ()
	{
		base.InitGamePlay ();
		ScoreManager.Instance.opponentCurrentQuizScore = new System.Collections.Generic.List<ScoreManager.Score> ();
	}

	void InitAnswerBar ()
	{
		for (int i = 0; i < m_answerBarPlayerArray.Length; i++) {
			m_answerBarPlayerArray [i].color = gamePlayPanel.normalColor;
		}

		for (int i = 0; i < m_answerBarOpponentArray.Length; i++) {
			m_answerBarOpponentArray [i].color = gamePlayPanel.normalColor;
		}
	}

	void GetStatusUpdate ()
	{
		QuizManager.Instance.opponentStatus = UpdateOpponentAnswer;
		QuizManager.Instance.StartStatusUpdate (QuizManager.Instance.currentQuizGame.userQuizId, GetStatusUpdateCallback);

		m_opponentSessionTime = Time.time;
		StartCoroutine ("CheckOpponentWaitingTime");
	}

	IEnumerator CheckOpponentWaitingTime ()
	{
		while (m_opponentSessionTime + 20f > Time.time) {
			yield return 0;
		}

		if (m_opponentSessionTime != 0) {
			SportsQuizManager.Instance.opponentInformation.isOnline = false;
			OfflineStatus ();
		}
	}

	void GetStatusUpdateCallback ()
	{
		SportsQuizManager.Instance.opponentInformation.isOnline = true;
		OfflineStatus ();

		m_opponentSessionTime = 0;
		StopCoroutine ("CheckOpponentWaitingTime");

		if (this.gameObject.activeInHierarchy)
			StartCoroutine ("WaitForSecondsCallback");
	}

	IEnumerator WaitForSecondsCallback ()
	{
		yield return new WaitForSeconds (2f);
		GetStatusUpdate ();
	}

	public void UpdateOpponentAnswer (SeralizedClassServer.Status opponenetStatus)
	{
		Q.Utils.QDebug.Log ("UpdateOpponentAnswer");

		ScoreManager.Instance.opponentCurrentQuizScore = new List<ScoreManager.Score> ();

		List <SeralizedClassServer.StatusData> data = 
			JsonConvert.DeserializeObject<List<SeralizedClassServer.StatusData>> (JsonConvert.SerializeObject (opponenetStatus.data));

		for ( int i = 0; i < data.Count; i++) {

			int _correctAnswer = QuizManager.Instance.GetOpponentCorrectAnswer (data[i].question_id);
			
			ScoreManager.Instance.AddOpponentScore (data[i].question_id, data[i].score);
		}

		if (data.Count < QuizManager.Instance.currentQuizGame.TotalQuestion - 1) {
			
			MarkQuestion (data.Count - 1, false);
			MarkQuestion (data.Count, false);
		}
		
		ShowOpponentScore (ScoreManager.Instance.GetTotalScore (false));
	}

	public override void MarkQuestion (int index, bool player = true)
	{
		if (player) {
			m_AnswerDotPlayerArray [index].ToggleImage ();
		} else {
			m_AnswerDotOpponentArray [index].ToggleImage ();
		}
	}

	public override Image GetAnswerBar (int index, bool player = true)
	{
		if (player)
			return m_answerBarPlayerArray [index];
		else
			return m_answerBarOpponentArray [index];
	}

	public void OfflineStatus ()
	{
		if (SportsQuizManager.Instance.opponentInformation.isOnline == true) {
			m_offLineText.color = Color.green;
		} else {
			m_offLineText.color = Color.red;
		}
	}

	public override void ShowPlayerScore (int score)
	{
		m_playerScoreText.text = score.ToString ();
	}

	public override void LoadPlayerProfileImage (Texture2D texture)
	{
		m_playerProfileImage.CreateImageSprite (texture);
	}

	public override void ShowOpponentScore (int score)
	{
		m_opponentScoreText.text = score.ToString ();
	}

	public override void LoadOpponentProfileImage (Texture2D texture)
	{
		m_opponentProfileImage.CreateImageSprite (texture);
	}

	public override void SaveScore (int _questionID, float _timeTaken)
	{
		base.SaveScore (_questionID, _timeTaken);

		//UPDATE TO OPPONENT....
		int _currentQuizIndex = QuizManager.Instance.currentQuizGame.currentQuestion;
		SeralizedClassServer.StatusData data = new SeralizedClassServer.StatusData ();
		data.count = _currentQuizIndex;
		data.question_id = QuizManager.Instance.currentQuizGame.answerSelectedList [_currentQuizIndex].questionId;
		data.answer_id =  QuizManager.Instance.currentQuizGame.answerSelectedList [_currentQuizIndex].answerId;
		data.score = ScoreManager.Instance.GetCurrentScore (true);

		QuizManager.Instance.UpdateStatus (data);
	}
}

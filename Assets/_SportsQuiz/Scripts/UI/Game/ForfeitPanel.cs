﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class ForfeitPanel : MonoBehaviour {

	[SerializeField] private Button yesButton;
	[SerializeField] private Button noButton;
	[SerializeField] private Text headerText, descritionText;
	[SerializeField] private Image panel;
	[SerializeField] private Image subpanel;

	public System.Action forfeitCallback;

	void OnEnable ()
	{
		descritionText.text = TagConstant.PopUpMessages.SURRENDER_INFO;
	}

	// Use this for initialization
	void Start () {
		yesButton.onClick.AddListener(OnClickOkButton);
		noButton.onClick.AddListener(OnClickNoButton);
	}

	void OnClickOkButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Forfeit_Button_Click);
		GenericAudioManager.StopSoundFX (GenericAudioManager.SFXSounds.Clock_Ticking);
		QuizManager.Instance.QuizComplete (QuizManager.eQuizComplete.Cancelled);
		this.gameObject.SetActive (false);
	}

	void OnClickNoButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		this.gameObject.SetActive (false);
		forfeitCallback ();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class QuizPanelManager : MonoBehaviour {

	public static QuizPanelManager Instance; 

	public bool debugMode = false;

	public enum eQuizPanelState
	{
		UnKnown = -1,
		StartPanel = 0,
		PlayPanel,
		ResultPanel,
		CategoryPanel
	}

	public QuizManager.eQuizMode modeSelected;
	public eQuizPanelState currentPanelState;

	public GameInitPanel gameInitPanel;
	public GamePlayPanel gamePlayPanel;
	public GameResultPanel gameSingleResultPanel;
	public GameResultPanel gameDuelResultPanel;
	public HintPanel hintPanel;
	public ForfeitPanel forfeitPanel;
	public bool isFromResultNotification = false;

    [Header("AFTER TEST PANEL")]
    public AfterTastePanel afterTastePanel;
    
    void Awake () {

		if (Instance == null)
			Instance = this;
		else
			DestroyImmediate (this);

		gameInitPanel.gameObject.SetActive (false);
		gamePlayPanel.gameObject.SetActive (false);
		gameSingleResultPanel.gameObject.SetActive (false);
        gameDuelResultPanel.gameObject.SetActive (false);
        hintPanel.gameObject.SetActive (false);
		forfeitPanel.gameObject.SetActive (false);
        afterTastePanel.gameObject.SetActive(false);


    }

	// Use this for initialization
	void Start () {
		#if DEBUG_MODE
		SwitchPanel (eQuizPanelState.InitPanel);
		#endif

		/*endMatchOkButton.onClick.AddListener (OnClickEndMatchOkButton);
		endMatchCancelButton.onClick.AddListener (OnClickEndMatchCancelButton);*/
	}
	

	public void SwitchPanel(eQuizPanelState state)
	{
		currentPanelState = state;

		#if DEBUG_MODE
		gameInitPanel.gameObject.SetActive (state == eQuizPanelState.InitPanel);
		#else
		if (state == eQuizPanelState.CategoryPanel) {
			MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.CategoryPanel);
		}
		else if (state == eQuizPanelState.StartPanel) {
			MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.Gameplay);
		}
		#endif

		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual &&
			SportsQuizManager.Instance.opponentInformation.serverID == 0) {

			gamePlayPanel.gameObject.SetActive (state == eQuizPanelState.PlayPanel);
			
		} else {
			gamePlayPanel.gameObject.SetActive (state == eQuizPanelState.PlayPanel);
			gameInitPanel.gameObject.SetActive (state == eQuizPanelState.StartPanel);
		}

        if (state == eQuizPanelState.ResultPanel)
        {
            if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure)
            {
		        gameSingleResultPanel.gameObject.SetActive (true);
                gameDuelResultPanel.gameObject.SetActive (false);
            }
            else
            {
                gameSingleResultPanel.gameObject.SetActive(false);
                gameDuelResultPanel.gameObject.SetActive(true);
            }

		} else {
			gameSingleResultPanel.gameObject.SetActive (false);
			gameDuelResultPanel.gameObject.SetActive (false);
		}
	}

	/*public void ShowEndMatch (bool show)
	{
		surrenderInfoText.text = TagConstant.PopUpMessages.SURRENDER_INFO;
		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {
			endPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {
			endPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LUCKY_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {
			endPanel.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DUAL_MODE);
		} 
		endMatchPanel.SetActive (show);
	}*/

	/*void OnClickEndMatchOkButton () {

		//STOP CLOCK SFX
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Forfeit_Button_Click);
		GenericAudioManager.StopSoundFX (GenericAudioManager.SFXSounds.Clock_Ticking);
		QuizManager.Instance.QuizComplete (QuizManager.eQuizComplete.Cancelled);
		ShowEndMatch (false);
	}*/

	public void ShowNotificationResult (SeralizedClassServer.NotificationData data)
	{
//		data.category_id;
//		data.category_title;

		isFromResultNotification = true;
		SportsQuizManager.Instance.currentCategory.topicLabel = data.category_title;
		SportsQuizManager.Instance.selectedMode = (QuizManager.eQuizMode)data.quiz_type;
		SportsQuizManager.Instance.currentCategory.mainCategorySelected = data.category_id;

		SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();
		SportsQuizManager.Instance.currentSelectedTopics.category_id = data.category_id;
		SportsQuizManager.Instance.currentSelectedTopics.category_title = data.category_title;
		SportsQuizManager.Instance.currentSelectedTopics.type = data.quiz_type;

		if (QuizManager.Instance.currentQuizGame == null) {
			QuizManager.Instance.currentQuizGame = new QuizManager.CurrentQuizPlay ();
		}
		QuizManager.Instance.currentQuizGame.userQuizId = data.user_quiz_id;

		SportsQuizManager.Instance.GetOpponentDetail (data.user_id, OpponentDetailCall);

	}

	void OpponentDetailCall (SeralizedClassServer.OpponentDetails callback)
	{
		SportsQuizManager.Instance.opponentInformation.serverID = callback.user_info.user_id;
		SportsQuizManager.Instance.opponentInformation.name = callback.user_info.name;
		SportsQuizManager.Instance.opponentInformation.age = callback.user_info.age;
		SportsQuizManager.Instance.opponentInformation.avatarType = callback.user_info.avatar_type;
		SportsQuizManager.Instance.opponentInformation.country = callback.user_info.country;
		SportsQuizManager.Instance.opponentInformation.email = callback.user_info.email_id;
		SportsQuizManager.Instance.opponentInformation.facebook_id = callback.user_info.facebook_id;
		SportsQuizManager.Instance.opponentInformation.gender = callback.user_info.gender;

		if (SportsQuizManager.Instance.selectedDuelFriendDetail == null) {
			SportsQuizManager.Instance.selectedDuelFriendDetail = new SportsQuizManager.InviteFriendDetail ();
		}

		SportsQuizManager.Instance.selectedDuelFriendDetail.name = callback.user_info.name;
		SportsQuizManager.Instance.selectedDuelFriendDetail.userType = SportsQuizManager.eUserType.EmailUser;
		SportsQuizManager.Instance.selectedDuelFriendDetail.user_id = (object)callback.user_info.user_id;
		SportsQuizManager.Instance.otherUSerType = SportsQuizManager.eUserType.EmailUser;
		//MainMenuPanelManager.Instance.WaitingPanel (false);

		SwitchPanel (eQuizPanelState.ResultPanel);
	}

	/*void OnClickEndMatchCancelButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ShowEndMatch (false); 
	}*/
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignInPanel : MonoBehaviour {

	public InputField emailIdInputField;
	public InputField passwordInputField;

	public Button loginButton;
	public Button cancelButton;
	public Button signUpButton;
	public Button forgotPasswordButton;
	public Toggle showPassordButton;

	public GameObject forgotPasswordPanel;

	private string m_emailId;
	private string m_passwordid;
	[SerializeField]private FieldValidation fieldValidation;
	private float delay = 0.5f;


	void OnEnable () {
		GoogleAdHandler.Instance.ShowBanner ();
	}

	void OnDisable()
	{
		GoogleAdHandler.Instance.HideBanner ();
	}

	// Use this for initialization
	void Start () {
		//Invoke("CheckPreviousLogin", delay);
		fieldValidation = GetComponent<FieldValidation> ();

		emailIdInputField.onEndEdit.AddListener (OnEndEditEmailInputField);
		passwordInputField.onEndEdit.AddListener (OnEndEditPasswordInputField);
		loginButton.onClick.AddListener (OnClickLoginButton);
		signUpButton.onClick.AddListener (OnClickSignUpButton);
		cancelButton.onClick.AddListener (OnClickCancelButton);
		forgotPasswordButton.onClick.AddListener (OnClickForgotPasswordButton);
		showPassordButton.onValueChanged.AddListener (OnClickShowPassword);
	}

	//void CheckPreviousLogin()
	//{
	//	if (GamePrefs.LoginType != 0) {
	//		AutoSignIn ();
	//	}
	//}

	void OnClickShowPassword(bool isOn)
	{
		if (isOn == false) {
			passwordInputField.enabled = false;
			passwordInputField.contentType = InputField.ContentType.Standard;
			passwordInputField.enabled = true;
		} else {
			passwordInputField.enabled = false;
			passwordInputField.contentType = InputField.ContentType.Password;
			passwordInputField.enabled = true;
		}
	}

	void OnEndEditEmailInputField (string text)
	{
		m_emailId = text;
	}

	void OnEndEditPasswordInputField (string text)
	{
		m_passwordid = text;
	}

	void OnClickLoginButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		if (fieldValidation.GetEmailIdAuthentication (m_emailId)) {
			if (fieldValidation.GetPasswordAuthentication (m_passwordid)) {
				//SaveLogInDetails ();
				MainMenuPanelManager.Instance.WaitingPanel (true);
				ConnectionHandler.Instance.LoginEmail (m_emailId, m_passwordid);
				ClearFields ();
 				this.gameObject.SetActive (false);
				if (showPassordButton.isOn == false) {
					showPassordButton.isOn = true;
					OnClickShowPassword (true);
				}
			} else {
				fieldValidation.ShowErrorMessage ("Password should consist atleast 6 characters");
			}
		} else {
			fieldValidation.ShowErrorMessage ("Invalid Email ID");
		}
	}

	void OnClickSignUpButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.splashScreen.signUpHandler.gameObject.SetActive (true);
		this.gameObject.SetActive (false);
	}

	void OnClickCancelButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		this.gameObject.SetActive (false);
	}

	void OnClickForgotPasswordButton()
	{
		forgotPasswordPanel.SetActive (true);
	}

	public void SignUpEmail () 
	{
		fieldValidation.ShowErrorMessage ("Username doesn't exist! Please Register.");
		MainMenuPanelManager.Instance.splashScreen.signUpHandler.gameObject.SetActive (true);
		this.gameObject.SetActive (false);
	}

	public void ClearFields()
	{
		emailIdInputField.text = "";
		passwordInputField.text = "";
		m_emailId = "";
		m_passwordid = "";
	}

}
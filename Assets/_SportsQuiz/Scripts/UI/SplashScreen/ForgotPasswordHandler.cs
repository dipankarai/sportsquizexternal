﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ForgotPasswordHandler : MonoBehaviour 
{
	public InputField emailInputField;
	public Button resetButton;
	public Button cancelButton;

	void Start()
	{
		resetButton.onClick.AddListener (OnClickResetButton);
		cancelButton.onClick.AddListener (OnClickCancelButton);
	}

	void OnClickResetButton()
	{
		if (!string.IsNullOrEmpty (emailInputField.text)) {
			ServerManager.Instance.ResetPassword(emailInputField.text, ResetPasswordCallback);
		}
	}

	void ResetPasswordCallback(bool isSent)
	{
		if (isSent) {	
			MainMenuPanelManager.Instance.ShowMessage ("Reset link has been sent to your mail ID", MainMenuPanelManager.eMainMenuState.SignInUpPanel);
			emailInputField.text = "";
			this.gameObject.SetActive (false);
		} else {
			emailInputField.text = "";
			MainMenuPanelManager.Instance.ShowMessage ("Entered Email ID does not exists", MainMenuPanelManager.eMainMenuState.SignInUpPanel);
		}
	}

	void OnClickCancelButton()
	{
		emailInputField.text = "";
		this.gameObject.SetActive (false);
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignInUpPanel : MonoBehaviour {

	public enum eSignInUp
	{
		SignIn,
		SignUp,
		EmailSignIn,
		EmailSignUp
	}

	[Header("PANELS")]
	public GameObject signInPanel;
	public GameObject signUpPanel;
	public GameObject emailSignInPanel;
	public GameObject emailSignUpPanel;

    [Header("BUTTONS")]
	public Button backButton;
	public Button fbSignIn;
	public Button emailSignIn;
	public Button fbSignUp;
	public Button emailSignUp;
    public Button emailSignInSubmitButton;
    public Button emailSignUpSubmitButton;
	public Button forgetPassword;

    [Header("InputField")]
    public InputField emailSignInField;
    public InputField passwordSignInField;
    public InputField emailSignUpField;
	public InputField passwordSignUpField;
    public InputField passwordConfirmField;

	private eSignInUp m_currentState;
    private string m_signText;
	private string m_passwordText;
    private string m_passwordConfirmText;

	// Use this for initialization
    void Start () {
		backButton.onClick.AddListener (OnClickBackButton);

		fbSignIn.onClick.AddListener (OnClickFBSignInButton);
		emailSignIn.onClick.AddListener (OnClickEmailSignInButton);
		fbSignUp.onClick.AddListener (OnClickFBSignUpButton);

		emailSignUp.onClick.AddListener (OnClickEmailSignUpButton);
		forgetPassword.onClick.AddListener (OnClickForgetPasswordButton);

		emailSignInSubmitButton.onClick.AddListener (OnClickSignInToServerButton);
		emailSignUpSubmitButton.onClick.AddListener (OnClickSignUpToServerButton);

		emailSignInField.onEndEdit.AddListener (OnEndEditEmail);
		passwordSignInField.onEndEdit.AddListener (OnEndEditPassword);
        
		emailSignUpField.onEndEdit.AddListener (OnEndEditEmail);
		passwordSignUpField.onEndEdit.AddListener (OnEndEditPassword);
		passwordConfirmField.onEndEdit.AddListener (OnEndEditConfirmPassword);
    }

	void OnClickBackButton ()
	{
		if (m_currentState == eSignInUp.EmailSignIn)
		{
			EnablePanel (eSignInUp.SignIn);
		}
		else if (m_currentState == eSignInUp.EmailSignUp)
		{
			EnablePanel (eSignInUp.SignUp);
		}
		else if (m_currentState == eSignInUp.SignIn || m_currentState == eSignInUp.SignUp) {
			MainMenuPanelManager.Instance.splashScreen.DisableSignInUpPanel ();
		}
	}

	void OnClickFBSignInButton()
	{
        if (InternetConnection.Check)
            ConnectionHandler.Instance.LoginFacebook();
    }

	void OnClickFBSignUpButton()
	{
		if (InternetConnection.Check)
			ConnectionHandler.Instance.LoginFacebook();
	}

	void OnClickEmailSignInButton()
	{
		m_signText = "";
		m_passwordText = "";
		EnablePanel (eSignInUp.EmailSignIn);
	}
	void OnClickEmailSignUpButton()
	{
		m_signText = "";
		m_passwordText = "";
		EnablePanel (eSignInUp.EmailSignUp);
	}

	void OnClickForgetPasswordButton ()
	{
		if (string.IsNullOrEmpty (m_signText))
		{
			MainMenuPanelManager.Instance.ShowMessage ("Enter your email ID",
				MainMenuPanelManager.eMainMenuState.Unknown,
				PopUpMessage.eMessageType.Warning);
			
			return;
		}
	}

    void OnClickSignInToServerButton()
    {
		ConnectionHandler.Instance.LoginEmail (m_signText, m_passwordText);
    }

    void OnClickSignUpToServerButton ()
    {
		if (m_passwordText != m_passwordConfirmText)
		{
			passwordConfirmField.text = "";
			MainMenuPanelManager.Instance.ShowMessage ("Password Doesn't match!",
				MainMenuPanelManager.eMainMenuState.Unknown,
				PopUpMessage.eMessageType.Error);

			return;
		}
		Debug.Log ("Match");
//		ConnectionManager.Instance.SignUpEmail (m_signText, m_passwordText);
    }

    void OnEndEditEmail(string text)
    {
        m_signText = text;
    }

    void OnEndEditPassword(string text)
    {
        m_passwordText = text;
    }

	void OnEndEditConfirmPassword (string text)
	{
		m_passwordConfirmText = text;
	}

    public void EnablePanel (eSignInUp panelState)
	{
		m_currentState = panelState;
		signInPanel.SetActive (panelState == eSignInUp.SignIn);
		signUpPanel.SetActive (panelState == eSignInUp.SignUp);
		emailSignInPanel.SetActive (panelState == eSignInUp.EmailSignIn);
		emailSignUpPanel.SetActive (panelState == eSignInUp.EmailSignUp);
	}
	
	void OnDisable ()
	{
		ResetProperties ();
    }

	void ResetProperties ()
	{
		signInPanel.SetActive (true);
		signUpPanel.SetActive (false);
		emailSignInPanel.SetActive (false);
		emailSignUpPanel.SetActive (false);

		m_signText = "";
		m_passwordText = "";

		emailSignInField.text = "";
		passwordSignInField.text = "";
		emailSignUpField.text = "";
		passwordSignUpField.text = "";
		passwordConfirmField.text = "";
	}
}

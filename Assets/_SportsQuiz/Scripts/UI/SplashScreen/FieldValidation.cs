﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FieldValidation : MonoBehaviour
{
    public const string EMAIL_ID_ERROR = "Please enter a valid Email ID.";
    public const string ERROR = "Error Message";
    public const string PASSWORD_ERROR = "Password can't be left blank.";
    public const string PASSWORD_ERROR_LENGTH = "Password must containt minimum 6 characters.";
    public const string RETYPE_PASSWORD_ERROR = "Password mismatch.";

    private int numberOfCharacters;
    private string firstEmailId;

    public bool GetEmailIdAuthentication(string _emailId)
    {
//		return true;
		if (!string.IsNullOrEmpty (_emailId)) {			
			if ((_emailId.Contains ("@")) && _emailId.Contains (".")) { // _email id contain @ and . 
				if (GetNumberOfCharacterString (_emailId, '@') < 2) { // number character '.' and '@'
					string[] emailCharacterAtRate = _emailId.Split ('@');
					if (emailCharacterAtRate [1].Contains (".")) { // after  @ , . should come
						string[] emailDot = emailCharacterAtRate [1].Split ('.');
						if ((!emailCharacterAtRate [0].Equals ("")) && (!emailDot [0].Equals ("")) && (!emailDot [1].Equals (""))) { // after @ and . chareter is not empty
							if ((emailCharacterAtRate [0] + "@" + emailDot [0] + "." + emailDot [1]).Equals (_emailId)) { // _emailid is in this format " string@string.string"
								return true;
							} else { // _emailid is not in this format " string@string.string"
								return false;
							}
						} else { // after @ and . chareter is empty
							return false;
						}
					} else {  // after  @ , . is not coming
						return false;
					}
				} else {
					return false;
				}
			} else { // _email id  not contain @ and . 
				return false;
			}
		} else {
			return false;
		}
    }

	public bool GetPasswordAuthentication(string pwd)
	{
//		return true;
		if (!string.IsNullOrEmpty (pwd)) {
			if (pwd.Length >= 6)
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

    public bool GetMobileNumberAuthtication(string _mbl)
    {
        if (_mbl.Length >= 7)
            return true;
        else
            return false;
    }

    public bool GetNameAuthentication(string _name)
    {
        if (_name != "")
            return true;
        else
            return false;
    }

	public bool AgeValidation(int age)
	{
		if (age >= 12 && age <= 99) {
			return true;
		} else {
			return false;
		}
	}

    int GetNumberOfCharacterString(string _str, char _ch)
    {
        numberOfCharacters = 0;
        for (int i = 0; i < _str.Length; i++)
        {
            if (_str[i].Equals(_ch))
            {
                numberOfCharacters++;
            }
        }
        return numberOfCharacters;
    }

    public void ShowErrorMessage(string txt)
    {
		MainMenuPanelManager.Instance.ShowMessage (txt,
			MainMenuPanelManager.eMainMenuState.Unknown,
			PopUpMessage.eMessageType.Error);
    }    

}
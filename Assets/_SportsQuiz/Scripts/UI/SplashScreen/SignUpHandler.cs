﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignUpHandler : MonoBehaviour {

	public InputField usrNameInputField;
	public InputField emailIdInputField;
	public InputField passwordInputField;
	public InputField confirmPwdInputField;

	public Button signUpButton;
	public Button signInButton;
	public Button cancelButton;

	private string m_userName;
	private string m_emailId;
	private string m_passwordid;
	private string m_confirmPwdId;

	[SerializeField]private FieldValidation fieldValidation;

	// Use this for initialization
	void Start () {

		fieldValidation = GetComponent<FieldValidation> ();

		usrNameInputField.onEndEdit.AddListener (OnEndEditUsrNameInputField);
		emailIdInputField.onEndEdit.AddListener (OnEndEditEmailInputField);
		passwordInputField.onEndEdit.AddListener (OnEndEditPasswordInputField);
		confirmPwdInputField.onEndEdit.AddListener (OnEndEditConfirmPasswordInputField);
		signUpButton.onClick.AddListener (OnClickSignUpButton);
		signInButton.onClick.AddListener (OnClickSignInButton);
		cancelButton.onClick.AddListener (OnClickCancelButton);
	}

	void OnEndEditUsrNameInputField (string text)
	{
		m_userName = text;
	}

	void OnEndEditEmailInputField (string text)
	{
		m_emailId = text;
	}

	void OnEndEditPasswordInputField (string text)
	{
		m_passwordid = text;
	}

	void OnEndEditConfirmPasswordInputField (string text)
	{
		m_confirmPwdId = text;
	}

	void OnClickCancelButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ClearFields ();
		this.gameObject.SetActive (false);
	}

	private void OnClickSignInButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		MainMenuPanelManager.Instance.splashScreen.signInPanel.gameObject.SetActive (true);
		this.gameObject.SetActive (false);
	}

	private void OnClickSignUpButton()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);

		if (fieldValidation.GetNameAuthentication (m_userName)) {
			if (fieldValidation.GetEmailIdAuthentication (m_emailId)) {	
				if (fieldValidation.GetPasswordAuthentication (m_passwordid)) {	
					if (confirmPwdInputField.text.Equals (m_passwordid)) {
						//SaveLogInDetails (); 
						MainMenuPanelManager.Instance.WaitingPanel (true);
						ConnectionHandler.Instance.SignUpEmail (m_userName, m_emailId, m_passwordid);
						//					ClearFields ();
					} else {
						fieldValidation.ShowErrorMessage ("Password Doesn't match!");
					}
				} else {
					fieldValidation.ShowErrorMessage ("Password should consist atleast 6 characters!");
				}
			} else {
				fieldValidation.ShowErrorMessage ("Invalid Email ID");
			}
		} else {
			fieldValidation.ShowErrorMessage ("Enter valid User name");
		}
	}

	public void ClearFields()
	{
		usrNameInputField.text = "";
		emailIdInputField.text = "";
		passwordInputField.text = "";
		confirmPwdInputField.text = "";
		m_emailId = "";
		m_passwordid = "";
		m_confirmPwdId = "";
	}

	//public void SaveLogInDetails()
	//{
	//	if (!string.IsNullOrEmpty (m_emailId) && !string.IsNullOrEmpty (m_passwordid)) 
	//	{			
	//		GamePrefs.EmaiID = m_emailId;
	//		GamePrefs.EmailPassword = m_passwordid;	
	//	}
	//}
}
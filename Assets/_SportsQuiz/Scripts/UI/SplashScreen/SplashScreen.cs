﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Q.Utils;

public class SplashScreen : MonoBehaviour {

	public Button signInButton;
	public Button signUpButton;

	public GameObject buttonPanel;
	public SignInUpPanel signInUpPanel;

	public SignInPanel signInPanel;
	public SignUpHandler signUpHandler;
	public TryPanel tryPanel;
	public Button tryModeButton;
	public Button facebookLogInButton;
	public Button googlePlusButton;
	public Button emailSignInButton;

    void Awake ()
    {
    }

	// Use this for initialization
	void Start () {

		signInPanel.gameObject.SetActive (false);
		tryPanel.gameObject.SetActive (false);
		signUpHandler.gameObject.SetActive (false);
		tryModeButton.onClick.AddListener (OnClickTryModeButton);
		facebookLogInButton.onClick.AddListener (OnClickFacebookLogInButton);
		googlePlusButton.onClick.AddListener (OnClickGooglePlusLogInButton);
		emailSignInButton.onClick.AddListener (OnClickEmailSignInButton);
	}

	void OnClickTryModeButton ()
	{
		tryPanel.gameObject.SetActive (true);
	}

	void OnClickFacebookLogInButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ConnectionHandler.Instance.LoginFacebook();
	}

	void OnClickGooglePlusLogInButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		ConnectionHandler.Instance.LoginGooglePlus();
	}

	void OnClickEmailSignInButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		signInPanel.gameObject.SetActive (true);
	}

	void OnClickSignInButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		Q.Utils.QDebug.Log ("OnClickSignInButton");
		EnableSignInUpPanel (SignInUpPanel.eSignInUp.SignIn);
	}

	void OnClickSignUpButton ()
	{
		GenericAudioManager.PlayFX (GenericAudioManager.SFXSounds.Button_Click_1);
		Q.Utils.QDebug.Log ("OnClickSignUpButton");
		EnableSignInUpPanel (SignInUpPanel.eSignInUp.SignUp);
	}

	void EnableSignInUpPanel (SignInUpPanel.eSignInUp state)
	{
		buttonPanel.gameObject.SetActive (false);
		signInUpPanel.gameObject.SetActive (true);
		signInUpPanel.EnablePanel (state);
	}

	public void DisableSignInUpPanel ()
	{
		buttonPanel.gameObject.SetActive (true);
		signInUpPanel.gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SponseredQuiz : MonoBehaviour {

	private static SponseredQuiz m_instance;
	public static SponseredQuiz Instance
	{
		get
		{
			if (m_instance == null)
			{
				GameObject go = new GameObject ("SponseredQuiz");
				m_instance = go.AddComponent<SponseredQuiz> ();
			}
			return m_instance;
		}
		private set { }
	}

	public List<SeralizedClassServer.Category> sponseredQuizCategoryList = new List<SeralizedClassServer.Category> ();
	public SportsQuizManager.SponseredCategoryData sponseredCategoryCategory;
	public System.Action sponseredLoadedCallback;

	void Awake ()
	{
		if (m_instance != null) {
			DestroyImmediate (this);
		} else {
			m_instance = this;
			DontDestroyOnLoad (this);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateSponsoredQuiz (List<SeralizedClassServer.Category> dataList)
	{
		StartCoroutine ("LoadCategoryToLocal", dataList);
	}

	IEnumerator LoadCategoryToLocal (List<SeralizedClassServer.Category> dataList)
	{
		for (int i = 0; i < dataList.Count; i++) {
			if (GetCategoryFromLocal (dataList[i].category_id) == null) {

				SportsQuizManager.SponseredCategoryData _newData = new SportsQuizManager.SponseredCategoryData ();
				_newData.category_id = dataList[i].category_id;
				_newData.category_title = dataList[i].category_title;
				_newData.image = dataList[i].image;
				_newData.type = dataList[i].type;

				WWW image = new WWW (dataList[i].image);
				yield return image;
				if (image.error == null)
				{
					_newData.texture = image.texture;

					SportsQuizManager.Instance.sponsoredCategoryData.Add (_newData);

				}
				else {
					Q.Utils.QDebug.LogError(image.error);
				}

				Instance.sponseredCategoryCategory = _newData;

			} else {
				Instance.sponseredCategoryCategory = GetCategoryFromLocal (dataList [i].category_id);
			}
		}

		if (Instance.sponseredLoadedCallback != null)
			Instance.sponseredLoadedCallback ();
	}

	SportsQuizManager.SponseredCategoryData GetCategoryFromLocal (int id) {

		for (int i = 0; i < SportsQuizManager.Instance.sponsoredCategoryData.Count; i++) {
			if (SportsQuizManager.Instance.sponsoredCategoryData [i].category_id == id) {
				return SportsQuizManager.Instance.sponsoredCategoryData [i];
			}
		}

		return null;
	}
}

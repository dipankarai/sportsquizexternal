﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LayoutElementInitalizer : MonoBehaviour {

	private LayoutElement layoutElement;
	[SerializeField] private bool setContentAlso = true;
	[SerializeField] private float offset = 0;

	void Awake () {
		if (layoutElement == null)
			layoutElement = GetComponent<LayoutElement> ();

		if (layoutElement.minWidth != 0) {
			layoutElement.minWidth = (MainMenuPanelManager.Instance.maincanvas.GetComponent<RectTransform> ().sizeDelta.x - offset);

			if (setContentAlso)
			{
				Vector2 _anchoredPosition = transform.parent.GetComponent<RectTransform> ().anchoredPosition;
				_anchoredPosition.x = MainMenuPanelManager.Instance.maincanvas.GetComponent<RectTransform> ().sizeDelta.x / 2f;
				_anchoredPosition.x *= -1f;
				transform.parent.GetComponent<RectTransform> ().anchoredPosition = _anchoredPosition;
			}
		}
	}

	// Use this for initialization
	void Start () {
	
	}
}

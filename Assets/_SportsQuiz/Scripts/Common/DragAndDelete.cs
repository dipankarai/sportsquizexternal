﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;

public class DragAndDelete : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	private Vector2 startPosition;
	private Vector2 m_offset;
	private bool m_isDeleted = false;
	private CanvasGroup m_CanvasGroup;
	//private float m_startAlphaSet;
	private float m_screenWidth;

	private Action m_DeleteCallback;
	private Action<float> m_AlphaCallback;
	private UnityEngine.UI.ScrollRect m_scrollRect;
	private RectTransform m_rectTransform;
	private Vector2 m_previousEventPosition;
	private bool m_canDrag = false;

	void Init () {
		if (m_CanvasGroup == null) {
			if (GetComponent<CanvasGroup> () != null) {
				m_CanvasGroup = GetComponent<CanvasGroup> ();
			} else {
				m_CanvasGroup = this.gameObject.AddComponent<CanvasGroup> ();
			}
		}

		if (m_rectTransform == null) {
			m_rectTransform = GetComponent<RectTransform> ();
		}

		m_screenWidth = MainMenuPanelManager.Instance.maincanvas.GetComponent<UnityEngine.UI.CanvasScaler> ().referenceResolution.x;
	}

	public void InitDragAndDrop (Action callbackDelete, Action<float> alphaCalllback, UnityEngine.UI.ScrollRect scrollRect = null)
	{
		Init ();
		m_DeleteCallback = callbackDelete;
		m_AlphaCallback = alphaCalllback;
		if (scrollRect != null) {
			m_scrollRect = scrollRect;
		}
	}

	public void OnBeginDrag (PointerEventData eventData)
	{
		m_scrollRect.OnBeginDrag (eventData);
		if (m_CanvasGroup == null) {
			Init ();
		}
		m_previousEventPosition = eventData.position;
		startPosition = new Vector2 (this.transform.localPosition.x, this.transform.localPosition.y );
		m_offset = new Vector2 (transform.position.x, transform.position.y);
		m_offset.x -= eventData.position.x;
		m_CanvasGroup.blocksRaycasts = false;

	}

	public void OnDrag (PointerEventData eventData)
	{
		if ((m_canDrag == false) && VerticalDrag (eventData.position)) {
			m_scrollRect.OnDrag (eventData);
			return;
		}

		m_canDrag = HorizontalDrag (eventData.position);
		if (m_canDrag) {
			Vector2 _tempPosition = this.transform.position;
			_tempPosition.x = eventData.position.x + m_offset.x;
			this.transform.position = _tempPosition;
			
			m_CanvasGroup.alpha = GetAlphaVAlue ();
			m_AlphaCallback (GetAlphaVAlue ());
			
			if (m_rectTransform.anchoredPosition.x > (m_screenWidth / 2.5f)) {
				m_isDeleted = true;
				if (m_DeleteCallback != null) {
					m_DeleteCallback ();
				}
			}
		} else {
			m_scrollRect.OnDrag (eventData);
		}
	}

	bool VerticalDrag (Vector2 verticalPos)
	{
		if (m_previousEventPosition.y < verticalPos.y) {
			if ((m_previousEventPosition.y + 25f) < verticalPos.y) {
				return true;
			}
		} else if (m_previousEventPosition.y > verticalPos.y) {
			if ((m_previousEventPosition.y - 25f) > verticalPos.y) {
				return true;
			}
		}
	
		return false;
	}

	bool HorizontalDrag (Vector2 horizontalPos)
	{
		if (m_previousEventPosition.x < horizontalPos.x) {
			if ((m_previousEventPosition.x + 25f) < horizontalPos.x) {
				return true;
			}
		} else if (m_previousEventPosition.x > horizontalPos.x) {
			if ((m_previousEventPosition.x - 25f) > horizontalPos.x) {
				return true;
			}
		}

		return false;
	}

	public void OnEndDrag (PointerEventData eventData)
	{
		m_scrollRect.OnEndDrag (eventData);
		m_CanvasGroup.blocksRaycasts = true;
		m_canDrag = false;
		if(m_isDeleted == false) {
			this.transform.localPosition = startPosition;
			m_CanvasGroup.alpha = 1f;
			m_AlphaCallback (1f);
		}
	}

	float GetAlphaVAlue () {
		float _screen = ((float)m_screenWidth) / 2f;
		float _temp = m_rectTransform.anchoredPosition.x;
		if (_temp < 0) {
			_temp = _temp * -1f;
		}

		float _alpha = (_screen - _temp) / _screen;
		return _alpha;
	}

}

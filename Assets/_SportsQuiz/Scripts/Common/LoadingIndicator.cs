﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingIndicator : MonoBehaviour {

	public Image indicator;
	public bool isEnabled;
	private float loadedTime;
	private bool isloaded = false;

	void OnEnable ()
	{
		isEnabled = true;
		loadedTime = Time.time;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		indicator.transform.Rotate (0, 0, -(400 * Time.deltaTime));

		if (QuizManager.Instance.quizLoadState == QuizManager.eQuizLoadState.Loading) {
		}
		if (QuizManager.Instance.quizLoadState == QuizManager.eQuizLoadState.IsLoaded) {
			if (isloaded == false) {
				isloaded = true;
				StartCoroutine ("SelfDeActivate");
			}
		}
	}

	IEnumerator SelfDeActivate ()
	{
		if ((Time.time - loadedTime) < 1f) {
			yield return new WaitForSeconds (1f);
		} else {
			yield return 0;
		}

		gameObject.SetActive (false);
		isloaded = false;
	}

	void OnDisable ()
	{
		isEnabled = false;
		QuizManager.Instance.quizLoadState = QuizManager.eQuizLoadState.UnKnown;
	}
}

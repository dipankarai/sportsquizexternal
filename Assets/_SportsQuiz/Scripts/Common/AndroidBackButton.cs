﻿using UnityEngine;
using System.Collections;

public class AndroidBackButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	public enum BackButtonMode
	{
		GotoHome,
		MainMenuTab,
		GotoCategory,
		ClosePopUp,
		GotoRandomTopic,
		CloseParent,
		GotoHomeResult,
		QuitGame,
		CloseFriendSelect,
		CloseLuckyWaiting,
		BadgePopUp,
		ParentMethod
	}

	public BackButtonMode backButtonMode;
	public bool isPopup;
	public static bool popupenabled = false;
	public string methodName;
	private bool quitTimerStart;
	private bool canQuit = false;

	#if UNITY_EDITOR || UNITY_ANDROID
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape) &&
			this.gameObject.activeSelf == true) {

			BackButtonPressed ();
		}

	}

	void OnEnable ()
	{
		if (isPopup == true) {
			popupenabled = true;
		}
	}

	void OnDisable ()
	{
		if (isPopup == true) {
			popupenabled = false;
		}
	}

	void BackButtonPressed ()
	{
	#if UNITY_EDITOR
		Q.Utils.QDebug.Log ("Android back button "+backButtonMode);
	#endif

		if (MainMenuPanelManager.Instance.loadingIndicator.gameObject.activeInHierarchy == true)
			return;
		
		if ((isPopup == false) && (popupenabled == true))
			return;


		switch (backButtonMode)
		{
		case BackButtonMode.GotoHome:
			MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
			break;

		case BackButtonMode.MainMenuTab:
			MainMenuPanelManager.Instance.mainMenuPanel.MainToogle ();
			break;

		case BackButtonMode.GotoCategory:
			MainMenuPanelManager.Instance.categoryPanel.OnClickBackCommonButton ();
			break;

		case BackButtonMode.ClosePopUp:
			MainMenuPanelManager.Instance.popUpMessage.ClosePopUp ();
			break;

		case BackButtonMode.GotoRandomTopic:
			{
				QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.CategoryPanel);
				if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {
				MainMenuPanelManager.Instance.categoryPanel.OpenMainCategory ();
				} else {
				MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();
				}
				break;
			}

		case BackButtonMode.CloseParent:
			this.transform.parent.gameObject.SetActive (false);
			break;

		case BackButtonMode.GotoHomeResult:
			this.transform.parent.gameObject.SetActive (false);
			MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
			break;

		case BackButtonMode.QuitGame:
			MainMenuPanelManager.Instance.quitAppPanel.gameObject.SetActive (true);
			break;

		case BackButtonMode.CloseFriendSelect:
			MainMenuPanelManager.Instance.categoryPanel.inviteFriendPanel.SwitchSubPanel (2);
			break;

		case BackButtonMode.CloseLuckyWaiting:
			MainMenuPanelManager.Instance.categoryPanel.lookingForOpponentPanel.BackToCategory ();
			break;

		case BackButtonMode.BadgePopUp:
			transform.GetComponentInParent<BadgeAchievedPopUp> ().ClosePanel ();
			break;

		case BackButtonMode.ParentMethod:
			this.transform.parent.gameObject.SendMessage (methodName);
			break;
		}
	}

	#endif
}

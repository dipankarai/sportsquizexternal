﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class FacebookCommonHandler : MonoBehaviour{

	public void HandleResult(IResult result)
	{
		if (result == null)
		{
			Debug.Log("Null Response\n");
			return;
		}
		// Some platforms return the empty string instead of null.
		if (!string.IsNullOrEmpty(result.Error))
		{
			Debug.Log ("Error Response:\n" + result.Error);
		}
		else if (result.Cancelled)
		{
			Debug.Log ("Cancelled Response:\n" + result.RawResult);
		}
		else if (!string.IsNullOrEmpty(result.RawResult))
		{
			Debug.Log ("Success Response:\n" + result.RawResult);
		}
		else
		{
			Debug.Log ("Empty Response\n");
		}
		Debug.Log(result.ToString());
	}
}

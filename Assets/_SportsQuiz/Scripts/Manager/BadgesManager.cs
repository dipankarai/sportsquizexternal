﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class BadgesManager : MonoBehaviour {

	private static BadgesManager m_instance;
	public static BadgesManager Instance
	{
		get
		{
			if (m_instance == null)
			{
				GameObject go = new GameObject ("BadgesManager");
				m_instance = go.AddComponent<BadgesManager> ();
			}
			return m_instance;
		}
		private set { }
	}


    public enum eBadges
	{
		Unknown = 0,
		Genius = 6,			//Don't use any hint for a quiz!
		Topper = 7,			//Score maximum points in a quiz!
		Smart = 17,			//Score more than 50% in a quiz!
		Mastermind = 18,	//Score more than 75% in a quiz!
		Wizardry = 19,		//Score more than 90% in a quiz!
		Cliffhanger = 27,	//Finish a quiz with 10 seconds spare in each mode!
		CloseCall = 28,		//Finish a quiz with 20 seconds spare in each mode!
		SadLuck = 30		//Get the last question wrong in a quiz!
	}

    public enum eSpareTime
    {
        TenSeconds = 10,
        TwentySeconds = 20
    }

	public List<SeralizedClassServer.UserBadges> achievedBadgeList = new List<SeralizedClassServer.UserBadges> ();
	public List<int> removedBadgeList = new List<int> ();
	private bool checkAchievemenCalled = false;

	void Awake ()
	{
		if (m_instance != null) {
			DestroyImmediate (this.gameObject);
		} else {
			m_instance = this;
			DontDestroyOnLoad (this);
		}
	}

	// Use this for initialization
	void Start () {
		Invoke ("CheckForNewBadgesAchieved", 5f);
	}

	public void ClearPreviousData ()
	{
		achievedBadgeList.Clear ();
		removedBadgeList.Clear ();
	}

    void CheckForNewBadgesAchieved ()
    {
		if (MainMenuPanelManager.Instance.notificationPopUp.gameObject.activeInHierarchy == false) {
			if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.MainMenuPanel)
			{
				if (MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel != MainMenuPanel.eSubMenuPanel.Settings)
				{
					if (achievedBadgeList.Count > 0)
					{
						checkAchievemenCalled = true;
						//BADGES ACHIEVED....
						MainMenuPanelManager.Instance.BadgeAchieved(achievedBadgeList[achievedBadgeList.Count - 1], 
							BadgesPopUpCallback);
						Q.Utils.QDebug.Log ("CheckForNewBadgesAchieved "+achievedBadgeList[achievedBadgeList.Count - 1].badge_id);
					}
				}
			}
		}

		Invoke ("CheckForNewBadgesAchieved", 3f);
    }

	void BadgesPopUpCallback (SeralizedClassServer.UserBadges badgeType)
    {
		Q.Utils.QDebug.Log ("BadgesPopUpCallback "+badgeType.badge_id);
        SaveToServer(badgeType.badge_id, 1);
		achievedBadgeList.Remove(badgeType);
		checkAchievemenCalled = false;

		Invoke ("CheckForNewBadgesAchieved", 1f);
    }

	public void AddNewBadges (SeralizedClassServer.UserBadges badges)
	{
		if (CheckAlreadyBadgeContain (badges.badge_id) == false) {
			achievedBadgeList.Add (badges);
		}
	}

	bool CheckAlreadyBadgeContain (int id)
	{
		for (int i = 0; i < achievedBadgeList.Count; i++) {
			if (achievedBadgeList [i].badge_id == id) {
				return true;
			}
		}

        for (int i = 0; i < removedBadgeList.Count; i++)
        {
            if (removedBadgeList[i] == id)
            {
                return true;
            }
        }
		
		return false;
	}

    public void BadgesCalculation()
    {
        float _total_Time = 0;
        for (int i = 0; i < QuizManager.Instance.currentQuizGame.currentQuizList.Count; i++)
        {
            if (i < 5)
                _total_Time += 10f;
            else
                _total_Time += 5;
        }

        float _total_Time_Taken = 0;
        for (int i = 0; i < QuizManager.Instance.currentQuizGame.timeTakenPerQuestion.Count; i++)
            _total_Time_Taken += QuizManager.Instance.currentQuizGame.timeTakenPerQuestion[i];

        float _spare_time = _total_Time - _total_Time_Taken;

        //Finish the quiz with -- seconds spare (in each mode)
        if (_spare_time >= (float)((int)BadgesManager.eSpareTime.TenSeconds))
            CompletedSpareTime(BadgesManager.eSpareTime.TenSeconds);
        else if (_spare_time >= (float)((int)BadgesManager.eSpareTime.TwentySeconds))
            CompletedSpareTime(BadgesManager.eSpareTime.TwentySeconds);

        //Score all points in quiz
        if (QuizManager.Instance.currentQuizGame.correctAnswer.Count == QuizManager.Instance.currentQuizGame.TotalQuestion)
            ScoreAllPoints();

        //Don't use any hint for a quiz
		if (QuizManager.Instance.currentQuizGame.hintUsed == 0) {
			if (QuizManager.Instance.currentQuizGame.correctAnswer.Count == QuizManager.Instance.currentQuizGame.TotalQuestion) {
				
				CheckHintBadge();
			}
		}

        //Score more than --%
        int _total_Scored = ScoreManager.Instance.GetTotalScore(true);
		int _nintyPercent = ScoreManager.Instance.TotalScore() * 90 / 100;
		int _seventyFivePercent = ScoreManager.Instance.TotalScore() * 75 / 100;
		int _fiftyPercent = ScoreManager.Instance.TotalScore() * 50 / 100;

        //90%
        if (_total_Scored > _nintyPercent)
            ScoreMoreThan(SCORE_MORE_THAN_NINETY);
        else if (_total_Scored > _seventyFivePercent)
            ScoreMoreThan(SCORE_MORE_THAN_SEVENTYFIVE);
        else if (_total_Scored > _fiftyPercent)
            ScoreMoreThan(SCORE_MORE_THAN_FIFTY);
    }

    #region HINT BADGE

    public const string HINT_NOT_USED_BADGE = "hintnotusedbadge";

	void CheckHintBadge ()
	{
        //BADGES EARNED for "Don't use any hint for a quiz"
		if (GamePrefs.HasKey(HINT_NOT_USED_BADGE) == false) {

			SaveToServer((int)eBadges.Genius);
        }
	}

	#endregion

	#region ANSWERED WRONG 6th QUESTION

	public const string ANSWERED_WRONG_SIXTH = "answeredwrongsixth";

	public void AnsweredWrong ()
	{
        //BADGES EARNED for "In a quiz get the last question (5 second question) wrong"
        if (GamePrefs.HasKey (ANSWERED_WRONG_SIXTH) == false) {

			SaveToServer((int)eBadges.SadLuck);
        }
    }

    #endregion

    #region SPARE TIME

    [System.Serializable]
    public class BadgeSpareTime
    {
        public QuizManager.eQuizMode leisureMode;
        public QuizManager.eQuizMode luckyMode;
        public QuizManager.eQuizMode dualMode;
    }
    
    public const string SPARE_TIME_TEN = "sparetimewithtenseconds";
    public const string SPARE_TIME_TWENTY = "sparetimewithtwentyseconds";

    void CompletedSpareTime (eSpareTime spareTime)
    {
        if (spareTime == eSpareTime.TenSeconds) {
            //BADGES EARNED for "Finish the quiz with 10 seconds spare (in each mode)"
            BadgeSpareTime _spareTime = new BadgeSpareTime();

            if (GamePrefs.HasKey(SPARE_TIME_TEN) == true)
                _spareTime = GamePrefs.BadgeSpareTimeTen;

            if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure)
                _spareTime.leisureMode = QuizManager.eQuizMode.Leisure;
            else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky)
                _spareTime.leisureMode = QuizManager.eQuizMode.IMFLucky;
            else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual)
                _spareTime.leisureMode = QuizManager.eQuizMode.Dual;

            GamePrefs.BadgeSpareTimeTen = _spareTime;

            if (_spareTime.leisureMode == QuizManager.eQuizMode.Leisure &&
                _spareTime.luckyMode == QuizManager.eQuizMode.IMFLucky &&
                _spareTime.dualMode == QuizManager.eQuizMode.Dual) {

                //newBages.Add(eBadges.FinishQuizTen);
				SaveToServer((int)eBadges.Cliffhanger);
            }

        } else if (spareTime == eSpareTime.TwentySeconds) {
            //BADGES EARNED for "Finish the quiz with 20 seconds spare (in each mode)"
            BadgeSpareTime _spareTime = new BadgeSpareTime();

            if (GamePrefs.HasKey(SPARE_TIME_TWENTY) == true)
                _spareTime = GamePrefs.BadgeSpareTimeTwenty;
            
            if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure)
                _spareTime.leisureMode = QuizManager.eQuizMode.Leisure;
            else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky)
                _spareTime.leisureMode = QuizManager.eQuizMode.IMFLucky;
            else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual)
                _spareTime.leisureMode = QuizManager.eQuizMode.Dual;

            GamePrefs.BadgeSpareTimeTwenty = _spareTime;

            if (_spareTime.leisureMode == QuizManager.eQuizMode.Leisure &&
                _spareTime.luckyMode == QuizManager.eQuizMode.IMFLucky &&
                _spareTime.dualMode == QuizManager.eQuizMode.Dual)
            {
                //newBages.Add(eBadges.FinishQuizTwenty);
				SaveToServer((int)eBadges.CloseCall);
            }
        }
    }

    #endregion

    #region SCORE ALL POINTS

    public const string SCORE_ALL_POINTS = "scoreallpointsinquiz";

    void ScoreAllPoints ()
    {
        if (GamePrefs.HasKey(SCORE_ALL_POINTS) == false)
        {
			SaveToServer((int)eBadges.Topper);
        }
    }

    #endregion

    #region SCORE MORE THAN

    public const string SCORE_MORE_THAN_FIFTY = "scoremorethanfifty";
    public const string SCORE_MORE_THAN_SEVENTYFIVE = "scoremorethanseventyfive";
    public const string SCORE_MORE_THAN_NINETY = "scoremorethanninety";

    void ScoreMoreThan (string percent)
    {
		if (percent == SCORE_MORE_THAN_FIFTY) {
			
			SaveToServer((int)eBadges.Smart);
		} else if (percent == SCORE_MORE_THAN_SEVENTYFIVE) {
			
			SaveToServer((int)eBadges.Mastermind);
		} else if (percent == SCORE_MORE_THAN_NINETY) {
			
			SaveToServer((int)eBadges.Wizardry);
		}
    }

    #endregion

	void SaveToServer (int badgeID, int notifiedToUser = 2)
    {
        if (InternetConnection.Check)
        {
		    ServerManager.Instance.SaveBadge(badgeID, notifiedToUser, SaveToServerCallback);
            removedBadgeList.Add(badgeID);
        }
    }

	void SaveToServerCallback (bool status)
	{
		if (status) {
			Q.Utils.QDebug.Log("SUCCESS");
		} else {
			Q.Utils.QDebug.Log("FAILED");
		}
	}

	eBadges GetBadgeNameByID (int id)
	{
		foreach (int badge in System.Enum.GetValues (typeof (eBadges))) {
			if (id == badge) {

				return (eBadges)badge;
			}
		}

		return eBadges.Unknown;
	}
}

﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class GoogleMobileAdsHandler : IDefaultInAppPurchaseProcessor
{
	private readonly string[] validSkus = { "android.test.purchased" };

	//Will only be sent on a success.
	public void ProcessCompletedInAppPurchase(IInAppPurchaseResult result)
	{
		result.FinishPurchase();
		//"Purchase Succeeded! Credit user here.";
	}

	//Check SKU against valid SKUs.
	public bool IsValidPurchase(string sku)
	{
		foreach(string validSku in validSkus)
		{
			if (sku == validSku)
			{
				return true;
			}
		}
		return false;
	}

	//Return the app's public key.
	public string AndroidPublicKey
	{
		//In a real app, return public key instead of null.
		get { return null; }
	}
}

public class GoogleAdHandler : MonoBehaviour {

	private static GoogleAdHandler m_instance;
	public static GoogleAdHandler Instance { get {return m_instance; } }

	private const string AD_UNIT_ID_iOS = "ca-app-pub-9272641320028711/9951329381";
	private const string INTERSTIAL_UNIT_ID_iOS = "ca-app-pub-9272641320028711/2288461786";

	private const string AD_UNIT_ID_ANDROID = "ca-app-pub-6239837178715306/8970723278";
	private const string INTERSTIAL_UNIT_ID_ANDROID = "ca-app-pub-6239837178715306/8831122474";

	private BannerView m_bannerView;
	private bool m_bannerRequested;
	public bool IsBannerShowing { get { return m_showBanner; } }
	private bool m_showBanner;
	private InterstitialAd interstitial;

	public static GoogleAdHandler Init (GameObject obj)
	{
		if (m_instance == null)
			m_instance = obj.AddComponent<GoogleAdHandler> ();

		return m_instance;
	}

	// Use this for initialization
	void Start () {

//		RequestBanner ();
		RequestInterstitial ();
	}

	private void RequestBanner()
	{
		string adUnitId = "";
		#if UNITY_EDITOR
		adUnitId = "unused";
		#elif UNITY_ANDROID
		adUnitId = AD_UNIT_ID_ANDROID;
		#elif (UNITY_5 && UNITY_IOS) || UNITY_IPHONE
		adUnitId = AD_UNIT_ID_iOS;
		#else
		adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the Bottom of the screen.
		m_bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		m_bannerView.OnAdLoaded += HandleAdLoaded;
		m_bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
		m_bannerView.OnAdLoaded += HandleAdOpened;
		m_bannerView.OnAdClosed += HandleAdClosed;
		m_bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
		// Load a banner ad.
		m_bannerView.LoadAd(CreateAdRequest());
	}

	private void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = INTERSTIAL_UNIT_ID_ANDROID;
		#elif UNITY_IPHONE
		string adUnitId = INTERSTIAL_UNIT_ID_iOS;
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ()
		.AddTestDevice (AdRequest.TestDeviceSimulator)
//		.AddTestDevice ("432DDDF4F7367F53AC8301059D34CB64")
		.AddTestDevice(SystemInfo.deviceUniqueIdentifier)
		.AddKeyword(TagConstant.APP_NAME)
		.AddExtra("color_bg", "9B30FF")
		.Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	// Returns an ad request with custom ad targeting.
	private AdRequest CreateAdRequest()
	{
		return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
//			.AddTestDevice ("432DDDF4F7367F53AC8301059D34CB64")
			.AddTestDevice(SystemInfo.deviceUniqueIdentifier)
			.AddKeyword(TagConstant.APP_NAME)
			.AddExtra("color_bg", "9B30FF")
			.Build();
	}

	public void ShowBanner ()
	{
		m_showBanner = true;
		if (m_bannerRequested == false) {
			RequestBanner ();
		} else {
			m_bannerView.Show ();
		}
	}
	
	public void HideBanner ()
	{
		m_showBanner = false;
		m_bannerView.Hide ();
	}
	
	public void DestroyBanner ()
	{
		m_showBanner = false;
		m_bannerView.Destroy ();
	}

	public void FullScreenAds ()
	{
		if (interstitial.IsLoaded()) {
			interstitial.Show();
		}
	}

	#region Banner callback handlers
	
	public void HandleAdLoaded(object sender, EventArgs args)
	{
		print("HandleAdLoaded event received.");
		m_bannerRequested = true;

		if (m_showBanner) {
			m_bannerView.Show ();
		} else {
			m_bannerView.Hide ();
		}
	}
	
	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleFailedToReceiveAd event received with message: " + args.Message);
	}
	
	public void HandleAdOpened(object sender, EventArgs args)
	{
		print("HandleAdOpened event received");
	}
	
	void HandleAdClosing(object sender, EventArgs args)
	{
		print("HandleAdClosing event received");
	}
	
	public void HandleAdClosed(object sender, EventArgs args)
	{
		print("HandleAdClosed event received");
	}
	
	public void HandleAdLeftApplication(object sender, EventArgs args)
	{
		print("HandleAdLeftApplication event received");
	}
	
	#endregion
	
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class ScoreManager : MonoBehaviour {
	
	private static ScoreManager m_instance;
	public static ScoreManager Instance
	{
		get
		{
			if (m_instance == null)
			{
				GameObject go = new GameObject ("ScoreManager");
				m_instance = go.AddComponent<ScoreManager> ();
			}
			return m_instance;
		}
		private set { }
	}

	public const float FIRST_SECONDS	= 15f;
	public const float LAST_SECONDS		= 10f;

    /*private const int FIRST_QUESTION    = 15;
    private const int SECOND_QUESTION   = 20;
    private const int THIRD_QUESTION    = 25;
    private const int FOURTH_QUESTION   = 30;
    */
    private const int FIFTH_QUESTION    = 35;
    private const int SIXTH_QUESTION    = 50;

    void Awake ()
	{
		if (m_instance != null) {
			DestroyImmediate (this.gameObject);
		} else {
			m_instance = this;
			DontDestroyOnLoad (this);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	[System.Serializable]
	public class Score
	{
		public int questionID;
		public float time;
		public int score;
	}

	public List<Score> playerCurrentQuizScore = new List<Score> ();
	public List<Score> opponentCurrentQuizScore = new List<Score> ();

	public void AddScore (int _questionNum, int _totalQuestion, float _timeTaken)
	{
		Score _score = new Score ();
		_score.questionID = _questionNum;
		_score.time = _timeTaken;
		_score.score = GetScoreCalculations (_questionNum, _totalQuestion, _timeTaken);

		playerCurrentQuizScore.Add (_score);
	}

	public void AddOpponentScore (int _questionID, int _scored)
	{
		Score _score = new Score ();
		_score.questionID = _questionID;
		_score.score = _scored;

		opponentCurrentQuizScore.Add (_score);
	}

	public int GetCurrentScore (bool _player)
	{
		if (_player)
			return playerCurrentQuizScore [playerCurrentQuizScore.Count - 1].score;
		else 
			return opponentCurrentQuizScore [opponentCurrentQuizScore.Count - 1].score;
	}

	public int GetTotalScore (bool _player)
	{
		int _totalScore = 0;
		if (_player) {
			for (int i = 0; i < playerCurrentQuizScore.Count; i++) {
				_totalScore += playerCurrentQuizScore [i].score;
			}
		}
		else {
			for (int i = 0; i < opponentCurrentQuizScore.Count; i++) {
				_totalScore += opponentCurrentQuizScore [i].score;
			}
		} 
		return _totalScore;
	}

	public string GetTotalScoreMD5 (bool _player, int user_quiz_id)
	{
		//TOTAL SCORE
		int _totalScore = 0;
		if (_player) {
			for (int i = 0; i < playerCurrentQuizScore.Count; i++) {
				_totalScore += playerCurrentQuizScore [i].score;
			}
		}
		else {
			for (int i = 0; i < opponentCurrentQuizScore.Count; i++) {
				_totalScore += opponentCurrentQuizScore [i].score;
			}
		} 

		//ALGORITHM:
		string SecretKey = "StUZuw$W$86e";
		string score_string = (_totalScore * user_quiz_id).ToString () + "#$#" + SecretKey;
		string score_data = CommonUtils.Md5Sum (score_string);

		return score_data;
	}

	int GetScoreCalculations (int _questionNum, int _totalQuestion, float _timeTaken) {
		if (_questionNum == (_totalQuestion - 1)) {
			if (_timeTaken < (LAST_SECONDS * 0.4f))
				return SIXTH_QUESTION;
			else if (_timeTaken < LAST_SECONDS)
				return SIXTH_QUESTION - 10;
		} else if (_questionNum < 5) {
			return GetScoreTimeCalculations (_timeTaken, ((_questionNum * 5) + 15));
		} else {
			return GetScoreTimeCalculations (_timeTaken, FIFTH_QUESTION);
		}

		return 0;
	}

	int GetScoreTimeCalculations (float _timeTaken, int totalscore)
	{
		if (_timeTaken < (FIRST_SECONDS * 0.2f))//20
			return totalscore;
		else if (_timeTaken < (FIRST_SECONDS * 0.6f))//60
			return totalscore - 5;
		else if (_timeTaken < FIRST_SECONDS)
			return totalscore - 10;
		else
			return 0;
	}

    public int TotalScore ()
	{
		int totalQuestion = QuizManager.Instance.currentQuizGame.TotalQuestion;
		int totalScore = 0;
		for (int i = 0; i < totalQuestion; i++) {
			if (i == totalQuestion - 1) {
				totalScore += SIXTH_QUESTION;
			} else if (i < 5) {
				totalScore += ((i * 5) + 15);
			} else {
				totalScore += FIFTH_QUESTION;
			}
		}

		return totalScore;
	}

	int GetQuestionNumber (int _questionID)
	{
		for (int i = 0; i < QuizManager.Instance.currentQuizGame.currentQuizList.Count; i++) {
			if (QuizManager.Instance.currentQuizGame.currentQuizList [i].question.questionId == _questionID)
				return (i + 1);
		}
			
		return 0;
	}
}

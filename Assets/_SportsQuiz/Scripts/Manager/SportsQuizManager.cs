﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Q.Utils;

public class SportsQuizManager : Singleton<SportsQuizManager>
{
	protected SportsQuizManager () {} 	// guarantee this will be always a singleton only - can't use the constructor!
	public float APP_VERSION = 1.0f;

	public enum eUserType
	{
		FacebookUser = 1,
		GuestUser = 2,
		EmailUser = 3,
		GPlus = 4
	}

	public enum eGender
	{
		Gender = 0,
		Female =1,
		Male
	}

	[System.Serializable]
	public class PlayerInfo
	{
		public string name;
		public string email;
		public string profileUrl;
		public string facebook_id;
		public string google_id;

		public int serverID;
		public string access_token;
		public string password;

		public int country;
		public int avatarType;
		public eGender gender;
		public int age;

		public int rank;
		public string rankTitle;

		public List<int> categoryPrefsList = new List<int> ();

		public Texture2D userProfile;
		public List<SeralizedClassServer.FollowerDetails> followerList = new List<SeralizedClassServer.FollowerDetails> ();
		public List<SeralizedClassServer.FollowingDetails> followingList = new List<SeralizedClassServer.FollowingDetails> ();

		public int total_win;
		public int total_lost;
		public int total_draw;

		public List<SeralizedClassServer.Score_Statistics> score_statistics = new List<SeralizedClassServer.Score_Statistics>();
		public List<SeralizedClassServer.UserBadges> userBadges = new List<SeralizedClassServer.UserBadges> ();
	}

    [System.Serializable]
    public class OpponentInfo
    {
        public string name;
        public string email;
        public string profileUrl;
		public string facebook_id;
		public string google_id;

        public int serverID;
        public string access_token;
        public string password;

        public int country;
        public int avatarType;
        public int gender;
        public int age;

		public int rank;
		public string rankTitle;

		public bool isOnline = false;

		public UnityEngine.UI.Image profileImage;
    }

    [System.Serializable]
	public class CurrentCategory
	{
		public int mainCategorySelected = 0;
		public int randomTopicLimit = 5;
		public string mainCategoryLabel;
		public string topicLabel;
		public Texture2D categoryLogo;
		public List<LogoTexture> categoryLogoList = new List<LogoTexture> ();
		public List<SeralizedClassServer.CategoryDictionary> categoryDictionaryList = new List<SeralizedClassServer.CategoryDictionary> ();
	}

	[System.Serializable]
	public class LogoTexture
	{
		public string url;
		public int id;
		public Texture2D logo;
	}

	[System.Serializable]
	public class InviteFriendDetail
	{
		public object user_id;
		public string name;
		public eUserType userType;
	}

    [System.Serializable]
    public class Badges
    {
        public int id;
        public Texture2D badge;
		public string title;
        public string description;
		public string url;
    }

	[System.Serializable]
	public class SponseredCategoryData
	{
		public int category_id;
		public string category_title;
		public int type;
		public string image;
		public Texture2D texture;
	}

    public int previousUserID;
    public PlayerInfo playerInformation;
    public PlayerInfo searchedUserInfo;
    public OpponentInfo opponentInformation;
    public CurrentCategory currentCategory;
    public QuizManager.eQuizMode selectedMode;
    public eUserType loginType;
    public eUserType otherUSerType;
    public References references;
    public SeralizedClassServer.Topics currentSelectedTopics;
    public SeralizedClassServer.Notification notification;
    public InviteFriendDetail selectedDuelFriendDetail;

    public List<OpponentInfo> opponentList = new List<OpponentInfo>();
    public List<SeralizedClassServer.Category> currentSelectedCategory;
    public List<SeralizedClassServer.GetChallengeList> challengeList;
    public List<Badges> badgesList = new List<Badges>();
    public List<SponseredCategoryData> sponsoredCategoryData = new List<SponseredCategoryData>();
    public List<int> previousQuizList = new List<int> ();

    //public SeralizedClassServer.LastStatus last;
    private GoogleAdHandler googleAdManager;

    void Awake ()
	{
		if (currentSelectedCategory == null) {
			currentSelectedCategory = new List<SeralizedClassServer.Category> ();
		}

		if (references == null) {
			GameObject obj = (GameObject) Resources.Load ("References");
			references = obj.GetComponent<References> ();
		}
	}

	// Use this for initialization
	void Start () {
		
		if (Instance.playerInformation == null) {
			Instance.playerInformation = new PlayerInfo ();
		}

		if (Instance.searchedUserInfo == null) {
			Instance.searchedUserInfo = new PlayerInfo ();
		}

		if (opponentInformation == null) {
			opponentInformation = new OpponentInfo ();
		}
		if (googleAdManager == null) {
			googleAdManager = GoogleAdHandler.Init (this.gameObject);
		}

		if (SportsQuizManager.Instance.currentCategory == null)
			SportsQuizManager.Instance.currentCategory = new SportsQuizManager.CurrentCategory ();

		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.SplashScreen) {
			MainMenuPanelManager.Instance.WaitingPanel (true);
			Invoke ("CheckForAutoSignIn", 1f);
		}
	}

	void CheckForAutoSignIn ()
	{
		if (PlayerPrefsSerializer.LoadData<GamePrefs.LoginData>(TagConstant.PrefsName.LOGIN_PREFS) != null) {
			SportsQuizManager.Instance.AutoSignIn();
		}else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
		}
	}
	
	void SavePlayerInformation ()
	{
		if (InternetConnection.Check)
			Q.Utils.QDebug.Log ("Save to server also....");

		//Saving Locally..
		PlayerPrefsSerializer.SaveData (TagConstant.PrefsName.PLAYER_INFORMATION, playerInformation);
	}

	public void ClearUserInfo ()
	{
		Instance.playerInformation = new PlayerInfo ();
	}

	public void StoreCurrentOpponentInformation (SeralizedClassServer.UserInfo info)
	{
		Instance.opponentInformation.name = info.name;
		Instance.opponentInformation.avatarType = info.avatar_type;
		Instance.opponentInformation.country = info.country;
		Instance.opponentInformation.gender = info.gender;
		Instance.opponentInformation.rank = info.rank;
		Instance.opponentInformation.rankTitle = info.rank_title;
		Instance.opponentInformation.serverID = info.user_id;
	}

	public void StorePlayerInformation (SeralizedClassServer.UserInfo info)
	{
		Instance.playerInformation.rank = info.rank;
		Instance.playerInformation.rankTitle = info.rank_title;
	}

	public void ResetCategoryData ()
	{
		if (SportsQuizManager.Instance.currentCategory != null) {
			SportsQuizManager.Instance.currentCategory.categoryLogo = null;
			SportsQuizManager.Instance.currentCategory.categoryDictionaryList = new List<SeralizedClassServer.CategoryDictionary> ();
			SportsQuizManager.Instance.currentCategory.mainCategoryLabel = "";
			SportsQuizManager.Instance.currentCategory.mainCategorySelected = 0;
			SportsQuizManager.Instance.currentCategory.randomTopicLimit = 8;
			SportsQuizManager.Instance.currentCategory.topicLabel = "";
		}

		if (SportsQuizManager.Instance.currentSelectedTopics != null)
			SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();

		if (SportsQuizManager.Instance.currentSelectedCategory.Count > 0)
			SportsQuizManager.Instance.currentSelectedCategory = new List<SeralizedClassServer.Category> ();
	}

	public Texture2D GetCurrentMainCategoryLogo ()
	{
		for (int i = 0; i < Instance.currentCategory.categoryLogoList.Count; i++) {
			if (Instance.currentCategory.categoryLogoList [i].id == Instance.currentCategory.mainCategorySelected) {
				
				#if !MEMORY_CHECK
				return Instance.currentCategory.categoryLogoList [i].logo;
				#else
				return References.GetImageFromLocal (
					Instance.currentCategory.categoryLogoList [i].id,
					Instance.currentCategory.categoryLogoList [i].url,
					References.eImageType.Category);
				#endif
			}
		}

		return null;
	}

	public Texture2D GetCurrentMainCategoryLogoByID (int catId)
	{
		for (int i = 0; i < Instance.currentCategory.categoryLogoList.Count; i++) {
			if (Instance.currentCategory.categoryLogoList [i].id == catId) {
				#if !MEMORY_CHECK
				return Instance.currentCategory.categoryLogoList [i].logo;
				#else
				return References.GetImageFromLocal (
					Instance.currentCategory.categoryLogoList [i].id,
					Instance.currentCategory.categoryLogoList [i].url,
					References.eImageType.Category);
				#endif
			}
		}

		return null;
	}

	public void AssignUserInfo(SeralizedClassServer.UserDetails userDetails)
	{
		Instance.playerInformation.name = userDetails.name;
		Instance.playerInformation.gender = (SportsQuizManager.eGender) userDetails.gender;
		Instance.playerInformation.avatarType = userDetails.avatar_type;
		Instance.playerInformation.age = userDetails.age;
		Instance.playerInformation.categoryPrefsList = userDetails.category_preference;
		Instance.playerInformation.total_win = userDetails.total_win;
		Instance.playerInformation.total_lost = userDetails.total_lost;
		Instance.playerInformation.total_draw = userDetails.total_draw;
		Instance.playerInformation.score_statistics = userDetails.score_statistics;
		Instance.playerInformation.userBadges = userDetails.user_badge;
		if (userDetails.country != 0) {
			Instance.playerInformation.country = userDetails.country;
		} else {
			ServerManager.Instance.UpdateUserInfoToServer (UserInfoUpdateCallback);
		}
		MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.UpdateUserInfo ();
	}

	void UserInfoUpdateCallback(SeralizedClassServer.UserDetails userDetails)
	{
		MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.UpdateUserdata ();
	}

	public void AssignSearchUserInfo(SeralizedClassServer.UserDetails userDetails)
	{
		Instance.searchedUserInfo.serverID = userDetails.user_id;
		Instance.searchedUserInfo.name = userDetails.name;
		Instance.searchedUserInfo.email = userDetails.email_id;
		Instance.searchedUserInfo.facebook_id = userDetails.facebook_id;
		Instance.searchedUserInfo.country = userDetails.country;
		Instance.searchedUserInfo.gender = (SportsQuizManager.eGender) userDetails.gender;
		Instance.searchedUserInfo.avatarType = userDetails.avatar_type;
		Instance.searchedUserInfo.age = userDetails.age;
		Instance.searchedUserInfo.google_id = userDetails.google_id;
		Instance.searchedUserInfo.categoryPrefsList = userDetails.category_preference;
		Instance.searchedUserInfo.score_statistics = userDetails.score_statistics;
		Instance.searchedUserInfo.total_win = userDetails.total_win;
		Instance.searchedUserInfo.total_lost = userDetails.total_lost;
		Instance.searchedUserInfo.total_draw = userDetails.total_draw;
		Instance.searchedUserInfo.userBadges = userDetails.user_badge;
	}

    void CheckNotificationEveryFewSeconds ()
    {
		if (MainMenuPanelManager.Instance.currentMainMenuState != MainMenuPanelManager.eMainMenuState.SplashScreen &&
			MainMenuPanelManager.Instance.currentMainMenuState != MainMenuPanelManager.eMainMenuState.CategoryPanel &&
			MainMenuPanelManager.Instance.currentMainMenuState != MainMenuPanelManager.eMainMenuState.Gameplay &&
			InternetConnection.Check)
        {
            ServerManager.Instance.GetNotification();
        }
    }

    public void AutoSignIn()
    {
		GamePrefs.LoginData logindata = (GamePrefs.LoginData) PlayerPrefsSerializer.LoadData<GamePrefs.LoginData>(TagConstant.PrefsName.LOGIN_PREFS);
		Q.Utils.QDebug.Log (JsonUtility.ToJson (logindata));

        switch (logindata.usertype)
        {
            case SportsQuizManager.eUserType.EmailUser:
                ConnectionHandler.Instance.LoginEmail(logindata.email_id, logindata.password);
                break;

            case SportsQuizManager.eUserType.FacebookUser:
                ConnectionHandler.Instance.LoginFacebook();
                break;

            case SportsQuizManager.eUserType.GPlus:
                ConnectionHandler.Instance.LoginGooglePlus();
                break;

            case SportsQuizManager.eUserType.GuestUser:
                MainMenuPanelManager.Instance.SwitchPanel(MainMenuPanelManager.eMainMenuState.SplashScreen);
                break;

            default:
                break;
        }
    }

    public void SaveLogindata ()
    {
        GamePrefs.LoginData logindata = new GamePrefs.LoginData();
        logindata.usertype = SportsQuizManager.Instance.loginType;
        logindata.email_id = SportsQuizManager.Instance.playerInformation.email;
        logindata.password = SportsQuizManager.Instance.playerInformation.password;

		Q.Utils.PlayerPrefsSerializer.SaveData(TagConstant.PrefsName.LOGIN_PREFS, logindata);
    }

    public void AssignUserProfileImage (UnityEngine.UI.Image image)
    {
		int avatarType = Instance.playerInformation.avatarType;
        Texture2D texture = null;

		if (avatarType == 0) {
			//LOAD FB or G+ profile image
			if (Instance.playerInformation.userProfile == null) {
				texture = Instance.references.GetAvatarImage (avatarType);
			} else {
				texture = Instance.playerInformation.userProfile;
			}
		} else {
			texture = Instance.references.GetAvatarImage (avatarType);
		}

		if (texture != null) {
			image.CreateImageSprite(texture);
		}
    }

    /*public void AssignUserProfileImage (UnityEngine.UI.Image image, int index)
	{
		int avatarType = 0;
//		if (MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.follow == UserProfilePanel.Follow.following) {
//			avatarType = Instance.playerInformation.followingList[index].avatar_type;
//		} else if (MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.follow == UserProfilePanel.Follow.follwers) {
//			avatarType = Instance.playerInformation.followerList[index].avatar_type;
//		}
//		else if(MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.follow == UserProfilePanel.Follow.frequentlyPlayed)
//		{
//			avatarType = MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.frequentlyPlayedHandler.frequentlyPlayedList[index].avatar_type;
//		}
		Texture2D texture = null;

		if (avatarType == 0) {
			//LOAD FB or G+ profile image
			if (Instance.playerInformation.userProfile == null) {
				texture = Instance.references.GetAvatarImage (avatarType);
			} 
		} else {
			texture = Instance.references.GetAvatarImage (avatarType);
		}

		if (texture != null) {
			image.CreateImageSprite(texture);
		}
	}*/

	public void GetOpponentDetail (int id, System.Action<SeralizedClassServer.OpponentDetails> callback)
	{
		for (int i = 0; i < Instance.opponentList.Count; i++) {

			if (Instance.opponentList[i].serverID == id) {

				SeralizedClassServer.OpponentDetails opponentDetails = new SeralizedClassServer.OpponentDetails ();
				opponentDetails.user_info = new SeralizedClassServer.OpponentInfo ();

				opponentDetails.user_info.user_id = opponentList[i].serverID;
				opponentDetails.user_info.facebook_id = opponentList[i].facebook_id;
				opponentDetails.user_info.google_id = opponentList[i].google_id;
				opponentDetails.user_info.name = opponentList[i].name;
				opponentDetails.user_info.age = opponentList[i].age;
				opponentDetails.user_info.avatar_type = opponentList[i].avatarType;
				opponentDetails.user_info.country = opponentList[i].country;
				opponentDetails.user_info.email_id = opponentList[i].email;
				opponentDetails.user_info.facebook_id = opponentList[i].facebook_id;
				opponentDetails.user_info.google_id = opponentList[i].google_id;
				opponentDetails.user_info.gender = opponentList[i].gender;
				opponentDetails.user_info.user_id = opponentList[i].serverID;
				
				callback (opponentDetails);
				return;
			}
		}

		ServerManager.Instance.GetOpponentDetail(id, callback);
	}

    public void AssignOpponentProfileImage (UnityEngine.UI.Image image, int opponentID)
    {
		for (int i = 0; i < Instance.opponentList.Count; i++)
        {
			if (Instance.opponentList[i].serverID == opponentID)
            {
				int avatarType = Instance.opponentList[i].avatarType;
                Texture2D texture = null;
                if (avatarType == 0)
                {
                    //LOAD FB or G+ profile image
//					if (Instance.opponentList[i].userProfile != null)
//						texture = Instance.opponentList[i].userProfile;
                }
                else
                {
					texture = Instance.references.GetAvatarImage(avatarType);
                }

                if (texture != null)
                    image.CreateImageSprite(texture);

                return;
            }
        }

        if (InternetConnection.Check)
        {
			OpponentInfo _info = new OpponentInfo ();
			_info.profileImage = image;
			_info.serverID = opponentID;
			Instance.opponentList.Add (_info);
			ServerManager.Instance.GetOpponentDetail(_info.serverID, GetOppnentDetailCallback);
        }
    }

    /*public Texture2D GetOpponentSocialProfileImage (string id)
	{
		for (int i = 0; i < Instance.opponentList.Count; i++) {

			if (Instance.opponentList [i].facebook_id == id ||
			    Instance.opponentList [i].google_id == id) {
			}
		}

		return null;
	}*/

	void GetOppnentDetailCallback (SeralizedClassServer.OpponentDetails callback)
    {
		for (int i = 0; i < Instance.opponentList.Count; i++) {
			if (Instance.opponentList [i].serverID == callback.user_info.user_id) {
				AssignOpponentProfileImage (Instance.opponentList [i].profileImage, callback.user_info.user_id);
			}
		}
    }

    public void AddOpponentInList (SeralizedClassServer.OpponentDetails opponentDetails)
    {
        for (int i = 0; i < Instance.opponentList.Count; i++)
        {
			if (Instance.opponentList [i].serverID == opponentDetails.user_info.user_id ||
				CheckSocialID (Instance.opponentList [i].facebook_id, opponentDetails.user_info.facebook_id) == true ||
				CheckSocialID (Instance.opponentList [i].google_id, opponentDetails.user_info.google_id)) {
				
				Instance.opponentList [i].serverID = opponentDetails.user_info.user_id;
				Instance.opponentList [i].age = opponentDetails.user_info.age;
				Instance.opponentList [i].avatarType = opponentDetails.user_info.avatar_type;
				Instance.opponentList [i].country = opponentDetails.user_info.country;
				Instance.opponentList [i].email = opponentDetails.user_info.email_id;
				Instance.opponentList [i].facebook_id = opponentDetails.user_info.facebook_id;
				Instance.opponentList [i].gender = opponentDetails.user_info.gender;
				Instance.opponentList [i].name = opponentDetails.user_info.name;

				return;
			}
        }

        OpponentInfo newOppOnfo = new OpponentInfo();
        newOppOnfo.name = opponentDetails.user_info.name;
        newOppOnfo.age = opponentDetails.user_info.age;
        newOppOnfo.avatarType = opponentDetails.user_info.avatar_type;
        newOppOnfo.country = opponentDetails.user_info.country;
        newOppOnfo.email = opponentDetails.user_info.email_id;
        newOppOnfo.facebook_id = opponentDetails.user_info.facebook_id;
		newOppOnfo.google_id = opponentDetails.user_info.google_id;
        newOppOnfo.gender = opponentDetails.user_info.gender;
        newOppOnfo.serverID = opponentDetails.user_info.user_id;

        Instance.opponentList.Add(newOppOnfo);
    }

	bool CheckSocialID (string savedID, string newID)
	{
		if (string.IsNullOrEmpty (savedID) == true)
			return false;
		else if (string.IsNullOrEmpty (newID) == true)
			return false;
		else if (savedID == "0" || newID == "0")
			return false;
		else if (savedID == newID)
			return true;

		return false;
	}

	public void UpdateFacebookProfileURL ()
	{
		StartCoroutine ("UpdateFacebookProfileImage");
	}

	IEnumerator UpdateFacebookProfileImage ()
	{
		WWW image = new WWW (Instance.playerInformation.profileUrl);
		yield return image;

		Q.Utils.QDebug.Log ("LoadFacebookProfileImage "+image.error);

		if (image.error == null) {

			Instance.playerInformation.userProfile = image.texture;

		} else {
			Q.Utils.QDebug.Log (image.error);
		}
		Q.Utils.QDebug.Log ("LoadFacebookProfileImage Done");
	}

	public void LoadOpponentProfile (UnityEngine.UI.Image image)
	{
		StartCoroutine (LoadOpponentProfileImage(image));
	}

	IEnumerator LoadOpponentProfileImage (UnityEngine.UI.Image image)
	{
		if (Instance.opponentInformation.avatarType == 0) {

			if (SportsQuizManager.IsNullOrEmptyOrZero (Instance.opponentInformation.facebook_id)) {
				image.CreateImageSprite (Instance.references.GetAvatarImage (Instance.opponentInformation.avatarType));
			} else {
				string _facebookImageURL = SocialHandler.Instance.GetFacebookFriendURL (SportsQuizManager.Instance.opponentInformation.facebook_id);

				if (string.IsNullOrEmpty (_facebookImageURL)) {
					image.CreateImageSprite (Instance.references.GetAvatarImage (Instance.opponentInformation.avatarType));
				} else {
					WWW imageTexture = new WWW (_facebookImageURL);
					yield return imageTexture;
					
					if (imageTexture.error == null) {
						image.CreateImageSprite (imageTexture.texture);
					} else {
						image.CreateImageSprite (Instance.references.GetAvatarImage (Instance.opponentInformation.avatarType));
					}
				}
			}
		} else {
			image.CreateImageSprite (Instance.references.GetAvatarImage (Instance.opponentInformation.avatarType));
		}
	}

	public string GetQuizModeLabel ()
	{
		string _modeLabel = "";
		switch (SportsQuizManager.Instance.selectedMode)
		{
		case QuizManager.eQuizMode.Leisure:
			_modeLabel = "LEISURE";
			break;
		case QuizManager.eQuizMode.Dual:
			_modeLabel = "DUEL";
			break;
		case QuizManager.eQuizMode.IMFLucky:
			_modeLabel = "I'M FEELING LUCKY";
			break;
		case QuizManager.eQuizMode.Tournament:
			_modeLabel = "TOURNAMENT";
			break;
		}

		return _modeLabel;
	}

    public Badges GetBadge (int id)
    {
        for (int i = 0; i < Instance.badgesList.Count; i++)
        {
            if (Instance.badgesList[i].id == id)
                return Instance.badgesList[i];
        }

        return null;
    }

	public SportsQuizManager.Badges CheckBadgeAlreadyExist (int id)
	{
		for (int i = 0; i < Instance.badgesList.Count; i ++)
		{
			if (Instance.badgesList[i].id == id)
				return Instance.badgesList[i];
		}
		return null;
	}

	public static string UserDetailsColorFormat (SportsQuizManager.eGender gender, string countryname)
	{
		string _details = "";

		string _gender = "";
		if (gender != eGender.Gender) {
			_gender = gender.ToString () + ", ";
		}

		_details = _gender + countryname;

		return _details;
	}

	public static string RankCountry (int rank, int countryCode)
	{
		string _details = "";
		string _country = "";
		if (string.IsNullOrEmpty(Country.GetCountryName (countryCode)) == false) {
			_country = " in " + Country.GetCountryName (countryCode);
		}

		if (rank > 0) {
			_details = "TOP # " + rank.ToString () + _country;
		}

		return _details;
	}

    /*public void ChangeBGColor (UnityEngine.UI.Image image)
	{
		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Leisure) {
			image.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LEISURE_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.Dual) {
			image.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.DUAL_MODE);
		} else if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {
			image.color = CommonUtils.HexToColor (TagConstant.ColorHexCode.LUCKY_MODE);
		}
	}*/

	public SeralizedClassServer.Category GetCategoryClass (int id)
	{
		for (int i = 0; i < Instance.currentCategory.categoryDictionaryList [0].categoryList.Count; i++) {
			if (Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id == id) {
				return Instance.currentCategory.categoryDictionaryList [0].categoryList [i];
			}
		}

		return null;
	}

    /*public static void LoadImageFromLocal (UnityEngine.UI.Image image, int id, string url, References.eImageType type)
	{
		if (References.GetImageFromLocal (id, url, type) != null) {
			
			image.CreateImageSprite (References.GetImageFromLocal (id, url, type));
		}
	}*/

	public static bool IsNullOrEmptyOrZero (string text)
	{
		if (string.IsNullOrEmpty (text))
			return true;

		if (text == "0")
			return true;

		return false;
	}
}

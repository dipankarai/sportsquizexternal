﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Q.Utils;

public class QuizManager : MonoBehaviour {

	private static QuizManager m_instance;
	public static QuizManager Instance
	{
		get
		{
			if (m_instance == null)
			{
				GameObject go = new GameObject ("QuizManager");
				m_instance = go.AddComponent<QuizManager> ();
			}
			return m_instance;
		}
		private set { }
	}

	public enum eQuizLoadState
	{
		UnKnown	= 0,
		Loading,
		IsLoaded
	}

	public enum eQuizMode
	{
        UnKnown = 0,
		Leisure = 1,
		IMFLucky = 2,
		Dual = 3,
		Tournament = 4
	}

    /*public enum eQuizCategory
	{
		Cricket = 0,
		Football
	}*/

	public enum eQuizComplete
	{
		TimeOver,
		Cancelled,
		GameOver
	}

	public enum eQuizType
	{
		Text	= 1,
		Image	= 2,
		Video	= 3
	}

	[System.Serializable]
	public class QuizQuestions
	{
		public string question;
		public int quiz_type;
		public string after_taste;
		public string hint;
		public string resource_url;
		public string[] options;
		public string answer;
	}

	[System.Serializable]
	public class CurrentQuizPlay
	{
		public int userQuizId;
		public int quizId;
        public int currentQuestion = 0;
        public int score;
        public int hintUsed;
        public string afterTaste;

        public SeralizedClassServer.UserInfo userDetails;
        public SeralizedClassServer.UserInfo opponentDetails;
        public List<CurrentQuiz> currentQuizList = new List<CurrentQuiz>();
        public List<SeralizedClassServer.AnswerSubmit> answerSelectedList = new List<SeralizedClassServer.AnswerSubmit> ();
        public List<float> timeTakenPerQuestion = new List<float> ();
        public List<int> correctAnswer = new List<int> ();
        public List<int> wrongAnswer = new List<int> ();

        //Return type:
        public CurrentQuiz GetCurrentQuiz { get { return currentQuizList [currentQuestion]; } }
        public float GetCurrentTimeTaken { get { return timeTakenPerQuestion [currentQuestion]; } }
        public int TotalQuestion { get { return currentQuizList.Count; } }
		public int GetCorrectAnswer {
			get { 
				for (int i = 0; i < GetCurrentQuiz.answers.Count; i++) {
					if (GetCurrentQuiz.answers [i].is_correct == 1)
						return GetCurrentQuiz.answers [i].answer_id;
				}

				return 0;
			} 
		}
		public float GetTotalTime
		{
			get
			{
				if (currentQuestion < (TotalQuestion - 1))
					return ScoreManager.FIRST_SECONDS;
				else
					return ScoreManager.LAST_SECONDS;
			}
		}
	}

	[System.Serializable]
	public class CurrentQuizAssets
	{
		public int questionid;
		public Texture2D image;
		public AudioClip audio;
		public string mediaPath;
		public YoutubeVideo.eVideoQuality quality = YoutubeVideo.eVideoQuality.medium480;
	}

	[System.Serializable]
	public class CurrentQuiz
	{
		public eQuizType quizType;
		public SeralizedClassServer.Question question = new SeralizedClassServer.Question();
		public List<SeralizedClassServer.Answer> answers = new List<SeralizedClassServer.Answer> ();
		public CurrentQuizAssets quizAssets = new CurrentQuizAssets();
	}

	public CurrentQuizPlay currentQuizGame;
	public eQuizLoadState quizLoadState;
	public SeralizedClassServer.ScoreCallback finalSore;

	private float m_timePlayedPerQuestion;
	private SeralizedClassServer.SingleQuizQuestionsList m_currentSingleQuizGameList;
	private SeralizedClassServer.DuelQuizQuestionsList m_currentDuelQuizGameList;
	[SerializeField] private MediaPlayerCtrl m_mediaPlayerCtrl;
	public eQuizComplete gameComplete;
	private YoutubeVideo m_youtubeVideoStreamer;
	private SeralizedClassServer.Status m_currentStatus;
	private System.Action m_statusCallback;
    private List<SeralizedClassServer.StatusData> statusDataPlayer = new List<SeralizedClassServer.StatusData>();

	public System.Action<SeralizedClassServer.Status> opponentStatus;

	void Awake ()
	{
		if (m_instance != null) {
			DestroyImmediate (this.gameObject);
		} else {
			m_instance = this;
			DontDestroyOnLoad (this);

			if (m_mediaPlayerCtrl == null) {
				if (GetComponent<MediaPlayerCtrl> () == null) {
					m_mediaPlayerCtrl = gameObject.AddComponent<MediaPlayerCtrl> ();
				} else {
					m_mediaPlayerCtrl = GetComponent<MediaPlayerCtrl> ();
				}
			}

			if (m_youtubeVideoStreamer == null) {
				if (GetComponent<YoutubeVideo> () == null) {
					m_youtubeVideoStreamer = gameObject.AddComponent<YoutubeVideo> ();
				} else {
					m_youtubeVideoStreamer = GetComponent<YoutubeVideo> ();
				}
			}

			m_mediaPlayerCtrl.m_bAutoPlay = false;
			m_mediaPlayerCtrl.m_bLoop = false;
		}
	}

	// Use this for initialization
	void Start () {

	}

	public void LoadQuiz ()
	{
		if (InternetConnection.Check == false) {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
			return;
		}

        MainMenuPanelManager.Instance.WaitingPanel(true);
		quizLoadState = eQuizLoadState.UnKnown;

        if (SportsQuizManager.Instance.selectedMode == eQuizMode.Leisure) {
			ServerManager.Instance.GetLeisureMode (SportsQuizManager.Instance.currentSelectedTopics.category_id, QuizQuestionSingleCallback);
		} else if (SportsQuizManager.Instance.selectedMode == eQuizMode.Dual) {

			ServerManager.Instance.GetDuelMode (
				SportsQuizManager.Instance.currentSelectedTopics.category_id,
				SportsQuizManager.Instance.otherUSerType,
				QuizQuestionDuelCallback);

        } else if (SportsQuizManager.Instance.selectedMode == eQuizMode.IMFLucky) {

			if (SportsQuizManager.Instance.currentSelectedTopics == null)
				SportsQuizManager.Instance.currentSelectedTopics = new SeralizedClassServer.Topics ();

			SportsQuizManager.Instance.currentSelectedTopics.category_id = SportsQuizManager.Instance.currentCategory.mainCategorySelected;
			SportsQuizManager.Instance.currentSelectedTopics.category_title = SportsQuizManager.Instance.currentCategory.mainCategoryLabel;

			ServerManager.Instance.GetRandom (SportsQuizManager.Instance.currentCategory.mainCategorySelected, 0, QuizQuestionDuelCallback);

		} else if (SportsQuizManager.Instance.selectedMode == eQuizMode.Tournament) {

		}
	}

    public void LoadQuizData(SeralizedClassServer.DuelQuizQuestionsList callback)
    {
        MainMenuPanelManager.Instance.WaitingPanel(true);
        QuizQuestionDuelCallback(callback);
    }

	void QuizQuestionSingleCallback (SeralizedClassServer.SingleQuizQuestionsList callback)
	{
		if (callback == null)
			return;
		
		if (callback.questionList.Count > 6) {
			Q.Utils.QDebug.LogError ("LIST MORE THAN 6");
			callback.questionList.RemoveRange (6, callback.questionList.Count - 6);
		}

		m_currentSingleQuizGameList = callback;
		quizLoadState = eQuizLoadState.Loading;

		StartCoroutine (LoadAllSingleQuizData ());
	}

	void QuizQuestionDuelCallback (SeralizedClassServer.DuelQuizQuestionsList callback)
	{
		if (callback == null)
			return;

        if (callback.questionList.Count > 6) {
            Q.Utils.QDebug.LogError ("LIST MORE THAN 6");
            callback.questionList.RemoveRange (6, callback.questionList.Count - 6);
        }

		m_currentDuelQuizGameList = callback;
		quizLoadState = eQuizLoadState.Loading;

		StartCoroutine (LoadAllDuelQuizData ());
	}

	IEnumerator LoadAllSingleQuizData ()
	{
		currentQuizGame = new CurrentQuizPlay ();
		currentQuizGame.userQuizId = m_currentSingleQuizGameList.userQuizId;
		currentQuizGame.quizId = m_currentSingleQuizGameList.quiz_id;
		currentQuizGame.userDetails = m_currentSingleQuizGameList.user;
		SportsQuizManager.Instance.StorePlayerInformation (m_currentSingleQuizGameList.user);

		for (int i = 0; i < m_currentSingleQuizGameList.questionList.Count; i++) {

			CurrentQuiz _tempCurrentQuiz = new CurrentQuiz ();

			//CALL THIS FOR SHUFFLING THE ANSWER RANDOMLY
			//m_currentSingleQuizGameList.questionList [0].answer.Shuffle<SeralizedClassServer.Answer> ();

			_tempCurrentQuiz.question = m_currentSingleQuizGameList.questionList [i].question;
			_tempCurrentQuiz.answers = m_currentSingleQuizGameList.questionList [i].answer;

			if (m_currentSingleQuizGameList.questionList [i].question.type == 0)
				m_currentSingleQuizGameList.questionList [i].question.type = 1;
			
            _tempCurrentQuiz.quizType = (eQuizType)m_currentSingleQuizGameList.questionList[i].question.type;

			if (_tempCurrentQuiz.question.url == "index.jpg") {
				_tempCurrentQuiz.quizType = eQuizType.Text;
			} else if (_tempCurrentQuiz.quizType == eQuizType.Video && _tempCurrentQuiz.question.url.Contains ("index.jpg")) {
				_tempCurrentQuiz.quizType = eQuizType.Text;
			}

			if (_tempCurrentQuiz.quizType == eQuizType.Image ) {
				WWW www = new WWW (m_currentSingleQuizGameList.questionList [i].question.url);
				yield return www;
				_tempCurrentQuiz.quizAssets.image = www.texture;

			} else if (_tempCurrentQuiz.quizType == eQuizType.Video ) {
				string strURL = m_currentSingleQuizGameList.questionList [i].question.url;
				string parsedStrUrl = YoutubeVideo.Instance.RequestVideo (strURL, YoutubeVideo.eVideoQuality.medium480, _tempCurrentQuiz.quizAssets);
				if (string.IsNullOrEmpty (parsedStrUrl) == false) {

					parsedStrUrl = parsedStrUrl.Trim ();

					string write_directory = Application.persistentDataPath + "/StreamingMedia";
										
					string file_name = ConvertLongString (parsedStrUrl.Substring (parsedStrUrl.LastIndexOf ("/")));
					string write_path = write_directory + "/" + file_name + ".mp4";
					
					if (System.IO.Directory.Exists (write_directory) == false)
						System.IO.Directory.CreateDirectory (write_directory);
					
					Q.Utils.QDebug.Log (write_path);
					
					if (System.IO.File.Exists (write_path) == false) {
						
						WWW www = new WWW (parsedStrUrl);
						yield return www;
						if (string.IsNullOrEmpty(www.error)) {
							System.IO.File.WriteAllBytes(write_path, www.bytes );
							_tempCurrentQuiz.quizAssets.mediaPath = "file://" + write_path;
						}
						
						www.Dispose ();
						www = null;
					} else {
						_tempCurrentQuiz.quizAssets.mediaPath = "file://" + write_path;
					}
				}
			}

			currentQuizGame.currentQuizList.Add (_tempCurrentQuiz);
		}
		yield return null;

		quizLoadState = eQuizLoadState.IsLoaded;

		Q.Utils.QDebug.Log ("Load GAME");
		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.StartPanel);
	}

	IEnumerator LoadAllDuelQuizData ()
	{
		statusDataPlayer = new List<SeralizedClassServer.StatusData> ();
		currentQuizGame = new CurrentQuizPlay ();
		currentQuizGame.userQuizId = m_currentDuelQuizGameList.userQuizId;
		currentQuizGame.quizId = m_currentDuelQuizGameList.quiz_id;

		currentQuizGame.userDetails = m_currentDuelQuizGameList.user;
		SportsQuizManager.Instance.StorePlayerInformation (m_currentDuelQuizGameList.user);

		currentQuizGame.opponentDetails = m_currentDuelQuizGameList.opponent;
		SportsQuizManager.Instance.StoreCurrentOpponentInformation (m_currentDuelQuizGameList.opponent);

		currentQuizGame.afterTaste = m_currentDuelQuizGameList.after_taste;

		for (int i = 0; i < m_currentDuelQuizGameList.questionList.Count; i++) {

			CurrentQuiz _tempCurrentQuiz = new CurrentQuiz ();

			//CALL THIS FOR SHUFFLING THE ANSWER RANDOMLY
			//m_currentDuelQuizGameList.questionList [0].answer.Shuffle<SeralizedClassServer.Answer> ();

			_tempCurrentQuiz.question = m_currentDuelQuizGameList.questionList [i].question;
			_tempCurrentQuiz.answers = m_currentDuelQuizGameList.questionList [i].answer;

			if (m_currentDuelQuizGameList.questionList [i].question.type == 0)
				m_currentDuelQuizGameList.questionList [i].question.type = 1;

            _tempCurrentQuiz.quizType = (eQuizType)m_currentDuelQuizGameList.questionList [i].question.type;

			if (_tempCurrentQuiz.question.url == "index.jpg") {
				_tempCurrentQuiz.quizType = eQuizType.Text;
			} else if (_tempCurrentQuiz.quizType == eQuizType.Video && _tempCurrentQuiz.question.url.Contains ("index.jpg")) {
				_tempCurrentQuiz.quizType = eQuizType.Text;
			}

			if (_tempCurrentQuiz.quizType == eQuizType.Image ) {
				WWW www = new WWW (m_currentDuelQuizGameList.questionList [i].question.url);
				yield return www;
				_tempCurrentQuiz.quizAssets.image = www.texture;

			} else if (_tempCurrentQuiz.quizType == eQuizType.Video ) {
				string strURL = m_currentDuelQuizGameList.questionList [i].question.url;
				string parsedStrUrl = YoutubeVideo.Instance.RequestVideo (strURL, YoutubeVideo.eVideoQuality.medium480, _tempCurrentQuiz.quizAssets);
				if (string.IsNullOrEmpty (parsedStrUrl) == false) {

					parsedStrUrl = parsedStrUrl.Trim ();

					string write_directory = Application.persistentDataPath + "/StreamingMedia";

					string file_name = ConvertLongString (parsedStrUrl.Substring (parsedStrUrl.LastIndexOf ("/")));
					string write_path = write_directory + "/" + file_name + ".mp4";

					if (System.IO.Directory.Exists (write_directory) == false)
						System.IO.Directory.CreateDirectory (write_directory);

					Q.Utils.QDebug.Log (write_path);

					if (System.IO.File.Exists (write_path) == false) {

						WWW www = new WWW (parsedStrUrl);
						yield return www;
						if (string.IsNullOrEmpty(www.error)) {
							System.IO.File.WriteAllBytes(write_path, www.bytes );
							_tempCurrentQuiz.quizAssets.mediaPath = "file://" + write_path;
						}

						www.Dispose ();
						www = null;
					} else {
						_tempCurrentQuiz.quizAssets.mediaPath = "file://" + write_path;
					}

				}
			}

			currentQuizGame.currentQuizList.Add (_tempCurrentQuiz);
		}
		yield return null;

		quizLoadState = eQuizLoadState.IsLoaded;
		Q.Utils.QDebug.Log ("Load GAME");
		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.StartPanel);
	}

	public void StartStatusUpdate (int quizID, System.Action callback)
	{
		m_statusCallback = callback;
		if (m_currentStatus == null)
		{
			m_currentStatus = new SeralizedClassServer.Status ();
			m_currentStatus.status_id = 0;
		}
		ServerManager.Instance.GetStatusIDictionary (m_currentStatus.status_id, quizID,  UpdateOpponentStatusIDictionary);
	}

    /*void UpdateOpponentStatus (SeralizedClassServer.LastStatus callback)
	{
		if (callback != null) {
			
			Q.Utils.QDebug.Log ("UpdateOpponentStatus "+callback.status.Count);
			
			for (int i = 0; i < callback.status.Count; i++)
			{
				Q.Utils.QDebug.Log ((ServerManager.eStatusUpdateType)callback.status[i].type);
				
				if ((ServerManager.eStatusUpdateType) callback.status[i].type == ServerManager.eStatusUpdateType.OpponentAnswer) {
					if (CheckStatusDataType (callback.status[i].data) == true)
					{
						List<SeralizedClassServer.StatusData> dataList = 
							JsonConvert.DeserializeObject<List<SeralizedClassServer.StatusData>> (JsonConvert.SerializeObject (callback.status [i].data));
						Q.Utils.QDebug.Log (callback.status [i].user_quiz_id +
							" :: " + dataList.Count);
						
						m_currentStatus.status_id = callback.status [i].status_id;
						m_currentStatus.user_quiz_id = callback.status [i].user_quiz_id;
						m_currentStatus.user_id = callback.status [i].user_id;
						m_currentStatus.type = callback.status [i].type;
						m_currentStatus.data = callback.status [i].data;
						
						if (opponentStatus != null)
							opponentStatus (m_currentStatus);
						
						break;
					}
				}
				else if ((ServerManager.eStatusUpdateType) callback.status[i].type == ServerManager.eStatusUpdateType.ChallengeAccept)
				{
					m_currentStatus.status_id = callback.status [i].status_id;
					m_currentStatus.user_quiz_id = callback.status [i].user_quiz_id;
					m_currentStatus.user_id = callback.status [i].user_id;
					m_currentStatus.type = callback.status [i].type;
					m_currentStatus.data = callback.status [i].data;
					
					if (opponentStatus != null) {
						opponentStatus (m_currentStatus);
					} else {
					}
				}
			}
		}

		m_statusCallback ();
	}*/

	void UpdateOpponentStatusIDictionary (IDictionary callback)
	{
		if (callback != null) {

			IDictionary statusMainDictionary = (IDictionary)callback;
			int last_status_id = int.Parse (statusMainDictionary ["last_status_id"].ToString ());
			IList statusList = (IList)statusMainDictionary ["status"];

			for (int i = 0; i < statusList.Count; i++)
			{
				IDictionary statusDictionary = (IDictionary)statusList [i];

				int status_id =  int.Parse (statusDictionary ["status_id"].ToString ());
				int user_quiz_id =  int.Parse (statusDictionary ["user_quiz_id"].ToString ());
				int user_id =  int.Parse (statusDictionary ["user_id"].ToString ());
				int type = int.Parse (statusDictionary ["type"].ToString ());
				object data = (object)statusDictionary ["data"];
				string created_at = statusDictionary ["created_at"].ToString ();
				int status = int.Parse (statusDictionary ["status"].ToString ());

				if (type == (int)ServerManager.eStatusUpdateType.OpponentAnswer) {
					if (CheckStatusDataType (data) == true && last_status_id == status_id) {
						//m_currentStatus.status_id = status_id;
						m_currentStatus.user_quiz_id = user_quiz_id;
						m_currentStatus.user_id = user_id;
						m_currentStatus.type = type;
						m_currentStatus.data = data;

						if (opponentStatus != null)
							opponentStatus (m_currentStatus);

						break;
					}
				} else if (type == (int)ServerManager.eStatusUpdateType.ChallengeAccept) {
					m_currentStatus.status_id = status_id;
					m_currentStatus.user_quiz_id = user_quiz_id;
					m_currentStatus.user_id = user_id;
					m_currentStatus.type = type;
					m_currentStatus.data = data;

					if (opponentStatus != null) {
						opponentStatus (m_currentStatus);
					} else {
					}
				} else if (type == (int)ServerManager.eStatusUpdateType.ChallengeReject) {
					m_currentStatus.status_id = status_id;
					m_currentStatus.user_quiz_id = user_quiz_id;
					m_currentStatus.user_id = user_id;
					m_currentStatus.type = type;
					m_currentStatus.data = data;

					if (opponentStatus != null) {
						opponentStatus (m_currentStatus);
					} else {
					}
				}
			}
		}

		m_statusCallback ();
	}

	bool CheckStatusDataType (object _datatype)
	{
		if (_datatype == null) {
			return false;
		} else if (_datatype is int) {
			return false;
		} else if (string.IsNullOrEmpty (_datatype.ToString())) {
			//IS EMPTY STRING
			return false;
		} else if (string.Equals(_datatype.ToString(), "{}")) {
			//IS EMPTY DICTIONARY
			return false;
		}

		return true;
	}

	public void UpdateStatus (SeralizedClassServer.StatusData data)
	{
		statusDataPlayer.Add (data);

		Q.Utils.QDebug.Log ("UpdateStatus "+ JsonConvert.SerializeObject (statusDataPlayer));
		ServerManager.Instance.UpdateStatus (currentQuizGame.userQuizId, statusDataPlayer, StatusUpdated);
	}

	void StatusUpdated (object status)
	{
		string status_response = (string) status;
		Q.Utils.QDebug.Log ("CALL BACK STATUS ID :: "+ status_response);
	}

	public bool QuestionAnswered (int optionId)
	{
		StopCoroutine ("CountDownTimer");

		int _correctAnswer = currentQuizGame.GetCorrectAnswer;

		SeralizedClassServer.AnswerSubmit answer = new SeralizedClassServer.AnswerSubmit ();
		answer.answerId = optionId;
		answer.questionId = currentQuizGame.GetCurrentQuiz.question.questionId;
		currentQuizGame.answerSelectedList.Add (answer);

		if (_correctAnswer == optionId) {
			//CORRECT ANSWER...
			currentQuizGame.timeTakenPerQuestion.Add (currentQuizGame.GetTotalTime - (m_timePlayedPerQuestion * currentQuizGame.GetTotalTime));
			currentQuizGame.correctAnswer.Add (optionId);

			return true;
		} else {
			//WRONG ANSWER...
			currentQuizGame.timeTakenPerQuestion.Add (0);
			currentQuizGame.wrongAnswer.Add (optionId);

			if (Instance.currentQuizGame.currentQuestion == (Instance.currentQuizGame.TotalQuestion - 1))
				BadgesManager.Instance.AnsweredWrong ();

			return false;
		}
	}

    /*public int GetCorrectAnswer (List<SeralizedClassServer.Answer> answerList)
	{
		for (int i = 0; i < answerList.Count; i++) {
			if (answerList [i].is_correct == 1)
				return answerList [i].answer_id;
		}

		return 0;
	}*/

	public int GetOpponentCorrectAnswer (int questionID) {
		for (int idx = 0; idx < currentQuizGame.currentQuizList.Count; idx++) {
			if (currentQuizGame.currentQuizList[idx].question.questionId == questionID) {
				for (int i = 0; i < currentQuizGame.currentQuizList[idx].answers.Count; i++) {
					if (currentQuizGame.currentQuizList[idx].answers [i].is_correct == 1)
						return currentQuizGame.currentQuizList[idx].answers [i].answer_id;
				}
			}
		}

		return 0;
	}

	public void QuizComplete (eQuizComplete quizComplete)
	{
		StopCoroutine ("CountDownTimer");
		gameComplete = quizComplete;
		switch (quizComplete)
		{
		case eQuizComplete.GameOver:
			{
                SubmitAnswer ();
                BadgesManager.Instance.BadgesCalculation();
				break;
			}
		case eQuizComplete.TimeOver:
			{
				int _getCorrectAnswer = currentQuizGame.GetCorrectAnswer;

				QuestionAnswered (_getCorrectAnswer++);

				QuizPanelManager.Instance.gamePlayPanel.OnCompleteTimeOver ();

				timerCallback (true, "0", 0);
				break;
			}
		case eQuizComplete.Cancelled:
			{
				#if NEW_RESULT_LOGIC
				SeralizedClassServer.StatusData data = new SeralizedClassServer.StatusData ();
				data.quizComplete = 1;
				data.count = QuizManager.Instance.currentQuizGame.currentQuestion;
				data.question_id = 0;
				data.answer_id = 0;
				data.score = 0;
				Instance.UpdateStatus (data);
				#endif

				currentQuizGame.answerSelectedList = new List<SeralizedClassServer.AnswerSubmit>();
				ScoreManager.Instance.playerCurrentQuizScore = new List<ScoreManager.Score>();
                
				SubmitAnswer ();

				break;
			}
		default:
			break;
		}

	}

	void SubmitAnswer ()
	{
		int _userQuizID = 0;
		if (SportsQuizManager.Instance.selectedMode == eQuizMode.Leisure) {
			_userQuizID = m_currentSingleQuizGameList.userQuizId;
		} else {
			_userQuizID = m_currentDuelQuizGameList.userQuizId;
		}

		if (InternetConnection.Check) {
			
			ServerManager.Instance.SubmitAnswerToServer (
				_userQuizID,
				ScoreManager.Instance.GetTotalScore (true),
				#if NON_MD5
				currentQuizGame.answerSelectedList,
				#else
				ScoreManager.Instance.GetTotalScoreMD5 (true, Instance.currentQuizGame.userQuizId),
				#endif
				SubmitAnswerCallback);
			MainMenuPanelManager.Instance.WaitingPanel (true);
		} else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			MainMenuPanelManager.Instance.ShowMessage ("Check internet connection to save score.", CheckConnectionFirst);
		}
	}

	void CheckConnectionFirst ()
	{
		MainMenuPanelManager.Instance.WaitingPanel (true);
		StartCoroutine ("CheckConnectionFirstCoroutine");
	}

	IEnumerator CheckConnectionFirstCoroutine ()
	{
		yield return new WaitForSeconds (5f);
		SubmitAnswer ();
	}

	void SubmitAnswerCallback (SeralizedClassServer.ScoreCallback callback)
	{
		if (callback == null) {
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.ResultPanel);
			return;
		}

		finalSore = callback;

		if (gameComplete == eQuizComplete.Cancelled) {
			
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.ResultPanel);
			return;
		}

		#if NEW_RESULT_LOGIC
		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.ResultPanel);
		#else
		if (SportsQuizManager.Instance.selectedMode != eQuizMode.Leisure) {
			StartCoroutine ("WaitForOpponentComplete");
		} else {
			QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.ResultPanel);
		}
		#endif
	}

	IEnumerator WaitForOpponentComplete ()
	{
		int _opponent_question_left = Instance.currentQuizGame.TotalQuestion -
		                              ScoreManager.Instance.opponentCurrentQuizScore.Count;

		int _percentageLeft = (Instance.currentQuizGame.TotalQuestion * 40 / 100);
		Q.Utils.QDebug.Log (_opponent_question_left + ":::" + _percentageLeft);

		float _timeLapse = Time.time;

		if (_opponent_question_left < _percentageLeft)
		{
			while (_opponent_question_left > 0)
			{
				_opponent_question_left = Instance.currentQuizGame.TotalQuestion -
					ScoreManager.Instance.opponentCurrentQuizScore.Count;

				if (_timeLapse + 15f < Time.time) //WAIT FOR 15 SECONDS FOR OPPONENT TO COMPLETE......
					_opponent_question_left = 0;

				yield return 0;
			}
		}

		QuizPanelManager.Instance.SwitchPanel (QuizPanelManager.eQuizPanelState.ResultPanel);

		yield return null;
	}

    /*public List<QuizQuestions> GetRandomQuiz(eQuizCategory category, int totalQuestion)
	{
		TextAsset assets = Resources.Load ("LocalDatabase/QuizDatabase") as TextAsset;

		List<QuizQuestions> tempTotalQizList = new List<QuizQuestions> ();
		tempTotalQizList = JsonConvert.DeserializeObject<List<QuizQuestions>> (
			GetQuizByCategory (category, assets.text));

		if (totalQuestion > tempTotalQizList.Count) {
			Q.Utils.QDebug.LogError ("Questions you want is larger than questions in database");
			return null;
		}

		List<QuizQuestions> tempQizList = new List<QuizQuestions> ();
		List<int> tempRandomList = MathUtil.GetRandomIndexNos (totalQuestion, tempTotalQizList.Count);

		for (int i=0; i<tempRandomList.Count; i++){
			tempQizList.Add (tempTotalQizList [tempRandomList [i]]);
		}

		return tempQizList;
	}*/

    /*string GetQuizByCategory (eQuizCategory category, string database)
	{
//		Q.Utils.QDebug.Log (database);
		IDictionary _databaseDict = (IDictionary) MiniJSON.Json.Deserialize (database);
		IList _categoryDict = (IList)_databaseDict [category.ToString ()];

//		Q.Utils.QDebug.Log (MiniJSON.Json.Serialize (_databaseDict));
//		Q.Utils.QDebug.Log (MiniJSON.Json.Serialize (_categoryDict));
		return MiniJSON.Json.Serialize (_categoryDict);
	}*/

    /*public CurrentQuizAssets GetAssets (int questionId)
	{
		for (int i = 0; i < QuizManager.Instance.currentQuizGame.quizAssets.Count; i++) {
			if (QuizManager.Instance.currentQuizGame.quizAssets[i].questionid == questionId) {
				return QuizManager.Instance.currentQuizGame.quizAssets [i];
			}
		}

		return null;
	}*/

	public MediaPlayerCtrl AttachScreenToMediaPlayer (GameObject screen)
	{
		m_mediaPlayerCtrl.m_TargetMaterial = new GameObject[1];
		m_mediaPlayerCtrl.m_objResize = null;

		m_mediaPlayerCtrl.m_TargetMaterial [0] = screen;
//		m_mediaPlayerCtrl.m_objResize [0] = screen;

		return m_mediaPlayerCtrl;
	}

	public void VideoPlayerClosed ()
	{
		QuizPanelManager.Instance.gamePlayPanel.VideoPlayComplete ();
	}

	string ConvertLongString (string _longString) {
		string stringId = "";
		System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex ("[^a-zA-Z0-9]");

		string str = rgx.Replace (_longString, "");

		List<char> stringIdList = new List<char> ();

		for (int i = 0; i < str.Length; i++) {
			if (stringIdList.Contains (str [i]) == false) {

				stringIdList.Add (str [i]);
			}
		}

		for (int i = 0; i < stringIdList.Count; i++) {
			stringId += stringIdList [i];
		}

		return stringId;
	}

	#region COUNT DOWN TIMER

	private string _timeString = "";
	private float _timer;
	private float m_seconds;
	private System.Action<bool, string, float> timerCallback;
	private float m_onbackgroundTime;

	public void StartCountDown (System.Action<bool, string, float> callback)
	{
		timerCallback = callback;
		m_timePlayedPerQuestion = currentQuizGame.GetTotalTime;
		StartCoroutine ("CountDownTimer", currentQuizGame.GetTotalTime);
	}

	private void UpdateTimer (float time)
	{
		_timeString = Mathf.RoundToInt (time).ToString ();
		_timer = time / currentQuizGame.GetTotalTime;
		timerCallback (false, _timeString, _timer);
		m_timePlayedPerQuestion = _timer;
	}

	IEnumerator CountDownTimer (float time)
	{
		m_seconds = time;
		while (m_seconds > 0)
		{
			m_seconds -= Time.deltaTime;
			UpdateTimer (m_seconds);

			yield return null;
		}

		QuizComplete (eQuizComplete.TimeOver);
		yield return 0;
	}

	void OnApplicationPause (bool pauseState)
	{
//		Q.Utils.QDebug.Log ("OnApplicationPause "+pauseState);
		if (pauseState) {
			m_onbackgroundTime = Time.realtimeSinceStartup;
		} else {
			m_seconds -= (Time.realtimeSinceStartup - m_onbackgroundTime);
		}
	}

	#endregion
}

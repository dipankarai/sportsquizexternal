﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NotificationHandler : MonoBehaviour {

	private static NotificationHandler m_instance;
	public static NotificationHandler Instance { get {return m_instance; } }

	private bool isStoringData = false;
	private int m_lastNotificationID = 0;
	public float callTime = 2f;
	public bool isFirstTimeLoad = true;
	private bool canCallAgain = true;

	[System.Serializable]
	public class NotificationSavedLocally
	{
		public List<NotificationLastID> notificationList = new List<NotificationLastID> ();
	}

	[System.Serializable]
	public class NotificationLastID
	{
		public int userid;
		public int lastid;
	}

	void Awake () {
		if (m_instance == null)
			m_instance = this;
	}

	public static NotificationHandler Init (GameObject obj)
	{
		if (m_instance == null)
			m_instance = obj.AddComponent<NotificationHandler> ();

		return m_instance;
	}

	// Use this for initialization
	void Start () {
		CheckForNotificationLoop ();
	}

	void Update () {
		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.MainMenuPanel)
		{
			if (MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel != MainMenuPanel.eSubMenuPanel.Settings)
			{
				if (notificationCalled == false) {
					StartCoroutine ("CallAgainAfterSeconds", callTime);
					callTime = 15f;
				}
			}
		}
	}
	
	void CheckForNotificationLoop ()
	{
		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.SplashScreen) {
			StartCoroutine ("CallAgainAfterSeconds", 20f);
			return;
		}

		if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.Gameplay) {
			StartCoroutine ("CallAgainAfterSeconds", 50f);
			return;
		}

		if (MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel == MainMenuPanel.eSubMenuPanel.Settings) {
			StartCoroutine ("CallAgainAfterSeconds", 10f);
			return;
		}

		if (InternetConnection.Check)
		{
			notificationCalled = true;
			ServerManager.Instance.GetNotification();
		} else {
			StartCoroutine ("CallAgainAfterSeconds", 10f);
		}
	}

	public void NotificationCallback (SeralizedClassServer.Notification callback)
	{
		if (isStoringData == false) {
			isStoringData = true;

			m_lastNotificationID = GetLastNotificationID ();

			if (callback == null) {
				StartCoroutine ("CallAgainAfterSeconds", 20f);
			} else {
				int badgeCount = 0;
				for (int i = 0; i < callback.content.Count; i++) {
					if (callback.content[i].notification_type == (int) NotificationItem.eNotificatioType.Badges) {
						LoadBadgesDetails (callback.content [i].data);
					}
				}

				int _unChecked_Notification = 0;
				if (callback.last_notification_id != m_lastNotificationID) {

					for ( int i = 0; i < callback.content.Count; i++) {
						if (callback.content[i].notification_id <= m_lastNotificationID) {
							break;
						} else {
							_unChecked_Notification++;
							if (callback.content[i].notification_type == (int)NotificationItem.eNotificatioType.Badges) {
								ConnectionHandler.Instance.GetUserInformation ();
							}
							CheckForChallengeNotification (callback.content[i]);
						}
					}
				} else {
					Q.Utils.QDebug.LogError ("Same Notification 3" + m_lastNotificationID);
				}

				SportsQuizManager.Instance.notification = callback;
				MainMenuPanelManager.Instance.mainMenuPanel.NewNotification (_unChecked_Notification);
				StartCoroutine ("CallAgainAfterSeconds", 30f);
			}

			bool isEnabled = BadgesManager.Instance.enabled;
			isStoringData = false;
			notificationCalled = false;
			canCallAgain = true;
		}
	}

    /// <summary>
    /// Check for any new badges achieved.
    /// If any new badges achieved it will get badges details from server.
    /// </summary>
    /// <param name="data">NotificationData Data.</param>
	void LoadBadgesDetails (SeralizedClassServer.NotificationData data)
	{
		SportsQuizManager.Badges badge = new SportsQuizManager.Badges();
		badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(data.badge_id);

		if (badge == null) {
			badge = new SportsQuizManager.Badges();
			badge.description = data.description;
			badge.title = data.title;
			badge.id = data.badge_id;
			badge.url = data.image;

			SportsQuizManager.Instance.badgesList.Add(badge);
			if (Instance.isFirstTimeLoad == false && canCallAgain) {
				canCallAgain = false;
				ConnectionHandler.Instance.GetUserInformation ();
			}

		} else {
			if (string.IsNullOrEmpty (badge.description)) {
				badge.description = data.description;
			}
		}
	}

	void CheckForChallengeNotification (SeralizedClassServer.NotificationDetails details)
	{
		if (details.notification_type == (int)NotificationItem.eNotificatioType.Challenge) {
			Q.Utils.QDebug.Log ("CheckForChallengeNotification");
			MainMenuPanelManager.Instance.notificationPopUp.InitNotificationPopUp (
				details.notification_id,
				details.notification_type,
				details.data,
				details.message);
		}
	}

	IEnumerator StoreAssets (SeralizedClassServer.Notification callback)
	{
		Q.Utils.QDebug.LogRed ("StoreAssets");

		m_lastNotificationID = GetLastNotificationID ();

		if (callback == null) {
			StartCoroutine ("CallAgainAfterSeconds", 20f);
		} else {

			int badgeCount = 0;
			for (int i = 0; i < callback.content.Count; i++) {
				
				if (callback.content[i].notification_type == (int) NotificationItem.eNotificatioType.Badges) {
					
					SeralizedClassServer.NotificationData data = callback.content [i].data;
					
					SportsQuizManager.Badges badge = new SportsQuizManager.Badges();
					badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(data.badge_id);
					if (badge == null) {
						
						badge = new SportsQuizManager.Badges();
						badge.description = data.description;
						badge.title = data.title;
						badge.id = data.badge_id;
						badge.url = data.image;
						
						SportsQuizManager.Instance.badgesList.Add(badge);
						if (Instance.isFirstTimeLoad == false && canCallAgain) {
							canCallAgain = false;
							ConnectionHandler.Instance.GetUserInformation ();
						}

					} else {
						if (string.IsNullOrEmpty (badge.description)) {
							badge.description = data.description;
						}
					}
				}
			}

			int _unChecked_Notification = 0;
			if (callback.last_notification_id != m_lastNotificationID) {
				
				for ( int i = 0; i < callback.content.Count; i++) {
					if (callback.content[i].notification_id <= m_lastNotificationID) {
						break;
					} else {
						_unChecked_Notification++;
						if (callback.content[i].notification_type == (int)NotificationItem.eNotificatioType.Badges) {
							ConnectionHandler.Instance.GetUserInformation ();
						}
					}
				}
			} else {
				Q.Utils.QDebug.LogError ("Same Notification 3" + m_lastNotificationID);
			}
			
			SportsQuizManager.Instance.notification = callback;
			MainMenuPanelManager.Instance.mainMenuPanel.NewNotification (_unChecked_Notification);
			
			Q.Utils.QDebug.Log ("New Notification "+ _unChecked_Notification);
			StartCoroutine ("CallAgainAfterSeconds", 30f);
		}

		bool isEnabled = BadgesManager.Instance.enabled;
		isStoringData = false;
		notificationCalled = false;
		canCallAgain = true;

		yield return null;
	}

	private bool notificationCalled = false;
	IEnumerator CallAgainAfterSeconds (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		StopCoroutine ("CallAgainAfterSeconds");
		CheckForNotificationLoop ();
	}

	public int GetLastNotificationID ()
	{
		if (GamePrefs.HasKey (TagConstant.PrefsName.LAST_NOTIFICATION)) {
			NotificationSavedLocally _data = GamePrefs.LastNotificationID;

			if (_data != null) {
				for (int i = 0; i < _data.notificationList.Count; i++) {
					if (_data.notificationList[i].userid == SportsQuizManager.Instance.playerInformation.serverID) {
						return _data.notificationList [i].lastid;
					}
				}
			}
		}

		return 0;
	}

	public void SaveLastNotification (int lastNotification)
	{
		NotificationSavedLocally _data = new NotificationSavedLocally ();
		if (GamePrefs.HasKey (TagConstant.PrefsName.LAST_NOTIFICATION)) {
			_data = GamePrefs.LastNotificationID;

			if (_data != null) {
				for (int i = 0; i < _data.notificationList.Count; i++) {
					if (_data.notificationList[i].userid == SportsQuizManager.Instance.playerInformation.serverID) {
						_data.notificationList [i].lastid = lastNotification;

						GamePrefs.LastNotificationID = _data;
						return;
					}
				}
			}
		}

		NotificationLastID _newData = new NotificationLastID ();
		_newData.userid = SportsQuizManager.Instance.playerInformation.serverID;
		_newData.lastid = lastNotification;
		_data.notificationList.Add (_newData);
		GamePrefs.LastNotificationID = _data;
	}
}

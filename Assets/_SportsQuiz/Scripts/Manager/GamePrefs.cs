﻿using UnityEngine;
using System.Collections;
using Q.Utils;

public static class GamePrefs
{
	public const string bgKey = "isBgOn";
	public const string FxKey = "isFxOn";

    [System.Serializable]
    public class LoginData
    {
        public SportsQuizManager.eUserType usertype;
        public string email_id;
        public string password;
    }

    public static void DeleteLoginDetails ()
    {
		PlayerPrefs.DeleteKey(TagConstant.PrefsName.LOGIN_PREFS);
    }

	public static void DeleteAll ()
	{
		PlayerPrefs.DeleteAll ();
	}

	public static void DeleteKey(string key)
	{
		PlayerPrefs.DeleteKey(key);
	}

	public static bool HasKey (string key)
	{
		return PlayerPrefs.HasKey (key);
	}

	public static int isFxOn
	{
		get {
			return PlayerPrefs.GetInt (FxKey);
		}
		set{
			PlayerPrefs.SetInt (FxKey, value);
		}
	}

	public static int isBgOn
	{
		get {
			return PlayerPrefs.GetInt (bgKey);
		}
		set{
			PlayerPrefs.SetInt (bgKey, value);
		}
	}

	public static int RateTheApp
	{
		get {
			return PlayerPrefs.GetInt (TagConstant.PrefsName.RATE_THE_APP);
		} set {
			PlayerPrefs.SetInt (TagConstant.PrefsName.RATE_THE_APP, value);
		}
	}

	public static string AndroidDeviceToken
	{
		get {
			return PlayerPrefs.GetString (TagConstant.PrefsName.ANDROID_DEVICE_TOKEN);
		} set {
			PlayerPrefs.SetString (TagConstant.PrefsName.ANDROID_DEVICE_TOKEN, value);
		}
	}

	public static string iOSDeviceToken
	{
		get {
			return PlayerPrefs.GetString (TagConstant.PrefsName.iOS_DEVICE_TOKEN);
		} set {
			PlayerPrefs.SetString (TagConstant.PrefsName.iOS_DEVICE_TOKEN, value);
		}
	}

	#region NOTIFICATION
	/*public static int LastNotificationID
	{
		get {
			return PlayerPrefs.GetInt (TagConstant.PrefsName.LAST_NOTIFICATION);
		}
		set {
			PlayerPrefs.SetInt (TagConstant.PrefsName.LAST_NOTIFICATION, value);
		}
	}*/

	public static NotificationHandler.NotificationSavedLocally LastNotificationID
	{
		get {
			return (NotificationHandler.NotificationSavedLocally)PlayerPrefsSerializer.LoadData<NotificationHandler.NotificationSavedLocally> (TagConstant.PrefsName.LAST_NOTIFICATION);
		}
		set {
			PlayerPrefsSerializer.SaveData (TagConstant.PrefsName.LAST_NOTIFICATION, value);
		}
	}

	public static int RandomLocalNotification
	{
		get {
			return PlayerPrefs.GetInt (TagConstant.PrefsName.LOCAL_NOTIFICATION_MSG);
		} set {
			PlayerPrefs.SetInt (TagConstant.PrefsName.LOCAL_NOTIFICATION_MSG, value);
		}
	}

	#if UNITY_IOS || UNITY_IPHONE
	public static PushNotificationManager.iOSLocalNotificationClass iOSPreviousNotification
	{
		get
		{
			return (PushNotificationManager.iOSLocalNotificationClass)PlayerPrefsSerializer.LoadData<PushNotificationManager.iOSLocalNotificationClass>(TagConstant.PrefsName.LAST_iOS_NOTIFICATION);
		}
		set
		{
			PlayerPrefsSerializer.SaveData(TagConstant.PrefsName.LAST_iOS_NOTIFICATION, value);
		}
	}
	#endif

	#endregion

    #region BADGES

    public static int HintBadge
	{
		get {
			return PlayerPrefs.GetInt (BadgesManager.HINT_NOT_USED_BADGE);
		}
		set {
			PlayerPrefs.SetInt (BadgesManager.HINT_NOT_USED_BADGE, value);
		}
	}

	public static int AnsweredWrong
	{
		get {
			return PlayerPrefs.GetInt (BadgesManager.ANSWERED_WRONG_SIXTH);
		}
		set {
			PlayerPrefs.SetInt (BadgesManager.ANSWERED_WRONG_SIXTH, value);
		}
	}

    public static BadgesManager.BadgeSpareTime BadgeSpareTimeTen
    {
        get {
            return (BadgesManager.BadgeSpareTime) PlayerPrefsSerializer.LoadData<BadgesManager.BadgeSpareTime>(BadgesManager.SPARE_TIME_TEN);
        }
        set {
            PlayerPrefsSerializer.SaveData(BadgesManager.SPARE_TIME_TEN, value);
        }
    }

    public static BadgesManager.BadgeSpareTime BadgeSpareTimeTwenty
    {
        get
        {
            return (BadgesManager.BadgeSpareTime)PlayerPrefsSerializer.LoadData<BadgesManager.BadgeSpareTime>(BadgesManager.SPARE_TIME_TEN);
        }
        set
        {
            PlayerPrefsSerializer.SaveData(BadgesManager.SPARE_TIME_TWENTY, value);
        }
    }

    public static int ScoreAllPoints
    {
        get
        {
            return PlayerPrefs.GetInt(BadgesManager.SCORE_ALL_POINTS);
        }
        set
        {
            PlayerPrefs.SetInt(BadgesManager.SCORE_ALL_POINTS, value);
        }
    }

    public static int ScoreMoreThanFifty
    {
        get { return PlayerPrefs.GetInt(BadgesManager.SCORE_MORE_THAN_FIFTY); }
        set { PlayerPrefs.SetInt(BadgesManager.SCORE_MORE_THAN_FIFTY, value); }
    }

    public static int ScoreMoreThanSeventyFive
    {
        get { return PlayerPrefs.GetInt(BadgesManager.SCORE_MORE_THAN_SEVENTYFIVE); }
        set { PlayerPrefs.SetInt(BadgesManager.SCORE_MORE_THAN_SEVENTYFIVE, value); }
    }

    public static int ScoreMoreThanNinety
    {
        get { return PlayerPrefs.GetInt(BadgesManager.SCORE_MORE_THAN_NINETY); }
        set { PlayerPrefs.SetInt(BadgesManager.SCORE_MORE_THAN_NINETY, value); }
    }

    #endregion

    public static int PlayCount
    {
    	get{
			return PlayerPrefs.GetInt ("PlayCount");
    	}
    	set{
			PlayerPrefs.SetInt("PlayCount",  value);
    	}
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class ConnectionHandler : MonoBehaviour
{
	private static ConnectionHandler m_instance;
	public static ConnectionHandler Instance { get {return m_instance; } }

	private NotificationHandler m_notificationManager;
	private InternetConnection m_connection;
	private SocialHandler m_socailManager;
	private ServerManager m_serverManager;
    private string m_email, m_password, m_usrName;
    private bool isFirstCheck = true;

	void Awake () {

		if (m_instance == null)
			m_instance = this;

		if (m_connection == null)
			m_connection = InternetConnection.Init(this.gameObject);

		if (m_socailManager == null)
			m_socailManager = SocialHandler.Init (this.gameObject);

		if (m_serverManager == null)
			m_serverManager = ServerManager.Init (this.gameObject);

		if (m_notificationManager == null)
			m_notificationManager = NotificationHandler.Init (this.gameObject);

        MainMenuPanelManager.Instance.WaitingPanel (true);
	}

    #region First initial check

    IEnumerator Start ()
    {
//        InitPanel ();
        while (!InternetConnection.isFirstChecked) {
            yield return null;
        }
        CheckInternet ();
    }

    void CheckInternet ()
    {
        if (InternetConnection.Check) {
            AppConfigurationVersionCheck ();
        } else {
            if (isFirstCheck == true) {
                isFirstCheck = false;
                StartCoroutine ("Start");
            } else {
                if (MainMenuPanelManager.Instance.popUpMessage.gameObject.activeInHierarchy == false) {
                    MainMenuPanelManager.Instance.ShowMessage (
                        TagConstant.PopUpMessages.NO_INTERNET_CONNECTION, WaitForInternetConnection);
                } else {
                    StartCoroutine ("NewCoroutineWait");
                }
            }
        }
    }

    void AppConfigurationVersionCheck ()
    {
        ServerManager.Instance.ConfigGet (AppConfigurationVersionCheckCallback);
    }

    void WaitForInternetConnection ()
    {
        StartCoroutine ("NewCoroutineWait");
    }

    IEnumerator NewCoroutineWait ()
    {
        while (InternetConnection.Check == false)
        {
            yield return 0;
        }

        AppConfigurationVersionCheck ();
    }

    void AppConfigurationVersionCheckCallback (SeralizedClassServer.ConfigGetVersion callback)
    {
        float currentVersion = float.Parse (CurrentBundleVersion.version);

        #if UNITY_ANDROID
        if (callback.ANDROID_VERSION > currentVersion) {
            MainMenuPanelManager.Instance.appUpdatesPanel.gameObject.SetActive (true);
            return;
        }
        #elif UNITY_IOS || UNITY_IPHONE
        if (callback.IOS_VERSION > currentVersion) {
        MainMenuPanelManager.Instance.appUpdatesPanel.gameObject.SetActive (true);
        return;
        }
        #endif

        float version = SportsQuizManager.Instance.APP_VERSION;
        PushNotificationManager.Instance.RequestDeviceToken ();
    }

    #endregion
	
    public static ConnectionHandler Init (GameObject obj)
	{
		if (m_instance == null) {
            GameObject go = new GameObject ("~ConnectionHandler");
			m_instance = go.AddComponent<ConnectionHandler> ();
		}

		return m_instance;
	}

	public void GuestLoign ()
	{
		if (InternetConnection.Check) {

			SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.GuestUser;
			SportsQuizManager.Instance.playerInformation.email = "dipankarai@gmail.com";
			SportsQuizManager.Instance.playerInformation.password = "rai";

			Q.Utils.QDebug.Log (SportsQuizManager.Instance.loginType+""+
				SportsQuizManager.Instance.playerInformation.email+""+
				SportsQuizManager.Instance.playerInformation.password);

			ServerManager.Instance.LogInToServer (LoginCallback);
		}
		else
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
	}

	public void LoginFacebook ()
    {
		if (InternetConnection.Check) {
			SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.FacebookUser;
			SocialHandler.Instance.FacebookLogin(LoginCallback);
		}
		else
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
    }

	public void LoginGooglePlus ()
	{
		if (InternetConnection.Check) {
			SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.GPlus;
			SocialHandler.Instance.GooglePlusLogin(LoginCallback);
		}
		else
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
	}

	public void LoginEmail (string email, string password)
    {
		if (InternetConnection.Check) {
			
			SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.EmailUser;
			SportsQuizManager.Instance.playerInformation.email = email;
			SportsQuizManager.Instance.playerInformation.password = password;
			
			Q.Utils.QDebug.Log (SportsQuizManager.Instance.loginType+""+
				SportsQuizManager.Instance.playerInformation.email+""+
				SportsQuizManager.Instance.playerInformation.password);
			
			ServerManager.Instance.LogInToServer (LoginCallback);
		}
		else
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);

    }

	public void SignUpEmail (string usrName, string email, string password)
    {
		m_usrName = usrName;
		m_email = email;
		m_password = password;
		ServerManager.Instance.SignUpByEmail (usrName, email, password, SignUpCallback);
    }

	public void ClearPreviousData ()
	{
		MainMenuPanelManager.Instance.mainMenuPanel.notificationsPanel.ClearPreviousData ();
		MainMenuPanelManager.Instance.mainMenuPanel.profilePanel.ClearPreviousData ();
		MainMenuPanelManager.Instance.mainMenuPanel.m_lastNotification = 0;
		BadgesManager.Instance.ClearPreviousData ();
	}

	public void OnClickCategoryButton ()
	{
		if (SportsQuizManager.Instance.selectedMode == QuizManager.eQuizMode.IMFLucky) {

			QuizManager.Instance.LoadQuiz ();
			
		} else {

			#if EVENT_QUIZ
				GetCategory ();
			#else
			if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1) {

				GetRandomTopics ();
			} else {
				
				GetCategory ();
			}
			#endif
		}
	}

	public void GetCategory()
	{
		if (InternetConnection.Check)
			ServerManager.Instance.GetCategoryFromServer (GetCategoryCallback);
		else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

    void LoginCallback (bool action)
	{
		if (action == false)
			return;
		
		NativeBridgeHandler.GetCountryCode ();
		MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.MainMenuPanel);
		SportsQuizManager.Instance.SaveLogindata();
		
		MainMenuPanelManager.Instance.splashScreen.signInPanel.ClearFields ();

		Instance.GetUserInformation ();
	}

	IEnumerator HideLoadingIndicator()
	{
		yield return new WaitForSeconds (1f);
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	void SignUpCallback (bool action)
	{
		if (action) {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			MainMenuPanelManager.Instance.splashScreen.signUpHandler.ClearFields ();
			MainMenuPanelManager.Instance.splashScreen.signUpHandler.gameObject.SetActive (false);
			Instance.LoginEmail (m_email, m_password);
		}
	}

	bool CheckConnection ()
	{
		return InternetConnection.Check;
	}

	void GetRandomTopics ()
	{
		if (InternetConnection.Check)
			ServerManager.Instance.GetRandomTopics (GetCategoryCallback);
		else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
		}
	}

	void GetRandomTopicsCallback (bool action)
	{
		if (action == true)
		{
			GetCategory ();
		}
	}

	void GetCategoryCallback (bool action)
	{
		if (action == true)
		{
			if (MainMenuPanelManager.Instance.currentMainMenuState ==
			    MainMenuPanelManager.eMainMenuState.CategoryPanel) {
				MainMenuPanelManager.Instance.categoryPanel.OpenSubCategory ();			
			} else {
				MainMenuPanelManager.Instance.SwitchPanel (MainMenuPanelManager.eMainMenuState.CategoryPanel);
			}
		} else {
			Q.Utils.QDebug.Log ("LOAD QUIZ");
			QuizManager.Instance.LoadQuiz ();
		}

		Q.Utils.QDebug.Log ("GetCategoryCallback");
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	public void GetUserInformation ()
	{
		Q.Utils.QDebug.Log (MainMenuPanelManager.Instance.currentMainMenuState +":::"+MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel);
		if (InternetConnection.Check) {
			if (MainMenuPanelManager.Instance.currentMainMenuState == MainMenuPanelManager.eMainMenuState.MainMenuPanel) {
				if (MainMenuPanelManager.Instance.mainMenuPanel.currentSubPanel != MainMenuPanel.eSubMenuPanel.Settings) {
					ServerManager.Instance.GetUserInfoFromServer (GetUserInformationCallback);
				}
			}
		}
	}

	public void GetUserInformationCallback ()
	{
		Q.Utils.QDebug.LogRed ("GetUserInformationCallback");
		MainMenuPanelManager.Instance.WaitingPanel (false);
	}

	IEnumerator WaitForUserInformationCallback (float time)
	{
		yield return new WaitForSeconds (time);
		StopCoroutine ("WaitForUserInformationCallback");
		Instance.GetUserInformation ();
	}
}

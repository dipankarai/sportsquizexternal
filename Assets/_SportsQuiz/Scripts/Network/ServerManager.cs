﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class ServerManager : MonoBehaviour {

	private static ServerManager m_instance;
	public static ServerManager Instance { get { return m_instance; } }
	private System.Action<bool> m_callback;
	private string errorMsg;

	public enum eStatusUpdateType
	{
		Unknown = 0,
		OpponentAnswer = 1,
		ChallengeAccept = 2,
		ChallengeReject = 3
	}

	void Awake ()
	{
		if (m_instance == null)
			m_instance = this;
	}

	public static ServerManager Init (GameObject obj)
	{
		if (m_instance == null)
		{
			m_instance = obj.AddComponent<ServerManager> ();
		}

		return m_instance;
	}

	bool CheckInternetCOnnection ()
	{
		if (InternetConnection.Check) {
			return true;
		} else {
			MainMenuPanelManager.Instance.ShowMessage (TagConstant.PopUpMessages.NO_INTERNET_CONNECTION);
			return false;
		}
	}

	public void ConfigGet (System.Action<SeralizedClassServer.ConfigGetVersion> callback)
	{
		string url = ServerConstant.BASEURL + ServerConstant.CONFIG_GET;

		WWWForm wwwForm = new WWWForm ();

		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		Q.Utils.QDebug.Log ("URL Send---> " + url + wwwForm.ToString ());
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (ConfigGetCallback (www, callback));
	}

	IEnumerator ConfigGetCallback (WWW www, System.Action<SeralizedClassServer.ConfigGetVersion> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				SeralizedClassServer.ConfigGetVersion configVersion = JsonConvert.DeserializeObject<SeralizedClassServer.ConfigGetVersion> (GetResponseMessage (www.text));

				if (configVersion != null) {
					callback (configVersion);
				} else {
					callback (null);
				}

			} else {
				callback (null);
			}
		} else {
			PopUpErrorMessage (www.error);
		}
	}

	public void ResetPassword (string mailId, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		string url = ServerConstant.BASEURL + ServerConstant.PASSWORD_RESET;

		WWWForm wwwForm = new WWWForm ();

		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		wwwForm.AddField (ServerConstant.USER_EMAIL_SIGNUP, mailId);

		Q.Utils.QDebug.Log ("URL Send---> " + url + wwwForm.ToString ());
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (ResetPasswordServerCallback (www, callback));
	}

	IEnumerator ResetPasswordServerCallback (WWW www,System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				bool isValidMail = JsonConvert.DeserializeObject<bool> (GetResponseMessage (www.text));

				if (isValidMail) {
					callback (true);
				} else {
					callback (false);
				}
			} else {
				callback (false);
			}
		} else {
			PopUpErrorMessage (www.error);
		}
	}

	public void LogInToServer (System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		m_callback = callback;

		string url = ServerConstant.BASEURL + ServerConstant.USER_LOGIN;

		WWWForm wwwForm = new WWWForm ();

		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		wwwForm.AddField (ServerConstant.LOGIN_TYPE, (int)SportsQuizManager.Instance.loginType);

		if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.EmailUser) {

			wwwForm.AddField (ServerConstant.USER_EMAIL_LOGIN, SportsQuizManager.Instance.playerInformation.email);
			wwwForm.AddField (ServerConstant.USER_PASSWORD, SportsQuizManager.Instance.playerInformation.password);
		
		} else if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.FacebookUser) {

			wwwForm.AddField (ServerConstant.FACEBOOK_TOKEN, SocialHandler.Instance.facebook_Access_token);
			wwwForm.AddField (ServerConstant.LOGIN_NAME, SportsQuizManager.Instance.playerInformation.name);

		} else if (SportsQuizManager.Instance.loginType == SportsQuizManager.eUserType.GPlus) {

			wwwForm.AddField (ServerConstant.GOOGLE_ID, SocialHandler.Instance.google_User_ID);
			wwwForm.AddField (ServerConstant.LOGIN_NAME, SocialHandler.Instance.google_Username);
		}

		#if UNITY_EDITOR
		wwwForm.AddField (ServerConstant.DEVICE_ID, ServerConstant.EDITOR_DEVICEID);
		#elif UNITY_IOS || UNITY_IPHONE
		wwwForm.AddField (ServerConstant.DEVICE_ID, SystemInfo.deviceUniqueIdentifier);
		if (GamePrefs.HasKey(TagConstant.PrefsName.iOS_DEVICE_TOKEN) == true) {
			if (string.IsNullOrEmpty (GamePrefs.iOSDeviceToken) == false) {
		
			wwwForm.AddField (ServerConstant.iOS_PUSH_Notification, GamePrefs.iOSDeviceToken);
			}
		}
		#elif UNITY_ANDROID
		wwwForm.AddField (ServerConstant.DEVICE_ID, SystemInfo.deviceUniqueIdentifier);
		if (GamePrefs.HasKey(TagConstant.PrefsName.ANDROID_DEVICE_TOKEN) == true) {
			if (string.IsNullOrEmpty (GamePrefs.AndroidDeviceToken) == false) {
		
			wwwForm.AddField (ServerConstant.ANDROID_PUSH_Notification, GamePrefs.AndroidDeviceToken);
			}
		}
		#endif

		Q.Utils.QDebug.Log ("URL Send---> " + url + wwwForm.ToString ());
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (LoginToServerCallback (www));
	}

	IEnumerator LoginToServerCallback (WWW www)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				IDictionary responseMsg = (IDictionary)Facebook.MiniJSON.Json.Deserialize (GetResponseMessage (www.text));

				SportsQuizManager.Instance.playerInformation.serverID =
					int.Parse (responseMsg [ServerConstant.USER_ID].ToString ());
				SportsQuizManager.Instance.playerInformation.name = responseMsg [ServerConstant.LOGIN_NAME].ToString ();
				SportsQuizManager.Instance.playerInformation.access_token = responseMsg [ServerConstant.ACCESS_TOKEN].ToString ();
				
				m_callback (true);

			} else {
				
			}
		} else {
			PopUpErrorMessage (www.error);
		}
	}

	public void SignUpByEmail (string usrName, string email, string password, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		m_callback = callback;

		string url = ServerConstant.BASEURL + ServerConstant.USER_REGISTER;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		wwwForm.AddField (ServerConstant.LOGIN_NAME, usrName);
		wwwForm.AddField (ServerConstant.LOGIN_TYPE, (int) SportsQuizManager.eUserType.EmailUser);
		wwwForm.AddField (ServerConstant.USER_EMAIL_SIGNUP, email);
		wwwForm.AddField (ServerConstant.USER_PASSWORD, password);

		Q.Utils.QDebug.Log ("URL Send---> "+url + wwwForm.ToString());
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (SignUpByEmailCallback (www));
	}

	IEnumerator SignUpByEmailCallback (WWW www)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

		if (IsResponseCodeCorrect (www.text)) {
				
				IDictionary responseMsg = (IDictionary)Facebook.MiniJSON.Json.Deserialize (GetResponseMessage (www.text));
				SportsQuizManager.Instance.playerInformation.serverID
				= int.Parse (string.Format ("{0}", responseMsg [ServerConstant.USER_ID]));
				SportsQuizManager.Instance.loginType = SportsQuizManager.eUserType.EmailUser;

				m_callback (true);

			} else {
				
			}
		} else {
			//PopUpErrorMessage (www.error);
		}
	}

	public void GetUserInfoFromServer(System.Action callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		Q.Utils.QDebug.LogRed ("GetUserInfoFromServer");

		string url = ServerConstant.BASEURL + ServerConstant.USER_GET;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);
		
		StartCoroutine (GetUserInfoFromServerCallback (www, callback));
	}
	
	IEnumerator GetUserInfoFromServerCallback (WWW www, System.Action callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);
			
			if (IsResponseCodeCorrect (www.text)) {
				
				IDictionary _user_info = (IDictionary) Facebook.MiniJSON.Json.Deserialize (GetResponseMessage (www.text));

				SeralizedClassServer.UserDetails userInfo = new SeralizedClassServer.UserDetails ();
				userInfo = JsonConvert.DeserializeObject<SeralizedClassServer.UserDetails> (JsonConvert.SerializeObject (_user_info ["user_info"]));	
				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (userInfo));
				
				if (userInfo != null) {
					//SportsQuizManager.Instance.AssignUserInfo(userInfo);
					StartCoroutine (LoadAllBadgeImage (userInfo, callback));
				} 
				else {
					//m_callback (false);
				}
			}
		}
		else
		{
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

	IEnumerator LoadAllBadgeImage (SeralizedClassServer.UserDetails userInfo, System.Action callback)
	{
		if (userInfo.user_badge.Count > 0)
		{
			int badgeCount = 0;
			for (int i = 0; i < userInfo.user_badge.Count; i++)
			{
				if (userInfo.user_badge[i].notified_to_user == 2) {
					BadgesManager.Instance.AddNewBadges (userInfo.user_badge [i]);
				}

				SportsQuizManager.Badges badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(userInfo.user_badge[i].badge_id);
				//				badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(userInfo.user_badge[i].badge_id);
				if (badge == null) {

					badge = new SportsQuizManager.Badges();
					badge.description = userInfo.user_badge[i].description;
					badge.title = userInfo.user_badge[i].title;
					badge.id = userInfo.user_badge[i].badge_id;
					badge.url = userInfo.user_badge[i].image;

					SportsQuizManager.Instance.badgesList.Add(badge);
				}

				/*if (References.GetImageFromLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge[i].image, References.eImageType.Badges) == null) {
					if (badgeCount < 4) {
						Q.Utils.QDebug.LogGreen (userInfo.user_badge [i].badge_id + " BADGE "+Time.time);
						if (string.IsNullOrEmpty (userInfo.user_badge [i].image) == false) {
							#if !MEMORY_CHECK
							if (badge.badge == null) {
							#endif
								WWW image = new WWW (userInfo.user_badge [i].image);
								yield return image;
								if (image.error == null) {
									References.SaveImageToLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge[i].image, image.texture, References.eImageType.Badges);
									#if !MEMORY_CHECK
									badge.badge = image.texture;
									#endif
								} else {
									Q.Utils.QDebug.Log (image.error);
								}
								#if !MEMORY_CHECK
							}
								#endif
						}
						badgeCount++;
						Q.Utils.QDebug.LogCyan (userInfo.user_badge [i].badge_id + " BADGE "+Time.time);
					}
				} else {
					#if !MEMORY_CHECK
					if (badge.badge == null) {
						badge.badge = References.GetImageFromLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge[i].image, References.eImageType.Badges);
					}
					#endif
				}*/
				yield return null;
			}
		}

		SportsQuizManager.Instance.AssignUserInfo(userInfo);
		callback ();
		NotificationHandler.Instance.isFirstTimeLoad = false;
	}

	public void GetOpponentInfoFromServer(System.Action callback, int searchUserID)
	{
		if (CheckInternetCOnnection () == false)
			return;

		string url = ServerConstant.BASEURL + ServerConstant.USER_GET;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		if (searchUserID != 0) {
			wwwForm.AddField (ServerConstant.OPPONENT_ID, searchUserID);
		}

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetOpponentInfoFromServerCallback (www, callback, searchUserID));
	}

	IEnumerator GetOpponentInfoFromServerCallback (WWW www, System.Action callback,int searchUserID)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {

				IDictionary _user_info = (IDictionary) MiniJSON.Json.Deserialize (GetResponseMessage (www.text));
				IDictionary _userInfoDictionary = (IDictionary) _user_info["user_info"];

				SeralizedClassServer.UserDetails userInfo = new SeralizedClassServer.UserDetails ();

				userInfo.age = int.Parse (_userInfoDictionary ["age"].ToString ());
				userInfo.country = int.Parse (_userInfoDictionary ["country"].ToString ());
				userInfo.gender = int.Parse (_userInfoDictionary ["gender"].ToString ());
				userInfo.avatar_type = int.Parse (_userInfoDictionary ["avatar_type"].ToString ());
				userInfo.user_id = int.Parse (_userInfoDictionary ["user_id"].ToString ());
				userInfo.name = (string) _userInfoDictionary ["name"].ToString();
				userInfo.email_id = (string) _userInfoDictionary ["email_id"].ToString();
				userInfo.facebook_id = (string) _userInfoDictionary ["facebook_id"].ToString();
				userInfo.google_id = (string) _userInfoDictionary ["google_id"].ToString();
				userInfo.total_win = int.Parse (_userInfoDictionary ["total_win"].ToString ());
				userInfo.total_lost = int.Parse (_userInfoDictionary ["total_lost"].ToString ());
				userInfo.total_draw = int.Parse (_userInfoDictionary ["total_draw"].ToString ());

				IList category_preference_List = (IList)_userInfoDictionary ["category_preference"];
				userInfo.category_preference = new List<int> ();
				for (int i=0; i<category_preference_List.Count; i++) {
					int preference = int.Parse (category_preference_List [i].ToString ());
					userInfo.category_preference.Add (preference);
				}

				IList score_statistics_List = (IList)_userInfoDictionary ["score_statistics"];
				userInfo.score_statistics = new List<SeralizedClassServer.Score_Statistics> ();
				for (int i=0; i <score_statistics_List.Count; i++) {
					SeralizedClassServer.Score_Statistics _stats = new SeralizedClassServer.Score_Statistics ();

					IDictionary _statsDictionary = (IDictionary)score_statistics_List [i];

					_stats.category_id = int.Parse (_statsDictionary ["category_id"].ToString ());
					_stats.category_title = _statsDictionary ["category_title"].ToString ();
					_stats.score = new List<SeralizedClassServer.Score> ();

					IList _score_List = (IList)_statsDictionary ["score"];
					for (int j=0; j<_score_List.Count; j++) {
						SeralizedClassServer.Score _score = new SeralizedClassServer.Score ();

						IDictionary _scoreDictionary = (IDictionary)_score_List [j];
						_score.score = int.Parse (_scoreDictionary ["score"].ToString ());
						_score.type = int.Parse (_scoreDictionary ["type"].ToString ());

						_stats.score.Add (_score);
					}

					userInfo.score_statistics.Add (_stats);
				}

				IList user_badge_List = (IList)_userInfoDictionary ["user_badge"];
				userInfo.user_badge = new List<SeralizedClassServer.UserBadges> ();
				for (int i=0; i<user_badge_List.Count; i++) {
					SeralizedClassServer.UserBadges _badge = new SeralizedClassServer.UserBadges ();
					IDictionary _badgeDictionary = (IDictionary)user_badge_List [i];
					_badge.badge_id = int.Parse (_badgeDictionary ["badge_id"].ToString ());
					_badge.notified_to_user = int.Parse (_badgeDictionary ["notified_to_user"].ToString ());
					_badge.title = _badgeDictionary ["title"].ToString ();
					_badge.image = _badgeDictionary ["image"].ToString ();
					_badge.description = _badgeDictionary ["description"].ToString ();

					userInfo.user_badge.Add (_badge);
				}

//				userInfo = JsonConvert.DeserializeObject<SeralizedClassServer.UserDetails> (JsonConvert.SerializeObject (_user_info ["user_info"]));	
				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (userInfo));

				if (userInfo != null) {
					StartCoroutine (LoadAllOpponentBadgeImage (userInfo, callback));
				} 
				else {
					//m_callback (false);
				}
			}
		}
		else
		{
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

	IEnumerator LoadAllOpponentBadgeImage (SeralizedClassServer.UserDetails userInfo,System.Action callback)
	{
		if (userInfo.user_badge.Count > 0)
		{
			int badgeCount = 0;
			for (int i = 0; i < userInfo.user_badge.Count; i++)
			{
				SportsQuizManager.Badges badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(userInfo.user_badge[i].badge_id);
				//				badge = SportsQuizManager.Instance.CheckBadgeAlreadyExist(userInfo.user_badge[i].badge_id);
				if (badge == null) {

					badge = new SportsQuizManager.Badges();
					badge.description = userInfo.user_badge[i].description;
					badge.title = userInfo.user_badge[i].title;
					badge.id = userInfo.user_badge[i].badge_id;
					badge.url = userInfo.user_badge[i].image;

					SportsQuizManager.Instance.badgesList.Add(badge);
				}
				/*if (References.GetImageFromLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge[i].image, References.eImageType.Badges) == null) {
					if (badgeCount < 4) {
						if (string.IsNullOrEmpty (userInfo.user_badge [i].image) == false) {
							#if !MEMORY_CHECK
						if (badge.badge == null) {
							#endif
							WWW image = new WWW (userInfo.user_badge [i].image);
							yield return image;
							if (image.error == null) {
								References.SaveImageToLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge [i].image, image.texture, References.eImageType.Badges);
								#if !MEMORY_CHECK	
								badge.badge = image.texture;
								#endif
							} else {
								Q.Utils.QDebug.Log (image.error);
							}
							#if !MEMORY_CHECK
						}
							#endif
						}
						badgeCount++;
					}
				} else {
					#if !MEMORY_CHECK
					if (badge.badge == null) {
						badge.badge = References.GetImageFromLocal (userInfo.user_badge [i].badge_id, userInfo.user_badge[i].image, References.eImageType.Badges);
					}
					#endif
				}*/
				yield return null;
			}
		}

		SportsQuizManager.Instance.AssignSearchUserInfo(userInfo);
		callback ();
	}

	public void GetRankInfoFromServer(System.Action<SeralizedClassServer.UserRankDetails> callback, int opponentId = 0)
	{
		if (CheckInternetCOnnection () == false) {
			callback (null);
			return;
		}

		string url = ServerConstant.BASEURL + ServerConstant.USER_RANK_TITLE;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		wwwForm.AddField (ServerConstant.OPPONENT_ID, opponentId);

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetRankInfoFromServerCallback (www, callback));
	}

	IEnumerator GetRankInfoFromServerCallback (WWW www, System.Action<SeralizedClassServer.UserRankDetails> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				
				SeralizedClassServer.UserRankDetails userInfo = new SeralizedClassServer.UserRankDetails(); 
				userInfo = JsonConvert.DeserializeObject<SeralizedClassServer.UserRankDetails> (GetResponseMessage(www.text));	
				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (userInfo));

				if (userInfo != null) {
					callback (userInfo);
				} 
				else {
					callback (null);
				}
			}
		}
		else
		{
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

    public void GetOpponentDetail (int opponentID, System.Action<SeralizedClassServer.OpponentDetails> callback)
    {
		if (CheckInternetCOnnection () == false)
			return;
		
        string url = ServerConstant.BASEURL + ServerConstant.USER_GET;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
#if TEST_BUILD
        wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
        wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
#endif
        wwwForm.AddField(ServerConstant.OPPONENT_ID, opponentID);

        Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetOpponentDetailCallback(www, callback));
    }

    IEnumerator GetOpponentDetailCallback(WWW www, System.Action<SeralizedClassServer.OpponentDetails> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Q.Utils.QDebug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {
                SeralizedClassServer.OpponentDetails userInfo = new SeralizedClassServer.OpponentDetails();
                userInfo = JsonConvert.DeserializeObject<SeralizedClassServer.OpponentDetails>
                    (GetResponseMessage(www.text));
                Q.Utils.QDebug.Log(JsonConvert.SerializeObject(userInfo));

                if (userInfo != null)
                {
                    SportsQuizManager.Instance.AddOpponentInList(userInfo);
                    callback(userInfo);
                }
                else {
                    callback(null);
                }
            }
        }
        else
        {
            MainMenuPanelManager.Instance.WaitingPanel(false);
            PopUpErrorMessage(www.error);
        }
    }

    public void UpdateUserInfoToServer (System.Action<SeralizedClassServer.UserDetails> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.USER_UPDATE;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		
		UserInfoUpdateFields(wwwForm);
		
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(UpdateUserInfoInServerCallBack(www, callback));
	}
	
	void UserInfoUpdateFields(WWWForm wwwForm)
	{
		wwwForm.AddField(ServerConstant.LOGIN_NAME, SportsQuizManager.Instance.playerInformation.name);
		wwwForm.AddField(ServerConstant.USER_COUNTRY, SportsQuizManager.Instance.playerInformation.country);
		wwwForm.AddField(ServerConstant.USER_GENDER, (int) SportsQuizManager.Instance.playerInformation.gender);
		wwwForm.AddField(ServerConstant.USER_AVATAR_TYPE, SportsQuizManager.Instance.playerInformation.avatarType);
		wwwForm.AddField(ServerConstant.USER_AGE, SportsQuizManager.Instance.playerInformation.age);

		#if UNITY_EDITOR
		#elif UNITY_IOS || UNITY_IPHONE
		if (GamePrefs.HasKey(TagConstant.PrefsName.iOS_DEVICE_TOKEN) == true) {
		if (string.IsNullOrEmpty (GamePrefs.iOSDeviceToken) == false) {
		
		wwwForm.AddField (ServerConstant.iOS_PUSH_Notification, GamePrefs.iOSDeviceToken);
		}
		}
		#elif UNITY_ANDROID
		if (GamePrefs.HasKey(TagConstant.PrefsName.ANDROID_DEVICE_TOKEN) == true) {
			if (string.IsNullOrEmpty (GamePrefs.AndroidDeviceToken) == false) {

			wwwForm.AddField (ServerConstant.ANDROID_PUSH_Notification, GamePrefs.AndroidDeviceToken);
			}
		}
		#endif
	}
	
	IEnumerator UpdateUserInfoInServerCallBack(WWW www, System.Action<SeralizedClassServer.UserDetails> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);
			
			if (IsResponseCodeCorrect (www.text)) {
                //				callback (JsonConvert.DeserializeObject<SeralizedClassServer.UserDetails>
                //					(GetResponseMessage (www.text)));
            }
            else {
				if (callback != null)
					callback (null);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void UpdateUserCategoryPrefsToServer (string category, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.USER_CATEGORY_PREFERENCE;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		wwwForm.AddField(ServerConstant.CATEGORY_ID, category);

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);

		StartCoroutine(UpdateUserCategoryPrefsCallBack(www, callback));
	}

	IEnumerator UpdateUserCategoryPrefsCallBack(WWW www, System.Action<bool> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (true);
			}
			else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void GetCategoryFromServer (System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		m_callback = callback;

		string url = ServerConstant.BASEURL + ServerConstant.GET_CATEGORY;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		int category = 0;
		if (SportsQuizManager.Instance.currentSelectedCategory.Count > 0)
			category = SportsQuizManager.Instance.currentSelectedCategory [SportsQuizManager.Instance.currentSelectedCategory.Count - 1].category_id;

		wwwForm.AddField (ServerConstant.PARENT_CATEGORY_ID, category);

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetCategoryFromServerCallback (www));
	}

	IEnumerator GetCategoryFromServerCallback (WWW www)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {

				SeralizedClassServer.CategoryDictionary newCategory = new SeralizedClassServer.CategoryDictionary ();
				newCategory = JsonConvert.DeserializeObject<SeralizedClassServer.CategoryDictionary> (GetResponseMessage (www.text));
				
				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (newCategory));
				
				if (newCategory != null) {

					SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Add (newCategory);

					if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1)
					{
						//LOAD IMAGES
						StartCoroutine ("LoadCategoryLogo");
					}
					else {
						m_callback (true);
					}
				} else {
					m_callback (false);
				}
			}
		} else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

	IEnumerator LoadCategoryLogo ()
	{
		for (int i = 0; i < SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList.Count; i++) {
			if (CheckCategoryLogoAlreadyExits (
				    SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id, 
				    SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image) == false) {

				if (References.GetImageFromLocal (
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
					References.eImageType.Category) == null) {
					
					WWW image = new WWW (SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image);
					yield return image;
					
					if (image.error == null) {
						SportsQuizManager.LogoTexture _logoTexture = new SportsQuizManager.LogoTexture ();
						_logoTexture.id = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id;
						#if !MEMORY_CHECK
						_logoTexture.logo = image.texture;
						#endif
						_logoTexture.url = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image;
						
						References.SaveImageToLocal (
							SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
							SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
							image.texture,
							References.eImageType.Category);
						
						SportsQuizManager.Instance.currentCategory.categoryLogoList.Add (_logoTexture);
					} else {
						Q.Utils.QDebug.Log (image.error);
					}
				} else {
					SportsQuizManager.LogoTexture _logoTexture = new SportsQuizManager.LogoTexture ();
					_logoTexture.id = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id;
					#if !MEMORY_CHECK
					_logoTexture.logo = References.GetImageFromLocal (
						SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
						SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
						References.eImageType.Category);
					#endif
					_logoTexture.url = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image;
					
					SportsQuizManager.Instance.currentCategory.categoryLogoList.Add (_logoTexture);
				}
			}
		}
		Q.Utils.QDebug.Log ("LoadCategoryLogo Done");
		m_callback (true);
	}

    public void GetMainCategoryOnly(System.Action<SeralizedClassServer.CategoryDictionary> callback)
    {
		if (CheckInternetCOnnection () == false)
			return;

        string url = ServerConstant.BASEURL + ServerConstant.GET_CATEGORY;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
#if TEST_BUILD
        wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
        wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
#endif
        wwwForm.AddField(ServerConstant.PARENT_CATEGORY_ID, 0);

        Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetMainCategoryOnlyCallback(www, callback));
    }

    IEnumerator GetMainCategoryOnlyCallback(WWW www, System.Action<SeralizedClassServer.CategoryDictionary> callback)
    {
        yield return www;
        if (www.error == null)
        {
            Q.Utils.QDebug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {
                SeralizedClassServer.CategoryDictionary newCategory = new SeralizedClassServer.CategoryDictionary();
                newCategory = JsonConvert.DeserializeObject<SeralizedClassServer.CategoryDictionary>(GetResponseMessage(www.text));

                Q.Utils.QDebug.Log(JsonConvert.SerializeObject(newCategory));

                if (newCategory != null)
                {
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Add (newCategory);
					if (SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Count == 1)
					{
						//LOAD IMAGES
						callback (SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0]);
						//StartCoroutine ("LoadCategoryLogoWithParameter", callback);
					}
					else {
						callback (null);
					}
                    //callback(newCategory);                   
                }
                else {
                    callback(null);
                }
            }
        }
        else {
            MainMenuPanelManager.Instance.WaitingPanel(false);
            PopUpErrorMessage(www.error);
        }
    }

	IEnumerator LoadCategoryLogoWithParameter (System.Action<SeralizedClassServer.CategoryDictionary> callback)
	{
		for (int i=0; i<SportsQuizManager.Instance.currentCategory.categoryDictionaryList[0].categoryList.Count; i++) {
			if (CheckCategoryLogoAlreadyExits (
				SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id, 
				SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image) == false) {

				if (References.GetImageFromLocal (
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
					References.eImageType.Category) == null) {
					
					WWW image = new WWW (SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image);
					yield return image;
					
					if (image.error == null) {
						
						SportsQuizManager.LogoTexture _logoTexture = new SportsQuizManager.LogoTexture ();
						_logoTexture.id = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id;
						#if !MEMORY_CHECK
						_logoTexture.logo = image.texture;
						#endif
						_logoTexture.url = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image;
						
						References.SaveImageToLocal (
							SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
							SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
							image.texture,
							References.eImageType.Category);
						
						SportsQuizManager.Instance.currentCategory.categoryLogoList.Add (_logoTexture);
					} else {
						Q.Utils.QDebug.Log (image.error);
					}
				} else {
					SportsQuizManager.LogoTexture _logoTexture = new SportsQuizManager.LogoTexture ();
					_logoTexture.id = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id;
					#if !MEMORY_CHECK
					_logoTexture.logo = References.GetImageFromLocal (
						SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].category_id,
						SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image,
						References.eImageType.Category);
					#endif
					_logoTexture.url = SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0].categoryList [i].image;
					
					SportsQuizManager.Instance.currentCategory.categoryLogoList.Add (_logoTexture);
				}
			}
		}
		Q.Utils.QDebug.Log ("LoadCategoryLogoWithParameter Done");
		callback (SportsQuizManager.Instance.currentCategory.categoryDictionaryList [0]);
	}

	bool CheckCategoryLogoAlreadyExits (int id, string url)
	{
		for (int i = 0; i < SportsQuizManager.Instance.currentCategory.categoryLogoList.Count; i++) {
			if (SportsQuizManager.Instance.currentCategory.categoryLogoList [i].id == id) {
				if (SportsQuizManager.Instance.currentCategory.categoryLogoList [i].url == url) {

					return true;
				}	
			}
		}

		return false;
	}

	public void GetSponsoredCategory (System.Action<List<SeralizedClassServer.Category>> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		string url = ServerConstant.BASEURL + ServerConstant.GET_SPONSERED_CATEGORY;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetSponsoredCategoryCallback (www, callback));
	}

	IEnumerator GetSponsoredCategoryCallback (WWW www, System.Action<List<SeralizedClassServer.Category>> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {

				List<SeralizedClassServer.Category> newCategoryList = new List<SeralizedClassServer.Category> ();
				newCategoryList = JsonConvert.DeserializeObject<List<SeralizedClassServer.Category>> (GetResponseMessage (www.text));

				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (newCategoryList));

				if (newCategoryList != null) {
					callback (newCategoryList);
				} else {
					callback (null);
				}
			}
		} else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

	public void GetRandomTopics (System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		m_callback = callback;

		string url = ServerConstant.BASEURL + ServerConstant.GET_RANDOM_TOPICS;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		int category = 0;
		if (SportsQuizManager.Instance.currentSelectedCategory.Count > 0)
			category = SportsQuizManager.Instance.currentSelectedCategory [SportsQuizManager.Instance.currentSelectedCategory.Count - 1].category_id;
		wwwForm.AddField (ServerConstant.CATEGORY_ID, category);
		wwwForm.AddField (ServerConstant.RANDOM_TOPICS_LIMIT, SportsQuizManager.Instance.currentCategory.randomTopicLimit);
		
		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);
		
		StartCoroutine (GetRandomTopicsCallback (www));
	}

	IEnumerator GetRandomTopicsCallback (WWW www)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);
			
			if (IsResponseCodeCorrect (www.text)) {
				
				//SportsQuizManager.Instance.currentCategory.topicsList = new List<SeralizedClassServer.Topics> ();
				//SportsQuizManager.Instance.currentCategory.topicsList = JsonConvert.DeserializeObject<List<SeralizedClassServer.Topics>> (GetResponseMessage (www.text));
				
				SeralizedClassServer.CategoryDictionary newCategory = new SeralizedClassServer.CategoryDictionary ();
				newCategory.categoryList = JsonConvert.DeserializeObject<List<SeralizedClassServer.Category>> (GetResponseMessage (www.text));

				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (newCategory));
				
				if (newCategory.categoryList != null) {
					SportsQuizManager.Instance.currentCategory.categoryDictionaryList.Add (newCategory);
					m_callback (true);
				} else {
					m_callback (false);
				}
			}
		} else {
			MainMenuPanelManager.Instance.WaitingPanel (false);
			PopUpErrorMessage (www.error);
		}
	}

	public void GetLeisureMode (int category, System.Action<SeralizedClassServer.SingleQuizQuestionsList> callback = null)
    {
		if (CheckInternetCOnnection () == false)
			return;
		
        string url = ServerConstant.BASEURL + ServerConstant.GET_LEISURE;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

        wwwForm.AddField(ServerConstant.CATEGORY_ID, category);

		if (SportsQuizManager.Instance.previousQuizList.Count > 0) {
			string _previousQuiz = "";
			for (int i = 0; i <SportsQuizManager.Instance.previousQuizList.Count; i++) {
				_previousQuiz += SportsQuizManager.Instance.previousQuizList [i].ToString ();
				if (i > 0 && i < (SportsQuizManager.Instance.previousQuizList.Count - 1)) {
					_previousQuiz += ",";
				}
			}

			Q.Utils.QDebug.Log ("PREVIOUS QUIZ " + _previousQuiz);
			wwwForm.AddField (ServerConstant.EXCLUDE_QUIZ_ID, _previousQuiz);
		}

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonUtility.ToJson(wwwForm));
        WWW www = new WWW(url, wwwForm);

		StartCoroutine(GetLeisureModeCallback(www, callback));
    }

	IEnumerator GetLeisureModeCallback (WWW www, System.Action<SeralizedClassServer.SingleQuizQuestionsList> callback = null)
    {
        yield return www;
        if (www.error == null)
        {
            Q.Utils.QDebug.Log(www.text);
            
			if (IsResponseCodeCorrect (www.text)) {
				callback (JsonConvert.DeserializeObject<SeralizedClassServer.SingleQuizQuestionsList>
					(GetResponseMessage (www.text)));
			} else {
				if (callback == null)
					callback (null);
			}
        }
        else
        {
			PopUpErrorMessage (www.error);
        }
    }

	public void GetDuelMode(int category, SportsQuizManager.eUserType otherUserType, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
    {
		if (CheckInternetCOnnection () == false)
			return;
		
        string url = ServerConstant.BASEURL + ServerConstant.GET_DUEL;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
        wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
        wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		int _other_user_type = 0;
		if (otherUserType == SportsQuizManager.eUserType.EmailUser) {
			wwwForm.AddField (ServerConstant.OTHER_USER_TYPE, 1);
			wwwForm.AddField(ServerConstant.OTHER_USER_ID, (int)SportsQuizManager.Instance.selectedDuelFriendDetail.user_id);
		}
		else if (otherUserType == SportsQuizManager.eUserType.FacebookUser){
			wwwForm.AddField (ServerConstant.OTHER_USER_TYPE, 2);
			wwwForm.AddField(ServerConstant.OTHER_USER_ID, (string)SportsQuizManager.Instance.selectedDuelFriendDetail.user_id);
		}
		else if (otherUserType == SportsQuizManager.eUserType.GPlus) {
			wwwForm.AddField (ServerConstant.OTHER_USER_TYPE, 3);
			wwwForm.AddField(ServerConstant.OTHER_USER_ID, (string)SportsQuizManager.Instance.selectedDuelFriendDetail.user_id);
		}

		if (SportsQuizManager.Instance.previousQuizList.Count > 0) {
			string _previousQuiz = "";
			for (int i = 0; i <SportsQuizManager.Instance.previousQuizList.Count; i++) {
				_previousQuiz += SportsQuizManager.Instance.previousQuizList [i].ToString ();
				if (i > 0 && i < (SportsQuizManager.Instance.previousQuizList.Count - 1)) {
					_previousQuiz += ",";
				}
			}

			Q.Utils.QDebug.Log ("PREVIOUS QUIZ " + _previousQuiz);
			wwwForm.AddField (ServerConstant.EXCLUDE_QUIZ_ID, _previousQuiz);
		}

		wwwForm.AddField(ServerConstant.CATEGORY_ID, category);
		wwwForm.AddField (ServerConstant.PREVIOUS_USER_ID, SportsQuizManager.Instance.previousUserID);

        Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

		StartCoroutine(GetDuelModeCallback(www, callback));
    }

	int GetOtherUsertype (SportsQuizManager.eUserType otherUserType)
	{
		if (otherUserType == SportsQuizManager.eUserType.EmailUser)
			return 1;
		else if (otherUserType == SportsQuizManager.eUserType.FacebookUser)
			return 2;
		else if (otherUserType == SportsQuizManager.eUserType.GPlus)
			return 3;

		return 0;
	}

	public int errorResponseCode;
	IEnumerator GetDuelModeCallback(WWW www, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
    {
        yield return www;
        if (www.error == null)
        {
            Q.Utils.QDebug.Log(www.text);

			IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(www.text);
			int responseCodeInt = int.Parse(string.Format("{0}", response[ServerConstant.RESPONSE_CODE]));
			if (responseCodeInt == ServerConstant.WORKING_FINE) {
				
				callback (JsonConvert.DeserializeObject<SeralizedClassServer.DuelQuizQuestionsList>
					(GetResponseMessage (www.text)));
			}
			else
			{
				errorResponseCode = responseCodeInt;

				if (responseCodeInt == ServerConstant.USER_DONOT_EXISTS) {

					if (callback != null)
						callback (null);
					
				} else {
					MainMenuPanelManager.Instance.WaitingPanel (false);
					PopUpErrorMessage (response [ServerConstant.RESPONSE_INFO].ToString ());
				}
			}
			return false;

			if (IsResponseCodeCorrect (www.text)) {
				
			} else {
				
			}
        }
        else
        {
			PopUpErrorMessage (www.error);
        }
    }

	public void GetRandom (int quiz_id, int previousRandomUser, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.GET_IMFLUCKY;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField(ServerConstant.CATEGORY_ID, SportsQuizManager.Instance.currentCategory.mainCategorySelected);
		#if TEST_RANDOM_QUIZ
		int _user = 39;
		#else
		int _user = 0;
		#endif

		wwwForm.AddField(ServerConstant.OTHER_USER_ID, _user);
		wwwForm.AddField(ServerConstant.PREVIOUS_USER_ID, previousRandomUser);
		//wwwForm.AddField(ServerConstant.OTHER_USER_ID, SportsQuizManager.Instance.opponentInformation.serverID);

		if (SportsQuizManager.Instance.previousQuizList.Count > 0) {
			string _previousQuiz = "";
			for (int i = 0; i <SportsQuizManager.Instance.previousQuizList.Count; i++) {
				_previousQuiz += SportsQuizManager.Instance.previousQuizList [i].ToString ();
				if (i > 0 && i < (SportsQuizManager.Instance.previousQuizList.Count - 1)) {
					_previousQuiz += ",";
				}
			}

			Q.Utils.QDebug.Log ("PREVIOUS QUIZ " + _previousQuiz);
			wwwForm.AddField (ServerConstant.EXCLUDE_QUIZ_ID, _previousQuiz);
		}

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(GetRandomCallback(www, callback));
	}
	
	IEnumerator GetRandomCallback(WWW www, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(www.text);
			int responseCodeInt = int.Parse(string.Format("{0}", response[ServerConstant.RESPONSE_CODE]));

			if (responseCodeInt == ServerConstant.WORKING_FINE) {
				
				Q.Utils.QDebug.Log (GetResponseMessage (www.text));
				callback (JsonConvert.DeserializeObject<SeralizedClassServer.DuelQuizQuestionsList>
					(GetResponseMessage (www.text)));

			}
			else
			{
				if (responseCodeInt == ServerConstant.ALL_USER_OFFLINE) {
					if (callback != null)
						callback (null);
					
				} else {
					MainMenuPanelManager.Instance.WaitingPanel (false);
					PopUpErrorMessage (response [ServerConstant.RESPONSE_INFO].ToString ());
				}

				Q.Utils.QDebug.Log(response[ServerConstant.RESPONSE_INFO].ToString());

			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void GetChallenge (int quiz_id, int notification_id, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		string url = ServerConstant.BASEURL + ServerConstant.GET_CHALLENGE;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		
		wwwForm.AddField(ServerConstant.USER_QUIZ_ID, quiz_id);
		wwwForm.AddField(ServerConstant.NOTIFICATION_ID, notification_id);
		
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(GetChallengeCallback(www, callback));
	}
	
	IEnumerator GetChallengeCallback(WWW www, System.Action<SeralizedClassServer.DuelQuizQuestionsList> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);
			
			if (IsResponseCodeCorrect (www.text)) {
				callback (JsonConvert.DeserializeObject<SeralizedClassServer.DuelQuizQuestionsList>
					(GetResponseMessage (www.text)));
			} else {
				if (callback != null)
					callback (null);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

//	public void SubmitAnswerToServer (int quiz_id, int totalScore, List<SeralizedClassServer.AnswerSubmit> answerlist, System.Action<SeralizedClassServer.ScoreCallback> callback)
	public void SubmitAnswerToServer (int quiz_id, int totalScore, string answerlist, System.Action<SeralizedClassServer.ScoreCallback> callback)
	{
		if (InternetConnection.Check == false) {
			return;
		}
		
		string url = ServerConstant.BASEURL + ServerConstant.SAVE_ANSWER;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField(ServerConstant.USER_QUIZ_ID, quiz_id);
		wwwForm.AddField (ServerConstant.ANSWER, JsonConvert.SerializeObject (answerlist));
		wwwForm.AddField (ServerConstant.SUBMIT_SCORE, totalScore);

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject (answerlist));
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(SubmitAnswerToServerCallback(www, callback));
	}

	IEnumerator SubmitAnswerToServerCallback(WWW www, System.Action<SeralizedClassServer.ScoreCallback> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (JsonConvert.DeserializeObject<SeralizedClassServer.ScoreCallback>
					(GetResponseMessage(www.text)));
			} else {
				if (callback != null)
					callback (null);
			}
		}
		else
		{
			if (callback != null)
				callback (null);
			PopUpErrorMessage (www.error);
		}
	}

	public void GetStatus (int lastStatus, int quizId, System.Action<SeralizedClassServer.LastStatus> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.GET_STATUS;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		
		wwwForm.AddField(ServerConstant.USER_QUIZ_ID, quizId);
		wwwForm.AddField(ServerConstant.LAST_STATUS_ID, lastStatus);
				
		Q.Utils.QDebug.Log ("URL Send---> " + url);
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(GetStatusCallback(www, callback));
	}

	IEnumerator GetStatusCallback(WWW www, System.Action<SeralizedClassServer.LastStatus> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {

				IDictionary responseDictionary = (IDictionary)MiniJSON.Json.Deserialize (www.text);
				IDictionary statusDictionary = (IDictionary) responseDictionary[ServerConstant.RESPONSE_MSG];
				IList statusList = (IList)statusDictionary ["status"];

				if (statusList.Count > 0) {
					SeralizedClassServer.LastStatus _lastStatus = 
						JsonConvert.DeserializeObject<SeralizedClassServer.LastStatus> (GetResponseMessage (www.text));
					if (_lastStatus != null)
					{
						callback (_lastStatus);
					}
					else
					{
						callback (null);
					}
				} else {
					callback (null);
				}

				
			} else {
				callback (null);
			}
		}
		else
		{
			callback (null);
			PopUpErrorMessage (www.error);
		}
	}

	public void GetStatusIDictionary (int lastStatus, int quizId, System.Action<IDictionary> callback)
	{
		if (InternetConnection.Check == false) {
			callback (null);
			return;
		}

		string url = ServerConstant.BASEURL + ServerConstant.GET_STATUS;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField(ServerConstant.USER_QUIZ_ID, quizId);
		wwwForm.AddField(ServerConstant.LAST_STATUS_ID, lastStatus);

		Q.Utils.QDebug.Log ("URL Send---> " + url + "QUIZID " + quizId + " LASTSTATUS " + lastStatus);
		WWW www = new WWW(url, wwwForm);

		StartCoroutine(GetStatusIDictionaryCallback(www, callback));
	}

	IEnumerator GetStatusIDictionaryCallback(WWW www, System.Action<IDictionary> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {

				IDictionary responseDictionary = (IDictionary)MiniJSON.Json.Deserialize (www.text);
				IDictionary statusDictionary = (IDictionary) responseDictionary[ServerConstant.RESPONSE_MSG];
				IList statusList = (IList)statusDictionary ["status"];
				Q.Utils.QDebug.Log (MiniJSON.Json.Serialize (statusDictionary));
				if (statusList.Count > 0) {
					callback (statusDictionary);
				} else {
					callback (null);
				}


			} else {
				callback (null);
			}
		}
		else
		{
			callback (null);
			PopUpErrorMessage (www.error);
		}
	}

//	void CheckStatusData (string response)
//	{
//		IDictionary dict = (IDictionary) Facebook.MiniJSON.Json.Deserialize (response);
//		IList _list = (IList)dict ["status"];
//		for (int i = 0; i < _list.Count; i++)
//		{
//			IDictionary _childDict = (IDictionary)_list [i];
//
//			string _data = _childDict ["data"].ToString ();
//			Q.Utils.QDebug.Log (_data);
//			if (_data == "")
//			{
//				_data = "{}";
//			}
//		}
//	}

	public void UpdateStatus (int quizId, List<SeralizedClassServer.StatusData> data, System.Action<object> callback)
	{
		if (InternetConnection.Check == false) {
			return;
		}
		
		string url = ServerConstant.BASEURL + ServerConstant.UPDATE_STATUS;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		
		wwwForm.AddField(ServerConstant.USER_QUIZ_ID, quizId);
		wwwForm.AddField(ServerConstant.UPDATE_TYPE, (int) eStatusUpdateType.OpponentAnswer);
		wwwForm.AddField (ServerConstant.STATUS_DATA, JsonConvert.SerializeObject (data));

		Q.Utils.QDebug.Log("URL Send---> " + url + data);
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(UpdateStatusCallback(www, callback));
	}

	IEnumerator UpdateStatusCallback(WWW www, System.Action<object> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);
			
			if (IsResponseCodeCorrect (www.text)) {

				IDictionary dict = (IDictionary)Facebook.MiniJSON.Json.Deserialize (GetResponseMessage (www.text));
				string status = dict ["statusId"].ToString ();

				callback ((object)status);
				
			} else {
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void GetFollowers (System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.GET_FOLLOWER_LIST;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(GetFollowersCallback(www, callback));
	}

	IEnumerator GetFollowersCallback(WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);
			
			if (IsResponseCodeCorrect (www.text)) {
				
				SportsQuizManager.Instance.playerInformation.followerList = 
					JsonConvert.DeserializeObject<List<SeralizedClassServer.FollowerDetails>> (GetResponseMessage (www.text));
				callback (true);
			} else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void GetFollowingList (System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false) {
			callback (false);
			return;
		}
		
		string url = ServerConstant.BASEURL + ServerConstant.GET_FOLLOWING_LIST;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);

		StartCoroutine(GetFollowingCallback(www, callback));
	}

	IEnumerator GetFollowingCallback(WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				SportsQuizManager.Instance.playerInformation.followingList = 
					JsonConvert.DeserializeObject<List<SeralizedClassServer.FollowingDetails>> (GetResponseMessage (www.text));
				callback (true);
			} else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void FollowUser (int followID,System.Action<int> callback = null)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.FOLLOW_USER;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		
		wwwForm.AddField (ServerConstant.FOLLOW_USER_ID, followID);
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);
		
		StartCoroutine(FollowUserCallback(www, callback));
	}
	
	IEnumerator FollowUserCallback(WWW www, System.Action<int> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {				
				IDictionary dict = (IDictionary)Facebook.MiniJSON.Json.Deserialize (GetResponseMessage (www.text));
				int statusId = int.Parse (dict ["user_follow_id"].ToString ());
				Q.Utils.QDebug.Log ("statusId : " + statusId);

				if (callback != null && statusId != 0) {
					callback (statusId);
				} 
			} else {
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}


	public void UnFollowUser (int unfollowID,System.Action<bool> callback = null)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.UNFOLLOW_USER;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.UNFOLLOW_USER_ID, unfollowID);
		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);

		StartCoroutine(UnFollowUserCallback(www, callback));
	}

	IEnumerator UnFollowUserCallback(WWW www, System.Action<bool> callback = null)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {				
				callback (true);
			} else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void SearchFollow (string categorylist, System.Action<SeralizedClassServer.FollowSearchUserInfo> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.SEARCH_FOLLOW;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.CATEGORY_ID, categorylist);

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (SearchFollowCallback (www, callback));
	}

	IEnumerator SearchFollowCallback(WWW www, System.Action<SeralizedClassServer.FollowSearchUserInfo> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				IDictionary _user_info = (IDictionary) MiniJSON.Json.Deserialize (GetResponseMessage (www.text));
				IDictionary _resultDicitonary = (IDictionary) _user_info ["result"];
				if (_resultDicitonary.Count > 0) {
					
					SeralizedClassServer.FollowSearchUserInfo userInfo = new SeralizedClassServer.FollowSearchUserInfo ();
					userInfo.age = int.Parse (_resultDicitonary ["age"].ToString ());
					userInfo.avatar_type = int.Parse (_resultDicitonary ["avatar_type"].ToString ());
					userInfo.country = int.Parse (_resultDicitonary ["country"].ToString ());
					userInfo.gender = int.Parse (_resultDicitonary ["gender"].ToString ());
					userInfo.user_id = int.Parse (_resultDicitonary ["user_id"].ToString ());
					userInfo.name = (string) _resultDicitonary ["name"].ToString();
					userInfo.email_id = (string) _resultDicitonary ["email_id"].ToString();
					userInfo.facebook_id = (string) _resultDicitonary ["facebook_id"].ToString();

					Q.Utils.QDebug.Log (JsonConvert.SerializeObject (userInfo));
					
					if (userInfo != null) {
						callback (userInfo);
					} 
				} else {

					Q.Utils.QDebug.Log ("NO SEARCH LIST");
					callback (null);
				}

//				userInfo = JsonConvert.DeserializeObject<SeralizedClassServer.FollowSearchUserInfo> (JsonConvert.SerializeObject (_user_info ["result"]));	

			} else {
				callback (null);
				Q.Utils.QDebug.LogError ("Couldn't Search");
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void GetChallengeList ()
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.GET_CHALLENGE_LIST;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		Q.Utils.QDebug.Log ("URL Send---> " + url);
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetChallengeListCallback (www));
	}

	IEnumerator GetChallengeListCallback(WWW www)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) 
			{
				SportsQuizManager.Instance.challengeList = new List<SeralizedClassServer.GetChallengeList> ();
				SportsQuizManager.Instance.challengeList = 
					JsonConvert.DeserializeObject<List<SeralizedClassServer.GetChallengeList>> (GetResponseMessage (www.text));

			} else {
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void RejectChallenge (int quizID, int notID, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.CHALLENGE_REJECT;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.USER_QUIZ_ID, quizID);
		wwwForm.AddField (ServerConstant.NOTIFICATION_ID, notID);

		Q.Utils.QDebug.Log ("URL Send---> " + url);
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (RejectChallengeCallback (www, callback));
	}

	IEnumerator RejectChallengeCallback(WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (true);
			} else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

	public void CancelChallenge (int quizID, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;

		string url = ServerConstant.BASEURL + ServerConstant.CHALLENGE_CANCEL;

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.USER_QUIZ_ID, quizID);

		Q.Utils.QDebug.Log ("URL Send---> " + url);
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (CancelChallengeCallback (www, callback));
	}

	IEnumerator CancelChallengeCallback(WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (true);
			} else {
				callback (false);
			}
		}
		else
		{
			PopUpErrorMessage (www.error);
		}
	}

    public void GetNotification ()
    {
		if (CheckInternetCOnnection () == false) {
			NotificationHandler.Instance.NotificationCallback (null);
			return;
		}
		
        string url = ServerConstant.BASEURL + ServerConstant.GET_NOTIFICATION;
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
#else
        wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
        wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
#endif

        wwwForm.AddField(ServerConstant.LAST_NOTIFICATION, 0);
        wwwForm.AddField(ServerConstant.NOTIFICATION_LIMIT, 10);

        Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
        WWW www = new WWW(url, wwwForm);

        StartCoroutine(GetNotificationCallback(www));
    }

	private int m_lastNotificationID = 0;
    IEnumerator GetNotificationCallback(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            Q.Utils.QDebug.Log(www.text);

            if (IsResponseCodeCorrect(www.text))
            {
				IDictionary checkDictionary = (IDictionary) MiniJSON.Json.Deserialize (www.text);
				IDictionary responseMsgDictionary = (IDictionary)checkDictionary ["responseMsg"];
				int lastNotificationID = int.Parse (responseMsgDictionary ["last_notification_id"].ToString ());

				if (lastNotificationID != m_lastNotificationID) {

					m_lastNotificationID = lastNotificationID;

					SeralizedClassServer.Notification newNotification = 
						JsonConvert.DeserializeObject<SeralizedClassServer.Notification>(GetResponseMessage(www.text));
					if (newNotification != null) {
						//SportsQuizManager.Instance.notification = newNotification;
						NotificationHandler.Instance.NotificationCallback (newNotification);
					} else {
						NotificationHandler.Instance.NotificationCallback (null);
					}
				} else {
					NotificationHandler.Instance.NotificationCallback (null);
				}
            }
            else {
				NotificationHandler.Instance.NotificationCallback (null);
            }
        }
        else
        {
            PopUpErrorMessage(www.error);
        }
    }

	public void ClearNotification (NotificationItem.eNotificationClearType type, Action<bool> callback, string notificationID = "")
	{
		string url = ServerConstant.BASEURL + ServerConstant.CLEAR_NOTIFICATION;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.UPDATE_TYPE, (int)type);
		if (type == NotificationItem.eNotificationClearType.Selected) {
			wwwForm.AddField (ServerConstant.NOTIFICATION_ID, notificationID);
		}

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);

		StartCoroutine (ClearNotificationCallback (www, callback));
	}

	IEnumerator ClearNotificationCallback(WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (true);
			} else {
				callback (false);
			}
		} else {
			PopUpErrorMessage (www.error);
		}
	}

	public void GetQuizResult (int quiz_ID, System.Action<SeralizedClassServer.QuizResult> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.QUIZ_RESULT;
		WWWForm wwwForm = new WWWForm();
		wwwForm.AddField(ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);

		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField(ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField(ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.USER_QUIZ_ID, quiz_ID);

		Q.Utils.QDebug.Log("URL Send---> " + url + JsonConvert.SerializeObject(wwwForm));
		WWW www = new WWW(url, wwwForm);

		StartCoroutine (GetOpponentResultCallback (www, callback));
	}

	IEnumerator GetOpponentResultCallback(WWW www, System.Action<SeralizedClassServer.QuizResult> callback)
	{
		yield return www;
		if (www.error == null)
		{
			Q.Utils.QDebug.Log(www.text);

			if (IsResponseCodeCorrect(www.text))
			{
				SeralizedClassServer.QuizResult _result = 
					JsonConvert.DeserializeObject<SeralizedClassServer.QuizResult> (GetResponseMessage (www.text));
				if (_result != null)
					callback (_result);
			}
			else {
				callback (null);
			}
		}
		else
		{
			PopUpErrorMessage(www.error);
		}
	}

	public void GetFrequentPlayedUserInfo(System.Action<List<SeralizedClassServer.FrequentlyPlayedPlayerInfo>>  callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.USER_FREQUENT_PLAYER;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (GetFrequentPlayedFromServerCallback (www, callback));
	}

	IEnumerator GetFrequentPlayedFromServerCallback (WWW www, System.Action<List<SeralizedClassServer.FrequentlyPlayedPlayerInfo>>  callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> userInfo = new List<SeralizedClassServer.FrequentlyPlayedPlayerInfo> ();
				userInfo = JsonConvert.DeserializeObject<List<SeralizedClassServer.FrequentlyPlayedPlayerInfo>> (GetResponseMessage (www.text));	
				Q.Utils.QDebug.Log (JsonConvert.SerializeObject (userInfo));

				if (userInfo != null) {
					callback (userInfo);
				} else {
				}
			} else {
				MainMenuPanelManager.Instance.WaitingPanel (false);
				PopUpErrorMessage (www.error);
			}
		}
	}

	public void SaveBadge (int badgeID, int notifiedTouser, System.Action<bool> callback)
	{
		if (CheckInternetCOnnection () == false)
			return;
		
		string url = ServerConstant.BASEURL + ServerConstant.SAVE_BADGE;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif

		wwwForm.AddField (ServerConstant.BADGE_ID, badgeID);
		wwwForm.AddField (ServerConstant.BADGE_NOTIFIED_USER, notifiedTouser);


		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (SaveBadgeCallback (www, callback));
	}

	IEnumerator SaveBadgeCallback (WWW www, System.Action<bool> callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
                callback(true);
            } else {
                callback(false);
				MainMenuPanelManager.Instance.WaitingPanel (false);
				PopUpErrorMessage (www.error);
			}
		}
	}

	public void FeedbackSave(string message, System.Action<bool> callback)
	{		
		string url = ServerConstant.BASEURL + ServerConstant.FEEDBACK_SAVE;
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (ServerConstant.APPLICATION_KEY, ServerConstant.APPLICATION_KEY_NO);
		#if TEST_BUILD
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.TEST_USER_ID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.TEST_ACCESS_TOKEN);
		#else
		wwwForm.AddField (ServerConstant.USER_ID_CATEGORY, SportsQuizManager.Instance.playerInformation.serverID);
		wwwForm.AddField (ServerConstant.ACCESS_TOKEN_QUESTION, SportsQuizManager.Instance.playerInformation.access_token);
		#endif
		wwwForm.AddField (ServerConstant.FEEDBACK_MESSAGE, message);

		Q.Utils.QDebug.Log ("URL Send---> " + url + JsonConvert.SerializeObject (wwwForm));
		WWW www = new WWW (url, wwwForm);

		StartCoroutine (FeedbackSaveCallbck (www, callback));
	}

	IEnumerator FeedbackSaveCallbck (WWW www, System.Action<bool>  callback)
	{
		yield return www;
		if (www.error == null) {
			Q.Utils.QDebug.Log (www.text);

			if (IsResponseCodeCorrect (www.text)) {
				callback (true);
			}
			else
			{
				MainMenuPanelManager.Instance.WaitingPanel (false);
				PopUpErrorMessage (errorMsg);
			}
		}
	}


    bool IsResponseCodeCorrect (string serverResponse)
    {
        IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(serverResponse);
        int responseCodeInt = int.Parse(string.Format("{0}", response[ServerConstant.RESPONSE_CODE]));
		if (responseCodeInt == ServerConstant.WORKING_FINE)
            return true;
        else
        {
			if (responseCodeInt == ServerConstant.EMAIL_DOESNT_EXIT) {
				MainMenuPanelManager.Instance.splashScreen.signInPanel.gameObject.SetActive (true);
				MainMenuPanelManager.Instance.splashScreen.signInPanel.SignUpEmail ();
			} else if (responseCodeInt == ServerConstant.ALREADY_ANSWERED) {

			} else if (responseCodeInt == ServerConstant.USER_ALREADY_REGISTERED) {
				PopUpErrorMessage ("Email exists in database");
			} else if (responseCodeInt == ServerConstant.IN_CORRECT_PASSWORD) {
				PopUpErrorMessage ("Incorrect Email Address or Password.");
			} else {
				MainMenuPanelManager.Instance.WaitingPanel (false);
				PopUpErrorMessage (response [ServerConstant.RESPONSE_INFO].ToString ());
			}

            Q.Utils.QDebug.Log(response[ServerConstant.RESPONSE_INFO].ToString());
			errorMsg = response [ServerConstant.RESPONSE_INFO].ToString ();
        }
        return false;
    }

	string GetResponseMessage (string serverResponse)
    {
        IDictionary response = (IDictionary)Facebook.MiniJSON.Json.Deserialize(serverResponse);

        string _response_msg = JsonConvert.SerializeObject(response[ServerConstant.RESPONSE_MSG]);
//        _response_msg = _response_msg.Replace ("null", "0");

        return _response_msg;
    }


	void PopUpErrorMessage (string message)
	{
		Q.Utils.QDebug.Log ("ERROR!!! " + message);

		if (message.Contains ("Couldn't resolve host")) {
			message = TagConstant.PopUpMessages.NO_INTERNET_CONNECTION;
		} else if (message.Contains ("Connection timed out")) {
			message = "Server problem please try again.";
		}

		MainMenuPanelManager.Instance.ShowMessage (message,
			MainMenuPanelManager.eMainMenuState.Unknown,
			PopUpMessage.eMessageType.Warning);
	}
}

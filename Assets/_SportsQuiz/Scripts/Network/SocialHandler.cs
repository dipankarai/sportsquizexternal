﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Newtonsoft.Json;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class SocialHandler : MonoBehaviour {

	private static SocialHandler m_instance;
	public static SocialHandler Instance { get {return m_instance; } }

	public enum FacebookState
	{
		InitState = 0,
		LoginSate,
		ApprequestSate,
		FriendList
	}

	private System.Action<bool> m_callback;

	//********FACEBOOK********
	public FacebookState currentFBState;
	public FacebookData fbLoginData;
	public FacebookFriendList facebookFriendList;
	public string facebook_Access_token;
	public string facebook_User_ID;

	//********GOOGLE********
	public string google_User_ID;
	public string google_Username;
	public List<GoogleFriendDetail> googleFriendList = new List<GoogleFriendDetail> ();

	void Awake ()
	{
		if (m_instance == null)
			m_instance = this;

		FacebookInit ();
	}

	public static SocialHandler Init (GameObject obj)
	{
		if (m_instance == null)
			m_instance = obj.AddComponent<SocialHandler> ();

		return m_instance;
	}

	#region FACEBOOK

	public void FacebookInit ()
	{
		FB.Init (this.OnInitComplete, null, null);
	}

	private void OnInitComplete()
	{
		Q.Utils.QDebug.Log ("Facebook Init Complte : FB Login " + FB.IsLoggedIn + " Initalized: " + FB.IsInitialized);

		switch (currentFBState)
		{
		case FacebookState.InitState:
			{
				
			}
			break;
		case FacebookState.LoginSate:
			{
				FacebookLogin (m_callback);
			}
			break;
		case FacebookState.ApprequestSate:
			{
				
			}
			break;
		case FacebookState.FriendList:
			{
				GetFacebookFriendList (m_callback);
			}
			break;
		default :
			break;
		}

	}

	public void FacebookLogin (System.Action<bool> callback)
	{
		m_callback = callback;
		currentFBState = FacebookState.LoginSate;

		MainMenuPanelManager.Instance.WaitingPanel (true);

		if (FB.IsInitialized == true)
		{
			if (FB.IsLoggedIn == false) {
				LogInWithReadPermissions ();
			} else {
				LoginAPI ();
			}
		}
		else
		{
			FacebookInit ();
		}

	}

	void LogInWithReadPermissions ()
	{
		FB.LogInWithReadPermissions (new List<string> () { "public_profile", "email", "user_friends", "user_location" }, FacebookLoginCallback);
	}

	void LoginAPI ()
	{
		FB.API("me?fields=id,name,location.fields(location),picture.type(large)", HttpMethod.GET, FacebookAPICallback);
	}

	private void FacebookLoginCallback(IResult result)
	{
		if (result != null)
		{
			// Some platforms return the empty string instead of null.
			if (!string.IsNullOrEmpty(result.Error))
			{
				currentFBState = FacebookState.InitState;
				Q.Utils.QDebug.Log ("Error - Check log for details");
				Q.Utils.QDebug.Log ("Error Response:\n" + result.Error);

				if (result.Error.Contains ("error 304")) {
				
					FacebookLogout ();
					Invoke ("FacebookLoginAfterError", 5f);	

				} else {
					
					MainMenuPanelManager.Instance.ShowMessage (result.Error,
						MainMenuPanelManager.eMainMenuState.Unknown,
						PopUpMessage.eMessageType.Error);
					MainMenuPanelManager.Instance.WaitingPanel (false);
					
					if (m_callback != null)
						m_callback (false);
				}
			}
			else if (result.Cancelled)
			{
				currentFBState = FacebookState.InitState;
				Q.Utils.QDebug.Log ("Cancelled - Check log for details");
				Q.Utils.QDebug.Log ("Cancelled Response:\n" + result.RawResult);

				MainMenuPanelManager.Instance.ShowMessage ("Login Cancelled!!",
					MainMenuPanelManager.eMainMenuState.Unknown,
					PopUpMessage.eMessageType.Error);
				MainMenuPanelManager.Instance.WaitingPanel (false);

				if (m_callback != null)
					m_callback (false);
			}
			else if (!string.IsNullOrEmpty(result.RawResult))
			{
				Q.Utils.QDebug.Log ("Success - Check log for details");
				Q.Utils.QDebug.Log ("Success Response:\n" + result.RawResult);

				LoginAPI ();
			}
			else
			{
				currentFBState = FacebookState.InitState;
				Q.Utils.QDebug.Log ("Empty Response");

				MainMenuPanelManager.Instance.ShowMessage ("Network Error!!",
					MainMenuPanelManager.eMainMenuState.Unknown,
					PopUpMessage.eMessageType.Error);
				MainMenuPanelManager.Instance.WaitingPanel (false);

				if (m_callback != null)
					m_callback (false);
			}
		}
		else
		{
			currentFBState = FacebookState.InitState;
			Q.Utils.QDebug.Log ("Login Failed!!!");
			MainMenuPanelManager.Instance.ShowMessage ("Login Failed!!",
				MainMenuPanelManager.eMainMenuState.Unknown,
				PopUpMessage.eMessageType.Error);

			MainMenuPanelManager.Instance.WaitingPanel (false);
		}
	}

	void FacebookLoginAfterError ()
	{
		FacebookLogin (m_callback);
	}

	public void GetFacebookFriendList (System.Action<bool> callback)
	{
		m_callback = callback;
		Q.Utils.QDebug.Log ("GetFacebookFriendList");
		currentFBState = FacebookState.FriendList;

		if (FB.IsInitialized == true) {
			if (FB.IsLoggedIn) {
				FacebookFriendListAPI ();
			} else {
				LogInWithReadPermissions ();
			}
		} else {
			FacebookInit ();
		}
	}

	void FacebookFriendListAPI ()
	{
//		FB.API("me?fields=friends{id,name,picture.type(large)}", HttpMethod.GET, FacebookFriendListAPICallback);
		FB.API("me?fields=friends.fields(id,name,picture.type(large))", HttpMethod.GET, FacebookFriendListAPICallback);
	}

	void FacebookFriendListAPICallback (IResult result)
	{
		if (result.Error == null) {
			Q.Utils.QDebug.Log ("Result fb> " + result.RawResult);

			facebookFriendList = JsonConvert.DeserializeObject<FacebookFriendList> (result.RawResult);
			Q.Utils.QDebug.Log (JsonConvert.SerializeObject (facebookFriendList));

			currentFBState = FacebookState.InitState;
			m_callback (true);
		} else {
			Q.Utils.QDebug.Log ("Result Error> " + result.Error);
			currentFBState = FacebookState.InitState;
			m_callback (false);
		}
	}

	void FacebookAPICallback (IResult result)
	{
		if(result.Error==null)
		{
			Q.Utils.QDebug.Log ("Login Result fb> " + result.RawResult+" Access Token --->"+ AccessToken.CurrentAccessToken.TokenString);

			fbLoginData = JsonConvert.DeserializeObject<FacebookData> (result.RawResult);
			Q.Utils.QDebug.Log (JsonConvert.SerializeObject (fbLoginData));

			switch (currentFBState)
			{
			case FacebookState.LoginSate:
				{
					facebook_Access_token = AccessToken.CurrentAccessToken.TokenString;
					facebook_User_ID = fbLoginData.id;
					SportsQuizManager.Instance.playerInformation.facebook_id = facebook_User_ID;
					SportsQuizManager.Instance.playerInformation.name = fbLoginData.name;
					SportsQuizManager.Instance.playerInformation.profileUrl = fbLoginData.picture.data.url;
					//SportsQuizManager.Instance.playerInformation.country = Q.Utils.Country.GetCountryCode (fbLoginData.location.location.country);
					SportsQuizManager.Instance.UpdateFacebookProfileURL ();
					currentFBState = FacebookState.InitState;

					ServerManager.Instance.LogInToServer (m_callback);
					GetFacebookFriendList (GetFriendCallback);
				}
				break;
			case FacebookState.FriendList:
				{
					facebook_Access_token = AccessToken.CurrentAccessToken.TokenString;
					facebook_User_ID = fbLoginData.id;

					FacebookFriendListAPI ();
					MainMenuPanelManager.Instance.WaitingPanel (false);
				}
				break;
			default :
				break;
			}

		}
	}

	public void SendAppRequest (string friendID)
	{
		string message = SportsQuizManager.Instance.playerInformation.name + "requested you for " + TagConstant.APP_NAME;
		string[] to = new string[] { friendID };
		FB.AppRequest (
			message: message,
			to: null,
			filters: null,
			excludeIds: null,
			maxRecipients: 0,
			data: "",
			title: TagConstant.APP_NAME,
			callback: SendAppRequestCallback
		);
	}

	void SendAppRequestCallback(IResult result)
	{
		if(result.Error==null)
		{
			Q.Utils.QDebug.Log ("SendAppRequestCallback " + result.RawResult);
		} else {
			Q.Utils.QDebug.LogError (result.Error);
		}
	}

	public string GetFacebookFriendURL (string id)
	{
		if (facebookFriendList != null && facebookFriendList.friends != null) {
			
			for (int i = 0; i < facebookFriendList.friends.data.Count; i++) {
				if (facebookFriendList.friends.data[i].id == id) {
					return facebookFriendList.friends.data [i].picture.data.url;
				}
			}
		}

		return "";
	}

	void GetFriendCallback (bool success)
	{
        Q.Utils.QDebug.Log ("Get Friend Callback " + success);
	}

    /*IEnumerator LoadFacebookImage ()
	{
		for (int i = 0; i < Instance.facebookFriendList.friends.data.Count; i++) {

			SportsQuizManager.OpponentInfo _userDetail = new SportsQuizManager.OpponentInfo ();
			_userDetail.facebook_id = Instance.facebookFriendList.friends.data [i].id;
			_userDetail.profileUrl = Instance.facebookFriendList.friends.data [i].picture.data.url;
			_userDetail.name = Instance.facebookFriendList.friends.data [i].name;

			WWW image = new WWW (_userDetail.profileUrl);
			yield return image;

			if (image.error == null) {
//				_userDetail.userProfile = image.texture;
			} else {
				Q.Utils.QDebug.Log (image.error);
			}

			bool isAlreadyAdded = false;
			for (int j = 0; j < SportsQuizManager.Instance.opponentList.Count; j++) {

				if (SportsQuizManager.Instance.opponentList[j].facebook_id == _userDetail.facebook_id) {

					isAlreadyAdded = true;
					SportsQuizManager.Instance.opponentList [j].profileUrl = _userDetail.profileUrl;
//					SportsQuizManager.Instance.opponentList [j].userProfile = _userDetail.userProfile;

					if (string.IsNullOrEmpty (SportsQuizManager.Instance.opponentList [j].name))
						SportsQuizManager.Instance.opponentList [j].name = _userDetail.name;

					break;
				}
			}

			if (isAlreadyAdded == false)
				SportsQuizManager.Instance.opponentList.Add (_userDetail);
		}
	}*/

    /*IEnumerator LoadFacebookImage (System.Action<bool> callback)
	{
		for (int i = 0; i < Instance.facebookFriendList.friends.data.Count; i++) {

			SportsQuizManager.OpponentInfo _userDetail = GetFacebokFriendFromDatabase (Instance.facebookFriendList.friends.data [i].id); //new SportsQuizManager.OpponentInfo ();

			if (_userDetail == null) {

				_userDetail = new SportsQuizManager.OpponentInfo ();
				_userDetail.facebook_id = Instance.facebookFriendList.friends.data [i].id;
				_userDetail.profileUrl = Instance.facebookFriendList.friends.data [i].picture.data.url;
				_userDetail.name = Instance.facebookFriendList.friends.data [i].name;

				SportsQuizManager.Instance.opponentList.Add (_userDetail);
			}

//			if (_userDetail.userProfile == null) {
				
				WWW image = new WWW (_userDetail.profileUrl);
				yield return image;
				
				if (image.error == null) {
//					_userDetail.userProfile = image.texture;
				} else {
					Q.Utils.QDebug.LogError (image.error);
				}
//			}
		}

		if (callback != null)
			callback (true);
	}*/

	SportsQuizManager.OpponentInfo GetFacebokFriendFromDatabase (string facebook_id)
	{
		for (int i = 0; i < SportsQuizManager.Instance.opponentList.Count; i++) {

			if (SportsQuizManager.Instance.opponentList[i].facebook_id == facebook_id) {
				return SportsQuizManager.Instance.opponentList [i];
			}
		}

		return null;
	}

    public void FacebookLogout ()
    {
        if (FB.IsLoggedIn)
        {
            FB.LogOut();
        }
        fbLoginData = new FacebookData();
        facebookFriendList = new FacebookFriendList();
        facebook_Access_token = "";
        facebook_User_ID = "";
    }

	#endregion

	#region GOOGLE

	public enum GoogleState
	{
		Init = 0,
		Login,
		GetFriend
	}
	public GoogleState currentGPlusState;

	public void GooglePlusLogin (System.Action<bool> callback)
	{
		m_callback = callback;
		currentGPlusState = GoogleState.Login;

		MainMenuPanelManager.Instance.WaitingPanel (true);

		LoginToGooglePlus ();
	}

	void LoginToGooglePlus ()
	{
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate ((bool success) => {

			if (success == true) {

				google_User_ID = Social.localUser.id;
				google_Username = Social.localUser.userName;
				UserState state = Social.localUser.state;

				Q.Utils.QDebug.LogError (google_User_ID +" :::1 "+ google_Username +" ::: "+ state);
				if (currentGPlusState == GoogleState.Login) {
					
					SportsQuizManager.Instance.playerInformation.google_id = google_User_ID;
					SportsQuizManager.Instance.playerInformation.name = google_Username;
//					SportsQuizManager.Instance.playerInformation.userProfile = Social.localUser.image;
					StartCoroutine ("KeepCheckingGooglePlusAvatar");

					ServerManager.Instance.LogInToServer (m_callback);

				}else if (currentGPlusState == GoogleState.GetFriend) {

					GoogleFriends (m_callback);
				}
			}

			m_callback (success);
			MainMenuPanelManager.Instance.WaitingPanel (false);
			currentGPlusState = GoogleState.Init;

		});
	}

	IEnumerator KeepCheckingGooglePlusAvatar()
	{
		float secondsOfTrying = 10;
		float secondsPerAttempt = 0.2f;
		while (secondsOfTrying > 0)
		{
			if (Social.localUser.image != null) {
				SportsQuizManager.Instance.playerInformation.userProfile = Social.localUser.image;
				break;
			} else {
				SportsQuizManager.Instance.playerInformation.userProfile = SportsQuizManager.Instance.references.GetAvatarImage (0);
				break;
			}

			secondsOfTrying -= secondsPerAttempt;
			yield return new WaitForSeconds(secondsPerAttempt);
		}
	}

	public void GoogleFriends (System.Action<bool> callback)
	{
		m_callback = callback;
		currentGPlusState = GoogleState.GetFriend;

		if (Social.localUser.authenticated) {
			
			Social.localUser.LoadFriends ((bool success) => {
				
				Q.Utils.QDebug.LogError("Friends loaded OK: " + success +" :: "+Social.localUser.friends.Length);
				
				foreach(IUserProfile profile in Social.localUser.friends) {
					Q.Utils.QDebug.LogError (profile.id +" ::: "+ profile.userName+"\n ::: "+ profile.image+"\n ::: "+ profile.isFriend+" ::: "+ profile.state);

					GoogleFriendDetail _friendDetail = new GoogleFriendDetail ();
					_friendDetail.id = profile.id;
					_friendDetail.name = profile.userName;
					_friendDetail.isFriends = profile.isFriend;
					_friendDetail.image = profile.image;
					_friendDetail.state = profile.state;

					googleFriendList.Add (_friendDetail);
				}

				currentGPlusState = GoogleState.Init;

				m_callback (success);
			});
		} else {

			LoginToGooglePlus ();
		}
	}

	public void GoogleShare ()
	{
		LocalNotification.GoogleShare (
			TagConstant.SocialShare.DISPLAY_TEXT,
			TagConstant.SocialShare.DEEP_LINK_ID,
			TagConstant.SocialShare.DEEP_LINK_TITLE,
			TagConstant.SocialShare.DEEP_LINK_DESC,
			TagConstant.SocialShare.DEEP_LINK_ICON);
	}

	public void GoogleSignOut ()
	{
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.SignOut ();
        }
        google_User_ID = "";
        google_Username = "";
        googleFriendList = new List<GoogleFriendDetail>();

    }

	#endregion

	#region CALLBACK RESULT PARSING CLASS

	// FB login data
	[System.Serializable]
	public class FacebookData
	{
		public string id;
		public string name;
		public LocationDetails location;
		public Picture picture;
	}
	[System.Serializable]
	public class LocationDetails
	{
		public Location location;
		public string id;
	}
	[System.Serializable]
	public class Location
	{
		public string city;
		public string country;
	}
	[System.Serializable]
	public class Picture
	{
		public PictureData data;
	}
	[System.Serializable]
	public class PictureData
	{
		public bool is_silhouette;
		public string url;
	}

	[System.Serializable]
	public class FacebookFriendList
	{
		public FacebookFriends friends;
		public string id;
	}

	[System.Serializable]
	public class FacebookFriends
	{
		public List<FacebookData> data;
		public FacebookPaging paging;
		public FacebookSummary summary;
	}

	[System.Serializable]
	public class FacebookPaging
	{
		public FacebookPagingCursors cursors;
	}

	[System.Serializable]
	public class FacebookPagingCursors
	{
		public string before;
		public string after;
	}

	[System.Serializable]
	public class FacebookSummary
	{
		public int total_count;
	}

	[System.Serializable]
	public class GoogleFriendDetail
	{
		public string id;
		public string name;
		public Texture2D image;
		public bool isFriends;
		public UserState state;
	}

	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class NativeBridgeHandler : MonoBehaviour 
{
	[DllImport("__Internal")]
	private static extern void GetCoutryCodeISO();
	private static string mainActivityClassName = "com.unity3d.player.UnityPlayerActivity";

	void Start ()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		Q.Utils.QDebug.Log ("Get NotificationToken");
		GetFCMNotificationToken ();
		#endif
	}

	public void GetFCMNotificationToken ()
	{
		Q.Utils.QDebug.Log ("GetFCMNotificationToken");
		using (AndroidJavaClass _class = new AndroidJavaClass("com.sportswizz.sportsqwizz.UnityPlayerActivity")) {
			_class.CallStatic ("GetFCMNotificationToken", TagConstant.APP_NAME);
		}
	}

	public void GetFCM_NotificationToken(string token)
	{
		Q.Utils.QDebug.Log ("NotificationToken " + token);
		GamePrefs.AndroidDeviceToken = token;
	}

	public static void GetCountryCode ()
	{
		Q.Utils.QDebug.Log ("GetCountryCode");
		#if UNITY_EDITOR
			Q.Utils.QDebug.Log ("GetCountryCode EDITOR");
		#elif UNITY_IOS || UNITY_IPHONE
		GetCoutryCodeISO ();
		#elif UNITY_ANDROID
		LocalNotification.GetCountryCode ();
		#endif
	}

	public void CountryCodeCallback (string countryISOCode)
	{
		Q.Utils.QDebug.Log (countryISOCode.ToUpper ());
		if (Q.Utils.Country.GetCountryCodeByISO (countryISOCode.ToUpper ()) != 0)
		SportsQuizManager.Instance.playerInformation.country = Q.Utils.Country.GetCountryCodeByISO (countryISOCode.ToUpper ());
	}
}

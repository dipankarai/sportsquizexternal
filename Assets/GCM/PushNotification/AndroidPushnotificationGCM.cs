using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
// Android GCM Push Notofocation DeviceId Featch
public class AndroidPushnotificationGCM : MonoBehaviour {
	
	// Project Number on Google API Console
	private string[] SENDER_IDS = {"98294001467"};
	
	private string _text = "(null)";
	
	// Use this for initialization
	void Start () {

		this.gameObject.name = "~PushNotification";

		if (SENDER_IDS == null || SENDER_IDS.Length == 0) {

			Debug.Log ("LENGTH 0");
			return;
		}


		#if UNITY_ANDROID

		// Create receiver game object
		GCM.Initialize (this.gameObject);
		
		// Set callbacks
		GCM.SetErrorCallback ((string errorId) => {
			_text = "Error: " + errorId;
		});
		
		GCM.SetMessageCallback ((Dictionary<string, object> table) => {
			_text = "Message: " + System.Environment.NewLine;
			foreach (var key in  table.Keys) {
				_text += key + "=" + table[key] + System.Environment.NewLine;
			}
		});
		
		GCM.SetRegisteredCallback ((string registrationId) => {
			_text = "Register: " + registrationId; 
			print ("registrationId");
			PlayerPrefs.SetString("AndroidDeviceToken", registrationId);
		});
		
		GCM.SetUnregisteredCallback ((string registrationId) => {
			_text = "Unregister: " + registrationId;
		});
		
		GCM.SetDeleteMessagesCallback ((int total) => {
			GCM.ShowToast ("DeleteMessaged!!!");
			_text = "DeleteMessages: " + total;
		});

		if( PlayerPrefs.GetString("AndroidDeviceToken").Equals(""))
		{
			GCM.Register (SENDER_IDS);
		}
		else{
			print (PlayerPrefs.GetString("AndroidDeviceToken"));
		}
		#endif
	}

	void ReceiveiOSDeviceHexToken (string devicetoken)
	{
		GamePrefs.iOSDeviceToken = devicetoken;

		Debug.Log (devicetoken);
	}
}

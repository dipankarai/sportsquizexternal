﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

#if UNITY_IOS || UNITY_IPHONE
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class PushNotificationManager : MonoBehaviour 
{
	const int DEVICE_COUNT = 5;
	const int LOCAL_NOTIFICATION_ID = 666;
	const float LOCAL_NOTIFICATION_TIME = 3600f;
	const string LOCAL_NOTIFICATION_MESSAGE_1 = "Come back! It's quiz time!!!";
	const string LOCAL_NOTIFICATION_MESSAGE_2 = "Throw some challenges !";
	const string LOCAL_NOTIFICATION_MESSAGE_3 = "Don't be idle... Be a winner!";
	const string LOCAL_NOTIFICATION_MESSAGE_4 = "Get, set, GO time!";

	#if UNITY_IOS || UNITY_IPHONE
	static UnityEngine.iOS.LocalNotification localNotification; 
	#elif UNITY_ANDROID || UNITY_EDITOR
	// Project Number on Google API Console
	private static string[] SENDER_IDS = {"61253268829",""};

	private string _text = "(null)";
	#endif

	[Serializable]
	public class iOSLocalNotificationClass
	{
		public string alertBody;
		public string soundName;
		public DateTime fireDate;
		public IDictionary userInfo;
	}

	private int deviceTokenCount = 0;

	public static Dictionary<string, string> notificationDict = new Dictionary<string, string>();

	private bool pollIOSDeviceToken = false;

	[DllImport("__Internal")]
	private static extern void CBG_RegisterForRemoteNotifications(int type);

	private static PushNotificationManager _instance;
	public static PushNotificationManager Instance
	{
		get
		{
			return Init();
		}
		private set{}
	}

	public static PushNotificationManager Init()
	{
		if(_instance == null)
		{
			GameObject instGO = new GameObject("PushNotification");
			_instance = instGO.AddComponent<PushNotificationManager>();
			DontDestroyOnLoad(instGO);
		}
		return _instance;
	}

	void Awake()
	{
		if(_instance == null)
		{
			_instance = this;
		}
		else
		{
			DestroyImmediate(this);
		}
	}

	void Start()
	{
//		ResetAppBadgeIcon();
	}

	void Update() 
	{
		// Unity does not tell us when the deviceToken is ready, so we have to keep polling after requesting it
		if(pollIOSDeviceToken)
		{
			#if UNITY_EDITOR

			#elif UNITY_IOS || UNITY_IPHONE
			RegisterIOSDevice();
			#elif UNITY_ANDROID
			RequestDeviceToken();
			#endif

		}
	}

	public void RequestDeviceToken() 
	{
		#if UNITY_IOS || UNITY_IPHONE
		if(NotificationServices.deviceToken == null) 
		{			
			pollIOSDeviceToken = true;
			#if UNITY_EDITOR
			
			#elif UNITY_IOS || UNITY_IPHONE
			CBG_RegisterForRemoteNotifications((int)NotificationType.Alert | 
			(int)NotificationType.Badge | 
			(int)NotificationType.Sound);
			#endif
		} 
		else 
		{
			RegisterIOSDevice();
		}
		#elif UNITY_ANDROID
		//GCMPushNotificaion();
		#endif
	}

	#if UNITY_IOS || UNITY_IPHONE
	/*
	 * Poll NotificationServices for deviceToken for iOS device
	 * If found, send it to the server (StoreDeviceID)
	 */
	private void RegisterIOSDevice() 
	{
		if(UnityEngine.iOS.NotificationServices.registrationError != null) 
		{
			Q.Utils.QDebug.Log(UnityEngine.iOS.NotificationServices.registrationError);
			deviceTokenCount ++;
			if(deviceTokenCount < DEVICE_COUNT)
			{
				pollIOSDeviceToken = true;
			}
		}
		if(UnityEngine.iOS.NotificationServices.deviceToken == null)
		{
			deviceTokenCount ++;
			if(deviceTokenCount < DEVICE_COUNT)
			{
				pollIOSDeviceToken = true;
			}
			return;
		}
		pollIOSDeviceToken = false;
		string hexToken = System.BitConverter.ToString(UnityEngine.iOS.NotificationServices.deviceToken).Replace ("-",string.Empty);
		Q.Utils.QDebug.Log("Device HexToken"+hexToken);

		GamePrefs.iOSDeviceToken = hexToken;
	}
	#endif

	public static void CreateLocalNotification(int notificationID = 10 ,float time = 15f, string msg = "Play Sports Quiz !!" )
	{
		#if UNITY_IOS || UNITY_IPHONE
		UnityEngine.iOS.LocalNotification ln = new UnityEngine.iOS.LocalNotification ();
		ln.alertBody = msg;
		ln.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
		ln.fireDate = DateTime.Now.AddSeconds(time);
		notificationDict ["ID"] = notificationID.ToString ();
		ln.userInfo = notificationDict;

		iOSLocalNotificationClass _iOSPreviousNotification = new iOSLocalNotificationClass ();
		_iOSPreviousNotification.alertBody = ln.alertBody;
		_iOSPreviousNotification.soundName = ln.soundName;
		_iOSPreviousNotification.fireDate = ln.fireDate;
		_iOSPreviousNotification.userInfo = ln.userInfo;

		GamePrefs.iOSPreviousNotification = _iOSPreviousNotification;

		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(ln);
		#elif UNITY_ANDROID
		long _time = System.Convert.ToInt64 (time);
		LocalNotification.SendNotification(
			notificationID, _time, TagConstant.APP_NAME, msg,
			Color.white);
		#endif
	}

	public static void CancelPreviousNotification (int notificationID = 10 ,float time = 15f, string msg = "Play Sports Quiz !!" )
	{
		#if UNITY_IOS || UNITY_IPHONE
		UnityEngine.iOS.LocalNotification ln = new UnityEngine.iOS.LocalNotification ();
		iOSLocalNotificationClass _iOSPreviousNotification = GamePrefs.iOSPreviousNotification;
		ln.alertBody = _iOSPreviousNotification.alertBody;
		ln.soundName = _iOSPreviousNotification.soundName;
		ln.fireDate = _iOSPreviousNotification.fireDate;
		ln.userInfo = _iOSPreviousNotification.userInfo;

		UnityEngine.iOS.NotificationServices.CancelLocalNotification (ln);
		#elif UNITY_ANDROID
		LocalNotification.CancelNotification(notificationID);
		#endif
	}
		
	public static void ResetAppBadgeIcon()
	{ 
		#if UNITY_IOS || UNITY_IPHONE
		UnityEngine.iOS.LocalNotification setCountNotif = new UnityEngine.iOS.LocalNotification(); 
		setCountNotif.applicationIconBadgeNumber = -1; 
		setCountNotif.hasAction = false; 
		UnityEngine.iOS.NotificationServices.PresentLocalNotificationNow(setCountNotif);
		#endif
	}

	public void GCMPushNotificaion ()
	{
		#if UNITY_ANDROID

		if (SENDER_IDS == null || SENDER_IDS.Length == 0) {
			return;
		}

		// Create receiver game object
		GCM.Initialize (this.gameObject);

		// Set callbacks
		GCM.SetErrorCallback ((string errorId) => {
		_text = "Error: " + errorId;
		});

		GCM.SetMessageCallback ((Dictionary<string, object> table) => {
		_text = "Message: " + System.Environment.NewLine;
		foreach (var key in  table.Keys) {
		_text += key + "=" + table[key] + System.Environment.NewLine;
		}
		});

		
		GCM.SetUnregisteredCallback ((string registrationId) => {
		_text = "Unregister: " + registrationId;
		});

		GCM.SetDeleteMessagesCallback ((int total) => {
		GCM.ShowToast ("DeleteMessaged!!!");
		_text = "DeleteMessages: " + total;
		});

		if( PlayerPrefs.GetString("AndroidDeviceToken").Equals(""))
		{
		SENDER_IDS [0] = "61253268829";
		print ("registrationId call  -------------->"+SENDER_IDS[0]);
		GCM.Register (SENDER_IDS);
		}
		else{
		print (PlayerPrefs.GetString("AndroidDeviceToken"));
		}
		#endif
	}

	void OnApplicationPause(bool bPause)
	{
		#if !UNITY_EDITOR
		if (bPause == true) {
			CreateLocalNotification (LOCAL_NOTIFICATION_ID, LOCAL_NOTIFICATION_TIME, GetRandomNotificationMessage ());
		} else {
			CancelPreviousNotification (LOCAL_NOTIFICATION_ID, LOCAL_NOTIFICATION_TIME, GetRandomNotificationMessage ());
		}
		#endif
	}

//	private int[] numbers = new int[] { 0, 1, 2, 3 };
	string GetRandomNotificationMessage ()
	{
		int random = 0;

		if (GamePrefs.HasKey (TagConstant.PrefsName.LOCAL_NOTIFICATION_MSG) == false) {
			random = UnityEngine.Random.Range (0, 4);
		} else {
			int _random = GamePrefs.RandomLocalNotification;
			List<int> numberList = new List<int> (new int[] { 0, 1, 2, 3 });

			numberList.Remove (_random);

			random = numberList [UnityEngine.Random.Range (0, numberList.Count)];
		}

		switch (random)
		{
		case 0:
			GamePrefs.RandomLocalNotification = 0;
			return LOCAL_NOTIFICATION_MESSAGE_1;
		case 1:
			GamePrefs.RandomLocalNotification = 1;
			return LOCAL_NOTIFICATION_MESSAGE_2;
		case 2:
			GamePrefs.RandomLocalNotification = 2;
			return LOCAL_NOTIFICATION_MESSAGE_3;
		case 3:
			GamePrefs.RandomLocalNotification = 3;
			return LOCAL_NOTIFICATION_MESSAGE_4;
		}

		return LOCAL_NOTIFICATION_MESSAGE_1;
	}
}
